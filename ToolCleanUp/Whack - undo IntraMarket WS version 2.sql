-- 1/2/13
-- intramarket wholesaled c4363A to ry1, should NOT have

-- first, the newly created ry1 VehicleInventoryItems 
ry1 VehicleInventoryItemID '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358'
SELECT VehicleItemID FROM VehicleInventoryItems WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358'

SELECT * FROM VehicleItemstatuses WHERE VehicleItemID = '22dba63c-1bd4-d143-aa6f-d83d6d937721'

DELETE 
FROM VehicleItemstatuses
WHERE VehicleItemID = '22dba63c-1bd4-d143-aa6f-d83d6d937721'
  AND category = 'Inventory'
  AND ThruTS IS NULL;
  
UPDATE VehicleItemstatuses
SET thruts = NULL 
WHERE VehicleItemID = '22dba63c-1bd4-d143-aa6f-d83d6d937721'
  AND category = 'Inventory';
  
DELETE 
FROM vehiclepricingdetails
WHERE vehiclepricingid = (
SELECT VehiclePricingID 
FROM vehiclepricings   
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358');
DELETE
FROM vehiclepricings   
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358';  
DELETE
FROM VehicleReconItems 
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358';
DELETE
FROM vehicleAcquisitions   
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358';
DELETE
FROM VehicleInventoryItemStatuses  
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358';
DELETE 
FROM SelectedReconPackages    
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358';
DELETE
FROM vehicleappeals   
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358';
DELETE
FROM VehicleInventoryItemNotes    
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358';
DELETE 
FROM VehicleInventoryItems   
WHERE VehicleInventoryItemID = '2be4ac8d-78ba-4677-9cdd-c1f37c1f0358';

-- now undo the ry3 ws
-- use sp.sub_wholesalevehicle
c4863a
VehicleInventoryItemID 'c2aa61d1-d830-4e0c-9efd-e4c61c810b57'
DELETE 
FROM vehiclesales
WHERE VehicleInventoryItemID = 'c2aa61d1-d830-4e0c-9efd-e4c61c810b57';


DELETE FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = 'c2aa61d1-d830-4e0c-9efd-e4c61c810b57'
  AND CAST(fromTS AS sql_date) = '01/02/2013';
UPDATE VehicleInventoryItemStatuses
SET thruts = NULL
WHERE VehicleInventoryItemID = 'c2aa61d1-d830-4e0c-9efd-e4c61c810b57' 
  AND CAST(thruTS AS sql_date) = '01/02/2013';

DELETE FROM VehicleItemPhysicalLocations
WHERE VehicleInventoryItemID = 'c2aa61d1-d830-4e0c-9efd-e4c61c810b57'
  AND CAST(fromTS AS sql_date) = '01/02/2013';
UPDATE VehicleItemPhysicalLocations
SET thruts = NULL
WHERE VehicleInventoryItemID = 'c2aa61d1-d830-4e0c-9efd-e4c61c810b57' 
  AND CAST(thruTS AS sql_date) = '01/02/2013';
  
update VehicleInventoryItems  
SET thruts = null  
WHERE VehicleInventoryItemID = 'c2aa61d1-d830-4e0c-9efd-e4c61c810b57';  





