-- cancel sale
/*
SELECT *
FROM VehicleInventoryItems
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26'
*/

UPDATE VehicleInventoryItems
SET ThruTS = NULL
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26';


/*
SELECT *
FROM vehiclesales 
WHERE VehicleInventoryItemID = 'd8875926-4f51-4781-980e-1e22ff209b0b'
*/

DELETE 
FROM vehiclesales 
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26';

/*
SELECT *
FROM VehicleInventoryItemStatuses  
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26'
ORDER BY thruts
*/
DELETE 
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26'
AND status IN ('RawMaterials_Sold', 'RawMaterials_Delivered');

-- OPEN statuses that were closed BY the sale
UPDATE VehicleInventoryItemStatuses
SET thruts = NULL
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26'
AND status = 'RMFlagAV_Available'
AND CAST(ThruTS AS sql_date) = '08/05/2011';

UPDATE VehicleInventoryItemStatuses
SET thruts = NULL
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26'
AND status = 'AppearanceReconProcess_NoIncompleteReconItems'
AND CAST(ThruTS AS sql_date) = '08/05/2011';

UPDATE VehicleInventoryItemStatuses
SET thruts = NULL
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26'
AND status = 'BodyReconProcess_NoIncompleteReconItems'
AND CAST(ThruTS AS sql_date) = '08/05/2011';

UPDATE VehicleInventoryItemStatuses
SET thruts = NULL
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26'
AND status = 'MechanicalReconProcess_NoIncompleteReconItems'
AND CAST(ThruTS AS sql_date) = '08/05/2011';

UPDATE VehicleInventoryItemStatuses
SET thruts = NULL
WHERE VehicleInventoryItemID = '486e5a67-0e64-410e-b6f2-92d99fb0df26'
AND status = 'RawMaterials_RawMaterials'
AND CAST(ThruTS AS sql_date) = '08/05/2011';
/*
SELECT *
FROM VehicleInventoryItemNotes   
WHERE VehicleInventoryItemID = 'd8875926-4f51-4781-980e-1e22ff209b0b'
*/
DELETE 
FROM VehicleInventoryItemNotes
WHERE VehicleInventoryItemID = 'd8875926-4f51-4781-980e-1e22ff209b0b'
AND category = 'RMFlagSB';

/*
SELECT *
FROM vehicleitemstatuses
WHERE VehicleItemID = (
  SELECT VehicleItemID
  FROM VehicleInventoryItems
  WHERE stocknumber = '15873XA')
*/
  
UPDATE VehicleItemStatuses
SET ThruTS = NULL
WHERE VehicleItemID = '92662a9f-6a69-9143-8ba7-50b8a9edde2f'
AND category = 'Inventory'; 



