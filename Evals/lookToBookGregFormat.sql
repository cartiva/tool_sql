SELECT MIN(thedate), MAX(thedate) 
FROM dds.day
WHERE thedate BETWEEN curdate() - 57 AND curdate() - 1


SELECT theweek, 
  CASE WHEN theweek = 1 THEN MIN(thedate) END AS DateFrom1,
  CASE WHEN theweek = 1 THEN MAX(thedate) END AS DateThru1,
  CASE WHEN theweek = 2 THEN MIN(thedate) END AS DateFrom2,
  CASE WHEN theweek = 2 THEN MAX(thedate) END AS DateThru2, 
  CASE WHEN theweek = 3 THEN MIN(thedate) END AS DateFrom3,
  CASE WHEN theweek = 3 THEN MAX(thedate) END AS DateThru3,
  CASE WHEN theweek = 4 THEN MIN(thedate) END AS DateFrom4,
  CASE WHEN theweek = 4 THEN MAX(thedate) END AS DateThru4     
FROM (
  SELECT thedate, dayname,
    CASE 
      WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
      WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
      WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
      WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
    END AS TheWeek
  FROM dds.day 
  WHERE TheDate >= CurDate() - 56
  AND TheDate <= CurDate() -1) a
GROUP BY theweek   



SELECT *
-- INTO #days
FROM (
  SELECT 
    (SELECT MIN(thedate)
      FROM dds.day
      WHERE TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1) AS From1,
    (SELECT MAX(thedate)
      FROM dds.day
      WHERE TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1) AS Thru1,
    (SELECT MIN(thedate)
      FROM dds.day
      WHERE TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15) AS From2,
    (SELECT MAX(thedate)
      FROM dds.day
      WHERE TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15) AS Thru2,
    (SELECT MIN(thedate)
      FROM dds.day
      WHERE TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29) AS From3,
    (SELECT MAX(thedate)
      FROM dds.day
      WHERE TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29) AS Thru3,
    (SELECT MIN(thedate)
      FROM dds.day
      WHERE TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43) AS From4,
    (SELECT MAX(thedate)
      FROM dds.day
      WHERE TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43) AS Thru4                 
  FROM system.iota) a  
  
  SELECT min(thedate) AS fromDate, MAX(thedate) AS ThruDate,
    CASE 
      WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
      WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
      WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
      WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
    END AS TheWeek
  FROM dds.day 
  WHERE thedate BETWEEN curdate() - 56 AND curdate() - 1
  GROUP BY theweek 
  
  
SELECT c.name, coalesce(b.fullname, 'Unfinished') as Evaluator, COUNT(*) AS Looked,
  SUM(CASE WHEN currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Booked,
  case when count(*) <> 0 then
    round(SUM(CASE WHEN currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) / (count(*) * 1.0) * 100.0, 0)
    else
	  0 end as Percentage
FROM VehicleEvaluations a
LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid 
LEFT JOIN organizations c ON a.locationid = c.partyid
WHERE a.typ <> 'VehicleEvaluation_Auction' 
  AND CAST(a.VehicleEvaluationTS AS sql_date) BETWEEN curdate() - 30 AND curdate()
GROUP BY c.name, b.fullname  


SELECT c.name, coalesce(b.fullname, 'Unfinished') as Evaluator
FROM VehicleEvaluations a
LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid 
LEFT JOIN organizations c ON a.locationid = c.partyid
LEFT JOIN #days d1 ON CAST(a.VehicleEvaluationTS AS sql_Date) BETWEEN d1.From1 AND d1.Thru1
WHERE a.typ <> 'VehicleEvaluation_Auction' 
GROUP BY c.name, b.fullname  


  
SELECT CAST(a.VehicleEvaluationTS AS sql_date) AS EvalDate, c.name AS Location,
  coalesce(b.fullname, 'Unfinished') as Evaluator, a.currentstatus
FROM VehicleEvaluations a
LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid
LEFT JOIN organizations c ON a.locationid = c.partyid
WHERE a.typ <> 'VehicleEvaluation_Auction'
  AND cast(VehicleEvaluationTS AS sql_date) BETWEEN CurDate() - 56 AND CurDate() - 1  
GROUP BY CAST(a.VehicleEvaluationTS AS sql_date), c.name,
  coalesce(b.fullname, 'Unfinished'), a.currentstatus  
 
SELECT location,
  SUM(CASE WHEN evaldate BETWEEN CurDate() - 14 AND CurDate() - 1 THEN 1 ELSE 0 END),
  SUM(CASE WHEN evaldate BETWEEN CurDate() - 28 AND CurDate() - 15 THEN 1 ELSE 0 END), 
  SUM(CASE WHEN evaldate BETWEEN CurDate() - 42 AND CurDate() - 29 THEN 1 ELSE 0 END), 
  SUM(CASE WHEN evaldate BETWEEN CurDate() - 56 AND CurDate() - 43 THEN 1 ELSE 0 END)  
FROM (  
  SELECT CAST(a.VehicleEvaluationTS AS sql_date) AS EvalDate, c.name AS Location,
    coalesce(b.fullname, 'Unfinished') as Evaluator, a.currentstatus
  FROM VehicleEvaluations a
  LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid
  LEFT JOIN organizations c ON a.locationid = c.partyid
  WHERE a.typ <> 'VehicleEvaluation_Auction'
    AND cast(VehicleEvaluationTS AS sql_date) BETWEEN CurDate() - 56 AND CurDate() - 1  
  GROUP BY CAST(a.VehicleEvaluationTS AS sql_date), c.name,
    coalesce(b.fullname, 'Unfinished'), a.currentstatus) d 
GROUP BY location 

SELECT left(trim(monthname(curdate() - 14)), 3) + ' ' +  trim(cast(dayofmonth(curdate() - 14) AS sql_char)) + ' thru ' + left(trim(monthname(curdate() - 1)), 3) + '  ' +  trim(cast(dayofmonth(curdate() - 1) AS sql_char))
FROM system.iota

SELECT DISTINCT currentstatus FROM VehicleEvaluations

SELECT *
FROM (
  SELECT min(thedate) AS fromDate, MAX(thedate) AS ThruDate,
    CASE 
      WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
      WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
      WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
      WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
    END AS TheWeek
  FROM dds.day 
  WHERE thedate BETWEEN curdate() - 56 AND curdate() - 1
  GROUP BY theweek) e 

-- ok, this gives me looked BY location
SELECT f.location, 'ALL' AS evaluator,
  SUM(CASE WHEN theweek = 1 THEN 1 ELSE 0 END) AS Look1,
  SUM(CASE WHEN theweek = 2 THEN 1 ELSE 0 END) AS Look2,
  SUM(CASE WHEN theweek = 3 THEN 1 ELSE 0 END) AS Look3,
  SUM(CASE WHEN theweek = 4 THEN 1 ELSE 0 END) AS Look4
-- SELECT *  
FROM (
  SELECT min(thedate) AS fromDate, MAX(thedate) AS ThruDate,
    CASE 
      WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
      WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
      WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
      WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
    END AS TheWeek
  FROM dds.day 
  WHERE thedate BETWEEN curdate() - 56 AND curdate() - 1
  GROUP BY theweek) e 
LEFT JOIN (    
  SELECT CAST(a.VehicleEvaluationTS AS sql_date) AS EvalDate, c.name AS Location
  FROM VehicleEvaluations a
  LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid
  LEFT JOIN organizations c ON a.locationid = c.partyid
  WHERE a.typ <> 'VehicleEvaluation_Auction'
    AND cast(VehicleEvaluationTS AS sql_date) BETWEEN CurDate() - 56 AND CurDate() - 1) f ON evaldate BETWEEN e.fromdate AND e.thrudate
GROUP BY f.location    



-- ok, this gives me looked and booked BY location & evaluator
SELECT f.location, 'ALL' AS evaluator,
  SUM(CASE WHEN theweek = 1 THEN 1 ELSE 0 END) AS Look1,
  SUM(CASE WHEN theweek = 1 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book1, 
  SUM(CASE WHEN theweek = 2 THEN 1 ELSE 0 END) AS Look2,
  SUM(CASE WHEN theweek = 2 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book2, 
  SUM(CASE WHEN theweek = 3 THEN 1 ELSE 0 END) AS Look3,
  SUM(CASE WHEN theweek = 3 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book3, 
  SUM(CASE WHEN theweek = 4 THEN 1 ELSE 0 END) AS Look4,
  SUM(CASE WHEN theweek = 4 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book4 
-- SELECT * 
FROM (
  SELECT min(thedate) AS fromDate, MAX(thedate) AS ThruDate,
    CASE 
      WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
      WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
      WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
      WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
    END AS TheWeek
  FROM dds.day 
  WHERE thedate BETWEEN curdate() - 56 AND curdate() - 1
  GROUP BY theweek) e 
LEFT JOIN (    
  SELECT CAST(a.VehicleEvaluationTS AS sql_date) AS EvalDate, c.name AS Location,
    coalesce(b.fullname, 'Unfinished') as Evaluator, a.currentstatus
  FROM VehicleEvaluations a
  LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid
  LEFT JOIN organizations c ON a.locationid = c.partyid
  WHERE a.typ <> 'VehicleEvaluation_Auction'
    AND cast(VehicleEvaluationTS AS sql_date) BETWEEN CurDate() - 56 AND CurDate() - 1) f ON f.evaldate BETWEEN e.fromdate AND e.thrudate
GROUP BY f.location  
UNION 
SELECT f.location, evaluator,
  SUM(CASE WHEN theweek = 1 THEN 1 ELSE 0 END) AS Look1,
  SUM(CASE WHEN theweek = 1 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book1, 
  SUM(CASE WHEN theweek = 2 THEN 1 ELSE 0 END) AS Look2,
  SUM(CASE WHEN theweek = 2 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book2, 
  SUM(CASE WHEN theweek = 3 THEN 1 ELSE 0 END) AS Look3,
  SUM(CASE WHEN theweek = 3 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book3, 
  SUM(CASE WHEN theweek = 4 THEN 1 ELSE 0 END) AS Look4,
  SUM(CASE WHEN theweek = 4 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book4 
FROM (
  SELECT min(thedate) AS fromDate, MAX(thedate) AS ThruDate,
    CASE 
      WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
      WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
      WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
      WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
    END AS TheWeek
  FROM dds.day 
  WHERE thedate BETWEEN curdate() - 56 AND curdate() - 1
  GROUP BY theweek) e 
LEFT JOIN (    
  SELECT CAST(a.VehicleEvaluationTS AS sql_date) AS EvalDate, c.name AS Location,
    coalesce(b.fullname, 'Unfinished') as Evaluator, a.currentstatus
  FROM VehicleEvaluations a
  LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid
  LEFT JOIN organizations c ON a.locationid = c.partyid
  WHERE a.typ <> 'VehicleEvaluation_Auction'
    AND cast(VehicleEvaluationTS AS sql_date) BETWEEN CurDate() - 56 AND CurDate() - 1) f ON f.evaldate BETWEEN e.fromdate AND e.thrudate
GROUP BY f.location, f.evaluator 


SELECT h.market, h.location, h.evaluator, h.look1, h.book1, 
  CASE 
    WHEN h.book1 <> 0 THEN round((h.book1*1.0/h.look1) * 100, 0)
    ELSE 0 
  END AS Percent1,
  h.look2, h.book2,  
  CASE 
    WHEN h.book2 <> 0 THEN round((h.book2*1.0/h.look2) * 100, 0)
    ELSE 0 
  END AS Percent2,    
  h.look3, h.book3,  
  CASE 
    WHEN h.book3 <> 0 THEN round((h.book3*1.0/h.look3) * 100, 0)
    ELSE 0 
  END AS Percent3,  
  h.look4, h.book4,  
  CASE 
    WHEN h.book4 <> 0 THEN round((h.book4*1.0/h.look4) * 100, 0)
    ELSE 0 
  END AS Percent4          
FROM (
  SELECT 'Grand Forks' AS market, 'ALL' AS location, 'ALL' as evaluator,
    SUM(CASE WHEN theweek = 1 THEN 1 ELSE 0 END) AS Look1,
    SUM(CASE WHEN theweek = 1 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book1, 
    SUM(CASE WHEN theweek = 2 THEN 1 ELSE 0 END) AS Look2,
    SUM(CASE WHEN theweek = 2 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book2, 
    SUM(CASE WHEN theweek = 3 THEN 1 ELSE 0 END) AS Look3,
    SUM(CASE WHEN theweek = 3 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book3, 
    SUM(CASE WHEN theweek = 4 THEN 1 ELSE 0 END) AS Look4,
    SUM(CASE WHEN theweek = 4 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book4 
  FROM (
    SELECT min(thedate) AS fromDate, MAX(thedate) AS ThruDate,
      CASE 
        WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
        WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
        WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
        WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
      END AS TheWeek
    FROM dds.day 
    WHERE thedate BETWEEN curdate() - 56 AND curdate() - 1
    GROUP BY theweek) e 
  LEFT JOIN (    
    SELECT CAST(a.VehicleEvaluationTS AS sql_date) AS EvalDate, c.name AS Location,
      coalesce(b.fullname, 'Unfinished') as Evaluator, a.currentstatus
    FROM VehicleEvaluations a
    LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid
    LEFT JOIN organizations c ON a.locationid = c.partyid
    WHERE a.typ <> 'VehicleEvaluation_Auction'
      AND cast(VehicleEvaluationTS AS sql_date) BETWEEN CurDate() - 56 AND CurDate() - 1) j ON j.evaldate BETWEEN e.fromdate AND e.thrudate
  UNION 
  SELECT 'Grand Forks' as  market, f.location, 'ALL' AS evaluator,
    SUM(CASE WHEN theweek = 1 THEN 1 ELSE 0 END) AS Look1,
    SUM(CASE WHEN theweek = 1 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book1, 
    SUM(CASE WHEN theweek = 2 THEN 1 ELSE 0 END) AS Look2,
    SUM(CASE WHEN theweek = 2 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book2, 
    SUM(CASE WHEN theweek = 3 THEN 1 ELSE 0 END) AS Look3,
    SUM(CASE WHEN theweek = 3 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book3, 
    SUM(CASE WHEN theweek = 4 THEN 1 ELSE 0 END) AS Look4,
    SUM(CASE WHEN theweek = 4 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book4 
  FROM (
    SELECT min(thedate) AS fromDate, MAX(thedate) AS ThruDate,
      CASE 
        WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
        WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
        WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
        WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
      END AS TheWeek
    FROM dds.day 
    WHERE thedate BETWEEN curdate() - 56 AND curdate() - 1
    GROUP BY theweek) e 
  LEFT JOIN (    
    SELECT CAST(a.VehicleEvaluationTS AS sql_date) AS EvalDate, c.name AS Location,
      coalesce(b.fullname, 'Unfinished') as Evaluator, a.currentstatus
    FROM VehicleEvaluations a
    LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid
    LEFT JOIN organizations c ON a.locationid = c.partyid
    WHERE a.typ <> 'VehicleEvaluation_Auction'
      AND cast(VehicleEvaluationTS AS sql_date) BETWEEN CurDate() - 56 AND CurDate() - 1) f ON f.evaldate BETWEEN e.fromdate AND e.thrudate
  GROUP BY f.location  
  UNION 
  SELECT 'Grand Forks' AS market, f.location, evaluator,
    SUM(CASE WHEN theweek = 1 THEN 1 ELSE 0 END) AS Look1,
    SUM(CASE WHEN theweek = 1 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book1, 
    SUM(CASE WHEN theweek = 2 THEN 1 ELSE 0 END) AS Look2,
    SUM(CASE WHEN theweek = 2 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book2, 
    SUM(CASE WHEN theweek = 3 THEN 1 ELSE 0 END) AS Look3,
    SUM(CASE WHEN theweek = 3 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book3, 
    SUM(CASE WHEN theweek = 4 THEN 1 ELSE 0 END) AS Look4,
    SUM(CASE WHEN theweek = 4 AND currentstatus = 'VehicleEvaluation_Booked' THEN 1 ELSE 0 END) AS Book4 
  FROM (
    SELECT min(thedate) AS fromDate, MAX(thedate) AS ThruDate,
      CASE 
        WHEN TheDate >= CurDate() - 14 AND TheDate <= CurDate() - 1 THEN 1
        WHEN TheDate >= CurDate() - 28 AND TheDate <= CurDate() - 15 THEN 2
        WHEN TheDate >= CurDate() - 42 AND TheDate <= CurDate() - 29 THEN 3
        WHEN TheDate >= CurDate() - 56 AND TheDate <= CurDate() - 43 THEN 4
      END AS TheWeek
    FROM dds.day 
    WHERE thedate BETWEEN curdate() - 56 AND curdate() - 1
    GROUP BY theweek) e 
  LEFT JOIN (    
    SELECT CAST(a.VehicleEvaluationTS AS sql_date) AS EvalDate, c.name AS Location,
      coalesce(b.fullname, 'Unfinished') as Evaluator, a.currentstatus
    FROM VehicleEvaluations a
    LEFT JOIN people b ON a.VehicleEvaluatorID = b.partyid
    LEFT JOIN organizations c ON a.locationid = c.partyid
    WHERE a.typ <> 'VehicleEvaluation_Auction'
      AND cast(VehicleEvaluationTS AS sql_date) BETWEEN CurDate() - 56 AND CurDate() - 1) f ON f.evaldate BETWEEN e.fromdate AND e.thrudate
  GROUP BY f.location, f.evaluator) h
  
  
  
