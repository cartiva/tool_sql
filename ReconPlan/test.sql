SELECT *
FROM VehicleInventoryItemStatuses
WHERE category IN ('MechanicalReconProcess', 'BodyReconProcess', 'AppearanceReconProcess')
AND thruts IS NULL
ORDER BY VehicleInventoryItemID 

          WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
          WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
          WHEN 'MechanicalReconProcess_NotStarted' THEN 'Open'

SELECT stocknumber, 
  (SELECT
      CASE 
          WHEN status = 'MechanicalReconProcess_InProcess' THEN 'WIP'
          WHEN status = 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
          WHEN status = 'MechanicalReconProcess_NotStarted' THEN 'Open      
      END
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND category = 'MechanicalReconProcess'
    AND ThruTS IS NULL) AS mStatus
/*    
  (SELECT status
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND category = 'BodyReconProcess'
    AND ThruTS IS NULL) AS bStatus,
  (SELECT status
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND category = 'AppearanceReconProcess'
    AND ThruTS IS NULL) AS mStatus  
*/          
FROM VehicleInventoryItems v
WHERE thruts IS NULL     


SELECT VehicleInventoryItemID, 
      CASE WHEN status = 'MechanicalReconProcess_InProcess' THEN 'WIP' END, 
      CASE    WHEN status = 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None' END ,
      CASE   WHEN status = 'MechanicalReconProcess_NotStarted' THEN 'Open'  END      

    FROM VehicleInventoryItemStatuses
--    WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
    WHERE category = 'MechanicalReconProcess'
    AND ThruTS IS NULL
    ORDER BY VehicleInventoryItemID 
    