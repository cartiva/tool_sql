
/*
StatusCode AS status
ScheduleDate AS Expected IN
ShopOutTime AS Expected OUT

0: Pending
100: Prospect
200: Scheduled
300: Arrived
400: Released
440: Paint
450: Reass
999: Archived
1100: Deleted
*/

/*
11/22
still getting dups
11/23
only dup currently IS 11987xx: 2 records IN bsjobs, one AS prospect one AS released
*/

SELECT op.stocknumber, op.bodystatus,
  bs.*
FROM vUsedCarsOpenRecon op
LEFT JOIN (
  SELECT 
    CASE 
      WHEN Position(' ' IN TRIM(customer)) > 0 THEN
        left(substring(TRIM(customer), 3, Position(' ' IN TRIM(customer))-3),20)
      WHEN Position('-' IN TRIM(customer)) > 0 THEN
        left(substring(TRIM(customer), 3, Position('-' IN TRIM(customer))-3),20) 
      WHEN Position('(' IN TRIM(customer)) > 0 THEN
        left(substring(TRIM(customer), 3, Position('(' IN TRIM(customer))-3),20)           
      ELSE
        left(substring(customer, 3, 21),20)
    END AS Stocknumber, 
    CASE 
      WHEN StatusCode = 0 THEN 'Pending' 
      WHEN StatusCode = 100 THEN 'Prospect'
      WHEN StatusCode = 200 THEN 'Scheduled'
      WHEN StatusCode = 300 THEN 'Arrived'
      WHEN StatusCode = 400 THEN 'Released'
      WHEN StatusCode = 440 THEN 'Paint'
      WHEN StatusCode = 450 THEN 'Reass'
      WHEN StatusCode = 999 THEN 'Archived'
      WHEN StatusCode = 1100 THEN 'Deleted'
    END AS Status,   
    ScheduleDate, ShopOutTime
  FROM cartivadps.bsjobs
  WHERE usedcar = true
    AND customer LIKE 'UC%') bs ON op.stocknumber = bs.stocknumber collate ADS_DEFAULT_CI
WHERE op.OwningLocationID = (
  SELECT partyid
  FROM organizations
  WHERE name = 'Rydells')    
AND op.Status LIKE 'BodyReconProcess%'
AND BodyStatus <> 'No Open Items'
ORDER BY op.stocknumber


