/*
SELECT * FROM partyprivileges WHERE partyid = '2064B418-3C9D-4405-A275-5EF0D73CC2E3'  
ORDER BY locationid, privilege
SELECT * FROM users WHERE username LIKE 'mtwet%'
SELECT DISTINCT privilege FROM partyprivileges
*/

SELECT distinct p.PartyID, left(pe.fullname, 25) AS name, left(cm.Description, 25) AS email, u.username, u.password, left(au.AppName, 25) AS AppName,
  left(pp.Privilege, 25) AS Privilege, left(pr.Typ, 35) AS PartyRel,
  (SELECT name FROM organizations WHERE partyid = pp.locationid) AS location
FROM Parties p
LEFT JOIN People pe ON pe.PartyID = p.PartyID
LEFT JOIN ContactMechanisms cm ON cm.PartyID = p.PartyID AND cm.typ = 'ContactMechanism_WorkEmail'
  AND cm.thruts IS NULL 
LEFT JOIN users u ON u.partyid = p.partyid
  AND u.active = true
LEFT JOIN applicationusers au ON au.partyid = u.partyid AND au.ThruTS IS NULL 
LEFT JOIN PartyPrivileges pp ON pp.partyid = p.partyid AND pp.ThruTS IS NULL 
LEFT JOIN PartyRelationships pr ON pr.partyid2 = p.partyid AND pr.ThruTS IS NULL 
--WHERE pp.privilege = 'Privilege_InvMgr'
WHERE pe.lastname LIKE '%homst%'

/* nick needed access to sales mgr for both stores
SELECT b.fullname, c.fullname, a.privilege
FROM partyprivileges a
INNER JOIN people b on a.partyid = b.partyid
INNER JOIN organizations c on a.locationid = c.partyid
WHERE a.thruts IS NULL 
  AND a.privilege = 'Privilege_InvMgr'
ORDER BY b.fullname  
*/



