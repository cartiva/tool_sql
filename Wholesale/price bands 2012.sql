SELECT soldto, fullname 
FROM vehiclesales s
LEFT JOIN organizations o ON s.soldto = o.partyid collate ads_default_ci
INNER JOIN partyrelationships p ON o.partyid = p.partyid2
WHERE s.typ = 'VehicleSale_Wholesale' AND CAST(SoldTS AS sql_date) > curdate() - 120
  AND p.typ = 'PartyRelationship_MarketAuctions'

SELECT i.stocknumber, CAST(i.fromts AS sql_date) AS "Owned From", CAST(s.SoldTS AS sql_date) AS "WS On",
  s.SoldAmount AS "WS For", 
  TRIM(v.yearmodel) + ' ' +  TRIM(v.make) + ' ' + TRIM(v.model) AS vehicle
FROM vehiclesales s
INNER JOIN VehicleInventoryItems i ON s.VehicleInventoryItemID = i.VehicleInventoryItemID 
  AND OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')
LEFT JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
WHERE typ = 'VehicleSale_Wholesale'
  AND CAST(SoldTS AS sql_date) > curdate() - 120
  
  
SELECT 
  SUM(CASE WHEN stocknumber = 'xxx' THEN 0 ELSE 1 END) AS Total, 
  SUM(CASE WHEN "WS For" < 2000 THEN 1 ELSE 0 END) AS "<$2k",
  SUM(CASE WHEN "WS For" BETWEEN 2000 AND 7999 THEN 1 ELSE 0 END) AS "$2k - $8k", 
  SUM(CASE WHEN "WS For" BETWEEN 8000 AND 14000 THEN 1 ELSE 0 END) AS "$8k - $14k",
  SUM(CASE WHEN "WS For" > 14000 THEN 1 ELSE 0 END) AS ">$14k"   
FROM (
SELECT i.stocknumber, CAST(i.fromts AS sql_date) AS "Owned From", CAST(s.SoldTS AS sql_date) AS "WS On",
  s.SoldAmount AS "WS For", 
  TRIM(v.yearmodel) + ' ' +  TRIM(v.make) + ' ' + TRIM(v.model) AS vehicle
FROM vehiclesales s
INNER JOIN VehicleInventoryItems i ON s.VehicleInventoryItemID = i.VehicleInventoryItemID 
LEFT JOIN VehicleItems v ON i.VehicleItemID = v.VehicleItemID 
LEFT JOIN organizations o ON s.soldto = o.partyid collate ads_default_ci
INNER JOIN partyrelationships p ON o.partyid = p.partyid2
WHERE s.typ = 'VehicleSale_Wholesale'
  AND CAST(SoldTS AS sql_date) > curdate() - 120
  AND p.typ = 'PartyRelationship_MarketAuctions') x  
  
-- BOPMAST
SELECT 
  SUM(CASE WHEN bmstk# = 'xxx' THEN 0 ELSE 1 END) AS Total, 
  SUM(CASE WHEN bmpric < 2000 THEN 1 ELSE 0 END) AS "<$2k",
  SUM(CASE WHEN bmpric BETWEEN 2000 AND 7999 THEN 1 ELSE 0 END) AS "$2k - $8k", 
  SUM(CASE WHEN bmpric BETWEEN 8000 AND 14000 THEN 1 ELSE 0 END) AS "$8k - $14k",
  SUM(CASE WHEN bmpric > 14000 THEN 1 ELSE 0 END) AS ">$14k"   
FROM (
SELECT bmstk#, bmvin, bmsnam, bmpric,bmvcst,bmicst,bmdtaprv, bmdtcap
FROM dds.stgArkonaBOPMAST
WHERE bmstat = 'U'
  AND bmwhsl = 'W'
  AND bmdtaprv > '12/31/2011'
  AND bmsnam IN (
    'ADESA','MID STATE AUCTION','MIDSTATE AUTO AUCTION')) x
  
  