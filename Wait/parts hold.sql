SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS, max(p.receivedTS) AS receivedTS
FROM #jon j
  LEFT JOIN (
  SELECT v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS
  FROM partsorders po
  LEFT JOIN VehicleReconItems v ON v.VehicleReconItemID = po.VehicleReconItemID
  WHERE orderedTS <> receivedTS
  GROUP BY v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS) p ON j.VehicleInventoryItemID = p.VehicleInventoryItemID 
WHERE j.VehicleInventoryItemID IS NOT NULL 
AND p.receivedTS > j.pulledTS 
AND p.receivedTS <= j.pbTS -- eliminates parts ord after pb
GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS -- handle multiple orders ordered at the same time, but received at diff times
ORDER BY j.VehicleInventoryItemID   

GROUP BY orderedts, THEN MAX(received) to handle multiple orders ordered at the same time, but received at diff times


SELECT w.VehicleInventoryItemID, w.pulledTS, w.pbTS, MechStart, MechEnd, pt.orderedTS, pt.receivedts 
FROM #WipStartTimes w
LEFT JOIN (
  SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS, max(p.receivedTS) AS receivedTS
  FROM #jon j
    LEFT JOIN (
    SELECT v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS
    FROM partsorders po
    LEFT JOIN VehicleReconItems v ON v.VehicleReconItemID = po.VehicleReconItemID
    WHERE orderedTS <> receivedTS
    GROUP BY v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS) p ON j.VehicleInventoryItemID = p.VehicleInventoryItemID 
  WHERE j.VehicleInventoryItemID IS NOT NULL 
  AND p.receivedTS > j.pulledTS 
  AND p.receivedTS <= j.pbTS -- eliminates parts ord after pb
  GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS) pt ON w.VehicleInventoryItemID = pt.VehicleInventoryItemID 
WHERE pt.orderedTS IS NOT NULL 

-- what i need to know IS WHEN IS the vehicle ready for mechanical
-- overlap with mech wait times

SELECT x.*, pt.orderedTS, pt.receivedTS, position('m' IN seq) AS mPos 
INTO #MechParts
FROM (
  SELECT w.*,
    CASE
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
        CASE
          WHEN MechStart < BodyStart THEN 'mb'
          ELSE 'bm'
        END
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN MechStart < AppStart THEN 'ma'
          ELSE 'am'
        END
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN BodyStart < AppStart THEN 'ba'
          ELSE 'ab'
        END 
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
        CASE 
          WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
          WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
        END 
    END AS seq     
  FROM #WipStartTimes w) x 
LEFT JOIN (
  SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS, max(p.receivedTS) AS receivedTS
  FROM #jon j
    LEFT JOIN (
    SELECT v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS
    FROM partsorders po
    LEFT JOIN VehicleReconItems v ON v.VehicleReconItemID = po.VehicleReconItemID
    WHERE orderedTS <> receivedTS
    GROUP BY v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS) p ON j.VehicleInventoryItemID = p.VehicleInventoryItemID 
  WHERE j.VehicleInventoryItemID IS NOT NULL 
  AND p.receivedTS > j.pulledTS 
  AND p.receivedTS <= j.pbTS -- eliminates parts ord after pb
  GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS) pt ON x.VehicleInventoryItemID = pt.VehicleInventoryItemID   
WHERE pt.orderedTS IS NOT NULL  
  
  

  
--IF seq m** THEN pull - mechstart
--IF seq *m* OR **m THEN the previous endTS
-- DROP TABLE #MechParts
SELECT  m.*, 
--  VehicleInventoryItemID, pulledTS, MechStart, orderedTS, receivedTS,
  CASE 
    WHEN mPos = 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, receivedTS, 'Service') FROM system.iota) --timestampdiff(sql_tsi_hour, pulledTS, receivedTS)
    
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN (select Utilities.BusinessHoursFromInterval(BodyEnd, receivedTS, 'Service') FROM system.iota) --timestampdiff(sql_tsi_hour, BodyEnd, receivedTS)
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN (select Utilities.BusinessHoursFromInterval(AppEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, AppEnd, receivedTS)
      END
     
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN (select Utilities.BusinessHoursFromInterval(BodyEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, BodyEnd, receivedTS)
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN (select Utilities.BusinessHoursFromInterval(AppEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, AppEnd, receivedTS)
      END  
      
  END AS PartsHoldBusHours
FROM #MechParts m
ORDER BY mPos, seq


2 records for 977ef457-18ad-4627-84fc-a185925c192d
-- there are 3 of them
SELECT VehicleInventoryItemID 
FROM #MechParts
GROUP BY VehicleInventoryItemID
HAVING COUNT(*) > 1

-- how am i going to handle these
-- have been trying to make base tables 1 row per vehicle
SELECT *
FROM #mechparts
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID 
  FROM #MechParts
  GROUP BY VehicleInventoryItemID
  HAVING COUNT(*) > 1)

so, i am looking to collapes the multiple rows IN #MechParts INTO single rows
CASE ON a COUNT ?
OR IN the construction of #MechParts
-- SELECT * FROM #MechParts
/*
SELECT VehicleInventoryItemID,
  CASE COUNT(VehicleInventoryItemID) 
  -- only need to derive ordS AND recTS
    -- shit what about 3 OR more
    -- DO a SUM ON hours?
    WHEN > 1 THEN 
      CASE -- multiples ALL contained with IN one interval 
      END 
  END 
FROM (
  SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS, max(p.receivedTS) AS receivedTS
  FROM #jon j
    LEFT JOIN (
    SELECT v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS
    FROM partsorders po
    LEFT JOIN VehicleReconItems v ON v.VehicleReconItemID = po.VehicleReconItemID
    WHERE orderedTS <> receivedTS
    GROUP BY v.VehicleInventoryItemID, po.OrderedTS, po.ReceivedTS) p ON j.VehicleInventoryItemID = p.VehicleInventoryItemID 
  WHERE j.VehicleInventoryItemID IS NOT NULL 
  AND p.receivedTS > j.pulledTS 
  AND p.receivedTS <= j.pbTS -- eliminates parts ord after pb

  GROUP BY j.VehicleInventoryItemID, j.pulledTS, j.pbTS, p.orderedTS)q
GROUP BY VehicleInventoryItemID 
*/
IF NOT IN the creations of the temp tables
THEN IN the derivation of parts hold hours
first thing, only need to return VehicleInventoryItemID, ordTS & recTS
GROUP BY VehicleInventoryItemID 
remove the ORDER by
MAX the cas
UNION ?

SELECT  m.VehicleInventoryItemID, 
  COUNT(VehicleInventoryItemID) AS ViiCount,
  MAX(
    CASE 
      WHEN mPos = 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, receivedTS, 'Service') FROM system.iota) --timestampdiff(sql_tsi_hour, pulledTS, receivedTS)
      WHEN mPos = 2 THEN 
        CASE
          WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN (select Utilities.BusinessHoursFromInterval(BodyEnd, receivedTS, 'Service') FROM system.iota) --timestampdiff(sql_tsi_hour, BodyEnd, receivedTS)
          WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN (select Utilities.BusinessHoursFromInterval(AppEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, AppEnd, receivedTS)
        END
      WHEN mPos = 3 THEN
        CASE
          WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN (select Utilities.BusinessHoursFromInterval(BodyEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, BodyEnd, receivedTS)
          WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN (select Utilities.BusinessHoursFromInterval(AppEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, AppEnd, receivedTS)
        END  
    END) AS PartsHoldBusHours
FROM #MechParts m
GROUP BY VehicleInventoryItemID 
ORDER BY COUNT(VehicleInventoryItemID) DESC 

SELECT  m.*, 
  CASE 
    WHEN mPos = 1 THEN (select Utilities.BusinessHoursFromInterval(pulledTS, receivedTS, 'Service') FROM system.iota) --timestampdiff(sql_tsi_hour, pulledTS, receivedTS)
    WHEN mPos = 2 THEN 
      CASE
        WHEN LEFT(seq, 1) = 'b' AND receivedTS > BodyEnd THEN (select Utilities.BusinessHoursFromInterval(BodyEnd, receivedTS, 'Service') FROM system.iota) --timestampdiff(sql_tsi_hour, BodyEnd, receivedTS)
        WHEN LEFT(seq, 1) = 'a' AND receivedTS > AppEnd THEN (select Utilities.BusinessHoursFromInterval(AppEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, AppEnd, receivedTS)
      END
    WHEN mPos = 3 THEN
      CASE
        WHEN substring(seq, 2, 1) = 'b' AND receivedTS > BodyEnd THEN (select Utilities.BusinessHoursFromInterval(BodyEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, BodyEnd, receivedTS)
        WHEN substring(seq, 2, 1) = 'a' AND receivedTS > AppEnd THEN (select Utilities.BusinessHoursFromInterval(AppEnd, receivedTS, 'Service') FROM system.iota) -- timestampdiff(sql_tsi_hour, AppEnd, receivedTS)
      END  
  END AS PartsHoldBusHours
FROM #MechParts m
WHERE VehicleInventoryItemID IN ('977ef457-18ad-4627-84fc-a185925c192d','73223400-2632-44d1-9090-d2bbe848aeb6','1167a920-b377-4e9d-9f5f-538a970e90fa')
ORDER BY VehicleInventoryItemID 

-- use this AS a basis for unioning with those vehicles with one parts hold 
-- the issue IS to now parse out the parts hold times for these records
SELECT mm.VehicleInventoryItemID, mm.minOrd, mm.maxRec, m.orderedTS, m.receivedTS 
FROM (
  SELECT VehicleInventoryItemID,
    MIN(orderedTS) AS minOrd, MAX(receivedTS) AS maxRec
  FROM #MechParts mp
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID
    FROM #MechParts
    GROUP BY VehicleInventoryItemID
    HAVING COUNT(*) > 1)
  GROUP BY VehicleInventoryItemID) mm
LEFT JOIN #MechParts m ON mm.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE mm.VehicleInventoryItemID IN ('977ef457-18ad-4627-84fc-a185925c192d','73223400-2632-44d1-9090-d2bbe848aeb6','1167a920-b377-4e9d-9f5f-538a970e90fa')
AND m.receivedTS < mm.maxRec AND m.orderedTS <> mm.minOrd
/*
        ordTS         recTS         MIN ordTS   MAX recTS
0fa     5/13  2:14  5/14  2:42      5/13 2:14   5/18 6:59   ALL orders are sequential, with gaps
        5/16 10:57  5/18  6:59
        
eb6     6/17 10:03  6/21  1:15      6/17 10:03  6/21 1:15   ALL orders within one interval
        6/20  7:35  6/21 10:55  
        
92d     4/01  3:15  6/08  9:45      4/01 3:15   6/8 9:45    ALL orders within one interval
        4/04  2:15  4/13  9:15 
possibilities:
    ALL orders within one interval
    ALL orders are sequential, with gaps
    some orders are within ON interval some are sequential
*/
-- this looks LIKE a functional base
-- this IS WHERE i need to DO business hours
SELECT mm.VehicleInventoryItemID, 
  CASE
    WHEN (m.receivedTS < mm.maxRec AND m.orderedTS <> mm.minOrd) THEN
      timestampdiff(sql_tsi_hour, mm.minOrd, mm.maxRec)
  ELSE timestampdiff(sql_tsi_hour, m.orderedTS, m.receivedTS)
  END AS hours
-- SELECT *  
FROM (
  SELECT VehicleInventoryItemID,
    MIN(orderedTS) AS minOrd, MAX(receivedTS) AS maxRec
  FROM #MechParts mp
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID
    FROM #MechParts
    GROUP BY VehicleInventoryItemID
    HAVING COUNT(*) > 1)
  GROUP BY VehicleInventoryItemID) mm
LEFT JOIN #MechParts m ON mm.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE mm.VehicleInventoryItemID IN ('977ef457-18ad-4627-84fc-a185925c192d','73223400-2632-44d1-9090-d2bbe848aeb6','1167a920-b377-4e9d-9f5f-538a970e90fa')

-- YES -- here are the raw hours
-- what's missing:  some orders are within ON interval some are sequential -- DO a test ON local CREATE an ORDER that fits this pattern
-- business hours
SELECT VehicleInventoryItemID, SUM(hours)
FROM (
  SELECT distinct mm.VehicleInventoryItemID,
    CASE
      WHEN (m.receivedTS < mm.maxRec AND m.orderedTS <> mm.minOrd) THEN
        timestampdiff(sql_tsi_hour, mm.minOrd, mm.maxRec)
    ELSE timestampdiff(sql_tsi_hour, m.orderedTS, m.receivedTS)
    END AS hours
  FROM (
    SELECT VehicleInventoryItemID,
      MIN(orderedTS) AS minOrd, MAX(receivedTS) AS maxRec
    FROM #MechParts mp
    WHERE VehicleInventoryItemID IN (
      SELECT VehicleInventoryItemID
      FROM #MechParts
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1)
    GROUP BY VehicleInventoryItemID) mm
  LEFT JOIN #MechParts m ON mm.VehicleInventoryItemID = m.VehicleInventoryItemID 
  WHERE mm.VehicleInventoryItemID IN ('977ef457-18ad-4627-84fc-a185925c192d','73223400-2632-44d1-9090-d2bbe848aeb6','1167a920-b377-4e9d-9f5f-538a970e90fa')) x
GROUP BY VehicleInventoryItemID

local test ON 0fa, ADD an ORDER FROM
hmmm overlaps AS well

/*
1.  rec1 - ord1 (min/max)
     ord1                          rec1   
      |-----------------------------|
            ord2          rec2      
              |-------------|
              
2.  (rec1 - ord1) + (rec2 - ord2)                  
     ord1             rec1   ord2        rec2
      |----------------|      |------------|                   
      
3.  (rec1 - ord1) + (rec3 = ord3)
     ord1                                rec1   
      |-----------------------------------|                    
            ord2          rec2                ord3      rec3     
              |-------------|                 |----------|        
                      ord4         rec4
                       |-------------|   
                       
4. (rec1 - ord1) + (rec2 - rec1)                          
     ord1                                rec1   
      |-----------------------------------|                    
                     ord1                          rec2
                       |-----------------------------|                

*/

        ordTS         recTS         MIN ordTS   MAX recTS
0fa     5/13  2:14  5/14  2:42      5/13 2:14   5/18 6:59   ALL orders are sequential, with gaps
        5/16 10:57  5/18  6:59
a series of different version of this TABLE differentiated BY WHERE clauses? 
a series of grouping based ON encasing intervals

-- pattern 1 -- ALL vehicles with mult parts orders that fit pattern 1
SELECT mm.VehicleInventoryItemID, mm.minOrd, mm.maxRec, orderedTS, receivedTS
FROM (
  SELECT VehicleInventoryItemID,
    MIN(orderedTS) AS minOrd, MAX(receivedTS) AS maxRec
  FROM #MechParts mp
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID
    FROM #MechParts
    GROUP BY VehicleInventoryItemID
    HAVING COUNT(*) > 1)
  GROUP BY VehicleInventoryItemID) mm
LEFT JOIN #MechParts m ON mm.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE mm.VehicleInventoryItemID IN ('977ef457-18ad-4627-84fc-a185925c192d','73223400-2632-44d1-9090-d2bbe848aeb6','1167a920-b377-4e9d-9f5f-538a970e90fa')
AND m.receivedTS < mm.maxRec AND m.orderedTS <> mm.minOrd

-- pattern 2 ALL vehicles with mult parts orders that fit pattern 2
SELECT mm.VehicleInventoryItemID, mm.minOrd, mm.maxRec, m.orderedTS, m.receivedTS 
FROM (
  SELECT VehicleInventoryItemID,
    MIN(orderedTS) AS minOrd, MAX(receivedTS) AS maxRec
  FROM #MechParts mp
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID
    FROM #MechParts
    GROUP BY VehicleInventoryItemID
    HAVING COUNT(*) > 1)
  GROUP BY VehicleInventoryItemID) mm
LEFT JOIN #MechParts m ON mm.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE mm.VehicleInventoryItemID IN ('977ef457-18ad-4627-84fc-a185925c192d','73223400-2632-44d1-9090-d2bbe848aeb6','1167a920-b377-4e9d-9f5f-538a970e90fa')
AND m.orderedTS > mm.minOrd
AND not (m.receivedTS < mm.maxRec AND m.orderedTS <> mm.minOrd)

/*
1.  rec1 - ord1 (min/max)
     ord1                          rec1   
      |-----------------------------|
            ord2          rec2      
              |-------------|
              
2.  (rec1 - ord1) + (rec2 - ord2)                  
     ord1             rec1   ord2        rec2
      |----------------|      |------------|                   
      
3.  (rec1 - ord1) + (rec3 = ord3)
     ord1                                rec1   
      |-----------------------------------|                    
            ord2          rec2                ord3      rec3     
              |-------------|                 |----------|        
                      ord4         rec4
                       |-------------|   
                       
4. (rec1 - ord1) + (rec2 - rec1)                          
     ord1                                rec1   
      |-----------------------------------|                    
                     ord1                          rec2
                       |-----------------------------|                

*/
-- TRY CASE instead of WHERE
-- keep trying to generalize to handle any number of parts orders
-- GROUP BY orderedTS
SELECT mm.VehicleInventoryItemID, mm.minOrd, mm.maxRec, orderedTS, receivedTS
FROM (
  SELECT VehicleInventoryItemID,
    MIN(orderedTS) AS minOrd, MAX(receivedTS) AS maxRec
  FROM #MechParts mp
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID
    FROM #MechParts
    GROUP BY VehicleInventoryItemID
    HAVING COUNT(*) > 1)
  GROUP BY VehicleInventoryItemID) mm
LEFT JOIN #MechParts m ON mm.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE mm.VehicleInventoryItemID IN ('977ef457-18ad-4627-84fc-a185925c192d','73223400-2632-44d1-9090-d2bbe848aeb6','1167a920-b377-4e9d-9f5f-538a970e90fa')


SELECT mm.VehicleInventoryItemID, max(mm.minOrd), max(mm.maxRec), orderedTS , max(receivedTS)
FROM (
  SELECT VehicleInventoryItemID,
    MIN(orderedTS) AS minOrd, MAX(receivedTS) AS maxRec
  FROM #MechParts mp
  WHERE VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID
    FROM #MechParts
    GROUP BY VehicleInventoryItemID
    HAVING COUNT(*) > 1)
  GROUP BY VehicleInventoryItemID) mm
LEFT JOIN #MechParts m ON mm.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE mm.VehicleInventoryItemID IN ('977ef457-18ad-4627-84fc-a185925c192d','73223400-2632-44d1-9090-d2bbe848aeb6','1167a920-b377-4e9d-9f5f-538a970e90fa')
GROUP BY mm.VehicleInventoryItemID, orderedTS
        
