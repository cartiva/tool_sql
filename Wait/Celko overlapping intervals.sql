-- p 657
DROP TABLE #intervals;
CREATE TABLE #intervals (
  x integer,
  y integer);
  
INSERT INTO #intervals values(1,3);
INSERT INTO #intervals values(2,5); 
INSERT INTO #intervals values(4,11); 
INSERT INTO #intervals values(10,12); 
INSERT INTO #intervals values(20,21); 
INSERT INTO #intervals values(120,130); 
INSERT INTO #intervals values(120,128); 
INSERT INTO #intervals values(120,122); 
INSERT INTO #intervals values(121,132); 
INSERT INTO #intervals values(121,122); 
INSERT INTO #intervals values(121,124); 
INSERT INTO #intervals values(121,123);  
INSERT INTO #intervals values(126,127);  

SELECT i1.x, MAX(i2.y) AS y
FROM #Intervals AS i1
INNER JOIN #Intervals AS i2 ON i2.y > i1.x
WHERE NOT EXISTS ( -- i1.x - 1 IS NOT BETWEEN any other interval
  SELECT *
  FROM #Intervals AS i3
  WHERE i1.x - 1 BETWEEN i3.x AND i3.y)
AND NOT EXISTS ( -- 
  SELECT *
  FROM #Intervals AS i4
  WHERE i4.y > i1.x
  AND i4.y < i2.y
  AND NOT EXISTS (
    SELECT *
    FROM #Intervals AS i5
    WHERE i4.y + 1 BETWEEN i5.x AND i5.y))
GROUP BY i1.x;


SELECT i1.x, MAX(i2.y) AS y
--SELECT *
FROM #Intervals AS i1
INNER JOIN #Intervals AS i2 ON i2.y > i1.x
GROUP BY i1.x;

-- this doesn't WORK
-- can't see I5
SELECT I1.x, MAX(I2.y) AS y
FROM #Intervals AS I1
INNER JOIN
#Intervals AS I2
ON I2.y > I1.x
LEFT OUTER JOIN
#Intervals AS I3
ON I1.x - 1 BETWEEN I3.x AND I3.y
LEFT OUTER JOIN
(#Intervals AS I4
LEFT OUTER JOIN
#Intervals AS I5
ON I4.y + 1 BETWEEN I5.x AND I5.y)
ON I4.y > I1.x
AND I4.y < I2.y
AND I5.x IS NULL
WHERE I3.x IS NULL
AND I4.x IS NULL

what i want are the DISTINCT non overlapping intervals