select VehicleEvaluationTS,
	(select blackbookpubdate
	from BlackBookPubDates 
	where VehicleEvaluationTS Between convert(FromDate, SQL_TIMESTAMP) AND convert(ThruDate, SQL_TIMESTAMP))
from VehicleEvaluations

select VehicleEvaluationTS,
	(select blackbookpubdate
	from BlackBookPubDates 
	where convert(VehicleEvaluationTS, SQL_DATE) Between FromDate AND ThruDate)
from VehicleEvaluations
