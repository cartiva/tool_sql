/*
SELECT DISTINCT status FROM VehicleInventoryItemStatuses 

  SELECT VehicleInventoryItemID, status, fromts, thruts
  FROM VehicleInventoryItemStatuses 
  WHERE status = 'RMFlagPulled_Pulled'
  AND timestampdiff(sql_tsi_day, fromts, now()) < 30
  
  
SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = 'cab4049e-2fe1-374d-99f2-9b414f1ff7d2'
  
*/
SELECT viix.stocknumber, viix.fromts, viix.thruts
FROM VehicleInventoryItems  viix
WHERE owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND status = 'RMFlagPulled_Pulled'
  AND CAST(fromTS AS sql_date) > curdate() - 30)


SELECT viix.stocknumber, cast(viisx.fromts AS sql_date) AS "Pull Date", 
  timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts) AS "Days To Avail"
-- SELECT COUNT(*)  
FROM VehicleInventoryItems  viix
INNER JOIN VehicleInventoryItemStatuses viisx ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND status = 'RMFlagPulled_Pulled'
  AND CAST(viisx.fromTS AS sql_date) > curdate() - 30 
WHERE viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND status = 'RMFlagAV_Available')  
  
SELECT avg("Days To Avail")
FROM ( 
SELECT viix.stocknumber, cast(viisx.fromts AS sql_date) AS "Pull Date", 
  timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts) AS "Days To Avail"
-- SELECT COUNT(*)  
FROM VehicleInventoryItems  viix
INNER JOIN VehicleInventoryItemStatuses viisx ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND status = 'RMFlagPulled_Pulled'
  AND CAST(viisx.fromTS AS sql_date) > curdate() - 30 
WHERE viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND status = 'RMFlagAV_Available')) wtf
  
  
SELECT "Days To Avail", COUNT(*)
FROM ( 
SELECT viix.stocknumber, cast(viisx.fromts AS sql_date) AS "Pull Date", 
  timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts) AS "Days To Avail"
-- SELECT COUNT(*)  
FROM VehicleInventoryItems  viix
INNER JOIN VehicleInventoryItemStatuses viisx ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND status = 'RMFlagPulled_Pulled'
  AND CAST(viisx.fromTS AS sql_date) > curdate() - 30 
WHERE viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND status = 'RMFlagAV_Available')) wtf 
GROUP BY "Days To Avail"   


-- 3/9 timeline FROM pull to available

-- these are vehicles that have been pulled
SELECT viix.VehicleInventoryItemID 
FROM VehicleInventoryItems viix
WHERE viix.ThruTS IS NULL
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
  AND category = 'RMFlagPulled')
AND viix.OwningLocationID = (SELECT partyid FROM organizations WHERE name = 'rydells')

-- Pull Date
SELECT viisx.VehicleInventoryItemID, viisx.FromTS
FROM VehicleInventoryItemStatuses viisx
WHERE category = 'RMFlagPulled'
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItems 
  WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND ThruTS IS NULL)

-- have been pulled more than once
-- what to DO with vehicles pulled more than once?
-- take the most recent pull ?
-- it doesn't matter, treat each pull separately, each pull identifiable BY VehicleInventoryItemID AND status fromTS

/* 
-- VehicleInventoryItems pulled more than once
SELECT VehicleInventoryItemID, COUNT(*)
FROM VehicleInventoryItemStatuses viisx
WHERE category = 'RMFlagPulled'
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItems 
  WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND ThruTS IS NULL)
GROUP BY VehicleInventoryItemID
HAVING COUNT(*) > 1

SELECT *
FROM VehicleInventoryItems
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItemStatuses viisx
  WHERE category = 'RMFlagPulled'
  AND EXISTS (
    SELECT 1
    FROM VehicleInventoryItems 
    WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
    AND ThruTS IS NULL)
  GROUP BY VehicleInventoryItemID
  HAVING COUNT(*) > 1)
*/

-- what IS the eventual disposition of each pull
-- pull	-> available
-- 		-> sold  

-- Pull Date
-- Recon status at time of pull

SELECT viisx.VehicleInventoryItemID, viisx.FromTS, 
  CASE 
    WHEN mech.status = 'MechanicalReconProcess_NotStarted' THEN 'NOT Started'
	WHEN mech.status =  'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
	WHEN mech.status =  'MechanicalReconProcess_InProcess' THEN 'WIP'
	ELSE 'WTF'
  END AS MechStatus,
  CASE 
    WHEN body.status = 'BodyReconProcess_NotStarted' THEN 'NOT Started'
	WHEN body.status =  'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
	WHEN body.status =  'BodyReconProcess_InProcess' THEN 'WIP'
	ELSE 'WTF'
  END AS BodyStatus 
FROM VehicleInventoryItemStatuses viisx
LEFT JOIN VehicleInventoryItemStatuses mech ON viisx.VehicleInventoryItemID = mech.VehicleInventoryItemID 
  AND mech.Category = 'MechanicalReconProcess'
  AND mech.FromTS <= viisx.fromts
LEFT JOIN VehicleInventoryItemStatuses body ON viisx.VehicleInventoryItemID = body.VehicleInventoryItemID 
  AND body.Category = 'BodyReconProcess'
  AND body.FromTS <= viisx.fromts
WHERE viisx.category = 'RMFlagPulled'
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItems 
  WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND ThruTS IS NULL)
ORDER BY viisx.VehicleInventoryItemID, mech.fromts  




SELECT viisx.VehicleInventoryItemID, viisx.FromTS, 
  CASE 
    WHEN mech.status = 'MechanicalReconProcess_NotStarted' THEN 'NOT Started'
	WHEN mech.status =  'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
	WHEN mech.status =  'MechanicalReconProcess_InProcess' THEN 'WIP'
	ELSE 'WTF'
  END AS MechStatus,
  CASE 
    WHEN mech.status = 'BodyReconProcess_NotStarted' THEN 'NOT Started'
	WHEN mech.status =  'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
	WHEN mech.status =  'BodyReconProcess_InProcess' THEN 'WIP'
	ELSE 'WTF'
  END AS BodyStatus 
FROM VehicleInventoryItemStatuses viisx
LEFT JOIN VehicleInventoryItemStatuses mech ON viisx.VehicleInventoryItemID = mech.VehicleInventoryItemID 
  AND mech.Category like '%econProcess'
  AND mech.FromTS <= viisx.fromts
WHERE viisx.category = 'RMFlagPulled'
AND EXISTS (
  SELECT 1
  FROM VehicleInventoryItems 
  WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
  AND ThruTS IS NULL)
ORDER BY viisx.VehicleInventoryItemID, mech.fromts  