
execute procedure sp_PackTable('reports1')

DELETE FROM reports1
WHERE CAST(reportTS AS sql_date) < '10/11/2014'

delete
FROM opentracksentandreceived
WHERE CAST(createdTS AS sql_date) < '10/01/2014'

execute procedure sp_PackTable('opentracksentandreceived')

SELECT *
FROM reports2
ORDER BY reportts DESC 

DELETE FROM reports2
WHERE CAST(reportts AS sql_date) < '09/01/2014'

execute procedure sp_PackTable('reports2')

SELECT * FROM vehiclesales

DELETE 
FROM viipictures 
WHERE VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID 
  FROM vehicleSales
  WHERE status = 'VehicleSale_Sold'
    AND CAST(soldts AS sql_date) < '09/01/2014')

execute procedure sp_PackTable('viipictures')    