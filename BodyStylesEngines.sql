SELECT bodystyle, COUNT(*)
FROM vehicleitems
GROUP BY bodystyle

SELECT engine, COUNT(*)
FROM vehicleitems
GROUP BY engine

SELECT engine, COUNT(*)
FROM vehicleitems
WHERE engine LIKE '%v6%'
GROUP BY engine

SELECT engine, COUNT(*)
FROM vehicleitems
WHERE engine LIKE '%flex%'
GROUP BY engine


SELECT engine, COUNT(*)
FROM vehicleitems
WHERE engine LIKE '%dsl%'
GROUP BY engine 

SELECT 
  (SELECT COUNT(*) FROM vehicleitems WHERE engine LIKE '%flex%') AS flex,
  (SELECT COUNT(*) FROM vehicleitems WHERE engine LIKE '%dsl%') AS dsl,
  (SELECT COUNT(*) FROM vehicleitems WHERE engine LIKE '%i-4%') AS "Inline 4",
  (SELECT COUNT(*) FROM vehicleitems WHERE engine LIKE '%i-5%') AS "Inline 5",
  (SELECT COUNT(*) FROM vehicleitems WHERE engine LIKE '%i-6%') AS "Inline 6",
  (SELECT COUNT(*) FROM vehicleitems WHERE engine LIKE '%v6%') AS V6,
  (SELECT COUNT(*) FROM vehicleitems WHERE engine LIKE '%v8%') AS V8
FROM system.iota  
  