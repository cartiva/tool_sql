SELECT *
-- SELECT COUNT(*) --(12k:1128 14k:1366)
FROM vehiclesales
WHERE CAST(soldts AS sql_date) > curdate() - 365
AND soldamount < 14000
AND typ = 'VehicleSale_Retail'


SELECT COUNT(*) -- (12k:1401 14k:1161)
FROM vehiclesales
WHERE CAST(soldts AS sql_date) > curdate() - 365
AND soldamount > 14000
AND typ = 'VehicleSale_Retail'


SELECT pb.priceband, COUNT(*)
FROM vehiclesales s
LEFT JOIN PriceBands pb ON s.SoldAmount BETWEEN pb.PriceFrom AND pb.PriceThru
WHERE CAST(s.soldts AS sql_date) > curdate() - 365
AND s.typ = 'VehicleSale_Retail'
GROUP BY pb.priceband
ORDER BY COUNT(*) desc