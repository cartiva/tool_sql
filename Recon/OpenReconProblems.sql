-- 11887x grid shows X, VehicleReconItems tires are NOT authorized
-- 13028XX
-- 13440xx shows twice ON grid
   SELECT DISTINCT 
  vii.stocknumber,
  CASE 
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.ReceivedTS IS NOT NULL then 'R'
	WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND po.OrderedTS IS NOT NULL THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	ELSE ''
  END AS Tires,
  CASE (
      SELECT status
      FROM VehicleInventoryItemStatuses 
      WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
      AND category = 'MechanicalReconProcess'
      AND ThruTS IS NULL)
    WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
    WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
    WHEN 'MechanicalReconProcess_NotStarted' THEN 
      CASE 
        WHEN (
          SELECT COUNT(VehicleInventoryItemID)
          FROM VehicleInventoryItemStatuses
          WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
          AND category = 'MechanicalReconDispatched'
          AND ThruTS IS NULL) = 0 THEN 'Not Started'
        ELSE
          'Dispatched'
        END
  END AS MechanicalStatus
FROM VehicleInventoryItems vii
INNER JOIN VehicleInventoryItemStatuses viis ON viis.VehicleInventoryItemID = vii.VehicleInventoryItemID 
  AND viis.ThruTS IS NULL  
INNER JOIN VehicleItems vi on vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc on mmc.Make = vi.Make
  AND mmc.Model = vi.Model
  AND mmc.ThruTS IS NULL
INNER JOIN (
  SELECT ra.VehicleInventoryItemID
  FROM ReconAuthorizations ra
  WHERE EXISTS (
    SELECT 1
    FROM AuthorizedReconItems 
    WHERE ReconAuthorizationID = ra.ReconAuthorizationID
    AND status <> 'AuthorizedReconItem_Complete')) r ON r.VehicleInventoryItemID = vii.VehicleInventoryItemID 
LEFT JOIN (
  SELECT * FROM (EXECUTE PROCEDURE GetViiPartsStatus()) wtf ) ps ON vii.VehicleInventoryItemID = ps.VehicleInventoryItemID
LEFT JOIN VehiclesToMove vtm ON vii.VehicleInventoryItemID = vtm.VehicleInventoryItemID 
  AND vtm.ThruTS IS NULL     
LEFT JOIN (
  SELECT vr.VehicleInventoryItemID, vr.VehicleReconItemID 
  FROM VehicleReconItems vr
  WHERE vr.typ = 'MechanicalReconItem_Tires'
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%')
  AND vr.partsinstock = true
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations raix
	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
	AND ThruTS IS NULL)) tvr ON vii.VehicleInventoryItemID = tvr.VehicleInventoryItemID 	
LEFT JOIN PartsOrders po ON tvr.VehicleReconItemID = po.VehicleReconItemID 
WHERE vii.ThruTS IS NULL
AND vii.VehicleInventoryItemID = 'c47fc4fb-eadf-4372-af64-de455e94f9bb'
--AND viis.category = 'MechanicalReconProcess'  AND viis.status <> 'MechanicalReconProcess_NoIncompleteReconItems'
--ORDER BY vii.stocknumber


  SELECT vr.VehicleInventoryItemID, vr.VehicleReconItemID 
  FROM VehicleReconItems vr
  WHERE vr.typ = 'MechanicalReconItem_Tires'
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%')
  AND vr.partsinstock = true
  AND EXISTS (
    SELECT 1
	FROM ReconAuthorizations raix
	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
	AND ThruTS IS NULL)