/*
DELETE VehicleReconItems
DELETE AuthorizedReconItems
DELETE ReconAuthorizations

verify statuses:
  ip closed
  wp OPEN
  ALL recon process s/b NoIncompleteReconItems: OPEN
  	  ALL FromTS should be the same, except RawMaterials_RawMaterials which IS 1sec earlier
	  
DELETE VehicleInspection	 
Notes
Location 

select * FROM VehicleInspections WHERE VehicleInventoryItemID = 'e97d09f7-11e7-43a6-b630-423a0f80c29f'
*/
DECLARE @VehicleInventoryItemID string;
DECLARE @InspTS timestamp;


@VehicleInventoryItemID = (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems 
  WHERE stocknumber = '18932C');
@InspTS = (
  SELECT VehicleInspectionTS 
  FROM VehicleInspections 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID);

BEGIN TRANSACTION;
TRY 

  DELETE 
  FROM AuthorizedReconItems
  WHERE ReconAuthorizationID IN (
    SELECT ReconAuthorizationID
    FROM ReconAuthorizations
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID);
  
  DELETE 
  FROM ReconAuthorizations 
  WHERE VehicleInventoryItemID =  @VehicleInventoryItemID;
  
  DELETE 
  FROM AuthorizedReconItems
  WHERE VehicleReconItemID IN (
    SELECT VehicleReconItemID
    FROM VehicleReconItems
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID);
    
  DELETE
  FROM VehicleReconItems
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  UPDATE VehicleInventoryItemStatuses
  SET ThruTS = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND status = 'RMFlagIP_InspectionPending';
  
  DELETE 
  FROM VehicleInspections
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  DELETE 
  FROM VehicleInventoryItemNotes
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND category = 'VehicleInspection';

  DELETE 
  FROM VehicleItemPhysicalLocations
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND FromTS = @InspTS;
  
  UPDATE VehicleItemPhysicalLocations
  SET ThruTS = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND ThruTS = @InspTS;


COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;   
  

