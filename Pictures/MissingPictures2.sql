alter PROCEDURE GetVehiclePictureCounts (
      MarketID Char(38),
      LocationID CHAR ( 38 ),
      WithLessThan Integer,
      Status CICHAR ( 60 ) OUTPUT,
      VehicleINventoryItemID CHAR ( 38 ) OUTPUT,
      LocationID CHAR ( 38 ) OUTPUT,
      OwnerLocationID CHAR ( 38 ) OUTPUT,
      StockNumber CICHAR ( 20 ) OUTPUT,
      VIN CICHAR ( 24 ) OUTPUT,
      YearModel CICHAR ( 4 ) OUTPUT,
      Make CICHAR ( 60 ) OUTPUT,
      Model CICHAR ( 60 ) OUTPUT,
      BodyStyle CICHAR ( 30 ) OUTPUT,
      VehicleDescription CICHAR ( 400 ) OUTPUT,
      PicCount Integer OUTPUT
   ) 
   
/*
EXECUTE PROCEDURE GetVehiclePictureCountsByLocation('A4847DFC-E00E-42E7-89EE-CD4368445A82','B6183892-C79D-4489-A58C-B526DF948B06', 7);
*/   
--BEGIN 
DECLARE @MarketID string;
DECLARE @LocationID string;
DECLARE @WithLessThan integer;

@MarketID = 'A4847DFC-E00E-42E7-89EE-CD4368445A82';--(SELECT MarketID FROM __input);
@LocationID = '';--(SELECT LocationID FROM __input);
@WithLessThan = 7;--(SELECT WithLessThan FROM __input);

--INSERT INTO __output
SELECT 

CASE
  WHEN sb.VehicleInventoryItemID IS NOT NULL THEN 'Sales Buffer'
  WHEN rmp.VehicleInventoryItemID IS NOT NULL THEN 'Raw Materials'
  WHEN av.VehicleInventoryItemID IS NOT NULL THEN 'Available'
  WHEN pb.VehicleInventoryItemID IS NOT NULL THEN 'Pricing Buffer'
  WHEN p.VehicleInventoryItemID IS NOT NULL THEN 'Pulled'
END AS status,
  vii.VehicleInventoryItemID, vii.LocationID, vii.OwningLocationID, 
  vii.StockNumber, vi.VIN,
  vi.YearModel, vi.Make, vi.Model, vi.BodyStyle,
  Coalesce(Trim(vi.Trim),'') + ' '  + Coalesce(Trim(vi.Engine), '') AS VehicleDescription,	   
  coalesce(t.countofpic, 0)
FROM VehicleInventoryItems vii
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN MakeModelClassifications mmc
          on mmc.Make = vi.Make
          AND mmc.Model = vi.Model
          AND mmc.ThruTS IS NULL
LEFT JOIN VehicleInventoryItemStatuses sb ON vii.VehicleInventoryItemID = sb.VehicleInventoryItemID 
  AND sb.status = 'RMFlagSB_SalesBuffer'
  AND sb.thruts IS NULL 		
LEFT JOIN VehicleInventoryItemStatuses wsb ON vii.VehicleInventoryItemID = wsb.VehicleInventoryItemID 
  AND wsb.status = 'RMFlagWSB_WholesaleBuffer'
  AND wsb.thruts IS NULL  
LEFT JOIN VehicleInventoryItemStatuses it ON vii.VehicleInventoryItemID = it.VehicleInventoryItemID 
  AND it.status = 'RMFlagPIT_PurchaseInTransit'
  AND it.thruts IS NULL     
LEFT JOIN VehicleInventoryItemStatuses tna ON vii.VehicleInventoryItemID = tna.VehicleInventoryItemID 
  AND tna.status = 'RMFlagTNA_TradeNotAvailable'
  AND tna.thruts IS NULL   
LEFT JOIN VehicleInventoryItemStatuses ip ON vii.VehicleInventoryItemID = ip.VehicleInventoryItemID 
  AND ip.status = 'RMFlagIP_InspectionPending'
  AND ip.thruts IS NULL
LEFT JOIN VehicleInventoryItemStatuses wp ON vii.VehicleInventoryItemID = wp.VehicleInventoryItemID 
  AND wp.status = 'RMFlagWP_WalkPending'
  AND wp.thruts IS NULL  
LEFT JOIN VehicleInventoryItemStatuses rmp ON vii.VehicleInventoryItemID = rmp.VehicleInventoryItemID 
  AND rmp.status = 'RMFlagRMP_RawMaterialsPool'
  AND rmp.thruts IS NULL 
LEFT JOIN VehicleInventoryItemStatuses av ON vii.VehicleInventoryItemID = av.VehicleInventoryItemID 
  AND av.status = 'RMFlagAV_Available'
  AND av.thruts IS NULL   
LEFT JOIN VehicleInventoryItemStatuses pb ON vii.VehicleInventoryItemID = pb.VehicleInventoryItemID 
  AND pb.status = 'RMFlagPB_PricingBuffer'
  AND pb.thruts IS NULL  
LEFT JOIN VehicleInventoryItemStatuses p ON vii.VehicleInventoryItemID = p.VehicleInventoryItemID 
  AND p.status = 'RMFlagPulled_Pulled'
  AND p.thruts IS NULL           
LEFT JOIN (
  SELECT count(viip.VehicleInventoryItemID) AS CountOfPic, viip.VehicleInventoryItemID
  FROM viipictures viip
  GROUP BY viip.VehicleInventoryItemID)	t ON vii.VehicleInventoryItemID = t.VehicleInventoryItemID 
LEFT JOIN VehicleSales vs ON vii.VehicleInventoryItemID = vs.VehicleInventoryItemID   	  
WHERE vii.ThruTS IS NULL
AND 
   CASE 
     WHEN @LocationID <> '' THEN vii.OwningLocationID = @LocationID
     ELSE vii.OwningLocationID IN (
       SELECT PartyID2
       FROM PartyRelationships
       WHERE PartyID1 = @MarketID
       AND typ = 'PartyRelationship_MarketLocations'
 AND ThruTS IS NULL)
   END
AND coalesce(t.countofpic, 0) < @WithLessThan
AND wsb.VehicleInventoryItemID IS NULL 
AND tna.VehicleInventoryItemID IS NULL 
AND ip.VehicleInventoryItemID IS NULL
AND wp.VehicleInventoryItemID IS NULL 
AND vs.VehicleInventoryItemID IS NULL;
--END; 
