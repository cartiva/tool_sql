SELECT *
FROM (
  SELECT cast(a.fromTS AS sql_date) AS dateAcquired, a.stocknumber, vin, yearmodel, make, model, coalesce(keystatus, 'Unknown') AS keys,
    cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
    Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
  LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
  WHERE a.thruts IS NULL 
    AND b.make IN ('Buick','Cadillac','Chevrolet','GMC','Hummer','Oldsmobile','Pontiac','Saturn')) c
WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer', 'In Transit')
  AND dateAcquired BETWEEN '08/12/2014' AND curdate()

  
/*
6/5/14
Jon, would you be able to send me a current list of all 2009 thru 2014 Traverse, Acadia, Enclave and Outlook that are in used car inventory for either store please?

I need to organize the latest recall.  

Thanks!

Bev
*/


SELECT DISTINCT model
FROM VehicleItems a
INNER JOIN VehicleInventoryItems b on a.VehicleItemID = b.VehicleItemID
  AND b.thruts IS NOT NULL 
WHERE model LIKE '%ruze%'  
WHERE model LIKE '%traverse%'
  OR model like '%acadia%'
  OR model LIKE '%enclave%'
  OR model LIKE '%outlook%'
  

SELECT *
FROM (
SELECT a.stocknumber, vin, yearmodel, model, coalesce(keystatus, 'Unknown') AS keys,
  cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
  Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
FROM VehicleInventoryItems a
INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
WHERE a.thruts IS NULL 
  AND b.model IN ('traverse','acadia','enclave','outlook')
  AND CAST(b.yearmodel AS sql_integer) BETWEEN 2009 AND 2014) c
WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer')  

/*
6/19
Could you please print me one more list showing all Traverse, Outlook, 
  Enclave and Acadia that are in inventory � all locations?
Could you please print me a list of Cobalt, Ion, HHR, G5  for all locations?
Could you please print me a list of all Cruze for all locations?
Bev 
*/

SELECT *
FROM (
SELECT a.stocknumber, vin, yearmodel, model, coalesce(keystatus, 'Unknown') AS keys,
  cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
  Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
FROM VehicleInventoryItems a
INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
WHERE a.thruts IS NULL 
  AND b.model IN ('traverse','acadia','enclave','outlook')
  AND CAST(b.yearmodel AS sql_integer) BETWEEN 2009 AND 2014) c
WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer') 

SELECT *
FROM (
SELECT a.stocknumber, vin, yearmodel, model, coalesce(keystatus, 'Unknown') AS keys,
  cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
  Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
FROM VehicleInventoryItems a
INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
WHERE a.thruts IS NULL 
  AND b.model IN ('cobalt','ion','hhr','g5')
  AND CAST(b.yearmodel AS sql_integer) BETWEEN 2009 AND 2014) c
WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer') 

SELECT *
FROM (
  SELECT a.stocknumber, vin, yearmodel, model, coalesce(keystatus, 'Unknown') AS keys,
    cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
    Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
  LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
  WHERE a.thruts IS NULL 
    AND b.model IN ('cruze')
    AND CAST(b.yearmodel AS sql_integer) BETWEEN 2009 AND 2014) c
  WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer') 

/*
6/23/14  Recall 14299
2005 - 2009 Buick LaCrosse
2006-2011 Buck Lucerne
2000 - 2005 Cadillac DeVille
2006 - 2011 Cadillac DTS
2006 - 2007 Chevrolet Monte Carlo
2006 - 2014 Chevrolet Impala Limited
*/


SELECT model, COUNT(*)
FROM VehicleItems a
INNER JOIN VehicleInventoryItems b on a.VehicleItemID = b.VehicleItemID
  AND b.thruts IS NOT NULL 
WHERE model LIKE '%acro%'  
  OR model LIKE '%uce%'
  OR model like '%devil%'
  OR model LIKE '%dts%'
  OR model LIKE '%carlo%'
  OR model LIKE '%impala%'
GROUP BY model  
  
SELECT *
FROM (
  SELECT a.stocknumber, vin, yearmodel, model, coalesce(keystatus, 'Unknown') AS keys,
    cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
    Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
  LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
  WHERE a.thruts IS NULL 
    AND b.model IN ('Impala Limited')
    AND CAST(b.yearmodel AS sql_integer) BETWEEN 2006 AND 2014) c
  WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer')   
  
-- ALL impalas  206 - 2014
SELECT *
FROM (
  SELECT a.stocknumber, vin, yearmodel, model, coalesce(keystatus, 'Unknown') AS keys,
    cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
    Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
  LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
  WHERE a.thruts IS NULL 
    AND b.model like '%impala%'
    AND CAST(b.yearmodel AS sql_integer) BETWEEN 2006 AND 2014) c
  WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer')   

/* 
May I please have a list of inventory units at all locations :         

2014 and 2015 Chev Silverado, Tahoe, Suburban

2014 and 2015 GMC Sierra, Yukon and Yukon XL
*/

SELECT model, COUNT(*)
FROM VehicleItems a
INNER JOIN VehicleInventoryItems b on a.VehicleItemID = b.VehicleItemID
  AND b.thruts IS NOT NULL 
WHERE model LIKE '%sier%'  
  OR model LIKE '%yuk%'
GROUP BY model  


SELECT *
FROM (
  SELECT a.stocknumber, vin, yearmodel, model, coalesce(keystatus, 'Unknown') AS keys,
    cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
    Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
  LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
  WHERE a.thruts IS NULL 
  AND (model LIKE '%sier%'  
    OR model LIKE '%yuk%')
    AND CAST(b.yearmodel AS sql_integer) BETWEEN 2014 AND 2015) c
  WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer')   
  
/*
10/9
Jon, could you send me a list of GM inventory units received after 10/1/2014 please?
*/  

SELECT *
FROM (
  SELECT cast(a.fromTS AS sql_date) AS dateAcquired, a.stocknumber, vin, yearmodel, make, model, coalesce(keystatus, 'Unknown') AS keys,
    cast(Utilities.CurrentViiLocation(a.VehicleInventoryItemID) AS sql_char) AS location,
    Utilities.CurrentViiSalesStatus(a.VehicleInventoryItemID) AS [Sales Status]
  FROM VehicleInventoryItems a
  INNER JOIN VehicleItems b ON a.VehicleItemID = b.VehicleItemID
  LEFT JOIN keyperkeystatus c ON a.stocknumber = c.stocknumber
  WHERE a.thruts IS NULL 
    AND b.make IN ('Buick','Cadillac','Chevrolet','GMC','Hummer','Oldsmobile','Pontiac','Saturn')) c
WHERE [sales status] NOT IN ('At Auction', 'Trade Buffer', 'In Transit')
  AND dateAcquired > '10/01/2014'