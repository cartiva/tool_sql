-- who IS entering notes
SELECT party, COUNT(*)
FROM (
  SELECT (SELECT fullname FROM people WHERE partyid = v.userid) AS party
  FROM VehicleInventoryItemNotes v
  WHERE userid IS NOT NULL
  AND timestampdiff(sql_tsi_day, notests, now()) < 100) wtf
GROUP BY party 
ORDER BY COUNT(*) DESC 

-- what kind of notes are being entered
SELECT category, subcategory, COUNT(*)
FROM VehicleInventoryItemNotes
GROUP BY category, subcategory
ORDER BY COUNT(*) desc

-- notes for an individual
select category, subcategory, notes, notests
FROM VehicleInventoryItemNotes
WHERE userid = (SELECT partyid FROM people WHERE fullname = 'Garrett Evenson')
AND timestampdiff(sql_tsi_day, notests, now()) < 100
ORDER BY notests desc


