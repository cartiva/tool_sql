/*
need Status TS, VehicleEvaluationID, UserID, VehicleInventoryItemID

**********
must include a note
TBReasons:
  Aftermarket
  New Car Order
  Recon
  Other
**********
*/

DECLARE @VehicleInventoryItemID string;
-- DECLARE @FromTS Timestamp;
DECLARE @TableKey string;
DECLARE @NowTS timestamp;
DECLARE @UserID string;
DECLARE @Note string;
DECLARE @ExpectedDate date;
DECLARE @TBReason string;

@VehicleInventoryItemID = 'c9c620e4-7f43-49a7-bf8c-6bec4b137f84';
@Note = 'per Bev, trade not available until recon on purchased vehicle finished';
@ExpectedDate = '04/30/2016';
@TBReason = 'Other';
@NowTS = (SELECT now() FROM system.iota);
--@FromTS = (
--  SELECT FromTS
--  FROM VehicleInventoryItemStatuses 
--  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
--  AND Category = 'RawMaterials');
@TableKey = (
  SELECT VehicleEvaluationID
  FROM VehicleEvaluations
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID);
@UserID = (
  SELECT PartyID
  FROM users
  WHERE username = 'jon');

BEGIN TRANSACTION;
TRY  
  INSERT INTO VehicleInventoryItemStatuses (VehicleInventoryItemID, status, category, fromts, basistable, tablekey, userid)
  VALUES(@VehicleInventoryItemID, 'RMFlagTNA_TradeNotAvailable', 'RMFlagTNA', @NowTS, 'VehicleEvaluations',@TableKey, @UserID); 

  IF (SELECT COUNT(*) FROM TNAVehicles WHERE VehicleInventoryItemID = @VehicleInventoryItemID) = 0 THEN  
    INSERT INTO TNAVehicles (VehicleInventoryItemID, Notes, ExpectedDate, TradeBufferReason)
      VALUES (@VehicleInventoryItemID, @Note, cast(@ExpectedDate AS sql_date), @TBReason);
  ELSE
    UPDATE TNAVehicles
    SET ReceivedByID = NULL,
        Notes = '',
        ExpectedDate = @ExpectedDate,
        TradeBufferReason = @TBReason
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  END IF;
  
  EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubcategory(@VehicleInventoryItemID, @UserID,
    'VehicleInventoryItems', @VehicleInventoryItemID, 'TNA/PIT', 
    'TNA/PIT_Status', @NowTS, @Note);  
        
  DELETE 
  FROM VehicleItemPhysicalLocations
  WHERE VehicleItemID = (
    SELECT VehicleItemId
    FROM VehicleInventoryItems
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID);
    
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY

