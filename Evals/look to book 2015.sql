SELECT CAST(a.vehicleEvaluationTS AS sql_date), c.description, d.fullname, e.stocknumber
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Rydells'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '01/01/2015' AND '03/31/2015'


SELECT d.fullname, COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Rydells'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '09/01/2015' AND '09/30/2015'
GROUP BY d.fullname  

UNION 

SELECT 'Total', COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Rydells'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '09/01/2015' AND '09/30/2015'

  
-- 11-24-15  tom aubol asking for honda  

SELECT 'Sep 2015', d.fullname, COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Honda Cartiva'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '09/01/2015' AND '09/30/2015'
GROUP BY d.fullname  
UNION 
SELECT 'Sep 2015', 'Total', COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Honda Cartiva'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '09/01/2015' AND '09/30/2015'
  
  
SELECT 'Oct 2015', d.fullname, COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Honda Cartiva'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '10/01/2015' AND '10/31/2015'
GROUP BY d.fullname  
UNION 
SELECT 'Oct 2015', 'Total', COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Honda Cartiva'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '10/01/2015' AND '10/31/2015'  
  
SELECT 'Nov 2015', d.fullname, COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Honda Cartiva'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '11/01/2015' AND '11/30/2015'
GROUP BY d.fullname  
UNION 
SELECT 'Nov 2015', 'Total', COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Honda Cartiva'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) BETWEEN '11/01/2015' AND '11/30/2015'    
  
-- 10/26/15
-- kevin foster looking for last 3 mos

SELECT aa.yearmonth, 'Total', COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS percentage
FROM VehicleEvaluations a
INNER JOIN dds.day aa on cast(a.VehicleEvaluationTS AS sql_date) = aa.thedate
  AND aa.yearmonth BETWEEN 201606 and 201610
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Rydells'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')  
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
--LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
GROUP BY aa.yearmonth

  
  
SELECT DISTINCT typ FROM vehicleevaluations  