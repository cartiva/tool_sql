DECLARE @id string;
@id = (SELECT VehicleInventoryItemID FROM __input);

SELECT (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RawMaterials_RawMaterials'
      AND thruts IS NULL)  AS RawMaterials,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RawMaterials_BookingPending'
      AND thruts IS NULL)  AS BookingPending, 
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagTNA_TradeNotAvailable'
      AND thruts IS NULL) AS TradeNotAvailable,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagPIT_PurchaseInTransit'
      AND thruts IS NULL) AS PurchaseInTransit,   
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagIP_InspectionPending'
      AND thruts IS NULL) AS InspectionPending,    
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagWP_WalkPending'
      AND thruts IS NULL) AS WalkPending,   
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagRMP_RawMaterialsPool'
      AND thruts IS NULL) AS RawMaterialsPool, 
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagRB_ReconBuffer'
      AND thruts IS NULL) AS ReconBuffer,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagPulled_Pulled'
      AND thruts IS NULL) AS Pulled,    
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagFLR_FrontLineReady'
      AND thruts IS NULL) AS FrontLineReady,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagPB_PricingBuffer'
      AND thruts IS NULL) AS PricingBuffer,                                 
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagAV_Available'
      AND thruts IS NULL) AS Available,
      
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagSB_SalesBuffer'
      AND thruts IS NULL) AS SalesBuffer,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagWSB_WholesaleBuffer'
      AND thruts IS NULL) AS WholesaleBuffer,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagAtAuction_AtAuction'
      AND thruts IS NULL) AS AtAuction,
  (
    SELECT 'X' 
    FROM VehicleSales 
    WHERE VehicleInventoryItemID = @id
      AND status = 'VehicleSale_Sold') AS Sold,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RawMaterials_Sold'
      AND thruts IS NULL) AS SoldNotDelivered,
  ( -- ??
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RawMaterials_Sold'
      AND EXISTS (
        SELECT 1
        FROM VehicleInventoryItems 
        WHERE VehicleInventoryItemID = @id
          AND thruTS IS NOT NULL)) AS Delivered,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemTransitions 
    WHERE VehicleInventoryItemID = @id
      AND thruts IS NULL) AS InTransition,
  (
    SELECT 'X' 
    FROM VehicleInventoryConsignments 
    WHERE VehicleInventoryItemID = @id
      AND ConsignmentReceivedTS IS NOT NULL 
      AND ConsignmentReturnedTS IS NULL) AS Consigned,
  (
    SELECT 'X' 
    FROM VehicleWalks 
    WHERE VehicleInventoryItemID = @id) AS HasBeenWalked,
  (
    SELECT 'X' 
    FROM VehiclesToMove 
    WHERE VehicleInventoryItemID = @id
      AND thruts IS NULL) AS MoveRequestPending,
  (
    SELECT 'X' 
    FROM ReconAuthorizations x
    WHERE VehicleInventoryItemID = @id
      AND thruts IS NULL
      AND EXISTS (
        SELECT 1 
        FROM AuthorizedReconItems 
        WHERE ReconAuthorizationID = x.ReconAuthorizationID
          AND status <> 'AuthorizedReconItem_Complete')) AS HasOpenRecon,
  (
    SELECT 'X' 
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @id
      AND status = 'RMFlagWTF_WTF'
      AND thruts IS NULL) AS WTF,
  (
    SELECT distinct 'X' 
    FROM VehiclePricings s
    WHERE s.VehicleInventoryItemID = @id
      AND EXISTS (
        SELECT 1
        FROM VehiclePricingDetails
        WHERE VehiclePricingID = s.VehiclePricingID
        AND typ = 'VehiclePricingDetail_BestPrice')) AS HasPrice,
  (
    SELECT COUNT(*)
    FROM viiPictures
    WHERE VehicleInventoryItemID = @id) AS NumberOfVehiclePictures,
  true AS HasDifferentiator, 
  (
    SELECT ToLocationPartyID
    FROM VehicleInventoryItemTransitions
      WHERE VehicleInventoryItemID = @id
      AND ThruTS IS NULL) AS TransitionToLocationID,
  (
    SELECT thruTS
    FROM VehicleInventoryItems
    WHERE VehicleInventoryItemID = @id) AS DateDelivered                

FROM system.iota;