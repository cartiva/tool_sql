
/*
        NowTS := DpsDM.GetTimestamp();
        UsedCarsDM.SetVehicleInventoryItemsStatusFlag(VehicleInventoryItemID, UserSession.UserID,
          crsRMFlagAV, srsRMFlagAV_Available, NowTS, 'VehicleInventoryItems',
          VehicleInventoryItemID);
        UsedCarsDM.SetVehicleInventoryItemMileage(VehicleInventoryItemID, 'VehicleInventoryItems', MilesTextBox.text, NowTS);
        UsedCarsDM.UpdateVehicleInventoryItemPriority(VehicleInventoryItemID);
        DpsDM.UpdateVehicleInventoryItemPhysicalLocation(VehicleInventoryItemID, 'Inventory',
          PhysVehLocAndNoteControl1.VehicleLocationValue, PhysVehLocAndNoteControl1.VehicleLocationDetail,
          UserSession.UserID, NowTS);
        UsedCarsDM.SetVehicleInventoryItemNotesByCategorySubCategory(VehicleInventoryItemID, UserSession.UserID,
          'VehicleInventoryItems',VehicleInventoryItemID, 'Inventory', 'Inventory_PricingBuffer',
          NowTS, PhysVehLocAndNoteControl1.Notes);
        UsedCarsDM.UpdateVehicleInventoryItemStatuses(VehicleInventoryItemID, UserSession.UserID, NowTS);
*/        
DECLARE @VehicleInventoryItemID string; 
DECLARE @UserID string;
DECLARE @Miles integer;
DECLARE @NowTS timestamp;
DECLARE @Note memo;
DECLARE @PhysLocationID string;
/*
DON'T FORGET TO UPDATE MILES
*/
@VehicleInventoryItemID = '1792900a-d27e-4d60-b78c-3fe110d7cd94';
@Miles = 114304;
@NowTS = '06/09/2011 16:16:16';
@Note = 'manually updated by jon, don''t know who actually did it or when';

@UserID = (SELECT partyid FROM users WHERE username = 'jon');
// Rydells: Used Car Lot
@PhysLocationID = 'fce0b0e1-0a35-9b4b-a87c-d3bd2de45790';

BEGIN TRANSACTION;
TRY
  TRY
    EXECUTE PROCEDURE SetVehicleInventoryItemsStatusFlag(
      @VehicleInventoryItemID,
      @UserID,
      'RMFlagAV',
      'RMFlagAV_Available',
      @NowTS,
      'VehicleInventoryItems',
      @VehicleInventoryItemID);
  CATCH ALL
    RAISE WhackException(100, 'SetVehicleInventoryItemsStatusFlag: ' + __errText);
  END TRY;
  TRY
    EXECUTE PROCEDURE SetVehicleInventoryItemMileage(
      @VehicleInventoryItemID,
      'VehicleInventoryItems',
      @Miles,
      @NowTS);
  CATCH ALL
    RAISE WhackException(101, 'SetVehicleInventoryItemMileage: ' + __errText);
  END TRY;
  TRY
    EXECUTE PROCEDURE UpdateVehicleInventoryItemPhysicalLocation(
      @VehicleInventoryItemID,
      'Inventory',
      @PhysLocationID,
      null,
      @UserID,
      @NowTS);
  CATCH ALL
    RAISE WhackException(102, 'UpdateVehicleInventoryItemPhysicalLocation: ' + __errText);
  END TRY;  
  TRY
    EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubCategory(
      @VehicleInventoryItemID,
      @UserID,
      'VehicleInventoryItems',
      @VehicleInventoryItemID,
      'RMFlagPB',
      'PutOnLot',
      @NowTS,
      @Note);
  CATCH ALL
    RAISE WhackException(103, 'SetVehicleInventoryItemNotesByCategorySubCategory: ' + __errText);
  END TRY;  
  TRY
    EXECUTE PROCEDURE UpdateVehicleInventoryItemStatuses(
      @VehicleInventoryItemID,
      @UserID,
      @NowTS);
  CATCH ALL
    RAISE WhackException(104, 'UpdateVehicleInventoryItemStatuses: ' + __errText);
  END TRY;   
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY           