CREATE TABLE zTest (
  Field1 CIChar(1),
  Seq integer);
  
INSERT INTO zTest VALUES('a',1);  
INSERT INTO zTest VALUES('b',6);
INSERT INTO zTest VALUES('a',3); 
INSERT INTO zTest VALUES('a',4); 
INSERT INTO zTest VALUES('b',5); 
INSERT INTO zTest VALUES('a',2);
INSERT INTO zTest VALUES('b',7); 

-- this query gives the correct result:

SELECT TOP 1 Seq
FROM zTest
WHERE Field1 = 'b'
ORDER BY Seq;

-- this query does not give the correct result:

DECLARE @Temp integer;
@Temp = (
  SELECT TOP 1 Seq
  FROM zTest
  WHERE Field1 = 'b'
  ORDER BY Seq);
SELECT @Temp FROM system.iota; 

-- this query does give the correct result:

DECLARE @Temp integer;
@Temp = (
  SELECT Seq
  FROM (
    SELECT TOP 1 Seq
    FROM zTest
    WHERE Field1 = 'b'
    ORDER BY Seq) wtf);
SELECT @Temp FROM system.iota;  

