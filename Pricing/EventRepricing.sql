DECLARE @VehiclePricingID string;
DECLARE @VehicleInventoryItemID string;
DECLARE @BasisTable string;
DECLARE @TableKey string;
DECLARE @PricedBy string;
DECLARE @Price Integer;
DECLARE @Invoice Integer;
DECLARE @PricingStrategy string;
DECLARE @Notes string;
DECLARE @NowTS TIMESTAMP;
DECLARE @VehicleItemID string;

@VehicleInventoryItemID = (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems 
  WHERE stocknumber = '10062a');
@Price = 2990;  

@VehiclePricingID = (SELECT newidstring(d) FROM system.iota);
@BasisTable = 'VehiclePricings';
@TableKey = @VehiclePricingID;
@PricedBy = (SELECT partyid FROM users WHERE username = 'dwilkie');
@Invoice = (
  SELECT Amount 
  FROM VehiclePricingDetails
  WHERE VehiclePricingID = (
    SELECT top 1 VehiclePricingID
    FROM VehiclePricings
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID
    ORDER BY VehiclePricingTS DESC)
  AND typ = 'VehiclePricingDetail_Invoice');  
@PricingStrategy = (  
  SELECT top 1 VehiclePricingStrategy 
  FROM vehiclepricings
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  ORDER BY vehiclepricingts DESC);
@Notes = 'Post summer event repricing';
@NowTS = now(); 
@VehicleItemID = (SELECT VehicleItemID FROM VehicleInventoryItems WHERE VehicleInventoryItemID = @VehicleInventoryItemID); 

BEGIN TRANSACTION;
TRY 

  INSERT INTO VehiclePricings (VehiclePricingID, VehicleInventoryItemID, PricedBy,
    VehiclePricingTS, BasisTable, TableKey, VehicleItemID, VehiclePricingStrategy)
  VALUES (@VehiclePricingID, @VehicleInventoryItemID, @PricedBy,  @NowTS,
    @BasisTable, @TableKey, @VehicleItemID, @PricingStrategy);
  --2.
  INSERT INTO VehiclePricingDetails
  VALUES(@VehiclePricingID, 'VehiclePricingDetail_BestPrice', @Price);
  --3.
  INSERT INTO VehiclePricingDetails
  VALUES(@VehiclePricingID, 'VehiclePricingDetail_Invoice', @Invoice);

  EXECUTE PROCEDURE SetVehicleInventoryItemNotesByCategorySubCategory(@VehicleInventoryItemID,
    @BasisTable, @TableKey, 'VehiclePricing', 'VehiclePricing_Pricing',
    @NowTS, @Notes);
	
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;  