/*
stocknumber
partsstatus
upholstery
tires
WORK link
priority
mech status
body status
body $
app status
year
make
model
color
move list

FUCK, grid doesn't even display sales status
*/

--SELECT stocknumber, COUNT(*)
--FROM (
--
SELECT viixx.stocknumber, ps.partsstatus, ss.SalesStatus, rs.MechStatus, rs.BodyStatus, rs.AppStatus,
  CASE 
    WHEN vtm.VehicleInventoryItemID IS NOT NULL THEN 'X'
	ELSE ''
  END AS OnMoveList,
  CASE WHEN uph.VehicleInventoryItemID IS NOT NULL THEN 'X' END AS Upholstry     
FROM VehicleInventoryItems viixx -- OPEN only 
INNER JOIN ( -- only VehicleInventoryItems with OPEN recon 
  SELECT viix.stocknumber, mrs.MechStatus, brs.BodyStatus, ars.AppStatus, viix.VehicleInventoryItemID 
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS MechStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'MechanicalReconProcess'
    AND status <> 'MechanicalReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) mrs ON viix.VehicleInventoryItemID = mrs.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS BodyStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'BodyReconProcess'
    AND status <> 'BodyReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) brs ON viix.VehicleInventoryItemID = brs.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS AppStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'AppearanceReconProcess'
    AND status <> 'AppearanceReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) ars ON viix.VehicleInventoryItemID = ars.VehicleInventoryItemID     
  WHERE viix.ThruTS IS NULL  
  AND (mrs.MechStatus IS NOT NULL OR brs.BodyStatus IS NOT NULL OR ars.AppStatus IS NOT NULL)) rs ON viixx.VehicleInventoryItemID = rs.VehicleInventoryItemID
-- 220 ms  
--LEFT JOIN (
--  SELECT * FROM (EXECUTE PROCEDURE GetViiPartsStatus()) wtf ) ps ON viixx.VehicleInventoryItemID = ps.VehicleInventoryItemID   
LEFT JOIN (
  SELECT rax.VehicleInventoryItemID, 
    CASE
      WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = 0 THEN 'No Parts Req''d'
      ELSE
        CASE 
          WHEN SUM(CASE WHEN vrix.PartsInStock = True THEN 1 ELSE 0 END) = SUM(CASE WHEN vrix.PartsInStock = True AND po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) THEN 'Received'
          ELSE
            CASE 
              WHEN SUM(CASE WHEN vrix.PartsInStock = True AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ordered'
              ELSE 'Ordered'
            END 
       END 
    END AS PartsStatus
  FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
    AND arix.Status <> 'AuthorizedReconItem_Complete' 
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
    AND vrix.typ LIKE 'M%'
  LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
  WHERE rax.ThruTS IS NULL 
  GROUP BY rax.VehicleInventoryItemID) ps ON viixx.VehicleInventoryItemID = ps.VehicleInventoryItemID 
LEFT JOIN VehiclesToMove vtm ON viixx.VehicleInventoryItemID = vtm.VehicleInventoryItemID 
  AND vtm.ThruTS IS NULL 
LEFT JOIN (
  SELECT vrix.VehicleInventoryItemID
  FROM VehicleReconItems vrix
  INNER JOIN AuthorizedReconItems arix on vrix.VehicleReconItemID = arix.VehicleReconItemID 
    AND arix.status <> 'AuthorizedReconItem_Complete'
  INNER JOIN ReconAuthorizations rax ON arix.ReconAuthorizationID = rax.ReconAuthorizationID      
  WHERE vrix.typ = 'AppearanceReconItem_Upholstry')uph ON viixx.VehicleInventoryItemID = uph.VehicleInventoryItemID        
LEFT JOIN (
  SELECT viix.VehicleInventoryItemID,
    CASE 
      WHEN dcu.VehicleInventoryItemID  IS NOT NULL THEN 'DCU'
      ELSE
      CASE 
        WHEN sb.VehicleInventoryItemID IS NOT NULL THEN 'SB'
        ELSE 
        CASE 
          WHEN wsb.VehicleInventoryItemID IS NOT NULL THEN 'WSB'
          ELSE
          CASE
            WHEN rm.VehicleInventoryItemID IS NOT NULL THEN 'RM'
            ELSE
            CASE
              WHEN av.VehicleInventoryItemID IS NOT NULL THEN 'AV'
              ELSE 
              CASE
                WHEN pb.VehicleInventoryItemID IS NOT NULL THEN 'PB'
                ELSE
                CASE
                  WHEN pul.VehicleInventoryItemID IS NOT NULL THEN 'PUL'
                END 
              END 
            END 
          END 
        END
      END 
    END AS SalesStatus
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses 
    WHERE status = 'RawMaterials_Sold'
    AND ThruTS IS NULL) AS dcu ON viix.VehicleInventoryItemID = dcu.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses 
    WHERE status = 'RMFlagSB_SalesBuffer'
    AND ThruTS IS NULL) AS sb ON viix.VehicleInventoryItemID = sb.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses 
    WHERE status = 'RMFlagWSB_WholesaleBuffer'
    AND ThruTS IS NULL) AS wsb ON viix.VehicleInventoryItemID = wsb.VehicleInventoryItemID  
  LEFT JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses 
    WHERE status = 'RMFlagRMP_RawMaterialsPool'
    AND ThruTS IS NULL) AS rm ON viix.VehicleInventoryItemID = rm.VehicleInventoryItemID    
  LEFT JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses 
    WHERE status = 'RMFlagAV_Available'
    AND ThruTS IS NULL) AS av ON viix.VehicleInventoryItemID = av.VehicleInventoryItemID  
  LEFT JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses 
    WHERE status = 'RMFlagPB_PricingBuffer'
    AND ThruTS IS NULL) AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID  
  LEFT JOIN (
    SELECT VehicleInventoryItemID
    FROM VehicleInventoryItemStatuses 
    WHERE status = 'RMFlagPulled_Pulled'
    AND ThruTS IS NULL) AS pul ON viix.VehicleInventoryItemID = pul.VehicleInventoryItemID          
  WHERE viix.ThruTS IS NULL) ss ON viixx.VehicleInventoryItemID = ss.VehicleInventoryItemID 
WHERE viixx.ThruTS IS NULL 
/*
) wtf
GROUP BY stocknumber
HAVING COUNT(*) > 1 
*/    