SELECT lastupdate FROM TradeEvaluations

DROP TABLE zzJonTestTable

CREATE TABLE zzJonTestTable(testTS timestamp, testChar CHAR(25), testTS1 timestamp);

INSERT INTO zzJonTestTable(testTS)
SELECT VehicleInspectionTS FROM VehicleInspections;

SELECT * FROM zzJonTestTable;



UPDATE zzJonTestTable SET testChar = Cast(testTS as SQL_CHAR);

UPDATE zzJonTestTable SET testTS1 = cast(testChar as SQL_Timestamp);

UPDATE zzJonTestTable SET testTS1 = cast('2007-04-20 15:56:23.781' as SQL_Timestamp);

SELECT * FROM zzJonTestTable;

UPDATE zzJonTestTable SET TestTS1 =  '2010-02-01 02:00:00.000'

UPDATE zzJonTestTable SET testTS1 = NULL;

UPDATE zzJonTestTable SET testts1 =
convert(convert(SELECT testts FROM zzJonTestTable),SQL_CHAR),SQL_Timestamp)



update zzJonTestTable set testts1 = convert('04/20/2007 03:56:23', SQL_Timestamp)

update zzJonTestTable set testts1 = null


/*
account for hour(midnight) = 0
WHEN doing timestampdiff(sql_tsi_hour ...)

SELECT dept,
  SUM(iif(hour(w.Closes)= 0, 24, hour(w.Closes)) - hour(w.Opens))
FROM workinghours w
GROUP BY dept 
*/

/* accurate timestampdiff(sql_tsi_day ...)
--ok, here's the problem, IF difference IS less than 24 hours,  timestampdiff(sql_tsi_day ...) returns 0
--SELECT timestampdiff(sql_tsi_day, cast('07/06/2011 08:06:32' AS sql_timestamp), cast('07/07/2011 07:37:53' AS sql_timestamp)) from system.iota

-- SELECT dayofyear(@ts2) - dayofyear(@ts1) FROM system.iota
-- fuck, that's wrong too, day of year won't WORK across years

-- but this works:  CASE timestampdiff(sql_tsi_day, CAST(@ts1 AS sql_date), CAST(@ts2 AS sql_date))
*/

