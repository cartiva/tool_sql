
SELECT stocknumber,
CASE 
  WHEN wip + buffer = 1 THEN 'Buffer'
  WHEN wip + buffer = 2 THEN 'WIP'
  ELSE 'Raw'
END AS status
FROM (        
  SELECT b.stocknumber, b.currentpriority,
    CASE
      WHEN EXISTS (
        SELECT 1
        FROM VehicleInventoryItemStatuses 
        WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
        AND status = 'BodyReconProcess_InProcess'
          AND ThruTs IS NULL) THEN 1 ELSE 0 
    END AS WIP,
    CASE
      WHEN EXISTS (
        SELECT 1
        FROM VehicleInventoryItemStatuses 
        WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
        AND status = 'BodyReconDispatched_Dispatched'
          AND ThruTs IS NULL) THEN 1 ELSE 0 
    END AS Buffer
  FROM VehicleInventoryItemStatuses a
  LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  WHERE a.category = 'BodyReconProcess'   
    AND a.status <> 'BodyReconProcess_NoIncompleteReconItems'
    AND a.thruts IS NULL) X
   
