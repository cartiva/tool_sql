SELECT a.stocknumber, 
  timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) AS "Days Avail", 
  timestampdiff(sql_tsi_day, cast(c.VehiclePricingTS AS sql_date), curdate()) AS "Days Since Priced",
  d.amount AS Price, cast(round(e.imcost, 0) AS sql_integer) AS Cost,
  d.amount - cast(round(e.imcost, 0) AS sql_integer) AS "Potential Gross"
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
  AND b.category = 'RMFlagAV'
  AND b.thruts IS NULL 
LEFT JOIN VehiclePricings c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
  AND c.VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID = c.VehicleInventoryItemID)
LEFT JOIN VehiclePricingDetails d ON c.VehiclePricingID = d.VehiclePricingID 
  AND d.typ = 'VehiclePricingDetail_BestPrice'
LEFT JOIN dds.stgArkonaINPMAST e ON a.stocknumber = e.imstk#  
ORDER BY stocknumber

--2/6
FROM wilkie
would like to see the potential lot gross for vehicles 0-7,8-14,15-21,22-28,
  29-35,36-42,43-49,50-56,57+(days on lot or available) to match our 
  weekly aging profile graph
totals & avg for entire lot AND each age GROUP


SELECT 0 AS FR, 7 as THRU FROM system.iota
union
SELECT n - 6, n
FROM dds.tally
WHERE mod(n, 7) = 0
  AND n > 8
  AND n < 57
UNION 
SELECT 57, 1000 FROM system.iota
  
SELECT Store, FR, THRU, COUNT(*) AS [How Many], SUM([Potential Gross]) AS [Pot Gross], 
  round(SUM([Potential Gross])/COUNT(*), 0) AS [Avg Pot Gross]
FROM (
  SELECT 0 AS FR, 7 as THRU FROM system.iota
  union
  SELECT n - 6, n
  FROM dds.tally
  WHERE mod(n, 7) = 0
    AND n > 8
    AND n < 57
  UNION 
  SELECT 57, 1000 FROM system.iota) a
LEFT JOIN (
  SELECT 
    CASE 
      WHEN LEFT(stocknumber, 1) = 'H' THEN 'RY2'
      ELSE 'RY1'
    END AS Store,
    a.stocknumber, 
    timestampdiff(sql_tsi_day, cast(b.fromts AS sql_date), curdate()) AS "Days Avail", 
    timestampdiff(sql_tsi_day, cast(c.VehiclePricingTS AS sql_date), curdate()) AS "Days Since Priced",
    d.amount AS Price, cast(round(e.imcost, 0) AS sql_integer) AS Cost,
    d.amount - cast(round(e.imcost, 0) AS sql_integer) AS "Potential Gross"
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID
    AND b.category = 'RMFlagAV'
    AND b.thruts IS NULL 
  LEFT JOIN VehiclePricings c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID 
    AND c.VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings
      WHERE VehicleInventoryItemID = c.VehicleInventoryItemID)
  LEFT JOIN VehiclePricingDetails d ON c.VehiclePricingID = d.VehiclePricingID 
    AND d.typ = 'VehiclePricingDetail_BestPrice'
  LEFT JOIN dds.stgArkonaINPMAST e ON a.stocknumber = e.imstk#) b ON b.[Days Avail] BETWEEN  a.FR AND a.THRU  
--WHERE store = 'RY1'  
GROUP BY Store, FR, THRU
