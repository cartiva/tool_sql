-- ExtractWIP3.sql innermost query for generating #ReconWIP
-- GROUP the the appearance categories together
SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, 
  CASE 
    WHEN category = 'MechanicalReconItem' THEN 'Mechanical'
    WHEN category = 'BodyReconItem' THEN 'Body'
    WHEN category = 'PartyCollection' OR category = 'AppearanceReconItem' THEN 'Appearance'
  END AS Category        
FROM VehicleReconItems ri
INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
LEFT JOIN TypCategories t ON ri.typ = t.typ
WHERE ar.status  = 'AuthorizedReconItem_Complete'
AND ar.startTS IS NOT NULL 
GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category

-- regenerate #ReconWip using this INNER re-categorization
-- seems to WORK for COUNT (1 OR 0) AND total hours
SELECT * FROM #reconwip 
WHERE VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #mult)
/*
select VehicleInventoryItemID INTO #mult FROM #reconwip WHERE appcount > 1
using old #ReconWip script these are the app items with multiple wip IN pull->pb interval
*/
-- ******************
-- #ReconWIP IS used only for COUNT AND total wip hours --
-- ******************

#wipStartTimes IS the fucker

-- base TABLE FROM @WipStartTimes
SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
FROM #jon j 
LEFT JOIN (
  SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
  FROM VehicleReconItems ri
  INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
  LEFT JOIN TypCategories t ON ri.typ = t.typ
  WHERE ar.status  = 'AuthorizedReconItem_Complete'
  AND ar.startTS IS NOT NULL 
  GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
    AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))
WHERE j.VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #mult)
ORDER BY j.VehicleInventoryItemID, startTS


-- the worry has to DO with sequence now
-- but let's see WHERE this goes

SELECT * FROM #wipstarttimes WHERE VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #mult)
-- so the problem here IS it can show only 1 start, stop time per vehicle, but the #mult TABLE
-- has atleast 2 per veicle


SELECT * FROM #wipstarttimes
WHERE VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #mult)
ORDER BY VehicleInventoryItemID 

SELECT VehicleInventoryItemID, 
  MAX(pulledTS) AS pulledTS, MAX(pbTS) AS pbTS,
  MAX(MechStart) AS MechStart, MAX(MechEnd) AS MechEnd, 
  MAX(BodyStart) AS BodyStart, MAX(BodyEnd) AS BodyEnd, 
  MAX(AppStart) AS AppStart, MAX(AppEnd) AS AppEnd
--INTO #WipStartTimes
FROM (
  SELECT VehicleInventoryItemID,-- this gives us one row for each vehicle/dept
    MAX(pulledTS) AS pulledTS,
    MAX(pbTS) AS pbTS,
    MAX(CASE WHEN category = 'MechanicalReconItem' THEN startTS END) AS MechStart,
    MAX(CASE WHEN category = 'BodyReconItem' THEN startTS END) AS BodyStart,  
    MAX(CASE  WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN startTS END) AS AppStart, 
    MAX(CASE WHEN category = 'MechanicalReconItem' THEN completeTS END) AS MechEnd,
    MAX(CASE WHEN category = 'BodyReconItem' THEN completeTS END) AS BodyEnd,  
    MAX(CASE  WHEN category = 'PartyCollection' or category = 'AppearanceReconItem' THEN completeTS END) AS AppEnd          
  FROM (
    SELECT j.VehicleInventoryItemID, j.pulledTS, j.pbTS, b.startTS, b.completeTS, b.category -- this gives a row for every time a vehicle IS IN WIP IN any dept
    FROM #jon j 
    LEFT JOIN (
      SELECT VehicleInventoryItemID , ar.startTS, ar.completeTS, category -- collapses multiple line items with the same startts AND completets INTO one row
      FROM VehicleReconItems ri
      INNER JOIN AuthorizedReconItems ar ON ri.VehicleReconItemID = ar.VehicleReconItemID 
      LEFT JOIN TypCategories t ON ri.typ = t.typ
      WHERE ar.status  = 'AuthorizedReconItem_Complete'
      AND ar.startTS IS NOT NULL 
      GROUP BY VehicleInventoryItemID, ar.startts, ar.completets, category) b ON j.VehicleInventoryItemID = b.VehicleInventoryItemID 
        AND (b.startTS <= j.pbTS AND (b.completeTS > j.pulledTS OR b.startTS >= j.pulledTS))) t  
  GROUP BY VehicleInventoryItemID, category) x
GROUP BY VehicleInventoryItemID; 

SELECT VehicleInventoryItemID, fromts, thruts 
FROM VehicleInventoryItemStatuses
WHERE category LIKE '%ppearan%'
AND status = 'AppearanceReconProcess_InProcess'
AND VehicleInventoryItemID IN (SELECT VehicleInventoryItemID FROM #mult)
ORDER BY VehicleInventoryItemID 