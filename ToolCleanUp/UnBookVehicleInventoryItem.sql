/*
6/29/11
    DELETE FROM viiFlowTasks
9/18/12
    added DELETE FROM WTF    
*/    
DECLARE @StockNumber string;
DECLARE @VehicleInventoryItemID string;
DECLARE @VehicleItemID string;
DECLARE @VehicleEvaluationID string;

@StockNumber = '29579b';
@VehicleInventoryItemID = (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems
  WHERE stocknumber = @Stocknumber);   
@VehicleItemID = (
  SELECT VehicleItemID  
  FROM VehicleInventoryItems
  WHERE stocknumber = @Stocknumber); 
@VehicleEvaluationID = (
  SELECT VehicleEvaluationID
  FROM VehicleEvaluations
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID);
  
BEGIN TRANSACTION;
TRY
  DELETE FROM VehicleInventoryItems WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  DELETE FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  UPDATE VehicleEvaluations
    SET CurrentStatus = 'VehicleEvaluation_Finished',
        VehicleInventoryItemID = NULL
    WHERE VehicleEvaluationID = @VehicleEvaluationID;
  
  UPDATE VehicleAppeals
    SET VehicleInventoryItemID = NULL
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  DELETE FROM VehicleItemStatuses 
    WHERE VehicleItemID = @VehicleItemID
    AND category = 'Inventory'
    AND ThruTS IS NULL;
  DELETE FROM VehicleItemStatuses
    WHERE VehicleItemID = @VehicleItemID
    AND status = 'VehicleEvaluation_Booked'
    AND FromTS = (
      SELECT MAX(FromTS)
      FROM VehicleItemStatuses 
      WHERE VehicleItemID = @VehicleItemID);
  UPDATE VehicleItemStatuses
    SET ThruTS = NULL
    WHERE VehicleItemID = @VehicleItemID
    AND status = 'VehicleEvaluation_Finished'
    AND FromTS = (
      SELECT MAX(FromTS)
      FROM VehicleItemStatuses 
      WHERE VehicleItemID = @VehicleItemID);    
    
  DELETE FROM TnaVehicles WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  DELETE FROM VehicleCostComponents
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID
    AND typ IN (
      'VehicleCostComponent_Transportation',
      'VehicleCostComponent_PurchaseAmount',
      'VehicleCostComponent_AuctionFee');
      
  DELETE FROM AuctionVehicles WHERE VehicleInventoryItemID = @VehicleInventoryItemID;  
  
  DELETE FROM VehicleAcquisitions WHERE VehicleInventoryItemID = @VehicleInventoryItemID; 
  
  UPDATE VehicleItemInsuranceInfo
    SET ThruTS = NULL
    WHERE VehicleItemID = @VehicleItemID
    AND FromTS = (
      SELECT MAX(FromTS)
      FROM VehicleItemInsuranceInfo
      WHERE VehicleItemID = @VehicleItemID);  
      
  UPDATE VehicleItemLicenseInfo
    SET ThruTS = NULL
    WHERE VehicleItemID = @VehicleItemID
    AND FromTS = (
      SELECT MAX(FromTS)
      FROM VehicleItemLicenseInfo
      WHERE VehicleItemID = @VehicleItemID); 
      
  UPDATE VehicleItemOwnerInfo
    SET ThruTS = NULL
    WHERE VehicleItemID = @VehicleItemID
    AND FromTS = (
      SELECT MAX(FromTS)
      FROM VehicleItemOwnerInfo
      WHERE VehicleItemID = @VehicleItemID);               
 
  UPDATE VehicleItemTitleInfo
    SET ThruTS = NULL
    WHERE VehicleItemID = @VehicleItemID
    AND FromTS = (
      SELECT MAX(FromTS)
      FROM VehicleItemTitleInfo
      WHERE VehicleItemID = @VehicleItemID); 
      
  UPDATE VehicleThirdPartyValues
    SET VehicleInventoryItemID = NULL
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID;           
  
  UPDATE VehiclePriceComponents
    SET VehicleInventoryItemID = NULL
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID; 
    
  UPDATE ReconNotes
    SET VehicleInventoryItemID = NULL
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID; 
    
  UPDATE SelectedReconPackages
    SET VehicleInventoryItemID = NULL
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID; 
    
  UPDATE ACVs
    SET VehicleInventoryItemID = NULL
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID; 
    
  DELETE FROM VehicleItemPhysicalLocations WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  DELETE FROM VehicleInventoryItemNotes WHERE VehicleInventoryItemID = @VehicleInventoryItemID;    
  
  DELETE FROM ViiFlowTasks WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  DELETE FROM ViiWTF WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
                  
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;    
