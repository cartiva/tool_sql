SELECT *
FROM VehicleInventoryItems 

SELECT sourcetyp, COUNT(*)
FROM VehicleInventoryItems
GROUP BY sourcetyp

SELECT typ, COUNT(*)
FROM VehicleEvaluations 
GROUP BY typ 


SELECT month(VehicleEvaluationTS), COUNT(*)
FROM VehicleEvaluations
WHERE typ = 'VehicleEvaluation_Trade'
  AND VehicleInventoryItemID IS NOT NULL 
  AND CAST(VehicleEvaluationTS AS sql_date) BETWEEN '01/01/2012' AND curdate()
GROUP BY month(VehicleEvaluationTS)  


-- vehicles traded IN 2012
SELECT VehicleInventoryItemID 
FROM VehicleEvaluations
WHERE typ = 'VehicleEvaluation_Trade'
  AND VehicleInventoryItemID IS NOT NULL 
  AND CAST(VehicleEvaluationTS AS sql_date) BETWEEN '01/01/2012' AND curdate()


-- vehicles that went to wsb immediately  
SELECT DISTINCT a.VehicleInventoryItemID
FROM (
  SELECT *
  FROM VehicleInventoryItemStatuses 
  WHERE CAST(FromTS AS sql_date) BETWEEN '01/01/2012' AND curdate()
    AND category = 'RMFlagRMP') a  
INNER JOIN (
  SELECT *
  FROM VehicleInventoryItemStatuses 
  WHERE CAST(FromTS AS sql_date) BETWEEN '01/01/2012' AND curdate()
    AND category = 'RMFlagWSB') b ON a.fromts = b.fromts

SELECT monthname(a.VehicleEvaluationTS) AS Month, COUNT(a.VehicleInventoryItemID) AS Trades,
  SUM(CASE WHEN c.VehicleInventoryItemID IS NOT NULL THEN 1 ELSE 0 END) AS [Direct to WS]
-- SELECT *   
FROM (    
  SELECT VehicleInventoryItemID, VehicleEvaluationTS 
  FROM VehicleEvaluations
  WHERE typ = 'VehicleEvaluation_Trade'
    AND VehicleInventoryItemID IS NOT NULL 
    AND CAST(VehicleEvaluationTS AS sql_date) BETWEEN '01/01/2012' AND curdate()) a   
LEFT JOIN (
  SELECT DISTINCT a.VehicleInventoryItemID
  FROM (
    SELECT *
    FROM VehicleInventoryItemStatuses 
    WHERE CAST(FromTS AS sql_date) BETWEEN '01/01/2012' AND curdate()
      AND category = 'RMFlagRMP') a  
  INNER JOIN (
    SELECT *
    FROM VehicleInventoryItemStatuses 
    WHERE CAST(FromTS AS sql_date) BETWEEN '01/01/2012' AND curdate()
      AND category = 'RMFlagWSB') b ON a.fromts = b.fromts) c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID     
GROUP BY monthname(a.VehicleEvaluationTS), month(a.VehicleEvaluationTS)  
ORDER BY month(a.VehicleEvaluationTS)    

-- new OR used
SELECT left(right(TRIM(stocknumber), 2), 1), COUNT(*)
FROM (    
  SELECT VehicleInventoryItemID, VehicleEvaluationTS 
  FROM VehicleEvaluations
  WHERE typ = 'VehicleEvaluation_Trade'
    AND VehicleInventoryItemID IS NOT NULL 
    AND CAST(VehicleEvaluationTS AS sql_date) BETWEEN '01/01/2012' AND curdate()) a  
LEFT JOIN VehicleInventoryItems b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID     
GROUP BY left(right(TRIM(stocknumber), 2), 1)

SELECT monthname(a.VehicleEvaluationTS) AS Month, COUNT(a.VehicleInventoryItemID) AS Trades,
  SUM(CASE WHEN c.VehicleInventoryItemID IS NOT NULL THEN 1 ELSE 0 END) AS [Direct to WS],
  SUM(CASE WHEN left(right(TRIM(v.stocknumber), 2), 1) IN ('0','1','2','3','4','5','6','7','8','9') THEN 1 ELSE 0 END) AS New,
  SUM(CASE WHEN left(right(TRIM(v.stocknumber), 2), 1) NOT IN ('0','1','2','3','4','5','6','7','8','9') THEN 1 ELSE 0 END) AS Used,
  SUM(CASE WHEN left(right(TRIM(v2.stocknumber), 2), 1) IN ('0','1','2','3','4','5','6','7','8','9') THEN 1 ELSE 0 END) AS [New Direct to WS],
  SUM(CASE WHEN left(right(TRIM(v2.stocknumber), 2), 1) NOT IN ('0','1','2','3','4','5','6','7','8','9') THEN 1 ELSE 0 END) AS [Used Direct to WS]  
-- SELECT *  
FROM VehicleInventoryItems v 
INNER JOIN  ( -- booked trade evals    
  SELECT VehicleInventoryItemID, VehicleEvaluationTS 
  FROM VehicleEvaluations
  WHERE typ = 'VehicleEvaluation_Trade'
    AND VehicleInventoryItemID IS NOT NULL 
    AND CAST(VehicleEvaluationTS AS sql_date) BETWEEN '01/01/2012' AND curdate()) a ON v.VehicleInventoryItemID = a.VehicleInventoryItemID   
LEFT JOIN ( -- direct to wsb
  SELECT DISTINCT a.VehicleInventoryItemID
  FROM (
    SELECT *
    FROM VehicleInventoryItemStatuses 
    WHERE CAST(FromTS AS sql_date) BETWEEN '01/01/2012' AND curdate()
      AND category = 'RMFlagRMP') a  
  INNER JOIN (
    SELECT *
    FROM VehicleInventoryItemStatuses 
    WHERE CAST(FromTS AS sql_date) BETWEEN '01/01/2012' AND curdate()
      AND category = 'RMFlagWSB') b ON a.fromts = b.fromts) c ON a.VehicleInventoryItemID = c.VehicleInventoryItemID     
LEFT JOIN VehicleInventoryItems v2 ON c.VehicleInventoryItemID = v2.VehicleInventoryItemID 
-- rydell only
WHERE v.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
GROUP BY monthname(a.VehicleEvaluationTS), month(a.VehicleEvaluationTS)  
ORDER BY month(a.VehicleEvaluationTS)    


  