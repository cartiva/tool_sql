-- bodystyle mismatch BETWEEN notes AND UsedCars
delete
FROM stpaulavailable 



select stocknumber, market, make, model, bodystyle, salesstatus, dateacquired, datesold
FROM usedcars
WHERE stocknumber = '60282X'

select segment, stocknumber, market, make, model, bodystyle, salesstatus, dateacquired, datesold
FROM usedcars
WHERE stocknumber = '63622B'

select segment, location, stocknumber, make, model, bodystyle, salesstatus, dateacquired, datesold
FROM stpaulavailable
WHERE stocknumber = '64339X' --'63622B'

select s.stocknumber, s.make, u.make, s.model, u.model, s.bodystyle, u.bodystyle, s.salesstatus, u.salesstatus
-- SELECT COUNT(*)
FROM stpaulavailable s
INNER JOIN usedcars u ON s.stocknumber = u.stocknumber 
  AND s.bodystyle <> u.bodystyle 
  AND s.location = u.location
ORDER BY s.stocknumber  



select s.stocknumber, s.make, s.model, s.bodystyle, u.bodystyle, s.salesstatus, u.salesstatus
-- SELECT COUNT(*)
FROM stpaulavailable s
INNER JOIN usedcars u ON s.stocknumber = u.stocknumber 
  AND s.salesstatus <> u.salesstatus 
  AND s.location = u.location
ORDER BY s.stocknumber  

select s.segment, u.segment, s.stocknumber, s.make, s.model, s.bodystyle, u.bodystyle, s.salesstatus, u.salesstatus
-- SELECT COUNT(*)
FROM stpaulavailable s
INNER JOIN usedcars u ON s.stocknumber = u.stocknumber 
  AND s.Segment <> u.Segment 
  AND s.location = u.location
ORDER BY s.stocknumber  

select s.stocknumber, s.make, u.make, s.model, s.bodystyle, u.bodystyle, s.salesstatus, u.salesstatus, s.location
-- SELECT COUNT(*)
FROM stpaulavailable s
INNER JOIN usedcars u ON s.stocknumber = u.stocknumber 
  AND s.salesstatus <> u.salesstatus 
  AND s.location = u.location
ORDER BY s.stocknumber  


SELECT UPPER(stocknumber)
FROM Usedcars
WHERE market = 'SP-St. Paul'
GROUP BY UPPER(stocknumber)
HAVING COUNT(*) > 1



SELECT *
FROM StPaulAvailable -- WHERE stocknumber = '63622B'
WHERE UPPER(trim(Stocknumber)) IN (
SELECT UPPER(trim(stocknumber))
FROM StPaulAvailable
-- WHERE market = 'SP-St. Paul'
GROUP BY UPPER(trim(stocknumber))
HAVING COUNT(*) > 1)
ORDER BY Stocknumber

select stocknumber, market, bodystyle, salesstatus, dateacquired, datesold 
FROM usedcars
WHERE market = 'SP-St. Paul'
AND datesold = '01/01/1900'
AND dateacquired < curdate() - 200
ORDER BY DateAcquired


select '''' + trim(StockNumber) + '''' + ','
FROM usedcars
WHERE market = 'SP-St. Paul'
AND datesold = '01/01/1900'
AND dateacquired < curdate() - 200
ORDER BY DateAcquired

-- old vehicles that don't exist IN notes
select StockNumber, "Year", Make, TrimLevel, Glump, Segment, 
  VIN, SalesStatus,  DateAcquired, DateSold, Location, 'SP-St. Paul'
-- SELECT COUNT(*)  
FROM UsedCars u
WHERE market = 'SP-St. Paul'
AND DateAcquired > '2008-01-01'
-- AND DateAcquired between '2008-01-01' AND '2010-12-31'
-- AND salesstatus NOT IN ('Sold','Wholesaled')
AND NOT EXISTS (
  SELECT 1 
  FROM StPaulAvailable
  WHERE stocknumber = u.stocknumber
  AND vin = u.vin)
ORDER BY dateacquired  



DELETE FROM usedcars
WHERE VehicleID IN (
select VehicleID
FROM UsedCars u
WHERE market = 'SP-St. Paul'
-- AND DateAcquired between '2008-01-01' AND '2010-12-31'
AND salesstatus NOT IN ('Sold','Wholesaled')
AND NOT EXISTS (
  SELECT 1 
  FROM StPaulAvailable
  WHERE stocknumber = u.stocknumber
  AND vin = u.vin))

  
select stocknumber, market, bodystyle, salesstatus, dateacquired, datesold, glump, vehicletype
FROM usedcars
WHERE stocknumber = '63488XA'  

select stocknumber, market, bodystyle, salesstatus, dateacquired, datesold, glump, vehicletype
SELECT *
FROM stpaulavailable
WHERE stocknumber = '63488XA'


SELECT vehicletype, bodystyle, COUNT(*)
FROM usedcars
WHERE market = 'SP-St. Paul'
AND DateAcquired > '2008-01-01'
GROUP BY vehicletype, bodystyle
AND VehicleType = 'Car'

SELECT bodystyle, vehicletype, COUNT(*)
FROM usedcars
WHERE market = 'SP-St. Paul'
AND DateAcquired > '2008-01-01'
GROUP BY bodystyle, vehicletype


SELECT DISTINCT vehicletype FROM usedcars
SELECT bodystyle, COUNT(*) FROM usedcars WHERE market = 'SP-St. Paul' GROUP BY bodystyle

SELECT stocknumber, market, make, model, bodystyle, salesstatus, dateacquired, datesold, glump, vehicletype
FROM usedcars
WHERE market = 'SP-St. Paul'
AND DateAcquired > '2009-01-01'
AND VehicleType = 'Car'
AND bodystyle LIKE 'P%'

SELECT stocknumber, market, make, model, bodystyle, salesstatus, dateacquired, datesold, glump, vehicletype
FROM usedcars
WHERE market = 'SP-St. Paul'
AND DateAcquired > '2008-01-01'
AND VehicleType <> 'Pickup'
AND bodystyle LIKE 'P%'



SELECT bodystyle, vehicletype, COUNT(*)
FROM usedcars
WHERE market = 'SP-St. Paul'
AND DateAcquired > '2009-01-01'
GROUP BY bodystyle, vehicletype


SELECT stocknumber, make, model, bodystyle, vehicletype, salesstatus, dateacquired, datesold, glump
FROM usedcars
WHERE market = 'SP-St. Paul'
AND DateAcquired > '2009-01-01'
AND bodystyle = ''
AND vehicletype <> 'Van'

UPDATE usedcars
SET VehicleType = 'SUV'
WHERE market = 'SP-St. Paul'
AND DateAcquired > '2009-01-01'
AND bodystyle = 'Sport Utility-2dr'
AND vehicletype <> 'SUV'












SELECT stocknumber, make, model, bodystyle, vehicletype
FROM usedcars
WHERE market = 'SP-St. Paul'
AND stocknumber = '62175A'