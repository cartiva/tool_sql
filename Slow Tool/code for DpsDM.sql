procedure LogPerformanceInfo(Page, Line, seq: string);


procedure TDpsDM.LogPerformanceInfo(Page, Line, seq: string);
var
  Command: AdsCommand;
  AdsConnection1: AdsConnection;
begin
  AdsConnection1 := FUserSession.GetConnection;
  try
    Command := AdsConnection1.CreateCommand;
    Command.CommandText := 'INSERT INTO zLogPerformanceInfo (page,line,ts,seq) ' +
                           ' VALUES (''' + page + ''', ''' + line + ''', ' +  QuotedStr(GetADSSqlTimeStamp(Now())) + ',' + QuotedStr(seq) + ')';
    Command.ExecuteNonQuery;
  finally
    FUserSession.ReleaseConnection;
  end;
end;

dpsdm.LogPerformanceInfo('OpenRecon','LoadOpenReconGrid','5');