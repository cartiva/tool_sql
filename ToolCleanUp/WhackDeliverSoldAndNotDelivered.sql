/*
check ALL statuses AND NOTES for a realistic timestamp

6/7 start keeping track of the misc cleanup tasks
-- viiWtf --
DELETE 
--SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '43dd32b0-331d-42dc-9114-bec9dcb1b8db'
  AND category = 'RMFlagWTF'
  AND ThruTS IS NULL ;
  
DELETE
--SELECT *
FROM viiwtf  
WHERE VehicleInventoryItemID = '43dd32b0-331d-42dc-9114-bec9dcb1b8db'
  AND ResolvedTS IS NULL 

SELECT *
FROM VehicleItemPhysicalLocations
WHERE VehicleInventoryItemID = '7faf1b60-a93b-46b5-95b4-55eec7bd60f6'
-- VehicleItemPhysicalLocations --
DELETE FROM VehicleItemPhysicalLocations
WHERE VehicleInventoryItemID = 'b7556310-8381-4f39-9730-3ad4262b08d8'
  AND CAST(fromts AS sql_Date) = '06/04/2012';
UPDATE VehicleItemPhysicalLocations
SET ThruTS = NULL
WHERE VehicleInventoryItemID = 'b7556310-8381-4f39-9730-3ad4262b08d8'
  AND CAST(fromts AS sql_Date) = '06/04/2012';
  
SELECT *
FROM reconplans
WHERE VehicleInventoryItemID = '7faf1b60-a93b-46b5-95b4-55eec7bd60f6'




*/
DECLARE @VehicleInventoryItemID string;
DECLARE @DeliverNote string;
DECLARE @NowTS timestamp;
DECLARE @SaleID string;
DECLARE @UserID string;

@VehicleInventoryItemID = '48f0da77-3b5b-458d-99f0-f786604ff93a';
@DeliverNote = 'Manually updated to delivered';
@NowTS = '11/19/2012 20:20:20';
@UserID = (SELECT partyid FROM users WHERE username = 'jon');

BEGIN TRANSACTION;
TRY
  TRY
    EXECUTE PROCEDURE DeliverVehicle(
      @VehicleInventoryItemID,
      @UserID,
      @DeliverNote,
      @NowTS);
  CATCH ALL
    RAISE WhackException(100, 'Deliver Vehicle: ' + __errText);
  END TRY;
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY      

/*
UPDATE reconplans
SET ThruTS = '01/16/2012 15:15:15'
--SELECT *
FROM reconplans
WHERE VehicleInventoryItemID = '603e5278-92c4-4468-9866-9c16162150df'

DELETE 
FROM reconplans
WHERE VehicleInventoryItemID = '603e5278-92c4-4468-9866-9c16162150df'
AND typ = 'ReconPlan_Appearance'
*/