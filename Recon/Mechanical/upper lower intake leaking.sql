SELECT stocknumber, b.yearmodel, b.make, b.model
-- SELECT *
FROM VehicleInventoryItems a
INNER JOIN VehicleItems b on a.VehicleItemID = b.VehicleItemID 
WHERE CAST(fromts AS sql_date) > curdatE() - 120


SELECT a.VehicleInventoryItemID, a.description, 
  a.totalpartsamount + a.laboramount AS Est,
  CASE 
    WHEN b.VehicleReconItemID IS NOT NULL THEN 'Yes'
    ELSE 'No'
  END AS Authorized
-- SELECT *
FROM VehicleReconItems a
LEFT JOIN AuthorizedReconItems b on a.VehicleReconItemID = b.VehicleReconItemID 
WHERE a.description LIKE '%intake%'

SELECT * FROM vehiclesales

SELECT distinct cast(a.fromts as sql_date) as fromDate, stocknumber, b.vin, 
  b.yearmodel, b.make, b.model, 
  left(cast(f.description AS sql_char), 100), f.estimate, f.authorized,
  CASE
    WHEN g.typ IS NULL THEN 'Inventory'
    WHEN g.typ = 'VehicleSale_Retail' THEN 'Retail'
    WHEN g.typ = 'VehicleSale_Wholesale' THEN 'WS'
  END AS Disposition
-- SELECT COUNT(*)
FROM VehicleInventoryItems a
INNER JOIN VehicleItems b on a.VehicleItemID = b.VehicleItemID 
LEFT JOIN (
  SELECT d.VehicleInventoryItemID, d.description, 
    d.totalpartsamount + d.laboramount AS Estimate,
    CASE 
      WHEN e.VehicleReconItemID IS NOT NULL THEN 'Yes'
      ELSE 'No'
    END AS Authorized
  -- SELECT *
  FROM VehicleReconItems d
  LEFT JOIN AuthorizedReconItems e on d.VehicleReconItemID = e.VehicleReconItemID 
  WHERE d.description LIKE '%intake%') f on a.VehicleInventoryItemID = f.VehicleInventoryItemID 
LEFT JOIN vehicleSales g on a.VehicleInventoryItemID = g.VehicleInventoryItemID 
WHERE CAST(fromts AS sql_date) > curdatE() - 365
  AND f.VehicleInventoryItemID IS NOT NULL 
  
AND b.vin IN ('1G4CW54K744161721','1G4HP52K434194020',
  '5GZEV13728J295807','2G2WP552771172879')  

  
SELECT DISTINCT ro, vin, thedate 
FROM dds.factRepairOrder a
INNER JOIN dds.dimVehicle b on a.vehicleKey = b.vehicleKey
INNER JOIN dds.day c on a.opendatekey = c.datekey
WHERE b.vin IN ('1G4CW54K744161721','1G4HP52K434194020',
  '5GZEV13728J295807','2G2WP552771172879')
  