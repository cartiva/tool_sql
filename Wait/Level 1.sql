-- Level 1
-- week -- fr/th -- COUNT -- days
-- requires:
--    #PullToPb
--    #WipBase

-- XmR Level1.xlsx page Level 1 AvgHoursPerVeh

SELECT TheWeek, 
  SUM(timestampdiff(sql_tsi_hour, p.PulledTS, p.pbTS))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg Hours/Vehicle]
FROM #PullToPB p
INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
  SELECT DISTINCT(VehicleInventoryItemID)
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
GROUP BY TheWeek
ORDER BY TheWeek desc

-- XmR Level1.xlsx page Level 1Days
-- graph

SELECT TheWeek, 
  SUM(timestampdiff(sql_tsi_day, cast(p.PulledTS AS sql_date), cast(p.pbTS AS sql_date)))/COUNT(DISTINCT p.VehicleInventoryItemID) AS [Avg Days/Vehicle]
FROM #PullToPB p
INNER JOIN ( -- excludes NULL p.VehicleInventoryItemID (days with no pb vehicle) AND veh/cat multiples
  SELECT DISTINCT(VehicleInventoryItemID)
  FROM #WipBase) w ON p.VehicleInventoryItemID = w.VehicleInventoryItemID 
GROUP BY TheWeek