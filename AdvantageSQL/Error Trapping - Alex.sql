The best way to trap this error is to define your own function. For example,

CREATE FUNCTION MyTimestamp( strData String )
RETURNS Timestamp
BEGIN
   DECLARE ts Timestamp;
   TRY
      ts = Cast( strData AS SQL_TIMESTAMP );
      return ts;
   CATCH ADS_SCRIPT_EXCEPTION
      IF ( __errcode <> 2109 ) AND ( __errcode <> 2236 ) THEN
         RAISE;
      ENDIF;
   END;

   -- try normal date
   ts = CAST ( CAST( strData AS SQL_DATE ) AS SQL_TIMESTAMP );
   return ts;
END;


This function works for the following:
SELECT myTimestamp( '1999-01-01 12:00:00' ) from system.iota

SELECT myTimestamp( '1999-01-01' ) from system.iota