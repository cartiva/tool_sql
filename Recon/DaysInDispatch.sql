SELECT avg("Days to Complete Mech")
FROM (
SELECT max(viix.stocknumber), viisx.VehicleInventoryItemID, viisx.FromTS, 
  max(timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts)) AS "Days to Complete Mech"
FROM VehicleInventoryItemStatuses viisx 
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE viisx.category = 'MechanicalReconDispatched'
AND cast(viisx.fromts AS sql_date) >  curdate()-120
AND viix.ThruTS IS not NULL 
GROUP BY viisx.VehicleInventoryItemID, viisx.FromTS) wtf

SELECT avg("Days to Complete App")
FROM (
SELECT max(viix.stocknumber), viisx.VehicleInventoryItemID, viisx.FromTS, 
  max(timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts)) AS "Days to Complete App"
FROM VehicleInventoryItemStatuses viisx 
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE viisx.category = 'AppearanceReconDispatched'
AND cast(viisx.fromts AS sql_date) >  curdate()-90
AND viix.ThruTS IS not NULL 
GROUP BY viisx.VehicleInventoryItemID, viisx.FromTS) wtf

SELECT avg("Days to Complete Body")
FROM (
SELECT max(viix.stocknumber), viisx.VehicleInventoryItemID, viisx.FromTS, 
  max(timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts)) AS "Days to Complete Body"
FROM VehicleInventoryItemStatuses viisx 
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE viisx.category = 'BodyReconDispatched'
AND cast(viisx.fromts AS sql_date) >  curdate()-120
AND viix.ThruTS IS not NULL 
GROUP BY viisx.VehicleInventoryItemID, viisx.FromTS) wtf


SELECT "Days to Complete Mech", COUNT(*)
FROM (
SELECT max(viix.stocknumber), viisx.VehicleInventoryItemID, viisx.FromTS, 
  max(timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts)) AS "Days to Complete Mech"
FROM VehicleInventoryItemStatuses viisx 
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE viisx.category = 'MechanicalReconDispatched'
AND cast(viisx.fromts AS sql_date) >  curdate()-120
AND viix.ThruTS IS not NULL 
GROUP BY viisx.VehicleInventoryItemID, viisx.FromTS) wtf
GROUP BY "Days to Complete Mech"

SELECT "Days to Complete App", COUNT(*)
FROM (
SELECT max(viix.stocknumber), viisx.VehicleInventoryItemID, viisx.FromTS, 
  max(timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts)) AS "Days to Complete App"
FROM VehicleInventoryItemStatuses viisx 
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE viisx.category = 'AppearanceReconDispatched'
AND cast(viisx.fromts AS sql_date) >  curdate()-120
AND viix.ThruTS IS not NULL 
GROUP BY viisx.VehicleInventoryItemID, viisx.FromTS) wtf
GROUP BY "Days to Complete App"

SELECT "Days to Complete Body", COUNT(*)
FROM (
SELECT max(viix.stocknumber), viisx.VehicleInventoryItemID, viisx.FromTS, 
  max(timestampdiff(sql_tsi_day, viisx.fromts, viisx.thruts)) AS "Days to Complete Body"
FROM VehicleInventoryItemStatuses viisx 
LEFT JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE viisx.category = 'BodyReconDispatched'
AND cast(viisx.fromts AS sql_date) >  curdate()-120
AND viix.ThruTS IS not NULL 
GROUP BY viisx.VehicleInventoryItemID, viisx.FromTS) wtf
GROUP BY "Days to Complete Body"