1/8/12
wilkie informed me that i fucked up
did NOT include those vehicles that had been sold IN january
i haven't figured out yet how to fix that
these are the vehicles i had to run to give him the jan sales
'12502A',
'13254B',
'14478A',
'14994A',
'15228A',
'15357A',
'15539A',
'15540A',
'15546P',
'15602A',
'15683X',
'15693A',
'15700A',
'15728A',
'15964X',
'14551XX',
'14952XX',
'14987XB',
'15120XX',
'15121XX',
'15295XX',
'15640XX',
'14511XXB',
'14909XXA',
'15304XXA',
'15379XXA',
'15394XXB',
'15450XXA',
'15514XXA',
'15598A'

/*

SELECT viix.stocknumber, g.ControlNumber, n.net, r.recon
FROM VehicleInventoryItems viix 
LEFT JOIN (
  select distinct ControlNumber
  from GlDetailsTable) g ON viix.stocknumber = g.ControlNumber
LEFT JOIN (
  select ControlNumber, sum(TransactionAmount) as Net
  from gldetailstable
  where AccountNumber in ('124000', '124100', '224000','224100') -- Inventory accounts only
  --  and trim(gtpost) <> 'V'  -- Ignore VOIDS
  group by ControlNumber
    having sum(TransactionAmount) > 0) n ON g.ControlNumber = n.ControlNumber
    
left join (	
  select ControlNumber, sum(TransactionAmount) as Recon
  from gldetailstable
  where AccountNumber in ('124000', '124100', '224000','224100') -- Inventory accounts only
--    and trim(gtpost) <> 'V'  -- Ignore VOIDS
	and Journal in ('SVI', 'SWA', 'SCA') -- Journals = Service Sales Internal, Service Sales Warranty, Service Sales Retail
  group by ControlNumber
    having sum(TransactionAmount) > 0) r on g.ControlNumber = r.ControlNumber	
WHERE viix.ThruTS IS NULL     
ORDER BY stocknumber

SELECT distinct(controlnumber) 
FROM gldetailstable g
WHERE (
  SELECT SUM(transactionAmount)
  FROM gldetailstable
  WHERE controlnumber = g.controlnumber
  and AccountNumber in ('124000', '124100', '224000','224100')
  GROUP BY controlnumber) <> 0
ORDER by controlnumber
*/
/*
SELECT *
FROM zJonLifoArkona 
WHERE dateacquired > '12/31/2010'

DELETE FROM zJonLifoArkona 
WHERE dateacquired > '12/31/2010'
*/
-- 1/6/12
-- this IS the query to generate the spreadsheet
-- 1/5/14 generated a separate TABLE for the year zJonLifoArkona2014 (fuck, should have been 2013, oh well)
-- db2 query IS current inventory plus vehicles sold IN january
-- 1/3/15 moved zJonLifoArkona2014 to zJonLifoArkona2013, reuse the zJonLifoArkona2014 TABLE
-- run aqt query: Inventory - Lifo.sql, export to excel, generate INSERT statement (see lifo notes.txt)
-- 1/5/15 nothing new, just put the arkona data INTO zJonLifoArkona2015
SELECT 
  z.source AS Source, left(z.stocknumber, 12) AS Stock#, z.vin AS VIN, 
  left(trim(z.yearmodel) + ' ' + trim(z.make) + ' ' + trim(z.model) + ' ' 
    + coalesce(TRIM(vix.TRIM), '') + ' ' + coalesce(TRIM(vix.BodyStyle), ''), 50) AS Vehicle, 
  left(vix.engine, 25) AS Engine, left(vix.transmission, 6) AS Trans, 
  replace(cast(vio.VehicleOptions AS sql_varchar), ',', ', ') AS Options, --replace(vio.VehicleOptions, ',', ', ') AS Options, 
  cast(vio.AdditionalEquipment AS sql_varchar) AS "Addtl Equp", z.miles AS Miles, 
  z.dateacquired AS "Date Acqd",
  z.net AS Net, z.recon AS Recon, z.cost AS Cost,
  CASE 
    WHEN viix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN viix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN viix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS Note , 'Good  Average  Rough' AS Condition   
--INTO #wtf  
FROM zJonLifoArkona2016 z
LEFT JOIN VehicleInventoryItems viix ON z.stocknumber = viix.stocknumber
LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
LEFT JOIN VehicleItemOptions vio ON viix.VehicleItemID = vio.VehicleItemID 
--WHERE LEFT(z.stocknumber, 1) <> 'H'
ORDER BY z.STOCKNUMBER  


-- 1/5/2014
-- sold IN january 2014
SELECT stock#, vin, vehicle, net, recon, cost, note, CAST(c.soldts AS sql_date)
FROM #wtf a
INNER JOIN VehicleInventoryItems b on a.stock# = b.stocknumber
INNER JOIN vehiclesales c on b.VehicleInventoryItemID = c.VehicleInventoryItemID 
WHERE c.status = 'VehicleSale_Sold'
  AND year(soldts) = 2015


-- NOT IN tool BY stocknumber

SELECT z.stocknumber, z.vin, z.dateacquired, vix.VehicleItemID,
  (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleItemID = vix.VehicleItemID),
  CASE 
    WHEN vix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN vix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN vix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS note   
FROM zJonLifoArkona2014 z
LEFT JOIN VehicleItems vix ON z.vin = vix.vin
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE stocknumber = z.stocknumber)
ORDER BY z.stocknumber  



--------------------------------------------------------------------------------

SELECT z.stocknumber, z.vin, z.dateacquired, vix.VehicleItemID,
  (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleItemID = vix.VehicleItemID),
  CASE 
    WHEN vix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN vix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN vix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS note, wtf.status  
FROM zJonLifoArkona z
LEFT JOIN VehicleItems vix ON z.vin = vix.vin
LEFT JOIN (
    SELECT top 1 vi.VehicleItemID, 
      CASE ve.CurrentStatus
        WHEN 'VehicleEvaluation_GettingData' THEN 'Eval: Getting Data'
        WHEN 'VehicleEvaluation_DataDone' THEN 'Eval: Data Collection Done'
        WHEN 'VehicleEvaluation_InAppraisal' THEN 'Eval: In  Appraisal'
        WHEN 'VehicleEvaluation_Finished' THEN 'Eval: Finished'
      END AS status  
    FROM VehicleItems vi
    LEFT JOIN VehicleEvaluations ve ON ve.VehicleitemID = vi.VehicleItemID
    WHERE ve.CurrentStatus <> 'VehicleEvaluation_Booked'
    ORDER BY ve.VehicleEvaluationTS DESC)  wtf ON vix.VehicleItemID = wtf.VehicleItemID 
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE stocknumber = z.stocknumber)
ORDER BY z.stocknumber      


-- NOT IN tool BY stocknumber, JOIN ON vin 
SELECT z.stocknumber, z.vin, z.dateacquired, vix.VehicleItemID,
  (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleItemID = vix.VehicleItemID),
  CASE 
    WHEN vix.VehicleItemId IS NULL AND net IS NOT NULL THEN 'Not in Tool'
    WHEN vix.VehicleItemId IS NULL AND net IS NULL THEN 'No Cost & Not in Tool'
    WHEN vix.VehicleItemID IS NOT NULL AND net IS NULL THEN 'No Cost'
  END AS note,  
  
  ( 
    SELECT top 1 
      CASE ve.CurrentStatus
        WHEN 'VehicleEvaluation_GettingData' THEN 'Eval: Getting Data'
        WHEN 'VehicleEvaluation_DataDone' THEN 'Eval: Data Collection Done'
        WHEN 'VehicleEvaluation_InAppraisal' THEN 'Eval: In  Appraisal'
        WHEN 'VehicleEvaluation_Finished' THEN 'Eval: Finished'
      END AS status  
    FROM VehicleItems vi
    LEFT JOIN VehicleEvaluations ve ON ve.VehicleitemID = vi.VehicleItemID
    WHERE ve.CurrentStatus <> 'VehicleEvaluation_Booked'
    AND vi.VehicleItemID = vix.VehicleItemID 
    ORDER BY ve.VehicleEvaluationTS DESC)   
FROM zJonLifoArkona z
LEFT JOIN VehicleItems vix ON z.vin = vix.vin
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItems
  WHERE stocknumber = z.stocknumber)
ORDER BY z.stocknumber  

/*
SELECT VehicleItemID  FROM vehicleitemoptions GROUP BY VehicleItemID HAVING COUNT(*) > 1

SELECT * FROM vehicleitemoptions


SELECT * FROM blackbookvalues            
SELECT * FROM blackbookaddsdeducts        

REPLACE( str1, str2, str3 )	Replace all occurrences of str2 in str1 with str3
SELECT VehicleOptions, replace(VehicleOptions, ',', ', ')
FROM vehicleitemoptions
WHERE VehicleItemID = 'ab22052f-5811-094e-974d-b4e3d694ae4b'
*/       
