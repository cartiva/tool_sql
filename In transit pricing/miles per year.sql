FROM GetAppraisalAppeal
  if
  (select TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(@Year, SQL_CHAR)) + '-01-01', SQL_DATE), CurDate()) * 12 from system.iota) = 0 then
    @MilesPerYear = @Mileage;
  else
    @MilesPerYear = Truncate(@Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(@Year, SQL_CHAR)) + '-01-01', SQL_DATE), CurDate()) * 12, 0);
  end if;
  
SELECT yearmodel, value,
  CASE
    WHEN (select TimeStampDiff(SQL_TSI_MONTH, cast(Trim(CAST(YearModel as SQL_CHAR)) + '-01-01' as SQL_DATE), CurDate()) * 12 from system.iota) = 0 THEN
      value
    ELSE
      Truncate(value / TimeStampDiff(SQL_TSI_MONTH, CAST(Trim(CAST(yearmodel AS SQL_CHAR)) + '-01-01' AS SQL_DATE), CurDate()) * 12, 0)
  END AS MilesPerYear 
FROM #jon  

SELECT * FROM VehicleEvaluationAppeals