1. merge tmp to keystatus
SELECT *
FROM (   
SELECT *
FROM keyperkeystatus a
full OUTER JOIN tmpkeyperkeystatus b ON a.stocknumber = b.stocknumber) c
WHERE (keystatus <> keystatus_1 OR stocknumber IS NULL OR stocknumber_1 IS NULL)    
ORDER BY 
  case 
    WHEN stocknumber_1 IS NULL THEN stocknumber_1
    else stocknumber 
  END 
   
2. purge/clean keystatus
SELECT *
-- SELECT COUNT(*)
FROM tmpkeyperkeystatus a
WHERE NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItems 
  WHERE ThruTS IS NULL 
    AND stocknumber = a.stocknumber)
    
SELECT *
FROM tmpkeyperkeystatus a
LEFT JOIN VehicleInventoryItems b ON a.stocknumber = b.stocknumber
  AND b.thruts IS NULL
WHERE b.stocknumber IS NULL       
 
-- uh oh, IF there are more than one records for a stocknumber ('16922P','H4831a') it's a keyper problem 
SELECT stocknumber
FROM keyperkeystatus
GROUP BY stocknumber
HAVING COUNT(*) > 1    

SELECT *
FROM keyperkeystatus
WHERE stocknumber IN ('16922P','H4831a')

-- which brings up the different categories of problems with keyper data
1. no record IN keyper
2. checked out but no user
that may be enough

ok, save the original scrape INTO keyperkeystatusorig
CREATE TABLE keyperkeystatusorig(
  stocknumber cichar(20),
  keystatus cichar(20));
INSERT INTO keyperkeystatusorig
SELECT * FROM keyperkeystatus;  


SELECT *
FROM (   
SELECT *
FROM keyperkeystatus a
full OUTER JOIN tmpkeyperkeystatus b ON a.stocknumber = b.stocknumber) c
WHERE (keystatus <> keystatus_1 OR stocknumber IS NULL OR stocknumber_1 IS NULL)    
ORDER BY 
  case 
    WHEN stocknumber_1 IS NULL THEN stocknumber_1
    else stocknumber 
  END 
  
 
-- hmmm WHERE to deal with dups
DELETE 
FROM tmpkeyperkeystatus
WHERE stocknumber IN (
  SELECT stocknumber
  FROM tmpkeyperkeystatus
  GROUP BY stocknumber
  HAVING COUNT(*) > 1)

alter PROCEDURE KeyperKeyStatusUpdate
   ( 
   ) 
BEGIN 

/*
DELETE FROM keyperkeystatus;
INSERT INTO keyperkeystatus
SELECT * FROM keyperkeystatusorig;  

EXECUTE PROCEDURE KeyperKeyStatusUpdate();
*/

  BEGIN TRANSACTION;
  TRY 
    DELETE -- include only currently owned used cars
    FROM tmpkeyperkeystatus 
    WHERE NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItems 
      WHERE ThruTS IS NULL 
        AND trim(stocknumber) = tmpkeyperkeystatus.stocknumber);  
    DELETE -- dups are just another keyper anomaly
    FROM tmpkeyperkeystatus
    WHERE stocknumber IN (
      SELECT stocknumber
      FROM tmpkeyperkeystatus
      GROUP BY stocknumber
      HAVING COUNT(*) > 1);
    
    merge keyperkeystatus a
    using tmpkeyperkeystatus b
    ON a.stocknumber = b.stocknumber
    WHEN matched THEN 
      UPDATE SET a.keystatus = b.keystatus    
    WHEN NOT matched THEN 
      INSERT values(b.stocknumber, b.keystatus);
          
    DELETE 
    FROM keyperkeystatus 
    WHERE NOT EXISTS (
      SELECT 1
      FROM VehicleInventoryItems 
      WHERE ThruTS IS NULL 
        AND stocknumber = keyperkeystatus.stocknumber);  
    DELETE 
    FROM keyperkeystatus
    WHERE stocknumber IN (
      SELECT stocknumber
      FROM keyperkeystatus
      GROUP BY stocknumber
      HAVING COUNT(*) > 1);        
               
  COMMIT WORK;
  CATCH ALL
    ROLLBACK WORK; 
    RAISE;
  END TRY;

END;


