/*
H3268A was ws'd to rydells AND booked AS 13997X
should have been H3020A
*/
--13997x  -- '35f442dd-bfc9-4b75-8833-624f98921e70'
EXECUTE PROCEDURE DeleteVehicleInventoryItem('35f442dd-bfc9-4b75-8833-624f98921e70')
SELECT * FROM VehiclePricings WHERE VehicleInventoryItemID = '35f442dd-bfc9-4b75-8833-624f98921e70'

-- VehiclePricingDetails needs to be added to sp.DeleteVehicleInventoryItem
SELECT COUNT(*
FROM VehiclePricingDetails v
WHERE NOT EXISTS (
  SELECT 1
  FROM VehiclePricings
  WHERE VehiclePricingID = v.VehiclePricingID)
  
-- H3268A -- '206d0950-aa92-4c4f-ac76-adbfac75649d'  -- 2G1WU58R979102212 
-- cancel sale
SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '206d0950-aa92-4c4f-ac76-adbfac75649d' 

SELECT *
FROM ReconAuthorizations 
where VehicleInventoryItemID = '206d0950-aa92-4c4f-ac76-adbfac75649d'

SELECT *
FROM AuthorizedReconItems
WHERE ReconAuthorizationID = '1562377b-de94-43b1-a43a-0d5ff0fa3fe7'

SELECT *
FROM vehiclesales
WHERE VehicleInventoryItemID = '206d0950-aa92-4c4f-ac76-adbfac75649d'

VehicleItemStatuses
VehicleItemPhysicalLocations
VehicleInventoryItemNotes 
VehicleInventoryItems 



