/*
SELECT * FROM vehiclesales
SELECT * FROM partyrelationships
*/

SELECT *
FROM VehicleSales vs
INNER JOIN VehicleInventoryItems viix 
  ON vs.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND LocationID IN (
    SELECT PartyID2
    FROM PartyRelationships
    WHERE typ = 'PartyRelationship_MarketLocations'
    AND ThruTS IS NULL)
INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID
INNER JOIN MakeModelClassifications mmc 
  ON vix.Make = mmc.Make
  AND vix.Model = mmc.Model
WHERE vs.typ = 'VehicleSale_Retail'
AND vs.status = 'VehicleSale_Sold'
AND cast(vs.SoldTS AS sql_date) > curdate() - 31


SELECT *
FROM VehicleSales vs
INNER JOIN VehicleInventoryItems viix ON vs.VehicleInventoryItemID = viix.VehicleInventoryItemID

INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID
INNER JOIN MakeModelClassifications mmc 
  ON vix.Make = mmc.Make
  AND vix.Model = mmc.Model
WHERE vs.typ = 'VehicleSale_Retail'
AND vs.status = 'VehicleSale_Sold'
AND cast(vs.SoldTS AS sql_date) > curdate() - 31
AND viix.LocationID IN (
  SELECT PartyID2
  FROM PartyRelationships
  WHERE typ = 'PartyRelationship_MarketLocations'
  AND ThruTS IS NULL
  AND PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82')
  
