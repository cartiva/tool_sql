this appears to be the failed attempt to utilize VehicleInventoryItemStatuses 
/*
  
-- the last 90 days, at rydells
-- these are the vehicles that completed the pull -> pb cycle
SELECT viix.stocknumber, pull.PulledTS AS @ts1, PBTS AS @ts2, viix.VehicleInventoryItemID 
FROM VehicleInventoryItems viix
LEFT JOIN (
  SELECT VehicleInventoryItemID, FromTS AS PulledTS
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
INNER JOIN (
  SELECT VehicleInventoryItemID, FromTS AS PBTS
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
WHERE cast(viix.FromTS AS sql_date) >  curdate() - 90  
AND viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
AND pb.pbts IS NOT NULL  


-- what IF it was pull -> pricing more than once
SELECT viix.stocknumber, pull.PulledTS AS @ts1, PBTS AS @ts2, viix.VehicleInventoryItemID 
FROM VehicleInventoryItems viix
LEFT JOIN (
  SELECT VehicleInventoryItemID, FromTS AS PulledTS
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
INNER JOIN (
  SELECT VehicleInventoryItemID, FromTS AS PBTS
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
 
WHERE cast(viix.FromTS AS sql_date) >  curdate() - 90  
AND viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
AND pb.pbts IS NOT NULL 
AND viix.stocknumber IN ('12902C','13387B','13537A','13574XX','14397XA','14420XX','14487A')

shit, how DO cancelled pulls show up?
which means i need to go IN the dream AND run the tool local
so the query IS showing the same pullTS for each record, which IS wrong.
shit LIKE backons (12902C)
13387B pulled once, made it to pb twice ON the single pull 
  pulled - 6/13
  pb - 6/15 - never made it to available
  6/15 - recon added
  6/16 - sales buffer
  6/17 - pb
  6/24 - sold
14397XA pulled once, made it to pb twice

maybe my base SET should be pulled to avaialable 
*/
-- this narrows it down to the back ON (12902C) showing up multiple times
-- but still the questions
-- what defines thes series of actions associated with a single pull
-- 1. pullts < avts but this will still generate multiple records
-- the short fix: limit the SET to vehicles pulled only once
-- AND was IN pb only once
-- this isn't the answer, but it puts off figuring out complete timelines
-- so this now becomes the base query
/*
SELECT distinct viix.stocknumber, pull.PulledTS AS @ts1, pbTS AS @ts2, viix.VehicleInventoryItemID 
INTO #wip
FROM VehicleInventoryItems viix
LEFT JOIN (
  SELECT VehicleInventoryItemID, FromTS AS PulledTS
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPulled') AS pull ON viix.VehicleInventoryItemID = pull.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, FromTS AS pbTS
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagPB') AS pb ON viix.VehicleInventoryItemID = pb.VehicleInventoryItemID   
WHERE cast(viix.FromTS AS sql_date) >  curdate() - 90  
AND viix.LocationID = (SELECT partyid FROM organizations WHERE name = 'Rydells')   
AND pb.pbts IS NOT NULL -- made it ALL the way to the lot 
AND NOT EXISTS (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
  AND category = 'RMFlagPulled'
  GROUP BY VehicleInventoryItemID 
  HAVING COUNT(*) > 1)
AND NOT EXISTS (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID 
  AND category = 'RMFlagPB'
  GROUP BY VehicleInventoryItemID 
  HAVING COUNT(*) > 1)  
*/  
-- SELECT * FROM #wip
-- FROM this, need to extract wip time
-- so let's put this data IN a temp TABLE #wip  

/*
SELECT w.*,
  (SELECT FromTS
    FROM VehicleInventoryItemStatuses vmf
    WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
    AND Status = 'MechanicalReconProcess_InProcess'
    AND FromTS >= w.@ts1 -- ?only WORK started after the pull ?
    AND ThruTS <= w.@ts2
    AND NOT EXISTS(
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
      AND Status = 'MechanicalReconProcess_InProcess'
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1)) AS MechFrom,
  (SELECT ThruTS
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
    AND Status = 'MechanicalReconProcess_InProcess'
    AND FromTS >= w.@ts1 -- ?only WORK started after the pull ?
    AND ThruTS <= w.@ts2 
    AND NOT EXISTS(
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
      AND Status = 'MechanicalReconProcess_InProcess'
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1)) AS MechThru,  
  (SELECT FromTS
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
    AND Status = 'BodyReconProcess_InProcess'
    AND FromTS >= w.@ts1 -- ?only WORK started after the pull ?
    AND ThruTS <= w.@ts2 
    AND NOT EXISTS(
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
      AND Status = 'BodyReconProcess_InProcess'
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1))AS BodyFrom,
  (SELECT ThruTS
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
    AND Status = 'BodyReconProcess_InProcess'
    AND FromTS >= w.@ts1 -- ?only WORK started after the pull ?
    AND ThruTS <= w.@ts2 
    AND NOT EXISTS(
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
      AND Status = 'BodyReconProcess_InProcess'
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1))AS BodyThru, 
  (SELECT FromTS
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
    AND Status = 'AppearanceReconProcess_InProcess'
    AND FromTS >= w.@ts1 -- ?only WORK started after the pull ?
    AND ThruTS <= w.@ts2 
    AND NOT EXISTS(
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
      AND Status = 'AppearanceReconProcess_InProcess'
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1))AS AppFrom,
  (SELECT ThruTS
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
    AND Status = 'AppearanceReconProcess_InProcess'
    AND FromTS >= w.@ts1 -- ?only WORK started after the pull ?
    AND ThruTS <= w.@ts2 
    AND NOT EXISTS(
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItemStatuses
      WHERE VehicleInventoryItemID = w.VehicleInventoryItemID
      AND Status = 'AppearanceReconProcess_InProcess'
      GROUP BY VehicleInventoryItemID
      HAVING COUNT(*) > 1))AS AppThru   
INTO #ReconWip            
FROM #wip w;  

*/


--ORDER BY stocknumber
-- uh oh - attempting to put INTO a temp TABLE 
-- got sub-query returned more than one row
-- so shit, IN wip multiple times?
-- mech: 14509xx 'b74edc51-cde2-4ae3-894a-bbf6c81a2919'
-- body: 14621x  '042bbba5-1341-48a5-8cc8-775d94fce128'
-- app: 14531XA, 14619X '6d35e9ad-73ac-4fae-a860-2044916ad620' '192fa224-dfb3-494e-9c72-547dd9f0c8d2'
-- so what the fuck, DO i eliminate those with mult dept wip, sure why not
/*
SELECT *
FROM #wip w
WHERE EXISTS (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = w.VehicleInventoryItemID  
    AND Status = 'MechanicalReconProcess_InProcess'
    AND FromTS >= w.@ts1 -- ?only WORK started after the pull ?
    AND ThruTS <= w.@ts2
  GROUP BY VehicleInventoryItemID 
  HAVING COUNT(*) > 1) 
*/  

--SELECT * FROM #ReconWip 
--ReconWIP: ts1, ts2(pull - pb), mba(FromThru) WIP
-- these are the wip hours that occurred IN the pull-pb interval - only
  
-- IS this the time AND place for conversion to BusHours? 
-- so back to TryingToGetFunctionToWork.sql
-- yep, this seems ok now
-- 8/21 think i've got the function working  Utilities.BusinessHoursFromInterval 
-- SELECT * FROM #ReconWIP
SELECT stocknumber, dayname(@ts1), @ts1, dayname(@ts2), @ts2, 
  timestampdiff(sql_tsi_hour, @ts1, @ts2) AS TotalHours,
--  (SELECT utilities.BusinessHoursFromInterval(@ts1, @ts2) FROM system.iota) AS TotalBusHours,
  MechFrom, MechThru, BodyFrom, BodyThru, AppFrom, AppThru,
  coalesce(timestampdiff(sql_tsi_hour, MechFrom, MechThru), 0) AS TotalMechHours,
  CASE
    WHEN MechFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(MechFrom, MechThru, 'Service') FROM system.iota)
    ELSE 0
  END AS MechBusHours,
  coalesce(timestampdiff(sql_tsi_hour, BodyFrom, BodyThru), 0) AS TotalBodyHours, 
  CASE
    WHEN BodyFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(BodyFrom, BodyThru, 'BodyShop') FROM system.iota)
    ELSE 0
  END AS BodyBusHours,
  coalesce(timestampdiff(sql_tsi_hour, AppFrom, AppThru), 0) AS TotalAppHours,  
  CASE
    WHEN AppFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(AppFrom, AppThru, 'Detail') FROM system.iota)
    ELSE 0
  END AS AppBusHours    
FROM #ReconWIP
-- looks pretty good
-- except 2 day intervals IS returning a negative number
-- ok, here's the problem, IF difference IS less than 24 hours, diff returns 0
-- so use: dayofyear(@ts2) - dayofyear(@ts1) to determine number of days enclosed IN the interval
-- fuck, that's wrong too, day of year won't WORK across years
-- but this works:  CASE timestampdiff(sql_tsi_day, CAST(@ts1 AS sql_date), CAST(@ts2 AS sql_date))
-- what about the notion of store business hours  
SELECT stocknumber, TotalHours, (TotalMechHours + TotalBodyHours + TotalAppHours) AS TotalWipHours,
  CASE 
    WHEN (TotalMechHours + TotalBodyHours + TotalAppHours) = 0 THEN 0
    ELSE (round(((BusMechHours*1.0 + BusBodyHours*1.0 + BusAppHours*1.0)/TotalHours), 2)*100)
  END AS PercWIP,
  TotalHours - (BusMechHours + BusBodyHours + BusAppHours) AS BasketHours
FROM (
  SELECT stocknumber, dayname(@ts1), @ts1, dayname(@ts2), @ts2, 
    timestampdiff(sql_tsi_hour, @ts1, @ts2) AS TotalHours,
--    (SELECT utilities.BusinessHoursFromInterval(@ts1, @ts2) FROM system.iota) AS TotalBusHours,
    MechFrom, MechThru, BodyFrom, BodyThru, AppFrom, AppThru,
    coalesce(timestampdiff(sql_tsi_hour, MechFrom, MechThru), 0) AS TotalMechHours,
    CASE
      WHEN MechFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(MechFrom, MechThru, 'Service') FROM system.iota)
      ELSE 0
    END AS BusMechHours,
    coalesce(timestampdiff(sql_tsi_hour, BodyFrom, BodyThru), 0) AS TotalBodyHours, 
    CASE
      WHEN BodyFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(BodyFrom, BodyThru, 'BodyShop') FROM system.iota)
      ELSE 0
    END AS BusBodyHours,
    coalesce(timestampdiff(sql_tsi_hour, AppFrom, AppThru), 0) AS TotalAppHours,  
    CASE
      WHEN AppFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(AppFrom, AppThru, 'Detail') FROM system.iota)
      ELSE 0
    END AS BusAppHours    
  FROM #ReconWIP) wtf
  
SELECT avg(PercWIP) -- average touchtime 
FROM (
SELECT stocknumber, TotalHours, (TotalMechHours + TotalBodyHours + TotalAppHours) AS TotalWipHours,
  CASE 
    WHEN (TotalMechHours + TotalBodyHours + TotalAppHours) = 0 THEN 0
    ELSE (round(((BusMechHours*1.0 + BusBodyHours*1.0 + BusAppHours*1.0)/TotalHours), 2)*100)
  END AS PercWIP,
  TotalHours - (BusMechHours + BusBodyHours + BusAppHours) AS BasketHours
FROM (
  SELECT stocknumber, dayname(@ts1), @ts1, dayname(@ts2), @ts2, 
    timestampdiff(sql_tsi_hour, @ts1, @ts2) AS TotalHours,
--    (SELECT utilities.BusinessHoursFromInterval(@ts1, @ts2) FROM system.iota) AS TotalBusHours,
    MechFrom, MechThru, BodyFrom, BodyThru, AppFrom, AppThru,
    coalesce(timestampdiff(sql_tsi_hour, MechFrom, MechThru), 0) AS TotalMechHours,
    CASE
      WHEN MechFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(MechFrom, MechThru, 'Service') FROM system.iota)
      ELSE 0
    END AS BusMechHours,
    coalesce(timestampdiff(sql_tsi_hour, BodyFrom, BodyThru), 0) AS TotalBodyHours, 
    CASE
      WHEN BodyFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(BodyFrom, BodyThru, 'BodyShop') FROM system.iota)
      ELSE 0
    END AS BusBodyHours,
    coalesce(timestampdiff(sql_tsi_hour, AppFrom, AppThru), 0) AS TotalAppHours,  
    CASE
      WHEN AppFrom IS NOT NULL THEN (select Utilities.BusinessHoursFromInterval(AppFrom, AppThru, 'Detail') FROM system.iota)
      ELSE 0
    END AS BusAppHours    
  FROM #ReconWIP) wtf)x  
  
-- now the issue IS TotalWIP > TotalBus  

