/* wrong vehicle walked, so we need to undo the walk 
-- VehicleInventoryItemStatuses 
raw materials
SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = 'e74190de-d593-48dc-9f71-d94d3e66aa97'
walk pending
SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = '91ee8f28-86eb-4ef5-b0c5-7070a9599f81'
part of the problem ON this one IS fucking trent removed ALL the recon after walking the wrong vehicle

-- this workd "ok", but, did NOT remove the pricing FROM 3/15, OR 3rdPartyValues FROM 3/15





*/

DECLARE @VehicleInventoryItemID string;
DECLARE @TS timestamp;
DECLARE @PricingID string;

--DECLARE @VehicleItemID string;
--DECLARE @VehicleEvaluationID string;
--DECLARE @VehicleInventoryItemID string;
DECLARE @ReconAuthorizationID string;
DECLARE @TableName string;
DECLARE @Stmt string;
DECLARE @VehicleWalkID string;
-- ALL the fucking bt/tk
DECLARE @BasisTables cursor AS
  SELECT parent
  FROM system.columns
  WHERE name = 'TableKey'
  AND parent <> 'VehicleInventoryItems';
  
@VehicleInventoryItemID = '9254a658-f5d2-4362-8cc6-57ee5e9453ad';
@TS = (
  SELECT fromts
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
    AND status = 'RMFlagIP_InspectionPending');
    
  
@PricingID = (
  SELECT VehiclePricingID
  FROM VehiclePricings
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
    AND basistable = 'VehicleWalks');
    
--@VehicleItemID = (SELECT VehicleItemID FROM __input);
@VehicleWalkID = (SELECT VehicleWalkID FROM VehicleWalks WHERE VehicleInventoryItemID = @VehicleInventoryItemID);
SELECT @VehicleWalkID FROM system.iota;   
BEGIN TRANSACTION;
TRY    
  -- VehicleInventoryItemStatuses     
  -- trent 
  DELETE FROM VehicleInventoryItemStatuses
  --SELECT * FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
    AND category IN ('AppearanceReconProcess',
      'MechanicalReconProcess',
      'BodyReconProcess')
    AND fromts <> @TS;    
    
  UPDATE VehicleInventoryItemStatuses   
  SET ThruTS = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND category = 'RMFlagWP';
    
  -- pricing
  DELETE 
  FROM VehiclePricingDetails 
  WHERE VehiclePricingID = @PricingID;
  
  DELETE FROM VehiclePricings
  WHERE VehiclePricingID = @PricingID;
  
  -- VehicleItemPhysicalLocations - leave it WHERE it is
  
  -- notes
  DELETE 
  FROM VehicleInventoryItemNotes
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
    AND category = 'VehiclePricing';
    
  -- recon (ReconAuthorizations,AuthorizedReconItems,VehicleReconItems)
  -- ReconAuthorizations & VehicleReconItems get handled BY @BasisTables 
  DELETE 
  FROM AuthorizedReconItems 
  WHERE VehicleReconItemID IN (
    SELECT VehicleReconItemID
    FROM VehicleReconItems
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
      AND tablekey = @VehicleWalkID); 
  
  -- ALL the fucking bt/tk  
  OPEN @BasisTables; 
  TRY
    WHILE FETCH @BasisTables DO 
      @TableName = TRIM(@BasisTables.parent);
      @Stmt = 'delete FROM ' + '' + @TableName + '' + ' where TableKey = ' + '''' + trim(@VehicleWalkID) + '''';
      EXECUTE immediate @Stmt;
    END WHILE;
  FINALLY
    CLOSE @BasisTables;
  END TRY;   
  
  DELETE FROM VehicleWalks WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
  EXECUTE PROCEDURE UpdateVehicleInventoryItemStatuses(@VehicleInventoryItemID,
    (SELECT partyid FROM users WHERE username = 'jon'), now());
     
  COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END;    
  
  
