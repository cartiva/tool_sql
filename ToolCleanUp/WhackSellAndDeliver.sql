
DECLARE @VehicleInventoryItemID string;
DECLARE @SalesConsultant string;
DECLARE @SalesConsultantID string;
DECLARE @Manager string;
DECLARE @ManagerID string;
DECLARE @Price integer;
DECLARE @Buyer string;
DECLARE @FundingType string;
DECLARE @SaleNote string;
DECLARE @DeliverNote string;
DECLARE @NowTS timestamp;
DECLARE @SaleID string;
DECLARE @UserID string;

/*
DealFunding_Cash
DealFunding_Finance
DealFunding_OutsideLien
*/

@VehicleInventoryItemID = 'c73bf863-f956-43ca-81b3-ea58c2f19f6d';
@SalesConsultant = 'Amanda Syverson';
@Manager = 'Dan Lizakowski';
@Price = 9640;
@Buyer = 'Lekh Pokhrel';
@FundingType = 'DealFunding_Finance';
@SaleNote = '';
@DeliverNote = '';
@NowTS = '04/19/2016 18:18:18';
@SalesConsultantID = coalesce((SELECT PartyID FROM people WHERE fullname = @SalesConsultant), @SalesConsultant);
@ManagerID = coalesce((SELECT partyid FROM people WHERE fullname = @Manager), @Manager);
@UserID = (SELECT partyid FROM users WHERE username = 'jon');
@SaleID = (SELECT newidstring(d) FROM system.iota);

BEGIN TRANSACTION;
TRY
  TRY 
    EXECUTE PROCEDURE SellVehicle(
      @SaleID,
      @VehicleInventoryItemID, 
      @UserID,
      @SalesConsultantID,
      @ManagerID,
      @Price,
      @Buyer,
      @FundingType,
      NULL,
      @SaleNote,
      @NowTS);
  CATCH ALL
    Raise WhackException(100, 'Sell Vehicle: ' + __errText);
  END TRY;  
  TRY
    EXECUTE PROCEDURE DeliverVehicle(
      @VehicleInventoryItemID,
      @UserID,
      @DeliverNote,
      @NowTS);
  CATCH ALL
    RAISE WhackException(100, 'Deliver Vehicle: ' + __errText);
  END TRY;
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY      