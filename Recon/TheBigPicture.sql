SELECT max(vii.stocknumber), max(vii.fromts), ra.VehicleInventoryItemID, COUNT(*) 
FROM ReconAuthorizations ra
INNER JOIN VehicleInventoryItems vii ON vii.VehicleInventoryItemID = ra.VehicleInventoryItemID 
WHERE vii.thruts IS NULL
GROUP BY ra.VehicleInventoryItemID 
ORDER BY COUNT(*) DESC

SELECT *
FROM ReconAuthorizations
WHERE VehicleInventoryItemID = '596bd0a4-f4fc-41dd-a074-ef415d2616a1'

SELECT *
FROM VehicleReconItems
WHERE VehicleInventoryItemID = '596bd0a4-f4fc-41dd-a074-ef415d2616a1'


-- ALL ARI's
SELECT ra.ReconAuthorizationID, left(ra.BasisTable, 25) AS RABasisTable,/*ra.VehicleInventoryItemID,*/ ra.fromts AS RAFrom, ra.thruts AS RAThru,
  ari.VehicleReconItemID, substring(ari.typ, position('_' IN ari.typ) + 1, 18) AS AriTyp, 
  substring(ari.status, position('_' IN ari.status) + 1, 12) AS AriStatus, ari.startts AS AriStart, ari.completets AS AriComplete,
  CASE
    WHEN vri.typ LIKE '%Appear%' THEN 'Appearance'
    WHEN vri.typ LIKE 'Body%' THEN 'Body'
    WHEN vri.typ LIKE 'Mech%' THEN 'Mechanical'
  END AS Dept,
  trim(substring(vri.typ, position('_' IN vri.typ) + 1, 19)) + ': ' + vri.description,
  vri.TotalPartsAmount + vri.LaborAmount AS Amount
FROM reconAuthorizations ra
LEFT JOIN AuthorizedReconItems ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
LEFT JOIN VehicleReconItems vri ON ari.VehicleReconItemID = vri.VehicleReconItemID
WHERE ra.VehicleInventoryItemID = '306cb405-b37d-49a9-b319-745be8f99aba'
AND ra.BasisTable <> 'VehicleInspections'
ORDER BY ra.FromTS, ari.VehicleReconItemID

-- completed ARI's
SELECT ra.ReconAuthorizationID, left(ra.BasisTable, 25) AS RABasisTable, ra.VehicleInventoryItemID, ra.fromts AS RAFrom, ra.thruts AS RAThru,
  ari.VehicleReconItemID, substring(ari.typ, position('_' IN ari.typ) + 1, 12) AS AriTyp, 
  substring(ari.status, position('_' IN ari.status) + 1, 12) AS AriStatus, ari.startts AS AriStart, ari.completets AS AriComplete,
  CASE
    WHEN vri.typ LIKE '%Appear%' THEN 'Appearance'
    WHEN vri.typ LIKE 'Body%' THEN 'Body'
    WHEN vri.typ LIKE 'Mech%' THEN 'Mechanical'
  END AS Dept,
  trim(substring(vri.typ, position('_' IN vri.typ) + 1, 19)) + ': ' + vri.description,
  vri.TotalPartsAmount + vri.LaborAmount AS Amount
FROM reconAuthorizations ra
LEFT JOIN AuthorizedReconItems ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
LEFT JOIN VehicleReconItems vri ON ari.VehicleReconItemID = vri.VehicleReconItemID
WHERE ra.VehicleInventoryItemID = '596bd0a4-f4fc-41dd-a074-ef415d2616a1'
AND ra.BasisTable <> 'VehicleInspections'
AND ari.Status = 'AuthorizedReconItem_Complete'
ORDER BY ra.FromTS, ari.VehicleReconItemID

-- this IS ALL well AND good, but am concerned that this IS NOT a one size fits ALL
-- includes ALL authorizations but NOT necessarily ALL VehicleReconItems

-- VehicleReconItems NOT Authorized:
SELECT wtf.stocknumber, vri.*
FROM VehicleReconItems vri
INNER JOIN (
  SELECT stocknumber, VehicleInventoryItemID  
  FROM VehicleInventoryItems 
  WHERE ThruTS IS NULL) wtf ON wtf.VehicleInventoryItemID = vri.VehicleInventoryItemID 
WHERE NOT EXISTS (
  SELECT 1
  FROM AuthorizedReconItems
  WHERE VehicleReconItemID = vri.VehicleReconItemID)
ORDER BY wtf.stocknumber   
--------------------------------------------------------------------------------
-- AuthorizedReconItems with no associated VehicleReconItem

SELECT *
-- SELECT COUNT(*) 
FROM AuthorizedReconItems arix
WHERE NOT exists (
  SELECT 1
  FROM VehicleReconItems 
  WHERE VehicleReconItemID = arix.VehicleReconItemID)
  
-- these are ALL old migrated data  
SELECT viix.stocknumber, viix.fromts, viix.thruts, rax.*
FROM ReconAuthorizations rax
LEFT JOIN VehicleInventoryItems viix ON viix.VehicleInventoryItemID = rax.VehicleInventoryItemID 
WHERE rax.ReconAuthorizationID IN (
  SELECT ReconAuthorizationID  
  FROM AuthorizedReconItems arix
  WHERE NOT exists (
    SELECT 1
    FROM VehicleReconItems 
    WHERE VehicleReconItemID = arix.VehicleReconItemID))  
ORDER BY viix.fromts DESC  
--------------------------------------------------------------------------------   
  

