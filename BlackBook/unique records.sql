SELECT *
FROM blackbookadt
WHERE model = 'impala'

-- vin vinyear uvc are suppose to be unique

SELECT vin, vinyear, uvc, blackbookpubdate
FROM blackbookadt
GROUP BY vin, vinyear, uvc, blackbookpubdate
HAVING COUNT(*) > 1