-- ry1 vehicles acq IN 2014
SELECT a.stocknumber, cast(a.fromts AS sql_date) AS acqDate, 
  cast(b.fromts AS sql_date) AS pullDate, 
  cast(c.fromts AS sql_date) AS availDate,
  cast(a.thruts AS sql_Date) AS soldDate
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'RMFlagPulled_Pulled'
INNER JOIN VehicleInventoryItemStatuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
  AND c.status = 'RMFlagAV_Available'  
WHERE year(a.fromts) = 2014
  AND stocknumber NOT LIKE 'h%'  
ORDER BY stocknumber  
  
   
21957a 
the thing with dups, there IS nothing IN the data to correlate a pull event
with an available event, so what i get IS cartesion

-- ry1 vehicles acq IN 2014
SELECT a.stocknumber, cast(a.fromts AS sql_date) AS acqDate, 
  cast(b.fromts AS sql_date) AS pullFromDate, cast(b.thruts AS sql_date) AS pullThruDate,
  cast(c.fromts AS sql_date) AS availFromDate, cast(c.thruts AS sql_date) AS availThruDate,
  cast(a.thruts AS sql_Date) AS soldDate
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'RMFlagPulled_Pulled'
INNER JOIN VehicleInventoryItemStatuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
  AND c.status = 'RMFlagAV_Available'  
--  AND NOT EXISTS (
--    SELECT 1
--    FROM VehicleInventoryItemStatuses 
--    WHERE VehicleInventoryItemID = b.VehicleInventoryItemID 
--      AND c.fromts > b.fromts)
WHERE year(a.fromts) = 2014
AND stocknumber = '21957A'
AND cast(b.fromts AS sql_date) <
  AND stocknumber NOT LIKE 'h%'  
ORDER BY stocknumber 

SELECT COUNT(DISTINCT stocknumber) -- 761
FROM (
SELECT a.stocknumber, cast(a.fromts AS sql_date) AS acqDate, 
  cast(b.fromts AS sql_date) AS pullDate, 
  cast(c.fromts AS sql_date) AS availDate,
  cast(a.thruts AS sql_Date) AS soldDate
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'RMFlagPulled_Pulled'
INNER JOIN VehicleInventoryItemStatuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
  AND c.status = 'RMFlagAV_Available'  
WHERE year(a.fromts) = 2014
  AND stocknumber NOT LIKE 'h%') x
  
SELECT COUNT(*) -- 848
FROM VehicleInventoryItems a
INNER JOIN VehicleInventoryItemStatuses b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  AND b.status = 'RMFlagPulled_Pulled'
INNER JOIN VehicleInventoryItemStatuses c on a.VehicleInventoryItemID = c.VehicleInventoryItemID
  AND c.status = 'RMFlagAV_Available'  
WHERE year(a.fromts) = 2014
  AND stocknumber NOT LIKE 'h%'  