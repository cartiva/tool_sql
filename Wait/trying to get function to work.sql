/*
the input interval ts1 AND ts2 are ON the same day (Day(ts2 - ts1) = 0
OPEN: start of business day
CLOSE: END of business day 
            Opens                         Closes         Test                                                  Output 
              |----------------------------|
1.    ts1|-------------|ts2                            ts1 < Opens AND ts2 > Opens AND ts2 <= Closes          ts2 - Opens
2.    ts1|-------------------------------------|ts2    ts1 < Opens AND ts2 >= Closes                          Closes- ts1           
3          ts1|--------------|ts2                      ts1 >= Opens AND ts1 < Closes AND ts2 <= Closes        ts2 - ts1
4.         ts1|--------------------------------|ts2    ts1 >= Opens AND Opens < Closes AND ts2 >= Closes      Closes - ts1                                                       
*/
/*
the input interval ts1 AND ts2 are consecutive days (Day(ts2 - ts1) = 1
patterns 1 & 3 never apply
patterns 2 & 4 can be simplified
  no need to check ts2::closes ON day 1
  no need to check ts1::opens ON day 2
/*
      SUM(CASE WHEN d.DayOfWeek IN (2,3,4,5,6) THEN wh.TotalHours ELSE 0 END) AS WeekDayHours,
      SUM(CASE WHEN d.DayOfWeek = 1 THEN wh.TotalHours ELSE 0 END) AS SundayHours,
      SUM(CASE WHEN d.DayOfWeek = 7 THEN wh.TotalHours ELSE 0 END) AS SaturdayHours, 
*/      

  
DECLARE @ts1 timestamp;
DECLARE @ts2 timestamp;    
--@ts1 = cast('05/26/2011 08:40:42' AS sql_timestamp); -- Wednesday 
--@ts2 = cast('05/27/2011 10:44:52' as sql_timestamp); -- Friday
@ts1 = cast('07/06/2011 08:06:32' AS sql_timestamp);
@ts2 = cast('07/07/2011 07:37:53' as sql_timestamp);
--SELECT cast('07/06/2011 08:06:32' AS sql_timestamp) from system.iota -- - timestampdiff(sql_tsi_hour,cast('07/07/2011 07:37:53' as sql_timestamp)) FROM system.iota
--ok, here's the problem, IF difference IS less than 24 hours, diff returns 0
--SELECT timestampdiff(sql_tsi_day, cast('07/06/2011 08:06:32' AS sql_timestamp), cast('07/07/2011 07:37:53' AS sql_timestamp)) from system.iota
--so
-- SELECT dayofyear(@ts2) - dayofyear(@ts1) FROM system.iota
-- fuck, that's wrong too, day of year won't WORK across years
-- but this works:  CASE timestampdiff(sql_tsi_day, CAST(@ts1 AS sql_date), CAST(@ts2 AS sql_date))
-- this IS WHERE i'll put logic for DAY(ts2-ts1) = 0/1/2...   
-- which IS necessary to make the function WORK, NOT to mention,getting accurate results 
SELECT 
--      CASE timestampdiff(sql_tsi_day, @ts1, @ts2)

--      CASE dayofyear(@ts2) - dayofyear(@ts1)
      CASE timestampdiff(sql_tsi_day, CAST(@ts1 AS sql_date), CAST(@ts2 AS sql_date))
        WHEN 0 THEN -- interval covers single day 
        MAX(
          CASE
            WHEN hour(@ts1) < hour(wh.opens) --1
              AND hour(@ts2) > hour(wh.opens) 
              AND hour(@ts2) <= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(wh.opens)  
            WHEN hour(@ts1) < hour(wh.opens) --2
              AND hour(@ts2) > iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)  
            WHEN hour(@ts1) >= hour(wh.opens) --3
              AND hour(@ts1) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) 
              AND hour(@ts2) <= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(@ts1)
            WHEN hour(@ts1) > hour(wh.opens) -- 4
              AND hour(wh.opens) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes))  
              AND hour(@ts2) >= iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)
          END) 
        WHEN 1 THEN -- interval covers 2 days 
        MAX(
          CASE  -- day 1
            WHEN hour(@ts1) < hour(wh.Opens) THEN wh.TotalHours --2
            ELSE iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1)
          END 
        +
          CASE -- day 2
            WHEN hour(@ts2) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN hour(@ts2) - hour(wh.Opens) --3
            ELSE wh.TotalHours
          END) 
        ELSE -- interval convers more than 2 days  
-- interval spans more than one week
-- day 1, 2 ...may NOT matter use dayofyear?
-- so IS this now just weekday + saturday + sunday
--  how DO i eliminate the first AND last days
-- don't eliminate, get total, THEN remove day 1 AND day2
          SUM(CASE WHEN d.DayOfWeek IN (2,3,4,5,6) THEN wh.TotalHours ELSE 0 END) 
          +
          SUM(CASE WHEN d.DayOfWeek = 1 THEN wh.TotalHours ELSE 0 END) 
          +
          SUM(CASE WHEN d.DayOfWeek = 7 THEN wh.TotalHours ELSE 0 END) 
          -
          MAX(
            CASE           
              WHEN (DayOfYear(@ts1) = d.DayOfYear and Year(@ts1) = d.TheYear) THEN -- matches first day of interval to the correct day
                CASE 
                  WHEN hour(@ts1) < hour(wh.Opens) THEN 0 -- nothing to subtract, interval covers entire day 1 -- 24 - wh.TotalHours --2
                  ELSE wh.TotalHours - (iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) - hour(@ts1))  
                END
              ELSE 0
            END 
            +
            CASE 
              WHEN (DayOfYear(@ts2) = d.DayOfYear and Year(@ts2) = d.TheYear) THEN -- matches last day of interval to the correct day
                CASE -- day 2
                  WHEN hour(@ts2) < iif(hour(wh.Closes)= 0, 24, hour(wh.Closes)) THEN wh.TotalHours - ((hour(@ts2) - hour(wh.Opens))) --3
                  ELSE 0 -- 24 - wh.TotalHours
                END
              ELSE 0
            END)
                             
      END AS BusHours
    FROM (-- any query that generates 2 timestamps: @ts1 < @ts2
      SELECT  @ts1,  @ts2
      FROM system.iota) ts,
      dds.Day d,
      WorkingHours wh
    WHERE wh.DayOfWeek = d.DayOfWeek 
    AND d.TheDate >= CAST(@ts1 AS sql_date)  
    AND d.TheDate <= CAST(@ts2 AS sql_date)
/*    
	AND 
      CASE timestampdiff(sql_tsi_day, @ts1, @ts2)
        WHEN 0 THEN d.TheDate >= CAST(@ts1 AS sql_date)  
          AND d.TheDate <= CAST(@ts2 AS sql_date) 
        WHEN 0 THEN d.TheDate >= CAST(@ts1 AS sql_date)  
          AND d.TheDate <= CAST(@ts2 AS sql_date)
        ELSE d.TheDate > CAST(@ts1 AS sql_date) -- leaves out first 
          AND d.TheDate < CAST(@ts2 AS sql_date) -- AND last day         
      END     
*/      




