-- how may IP at 8AM & 8PM, how many completed each day
-- we are IN a sitch WHERE there are 48 IN inspection pending AND mike does NOT
-- have a plan, wilkie IS freaking out
-- my thought IS some sort of baseline for reserved capacity for inspections

select
  vii.VehicleInventoryItemID, vii.StockNumber,
  coalesce(vii.CurrentPriority, 0) AS CurrentPriority, 
  vi.VIN,
  vi.YearModel, vi.Make, vi.Model
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii
 on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
INNER JOIN VehicleItems vi 
  ON vi.VehicleItemID = vii.VehicleItemID
 
WHERE viis.Status = 'RMFlagIP_InspectionPending'
AND viis.ThruTS IS NULL
AND NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = viis.VehicleInventoryITemID
  AND (status = 'RMFlagTNA_TradeNotAvailable' OR status = 'RMFlagPIT_PurchaseInTransit') 
  AND ThruTS IS NULL);
  
  
SELECT b.stocknumber, a.fromts, a.thruts
FROM VehicleInventoryItemStatuses a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations c on b.owningLocationID = c.partyid
WHERE a.status = 'RMFlagIP_InspectionPending'
  AND c.name = 'Rydells'
  AND CAST(a.fromts AS sql_date) > curdate() - 90
 

-- at what point does it become inspection pending
-- RMFlagIP thruts IS NULL AND IF ever PIT/TNA, one tick past that thruts  
SELECT b.stocknumber, a.fromts, a.thruts
FROM VehicleInventoryItemStatuses a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations c on b.owningLocationID = c.partyid
INNER JOIN dds.day d on CAST(a.fromts AS sql_date) = d.thedate
WHERE a.status = 'RMFlagIP_InspectionPending'
  AND c.name = 'Rydells'
  AND NOT EXISTS (
    SELECT 1
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID 
      AND status IN ('RMFlagTNA_TradeNotAvailable','RMFlagPIT_PurchaseInTransit')
      AND coalesce(thruTS, CAST('12/31/9999 01:00:00' AS sql_timestamp) < a.fromTS
