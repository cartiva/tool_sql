-- input: timestamp, number of hours
-- output: timestamp of input + number of hours accomodating available business hours
-- input must be during working hours

DECLARE @ints timestamp;
-- DECLARE @outts timestamp;
DECLARE @hours integer;

@ints = '10/22/2012 10:00:00';
@hours = 27;

--SELECT a.thedate, b.* 
--FROM dds.day a, workinghours b
--WHERE b.dept = 'BodyShop'
--  AND a.thedate BETWEEN curdate() AND curdate() + 30
--  AND a.dayofweek = b.dayofweek
  
  
  
SELECT @ints, @hours, d.thedate, wh.totalhours
FROM dds.Day d, WorkingHours wh
WHERE wh.DayOfWeek = d.DayOfWeek 
--  AND @ts1 BETWEEN wh.FromTS AND coalesce(wh.ThruTS, CAST('12/31/3030 00:00:00' AS sql_timestamp))
AND d.TheDate >= CAST(@ints AS sql_date)  
--  AND d.TheDate <= CAST(@ts2 AS sql_date)
AND //added 2001-10-12
  (d.Holiday is NULL OR d.Holiday = False)
AND wh.Dept = 'BodyShop'
ORDER BY thedate;


SELECT a.thedate, SUM(running)
FROM ( 
  SELECT d.thedate, wh.totalhours,27 AS hours
  FROM dds.Day d, WorkingHours wh
  WHERE wh.DayOfWeek = d.DayOfWeek 
    AND d.TheDate >= cast(CAST('10/22/2012 10:00:00' AS sql_timestamp) AS sql_date) 
    AND (d.Holiday is NULL OR d.Holiday = False)
    AND wh.Dept = 'BodyShop') a
INNER JOIN (
  SELECT thedate, SUM(totalhours) AS running 
  FROM dds.Day d, WorkingHours wh
  WHERE wh.DayOfWeek = d.DayOfWeek 
    AND d.TheDate >= cast(CAST('10/22/2012 10:00:00' AS sql_timestamp) AS sql_date) 
    AND (d.Holiday is NULL OR d.Holiday = False)
    AND wh.Dept = 'BodyShop'
    GROUP BY thedate) b ON b.thedate < a.thedate  
WHERE b.thedate < a.thedate
GROUP BY a.thedate    





