SELECT c.yearmonth, b.fullname, description, COUNT(*)
FROM correctionlog a 
INNER JOIN people b ON a.partyid = b.partyid
INNER JOIN dds.day c ON CAST(a.correctionts AS sql_date) = c.thedate
WHERE year(a.correctionts) = 2013
  AND description LIKE 'Ev%'
GROUP BY c.yearmonth, b.fullname, description