CREATE PROCEDURE GetMostRecentAddsDeductsTotalByVehicleItem
   ( 
      VehicleItemID CHAR ( 38 ),
      AddsDeductsTotal Integer OUTPUT
   ) 
BEGIN 
            /*
DROP PROCEDURE GetMostRecentAddsDeductsTotalByVehicleItem;
EXECUTE PROCEDURE GetMostRecentAddsDeductsTotalByVehicleItem('a3aae-709a-2245-b8d0-91cd6e62db38')
*/

DECLARE @AddsDeductsTotal Integer;

@AddsDeductsTotal = (SELECT AddsDeductsTotal 
                       FROM BlackBookValues bbv
                       WHERE bbv.TableKey = (SELECT TOP 1 bbv.TableKey 
					                           FROM BlackBookValues bbv
                                               WHERE  bbv.VehicleItemID = (SELECT VehicleItemID 
											                               FROM __INPUT)
						                       ORDER BY BlackBookValueTS DESC));
IF @AddsDeductsTotal is NULL THEN
  @AddsDeductsTotal = 0;
END;											   
 
insert into [__output] VALUES (@AddsDeductsTotal); 



   
END;

EXECUTE PROCEDURE sp_ModifyProcedureProperty( 'GetMostRecentAddsDeductsTotalByVehicleItem', 
   'COMMENT', 
   '');

select *
from blackbookvalues

-- CLOSE but no banana
-- need NOT most recently recorded value (blackbookvalues), but most recent values
SELECT stocknumber,yearmodel, make, model,  price - bb
FROM (
select viix.stocknumber, vix.yearmodel, left(vix.make,10) AS make, left(vix.model,10) AS model, 
  b.CleanBase + b.CleanMileageAdjustment + b.AddsDeductsTotal AS BB,
(SELECT TOP 1 vpd.Amount
        FROM VehiclePricings vp
        INNER JOIN VehiclePricingDetails vpd ON vpd.VehiclePricingID = vp.VehiclePricingID 
          AND vpd.Typ = 'VehiclePricingDetail_BestPrice'
        WHERE vp.VehicleInventoryItemID = viix.VehicleInventoryItemID 
        ORDER BY VehiclePricingTS DESC) AS Price 
from VehicleInventoryItems viix
INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
LEFT JOIN BlackBookValues b ON vix.VehicleItemID = b.VehicleItemID 
  AND BlackBookValueTS = (
    SELECT MAX(BlackBookValueTS)
	FROM BlackBookValues
	WHERE VehicleItemID = vix.VehicleItemID
	GROUP BY VehicleItemID)
WHERE viix.ThruTS IS NULL ) wtf
ORDER BY price - bb