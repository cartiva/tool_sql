
-- for each day IN the past year
--------------------------------------------------------------------------------
-- Calendar
--------------------------------------------------------------------------------
-- DROP TABLE #cal
DECLARE @back integer;
CREATE TABLE #cal (
  theDate date);
@back = 0;
WHILE @back < 366 do
  INSERT INTO #cal 
  select curdate() - @back FROM system.iota;
  @back = @back + 1;
END WHILE;

--------------------------------------------------------------------------------
-- tally
--------------------------------------------------------------------------------
-- DROP TABLE #cal
DECLARE @back integer;
CREATE TABLE #tally (
  n integer);
@back = 0;
WHILE @back < 1001 do
  INSERT INTO #tally 
  select @back FROM system.iota;
  @back = @back + 1;
END WHILE;

--------------------------------------------------------------------------------
-- for preceeding 30 days rydell cars < 6000 retailed
--------------------------------------------------------------------------------
SELECT c.theDate, COUNT(v.ThruTS) AS theCount
FROM (
  SELECT viix.ThruTS
    FROM VehicleInventoryItems viix
    INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
      AND vs.typ = 'VehicleSale_Retail'
      AND vs.SoldAmount < 6001
    INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID
    INNER JOIN MakeModelClassifications mmcx ON vix.model = mmcx.model
      AND mmcx.VehicleType = 'VehicleType_Car'
    WHERE viix.OwningLocationID = (
      SELECT partyid
      FROM Organizations
      WHERE name = 'rydells')) v,
#cal c
WHERE CAST(v.ThruTS AS sql_date) <= c.TheDate
AND CAST(v.ThruTS AS sql_date) >= c.TheDate - 30
GROUP BY c.theDate

--------------------------------------------------------------------------------
-- for each day: rydell cars < 6000 fresh available
--------------------------------------------------------------------------------
SELECT c.theDate, COUNT(v.FromTS) theCount -- fresh available
FROM
#cal c, (
  SELECT viix.stocknumber, viisx.FromTS, viisx.ThruTS -- coalesce(viisx.ThruTS, now()) AS ThruTS
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleInventoryItemStatuses viisx ON viix.VehicleInventoryItemID = viisx.VehicleInventoryItemID
    AND viisx.FromTS = (
      SELECT MAX(viisx.FromTS)
      FROM VehicleInventoryItemStatuses
      WHERE category = 'RMFlagAV'
      AND VehicleInventoryItemID = viix.VehicleInventoryItemID)
    AND viisx.category = 'RMFlagAV'
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
  INNER JOIN MakeModelClassifications mmcx ON vix.model = mmcx.model
    AND mmcx.VehicleType = 'VehicleType_Car'
  INNER JOIN VehiclePricings vpx ON viix.VehicleInventoryItemID = vpx.VehicleInventoryItemID
    AND vpx.VehiclePricingID = (
      SELECT MAX(VehiclePricingID)
      FROM VehiclePricings
      WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID)  
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
    AND vpdx.Amount < 6001  
  WHERE viix.LocationID = ( -- responsible party, NOT owner
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')) v 
WHERE CAST(v.FromTS AS sql_date) <= c.theDate 
AND CAST(coalesce(v.ThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) >= c.theDate
AND c.theDate - CAST(v.FromTS AS sql_date) < 31
GROUP BY c.theDate


--------------------------------------------------------------------------------
-- for each day: rydell cars < 6000 owned
--------------------------------------------------------------------------------

SELECT c.theDate, COUNT(v.Stocknumber) theCount -- for each day: rydell cars < 6000 owned
FROM
#cal c, (
  SELECT viix.stocknumber, viix.FromTS, viix.ThruTS
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
  INNER JOIN MakeModelClassifications mmcx ON vix.model = mmcx.model
    AND mmcx.VehicleType = 'VehicleType_Car'
  INNER JOIN VehiclePricings vpx ON viix.VehicleInventoryItemID = vpx.VehicleInventoryItemID
    AND vpx.VehiclePricingID = (
      SELECT MAX(VehiclePricingID)
      FROM VehiclePricings
      WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID)  
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
    AND vpdx.Amount < 6001 
  WHERE viix.LocationID = ( -- responsible party, NOT owner
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')) v 
WHERE CAST(v.FromTS AS sql_date) <= c.TheDate
AND CAST(coalesce(v.ThruTS, timestampadd(sql_tsi_year, 5, now())) AS sql_date) > c.theDate    
GROUP BY c.theDate

--------------------------------------------------------------------------------
-- for each day: rydell cars < 6000 retailed
--------------------------------------------------------------------------------
SELECT c.theDate, COUNT(v.Stocknumber) theCount -- for each day: rydell cars < 6000 retailed
FROM
#cal c, (
  SELECT viix.stocknumber, viix.FromTS, viix.ThruTS
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
    AND vs.typ = 'VehicleSale_Retail'
    AND vs.SoldAmount < 6001  
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
  INNER JOIN MakeModelClassifications mmcx ON vix.model = mmcx.model
    AND mmcx.VehicleType = 'VehicleType_Car'
  WHERE viix.ThruTS IS NOT NULL 
  AND viix.OwningLocationID = ( --  owner
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')) v 
WHERE CAST(v.ThruTS AS sql_date) = c.theDate    
GROUP BY c.theDate

--------------------------------------------------------------------------------
-- for each day: rydell cars < 6000 wholesaled
--------------------------------------------------------------------------------
SELECT c.theDate, COUNT(v.Stocknumber) theCount -- for each day: rydell cars < 6000 wholesaled
FROM
#cal c, (
  SELECT viix.stocknumber, viix.FromTS, viix.ThruTS
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
    AND vs.typ = 'VehicleSale_Wholesale'
    AND vs.SoldAmount < 6001  
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
  INNER JOIN MakeModelClassifications mmcx ON vix.model = mmcx.model
    AND mmcx.VehicleType = 'VehicleType_Car'
  WHERE viix.ThruTS IS NOT NULL 
  AND viix.OwningLocationID = ( --  owner
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')) v 
WHERE CAST(v.ThruTS AS sql_date) = c.theDate    
GROUP BY c.theDate

--------------------------------------------------------------------------------
-- for preceeding 30 days: rydell cars < 6000 
-- retailed total, sold out of raw (was never available), sold out of available
--------------------------------------------------------------------------------
SELECT c.theDate, COUNT(v.ThruTS) AS theCount,
  COUNT(CASE WHEN v.VehicleInventoryItemID IS NULL THEN 1 END) AS Raw,
  COUNT(CASE WHEN v.VehicleInventoryItemID IS NOT NULL THEN 1 END) AS Available
FROM #cal c, 
(
  SELECT viix.ThruTS, av.VehicleInventoryItemID 
    FROM VehicleInventoryItems viix
    INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
      AND vs.typ = 'VehicleSale_Retail'
      AND vs.SoldAmount < 6001
    INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID
    INNER JOIN MakeModelClassifications mmcx ON vix.model = mmcx.model
      AND mmcx.VehicleType = 'VehicleType_Car'
    LEFT JOIN VehicleInventoryItemStatuses av ON viix.VehicleInventoryItemID = av.VehicleInventoryItemID
      AND category = 'RMFlagAV'
    WHERE viix.OwningLocationID = (
      SELECT partyid
      FROM Organizations
      WHERE name = 'rydells')
    AND viix.ThruTS IS NOT NULL) v
WHERE CAST(v.ThruTS AS sql_date) < c.TheDate
AND CAST(v.ThruTS AS sql_date) >= c.TheDate - 30
GROUP BY c.theDate

--------------------------------------------------------------------------------
-- for preceeding 30 days rydell cars retailed
--------------------------------------------------------------------------------
SELECT c.theDate, COUNT(v.ThruTS) AS theCount
FROM (
  SELECT viix.ThruTS
    
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
    AND vs.typ = 'VehicleSale_Retail'
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID
  INNER JOIN MakeModelClassifications mmcx ON vix.model = mmcx.model
    AND mmcx.VehicleType = 'VehicleType_Car'
  WHERE viix.OwningLocationID = (
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')) v,
#cal c
WHERE CAST(v.ThruTS AS sql_date) <= c.TheDate
AND CAST(v.ThruTS AS sql_date) >= c.TheDate - 30
GROUP BY c.theDate


--------------------------------------------------------------------------------
-- AND the 2 together
--------------------------------------------------------------------------------
SELECT s.theDate, s.Sales, a.theCount
FROM ( 
-- plug IN the other query here it will be aliased AS 'v'
) v

LEFT JOIN (
-- plug IN the other query here it will be aliased AS 'a'

  ) a ON v.theDate = a.theDate 
ORDER BY v.theDate desc


