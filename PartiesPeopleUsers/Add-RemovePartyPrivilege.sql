/*i
-- ADD app carpic
INSERT INTO applicationusers 
values('B2B31AC2-B6A8-4F46-A9DE-B606EF33B433', 'carpic', 2, 120, NULL, NULL, now(), NULL)

12/2/12: ry1 detail needs ry2 recon privileges to schedule, corey, kevin & ben
*/
-- PartyPrivileges
/*
'Privilege_Internet'
'Privilege_InvMgr'
'Privilege_Recon'
'Privilege_SC'
'Privilege_Biller'
'Privilege_SalesManager'
'Privilege_AdjustRecon'
'Privilege_WholesaleWithinMarket'
'GF-Crookston'
'GF-Honda Cartiva'
'GF-Rydells'
/*
UPDATE users SET active = true WHERE username = 'jolderbak'

*/
DECLARE @Username string;
DECLARE @PartyID string;
DECLARE @Privilege string;
DECLARE @LocationID string; 
DECLARE @NowTS timestamp;
DECLARE @Location string;

@NowTS = Now();
@Username = 'drichardson';
@Privilege = 'Privilege_InvMgr';
@Location = 'GF-Rydells';
@LocationID = (SELECT PartyID FROM Organizations WHERE Fullname = @Location);
@PartyID = (SELECT PartyID FROM users WHERE username = @Username AND active = true);

  IF (SELECT COUNT(*) FROM Users WHERE username = @username) = 0 THEN
    RAISE UserException( 101, 'Bad Username');
  END IF;
  IF (SELECT COUNT(*) FROM Privileges WHERE privilege = @privilege) = 0 THEN
    RAISE UserException( 102, 'Bad Privilege');
  END IF; 
  IF @LocationID IS NULL OR @LocationID = '' THEN
    RAISE UserException( 103, 'Missing LocationID');
  END IF;   
  IF @PartyID IS NULL OR @PartyID = '' THEN
    RAISE UserException( 104, 'No PartyID');
  END IF;
-- UPDATE partyPrivileges SET thruTS = @nowTS WHERE partyID = @partyID 
--   AND privilege = @privilege AND locationid = @locationid;  
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@PartyID, @LocationID, @Privilege, @NowTS);

10/28/15: dan lizakowski FROM ry2 sales mgr to ry1 sc
12/4/15: AND he goes back to honda AS a sales mgr

DECLARE @Username string;
DECLARE @PartyID string;
DECLARE @Privilege string;
DECLARE @LocationID string; 
DECLARE @NowTS timestamp;
DECLARE @Location string;

@NowTS = Now();
@Username = 'dlizakowski';
@Privilege = 'Privilege_SalesManager';
@Location = 'GF-Honda Cartiva';
@LocationID = (SELECT PartyID FROM Organizations WHERE Fullname = @Location);
@PartyID = (SELECT PartyID FROM users WHERE username = @Username AND active = true);

  IF (SELECT COUNT(*) FROM Users WHERE username = @username) = 0 THEN
    RAISE UserException( 101, 'Bad Username');
  END IF;
  IF (SELECT COUNT(*) FROM Privileges WHERE privilege = @privilege) = 0 THEN
    RAISE UserException( 102, 'Bad Privilege');
  END IF; 
  IF @LocationID IS NULL OR @LocationID = '' THEN
    RAISE UserException( 103, 'Missing LocationID');
  END IF;   
  IF @PartyID IS NULL OR @PartyID = '' THEN
    RAISE UserException( 104, 'No PartyID');
  END IF;
-- UPDATE partyPrivileges SET thruTS = @nowTS WHERE partyID = @partyID 
--   AND privilege = @privilege AND locationid = @locationid;  
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@PartyID, @LocationID, @Privilege, @NowTS);
  
/*

6/13/13
richard kacher moved FROM bdc (Privilege_Internet) to sales consultant 'Privilege_SC'
SELECT * FROM users WHERE username = 'rkacher'
partyid : '32945f2c-ae70-914f-8fca-346f66874c80'

UPDATE partyprivileges
SET thruTS = now()
WHERE partyid = '32945f2c-ae70-914f-8fca-346f66874c80'
  AND privilege = 'Privilege_Internet'
  AND thruts IS NULL 


SELECT *
FROM partyprivileges
WHERE partyid = '1c8e9795-cb16-4145-a0af-5bff938350e6'

-- 2/17/12
-- remove mgr privileges FROM anthony michael (after adding sc priv)
UPDATE partyprivileges
SET thruTS = now()
WHERE partyid = '1c8e9795-cb16-4145-a0af-5bff938350e6'
AND Privilege IN ('Privilege_AdjustRecon','Privilege_SalesManager','Privilege_WholesaleWithin')

SELECT *
FROM partyprivileges
WHERE partyid = '1c8e9795-cb16-4145-a0af-5bff938350e6'



UPDATE PartyPrivileges
SET ThruTS = now()
WHERE partyid = (SELECT PartyID FROM users WHERE username = 'dnice')
AND Privilege = 'Privilege_Recon';  


  INSERT INTO PartyRelationShips (PartyID1, PartyID2, Typ, FromTS)
    VALUES(
	'A4847DFC-E00E-42E7-89EE-CD4368445A82', 
	(SELECT partyid FROM users WHERE username = 'jolderbak'), 'PartyRelationship_MarketBuyers', Now()); 
	
SELECT * FROM partyrelationships WHERE Typ = 'PartyRelationship_MarketLocations'	

SELECT * FROM partyrelationships WHERE partyid2 = (SELECT partyid FROM users WHERE username = 'jolderbak')


-- remove honda FROM trent

SELECT * FROM partyprivileges WHERE partyid = 'A8DE4389-EC8C-4AAD-91F9-FA439E5CED2D'
select

UPDATE PartyPrivileges
SET ThruTS = now()
WHERE partyid = (SELECT PartyID FROM users WHERE username = 'tolson')
AND locationid = (SELECT partyid FROM organizations WHERE name = 'Honda Cartiva')
 

-- 1/4/13
-- anthony michael sales consultant -> team leader
-- ADD adjustrecon, salesmanager, wholsesalewithin
-- revome salesconsultant
UPDATE partyprivileges
SET thruTS = now()
WHERE partyid = '1c8e9795-cb16-4145-a0af-5bff938350e6'
AND Privilege IN ('Privilege_AdjustRecon','Privilege_SalesManager','Privilege_WholesaleWithin')

4/7/15
jimmy weber & josh barker have become sales mgrs,
shut off the sc access

UPDATE partyprivileges
SET thruts = now()
WHERE partyid in ('31592037-f1ee-9d41-b11d-288e63743956','28792e78-1a4d-1c4e-b6c3-346bbde3705d')
AND privilege = 'Privilege_SC'

UPDATE partyprivileges
SET thruts = now()
WHERE partyid = (
  SELECT partyid
  FROM users
  WHERE username = 'cbjerk')
AND privilege = 'Privilege_SC'   

*/

-- 6/28/17 corey eden needs manager access 
-- currently only AS sc, both ry1 & ry2
SELECT *
FROM partyprivileges
WHERE partyid = (SELECT partyid FROM people WHERE lastname = 'eden')

SELECT * FROM users WHERE username LIKE 'ced%'
-- let's see how josh barker IS configured: WholesaleWithinMarket,AdjustRecon, SalesManager
SELECT *
FROM partyprivileges
WHERE partyid = (SELECT partyid FROM people WHERE lastname = 'barker')

-- CLOSE out sc access
UPDATE partyprivileges
SET thruts = now()
WHERE partyid = (SELECT partyid FROM people WHERE lastname = 'eden')
  AND privilege = 'Privilege_SC';
  
DECLARE @Username string;
DECLARE @PartyID string;
DECLARE @Privilege string;
DECLARE @LocationID string; 
DECLARE @NowTS timestamp;
DECLARE @Location string;
--
@NowTS = Now();
@Username = 'ceden';
@Privilege = 'Privilege_WholesaleWithinMarket';
@Location = 'GF-Rydells';
@LocationID = (SELECT PartyID FROM Organizations WHERE Fullname = @Location);
@PartyID = (SELECT PartyID FROM users WHERE username = @Username AND active = true);
  IF (SELECT COUNT(*) FROM Users WHERE username = @username) = 0 THEN
    RAISE UserException( 101, 'Bad Username');
  END IF;
  IF (SELECT COUNT(*) FROM Privileges WHERE privilege = @privilege) = 0 THEN
    RAISE UserException( 102, 'Bad Privilege');
  END IF; 
  IF @LocationID IS NULL OR @LocationID = '' THEN
    RAISE UserException( 103, 'Missing LocationID');
  END IF;   
  IF @PartyID IS NULL OR @PartyID = '' THEN
    RAISE UserException( 104, 'No PartyID');
  END IF;
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@PartyID, @LocationID, @Privilege, @NowTS);
  
-- CLOSE out sc access
UPDATE partyprivileges
SET thruts = now()
WHERE partyid = (SELECT partyid FROM people WHERE lastname = 'eden')
  AND privilege = 'Privilege_SC';  