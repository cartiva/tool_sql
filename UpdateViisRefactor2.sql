DECLARE @VehicleInventoryItemID string;
DECLARE @MechReconStatus string;
DECLARE @BodyReconStatus string;
DECLARE @AppReconStatus string;
DECLARE @AppViis string;
@VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496';
-- need the variable OUTPUT to feed the rest of updviis
@MechReconStatus = (
  SELECT 
    case coalesce (y.status, 1) 
      WHEN 3 THEN 'MechanicalReconProcess_InProcess'
      WHEN 2 THEN 'MechanicalReconProcss_NotStarted'
      ELSE 'MechanicalReconProcess_NoIncompleteReconItems'
    END
  FROM (
    SELECT *
      FROM (
        SELECT DISTINCT category
        FROM typcategories
        WHERE category = 'MechanicalReconItem') a, ReconAuthorizations b
        WHERE b.VehicleInventoryItemID = @VehicleInventoryItemID
          AND b.thruts IS NULL) x
    LEFT JOIN (
      SELECT a.VehicleInventoryItemID, d.category,
        MAX(
          case 
            when b.status = 'AuthorizedReconItem_InProcess' THEN 3
            WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
            WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
            ELSE 1
          END) AS status
      FROM ReconAuthorizations a
      LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
      LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
      LEFT JOIN typcategories d ON c.typ = d.typ
      WHERE a.VehicleInventoryItemID = @VehicleInventoryItemID
        AND a.thruts IS NULL 
      GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category);  
@BodyReconStatus = (
  SELECT 
    case coalesce (y.status, 1) 
      WHEN 3 THEN 'BodyReconProcess_InProcess'
      WHEN 2 THEN 'BodyReconProcss_NotStarted'
      ELSE 'BodyReconProcess_NoIncompleteReconItems'
    END
  FROM (
    SELECT *
      FROM (
        SELECT DISTINCT category
        FROM typcategories
        WHERE category = 'BodyReconItem') a, ReconAuthorizations b
        WHERE b.VehicleInventoryItemID = @VehicleInventoryItemID
          AND b.thruts IS NULL) x
    LEFT JOIN (
      SELECT a.VehicleInventoryItemID, d.category,
        MAX(
          case 
            when b.status = 'AuthorizedReconItem_InProcess' THEN 3
            WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
            WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
            ELSE 1
          END) AS status
      FROM ReconAuthorizations a
      LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
      LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
      LEFT JOIN typcategories d ON c.typ = d.typ
      WHERE a.VehicleInventoryItemID = @VehicleInventoryItemID
        AND a.thruts IS NULL 
      GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category);    
@AppReconStatus = (
  SELECT 
    MAX(
      case coalesce (y.status, 1) 
        WHEN 3 THEN 'AppearanceReconProcess_InProcess'
        WHEN 2 THEN 'AppearanceReconProcss_NotStarted'
        ELSE 'AppearanceReconProcess_NoIncompleteReconItems'
      END)
  FROM (
    SELECT *
      FROM (
        SELECT DISTINCT category
        FROM typcategories
        WHERE category IN ('AppearanceReconItem','PartyCollection')) a, ReconAuthorizations b
        WHERE b.VehicleInventoryItemID = @VehicleInventoryItemID
          AND b.thruts IS NULL) x
    LEFT JOIN (
      SELECT a.VehicleInventoryItemID, d.category,
        MAX(
          case 
            when b.status = 'AuthorizedReconItem_InProcess' THEN 3
            WHEN b.status = 'AuthorizedReconItem_NotStarted' THEN 2
            WHEN b.status = 'AuthorizedReconItem_Complete' THEN 1
            ELSE 1
          END) AS status
      FROM ReconAuthorizations a
      LEFT JOIN AuthorizedReconItems b ON a.ReconAuthorizationID = b.ReconAuthorizationID 
      LEFT JOIN VehicleReconItems c ON b.VehicleReconItemID = c.VehicleReconItemID 
      LEFT JOIN typcategories d ON c.typ = d.typ
      WHERE a.VehicleInventoryItemID = @VehicleInventoryItemID
        AND a.thruts IS NULL 
      GROUP BY a.VehicleInventoryItemID, d.category) y ON x.category = y.category);     
/*       
SELECT @MechReconStatus, @BodyReconStatus, @AppReconStatus FROM system.iota; 

SELECT *
FROM VehicleInventoryItemStatuses
WHERE category IN ('AppearanceReconProcess','BodyReconProcess','MechanicalReconProcess') 
  AND ThruTS IS NULL 
  AND VehicleInventoryItemID = @VehicleInventoryItemID;
/*
IF @MechReconStatus = 'MechanicalReconProcess_InProcess'  
IF @MechReconStatus = 'MechanicalReconProcess_NoIncompleteReconItems'
IF @MechReconStatus = 'MechanicalReconProcess_NotStarted'
IF @BodyReconStatus = 'BodyReconProcess_InProcess'
IF @BodyReconStatus = 'BodyReconProcess_NoIncompleteReconItems'
IF @BodyReconStatus = 'BodyReconProcess_NotStarted'
IF @AppReconStatus = 'AppearanceReconProcess_InProcess'
IF @AppReconStatus = 'AppearanceReconProcess_NoIncompleteReconItems'
IF @AppReconStatus = 'AppearanceReconProcess_NotStarted'
*/
/*
-- need to make viis.status agree w/@status
SELECT @AppReconStatus, status, category, substring(@AppReconStatus, position('_' IN @AppReconStatus) + 1, 20),
  substring(@AppReconStatus, position('_' IN @AppReconStatus) + 1, length(@AppReconStatus) - position('_' IN @AppReconStatus) + 1)
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND ThruTS IS NULL 
  and category = 'AppearanceReconProcess';
  
SELECT *
FROM (
  SELECT VehicleInventoryItemID, category, status
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
  AND ThruTS IS NULL
  AND category = 'MechanicalReconProcess') a
LEFT JOIN (
  SELECT VehicleInventoryItemID, category, status
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
  AND ThruTS IS NULL
  AND category = 'AppearanceReconProcess') b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN (  
  SELECT VehicleInventoryItemID, category, status
  FROM VehicleInventoryItemStatuses 
  WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
  AND ThruTS IS NULL
  AND category = 'BodyReconProcess') c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID;
  
-- JOIN this up to a SET of @MBAReconStatuses: 1 each for a VehicleInventoryItem
-- here's everything i need IN 1 row
SELECT *
FROM (
  SELECT *
  FROM (
    SELECT VehicleInventoryItemID, category, status
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
    AND ThruTS IS NULL
    AND category = 'MechanicalReconProcess') a
  LEFT JOIN (
    SELECT VehicleInventoryItemID, category, status
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
    AND ThruTS IS NULL
    AND category = 'AppearanceReconProcess') b ON a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  LEFT JOIN (  
    SELECT VehicleInventoryItemID, category, status
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
    AND ThruTS IS NULL
    AND category = 'BodyReconProcess') c ON b.VehicleInventoryItemID = c.VehicleInventoryItemID) x, 
  (SELECT @MechReconStatus AS rM, @BodyReconStatus AS rB, @AppReconStatus AS rA
  FROM system.iota) y;
*/ 
-- put a test around this to ensure 1 row per viid/category
SELECT VehicleInventoryItemID, category, status, @MechReconStatus
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
AND ThruTS IS NULL
AND category = 'MechanicalReconProcess'
UNION
SELECT VehicleInventoryItemID, category, status, @BodyReconStatus
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
AND ThruTS IS NULL
AND category = 'BodyReconProcess'
UNION
SELECT VehicleInventoryItemID, category, status, @AppReconStatus
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = 'ed175e51-5a22-4377-ac66-3067b5817496'
AND ThruTS IS NULL
AND category = 'AppearanceReconProcess';
  

/* 
SELECT @MechReconStatus, status, category, substring(@AppReconStatus, position('_' IN @MechReconStatus) + 1, 20),
  substring(@MechReconStatus, position('_' IN @MechReconStatus) + 1, length(TRIM(@MechReconStatus)) - position('_' IN @MechReconStatus) + 1)
FROM VehicleInventoryItemStatuses 
WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
  AND ThruTS IS NULL 
  and category = 'MechanicalReconProcess';  
*/
-- IF they agree, nothing to DO
/*
IF @AppReconStatus <> (
    SELECT status
    FROM VehicleInventoryItemStatuses 
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
      AND ThruTS IS NULL 
      and category = 'AppearanceReconProcess') THEN
SELECT 'wtf' FROM system.iota; 
END IF;   
*/
/*
@status         viis.stats      DO
InProcess       InProcess       nothing
InProcess       NotStarted      1. CLOSE NOT started    2. INSERT InProcess
InProcess       NoIncomplete    1. CLOSE NoIncomplete   2. INSERT InProcess
NotStarted      InProcess       1. CLOSE InProcess      2. INSERT NotStarted   
NotStarted      NotStarted      nothing
NotStarted      NoIncomplete    1. CLOSE NotStarted     2. INSERT NoIncomplete
NoIncomplete    InProcess       1. CLOSE NoIncomplete   2. INSERT InProcess
NoIncomplete    NotStarted      1. CLOSE NotStarted     2. INSERT NoIncomplete
NoIncomplete    InProcess       nothing
*/

