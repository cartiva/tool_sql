-- vehicles ws'd FROM honda with no walk 
SELECT *
FROM VehicleInventoryItems vii
WHERE locationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
AND NOT EXISTS (
  SELECT 1
  FROM VehicleWalks
  WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID)
AND NOT EXISTS (
  SELECT 1
  FROM VehiclePricings
  WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID) 
AND NOT EXISTS (
  SELECT 1
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagIP'
  AND thruts IS NULL
  AND VehicleInventoryItemID = vii.VehicleInventoryItemID)  
AND thruts IS null 
ORDER BY stocknumber 


SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '15ca8ef5-a744-0b4e-8de6-42c266b13d5e'

INSERT INTO VehicleInventoryItemStatuses
values('15ca8ef5-a744-0b4e-8de6-42c266b13d5e','RMFlagWP_WalkPending', 'RMFlagWP','09/10/2010 05:28:39', NULL, NULL, NULL, 'VehicleInventoryItems', '15ca8ef5-a744-0b4e-8de6-42c266b13d5e', '52397361-c9d6-3e47-91a9-7fbe64aa2eff')

DELETE FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '15ca8ef5-a744-0b4e-8de6-42c266b13d5e'
AND category = 'RMFlagWP'