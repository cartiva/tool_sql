/*******************************************************************************
2/2/11 - doesn't WORK for multiple VehicleReconItems 

*******************************************************************************/
/*
5/21/12,
fuck, so give me some scripts for handling multiple VehicleReconItems 
have to change VehicleInventoryItemStatuses & AuthorizedReconItems 
just fucking DO them one at a time for each VehicleReconItems
*/
DECLARE @VehicleInventoryItemID string;
DECLARE @UserID string;
DECLARE @VehicleReconItemID string;
DECLARE @ReconAuthorizationID string;
DECLARE @Dept string;
DECLARE @InProcess string;
DECLARE @NotStarted string;
@VehicleInventoryItemID = 'da5272cf-ff5b-450e-93a5-9e80550e3f34';
@UserID = (SELECT partyid FROM users WHERE username = 'jon');

@ReconAuthorizationID = (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
    AND ThruTS IS NULL);
 
/* multiple recon items -- need to DO the individual VehicleReconItems 
SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = 'da5272cf-ff5b-450e-93a5-9e80550e3f34' AND typ LIKE 'Mech%'

SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = 'da5272cf-ff5b-450e-93a5-9e80550e3f34'

SELECT * FROM AuthorizedReconItems WHERE VehicleReconItemID = '87af86df-26f1-4c07-a141-23f235c94088'
*/
/*   these need to be SET for each individual VehicleReconItems */
@VehicleReconItemID = '052f4498-52b1-4e45-a861-735ba387bb01';
--@Dept = 'Appearance';
@Dept = 'Mechanical';
--@Dept = 'Body';

@InProcess = @Dept + 'ReconProcess_InProcess';
@NotStarted = @Dept + 'ReconProcess_NotStarted';
    
/****************************************************************/
BEGIN TRANSACTION;
TRY
  DELETE
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID     
    AND status = @InProcess
    AND ThruTS IS NULL;
  
  UPDATE VehicleInventoryItemStatuses
  SET ThruTS = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
    AND status = @NotStarted
    AND ThruTS IS NOT NULL
    AND CAST(fromts AS sql_date) = '01/11/2017'; -- gets goofy IF there IS previous WORK done IN dept
  
  UPDATE AuthorizedReconItems -- little bit of overkill here ON ensuring the correct record
  SET Status = 'AuthorizedReconItem_NotStarted',
      StartTS = NULL
  WHERE ReconAuthorizationID = @ReconAuthorizationID 
    AND  VehicleReconItemID = @VehicleReconItemID 
    AND status = 'AuthorizedReconItem_InProcess'
    AND StartTS IS NOT NULL;      
  EXECUTE PROCEDURE UpdateViis( -- this may NOT always be necessary
    @VehicleInventoryItemID,
    @UserID,
    Now());
       
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END TRY;   


/*
SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '3255c49e-fe6c-4317-9197-4fdd759003d7'

-- ALL AuthorizedReconItems 
SELECT ra.ReconAuthorizationID, left(ra.BasisTable, 25) AS RABasisTable, ra.fromts AS RAFrom, ra.thruts AS RAThru,
  ari.VehicleReconItemID, substring(ari.typ, position('_' IN ari.typ) + 1, 12) AS AriTyp, 
  substring(ari.status, position('_' IN ari.status) + 1, 12) AS AriStatus, ari.startts AS AriStart, ari.completets AS AriComplete,
  CASE
    WHEN vri.typ LIKE '%Appear%' THEN 'Appearance'
    WHEN vri.typ LIKE 'Body%' THEN 'Body'
    WHEN vri.typ LIKE 'Mech%' THEN 'Mechanical'
  END AS Dept,
  trim(substring(vri.typ, position('_' IN vri.typ) + 1, 19)) + ': ' + vri.description,
  vri.TotalPartsAmount + vri.LaborAmount AS Amount
FROM reconAuthorizations ra
LEFT JOIN AuthorizedReconItems ari ON ari.ReconAuthorizationID = ra.ReconAuthorizationID
LEFT JOIN VehicleReconItems vri ON ari.VehicleReconItemID = vri.VehicleReconItemID
WHERE ra.VehicleInventoryItemID = '10e8e678-7f6d-4f8c-ac8d-126c8fed97aa'
AND ra.BasisTable <> 'VehicleInspections'
ORDER BY ra.FromTS, ari.VehicleReconItemID
*/

/*

need to be able to SELECT the correct VehicleReconItem

    VehicleReconItemID: 9bb2c203-4af4-7d48-bcbd-37a730faec50
    ReconAuthorizationID: 3e531ad4-d9e5-4ce8-9e70-468ab0bea950
    vri.typ: BodyReconItem_Glass
    vri.description: replace glass
    viis.BodyReconProcess_InProcess.FromTS : 12/23/2010 4:02:21 PM
        this IS also the ThruTS for viis.BodyReconProcess_NotStarted.ThruTS
        AS well AS the AuthorizedReconItem.StartTS
    

************only one AuthorizedReconItem IS IN wip that shouldn't be***********
need to
    AuthorizedReconItems: change status AND start time
    VehicleInventoryItemStatuses: 
        xxxReconProcess_InProcess - DELETE
        xxxReconProcess_NotStarted - NULL ThruTS
        
SELECT * FROM AuthorizedReconItems WHERE VehicleReconItemID = '9bb2c203-4af4-7d48-bcbd-37a730faec50';
SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID  = '10e8e678-7f6d-4f8c-ac8d-126c8fed97aa'  

SELECT * FROM ReconAuthorizations WHERE VehicleInventoryItemID = '10e8e678-7f6d-4f8c-ac8d-126c8fed97aa' AND thruts IS null 

SELECT *
FROM AuthorizedReconItems
WHERE ReconAuthorizationID = (  
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = '10e8e678-7f6d-4f8c-ac8d-126c8fed97aa'
    AND ThruTS IS NULL)
AND status = 'AuthorizedReconItem_InProcess'
AND CompleteTS IS NULL;

SELECT *
FROM VehicleReconItems
WHERE VehicleReconItemID = 'a3e1d76f-2115-234c-96ed-5e4a85328cb3'

SELECT DISTINCT typ FROM VehicleReconItems 

*/

/*
4/2/12 more pussy ON
the following un wips 3 body line items
BEGIN TRANSACTION;
TRY 
  DELETE
  --SELECT *
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = '41599daf-b943-4674-8074-603222c02028'
  AND FromTS = '03/29/2012 17:23:25';
  
  UPDATE VehicleInventoryItemStatuses 
  SET ThruTS = NULL 
  --SELECT *
  --FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = '41599daf-b943-4674-8074-603222c02028'
  AND ThruTS = '03/29/2012 17:23:25';
  
  UPDATE AuthorizedReconItems
  SET status = 'AuthorizedReconItem_NotStarted',
      startts = NULL 
  --SELECT *
  --FROM AuthorizedReconItems 
  WHERE ReconAuthorizationID = (
    SELECT ReconAuthorizationID
    FROM ReconAuthorizations
    WHERE VehicleInventoryItemID = '41599daf-b943-4674-8074-603222c02028'
    AND ThruTS IS NULL)
  AND status = 'AuthorizedReconItem_InProcess';  
  
  
  EXECUTE PROCEDURE UpdateVehicleInventoryItemStatuses('41599daf-b943-4674-8074-603222c02028',
    (SELECT partyid FROM users WHERE username = 'jon'),
    Now());
    
  COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END TRY;    

*/

/*
IF there IS a completed AuthorizedReconItem that has the same StartTS
AS the one want to fix brp_inproc ThruTS should the that completeTS 
i pussied out, this IS NOT yet complete
*/

DECLARE @VehicleInventoryItemID string;
DECLARE @UserID string;
DECLARE @VehicleReconItemID string;
DECLARE @ReconAuthorizationID string;
DECLARE @Dept string;
DECLARE @InProcess string;
DECLARE @NotStarted string;
@VehicleInventoryItemID = '6f7c6556-70cb-42e4-bc2c-a2e23c5b8657';
@UserID = (SELECT partyid FROM users WHERE username = 'jon');

@ReconAuthorizationID = (
  SELECT ReconAuthorizationID
  FROM ReconAuthorizations
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
    AND ThruTS IS NULL);
 
@VehicleReconItemID = (
  SELECT VehicleReconItemID
  FROM AuthorizedReconItems
  WHERE ReconAuthorizationID = (  
    SELECT ReconAuthorizationID
    FROM ReconAuthorizations
    WHERE VehicleInventoryItemID = @VehicleInventoryItemID
      AND ThruTS IS NULL)
  AND status = 'AuthorizedReconItem_InProcess'
  AND CompleteTS IS NULL);
 
/* multiple recon items -- need to DO the individual VehicleReconItems 

--@VehicleReconItemID = '606eecb2-e4da-4647-8d4e-205489279d93';
--@VehicleReconItemID = '3e922916-bdb8-7145-8370-2820b9473d10';    
@VehicleReconItemID = 'ad307a33-69c2-5a48-ac39-ac6982638aa8';
@Dept = 'Body'; 
*/

@Dept = (
  SELECT
    CASE
      WHEN typ LIKE 'Body%' THEN 'Body'
      WHEN typ LIKE 'Mechanical%' THEN 'Mechanical'
      WHEN typ LIKE '%Appearance%' THEN 'Appearance'
    END
  FROM VehicleReconItems
  WHERE VehicleReconItemID = @VehicleReconItemID); 

@InProcess = @Dept + 'ReconProcess_InProcess';
@NotStarted = @Dept + 'ReconProcess_NotStarted';
   

BEGIN TRANSACTION;
TRY

  DELETE
  FROM VehicleInventoryItemStatuses
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID     
    AND status = @InProcess
    AND ThruTS IS NULL;
  
  UPDATE VehicleInventoryItemStatuses
  SET ThruTS = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID 
    AND status = @NotStarted
    AND ThruTS IS NOT NULL;
  
  UPDATE AuthorizedReconItems -- little bit of overkill here ON ensuring the correct record
  SET Status = 'AuthorizedReconItem_NotStarted',
      StartTS = NULL
  WHERE ReconAuthorizationID = @ReconAuthorizationID 
    AND  VehicleReconItemID = @VehicleReconItemID 
    AND status = 'AuthorizedReconItem_InProcess'
    AND StartTS IS NOT NULL;  
    
  EXECUTE PROCEDURE UpdateVehicleInventoryItemStatuses( -- this may NOT always be necessary
    @VehicleInventoryItemID,
    @UserID,
    Now());
    
    
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END TRY;    