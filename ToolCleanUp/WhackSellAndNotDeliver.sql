
DECLARE @VehicleInventoryItemID string;
DECLARE @SalesConsultant string;
DECLARE @SalesConsultantID string;
DECLARE @Manager string;
DECLARE @ManagerID string;
DECLARE @Price integer;
DECLARE @Buyer string;
DECLARE @FundingType string;
DECLARE @SaleNote string;
DECLARE @DeliverNote string;
DECLARE @NowTS timestamp;
DECLARE @SaleID string;
DECLARE @UserID string;

/*
DealFunding_Cash
DealFunding_Finance
DealFunding_OutsideLien

need to figure out what to use AS default delivery date
*/

@VehicleInventoryItemID = 'ecc168cb-5a91-4a5e-bd5c-0bcf5eb129ef';
@SalesConsultant = 'Matt Flikka';
@Manager = 'Mike Tweeten';
@Price = 14599;
@Buyer = 'Norman Morris Roeder';
@FundingType = 'DealFunding_Cash';
@SaleNote = 'Manually updated to sold not delivered. At time of sale, was Inspection Pending, RJ gave a price. Inspected & Walked 9/6, per RJ, all recon work to be done.  Don''t know what the customer was told for a promise date';
@NowTS = '09/02/2010 19:19:19';
@SalesConsultantID = coalesce((SELECT PartyID FROM people WHERE fullname = @SalesConsultant), @SalesConsultant);
@ManagerID = coalesce((SELECT partyid FROM people WHERE fullname = @Manager), @Manager);
@UserID = (SELECT partyid FROM users WHERE username = 'jon');
@SaleID = (SELECT newidstring(d) FROM system.iota);

BEGIN TRANSACTION;
TRY
  TRY 
    EXECUTE PROCEDURE SellVehicle(
      @SaleID,
      @VehicleInventoryItemID, 
      @UserID,
      @SalesConsultantID,
      @ManagerID,
      @Price,
      @Buyer,
      @FundingType,
      NULL,
      @SaleNote,
      @NowTS);
  CATCH ALL
    Raise WhackException(100, 'Sell Vehicle: ' + __errText);
  END TRY;  
COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY      

