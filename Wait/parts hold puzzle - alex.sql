/*
need more test cases
*/

CREATE TABLE #t (
  id integer, 
  d1 date,
  d2 date);

-- DELETE FROM #t  
-- pattern 1 
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date)); 
-- pattern 2
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/10/2011' AS sql_date), CAST('08/12/2011' AS sql_date));
-- pattern 3  
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/02/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/12/2011' AS sql_date), CAST('08/14/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/03/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 4  
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/04/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 5  
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/06/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 6  
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/06/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/12/2011' AS sql_date), CAST('08/14/2011' AS sql_date));

  
--This SQL statement seems to get what you want (t is the table name of the 
--sampe table):

SELECT 
   d.duration, 
   d.duration - 
      IFNULL((
        SELECT SUM(timestampdiff(SQL_TSI_DAY, t1.d2, (SELECT MIN(d1) FROM #t t4 WHERE t4.id = t1.id AND t4.d1 > t1.d2))) 
        FROM #t t1 
        WHERE t1.id = d.id 
        AND (
          SELECT SUM(IIF(t1.d2 BETWEEN t2.d1 AND t2.d2, 1, 0)) 
          FROM #t t2 
          WHERE t2.id = t1.id 
          AND t2.d2 <> t1.d2 ) = 0 
          AND  d2 <> (
            SELECT MAX(d2) 
            FROM #t t3 WHERE t3.id = t1.id)), 0) "parts hold"
FROM (
  SELECT id, timestampdiff(SQL_TSI_DAY, MIN(d1), MAX(d2)) duration
  FROM #t GROUP BY id) d;

  
/*
The outer query gets the duration of the repair work. The complex subquery 
calculates the total number of days not waiting for parts. This is done by 
locating the start dates where the vehicle is not waiting for parts, and 
then count the number of days until it begins to wait for parts again:

1) The query for finding the starting dates when the vehicle is not waiting for parts, 
   i.e. finding all d2 that is not within any date range where the vehicle is waiting for part.

select d2 from t t1
where (select sum(IIF(t1.d2 between t2.d1 and t2.d2, 1, 0)) from t t2 
where t2.id = t1.id and t2.d2 <> t1.d2 ) = 0 AND 
           d2 <> (select max(d2) from t t3 where t3.id = t1.id))

2) The days where it vehicle is not waiting for part is the date from the above query till the vehicle is 
   waiting for part again

timestampdiff( SQL_TSI_DAY, t1.d2, ( SELECT min(d1) from t t4 where t4.id 
= t1.id and t4.d1 > t1.d2 ) )


Combining the two above and aggregating all such periods gives the number 
of days that the vehicle is not waiting for parts. The final query adds an 
extra condition to calculate result for each id from the outer query.

--
Alex
*/


--
-- revised version, posted to fix alleged problem with duplicate starting date of "no hold" periods

/* -- FROM newsgroup
Right after I posted that reply in the stackoverflow. I spot a problem with 
my solultion. If there are duplicate end dates that are the start of a no 
hold period, that period will be double counted. For example, if there is 
another part hold starting on 8/8 and ended on 8/10, then the statement will 
give incorrect result. To counter that, we need to get the distinct dates 
where there is no part hold. The modify the statement does the trick:

Another thing is that although this works, I am not certain how efficient 
this is going to be on a large table. On the other hand, this is getting 
the result for all ids in the table, If actual id is substituded in the statement 
to retrieve the result for one vehicle, then the performance should be good.
*/
--
SELECT
   d.id, 
   d.duration, 
   d.duration - 
   IFNULL((
     SELECT Sum(timestampdiff(SQL_TSI_DAY, no_hold.d2, (SELECT min(d1) FROM #t t4 WHERE t4.id = no_hold.id and t4.d1 > no_hold.d2)))
     FROM (
       SELECT DISTINCT id, d2 
       FROM #t t1 
       WHERE (
         SELECT sum(IIF(t1.d2 between t2.d1 and t2.d2, 1, 0 )) 
         FROM #t t2 
         WHERE t2.id = t1.id 
         AND t2.d2 <> t1.d2 ) = 0 
       AND d2 <> (
         SELECT  MAX(d2) 
         FROM #t t3 
         WHERE t3.id = t1.id))/* this was the missing paren*/ no_hold
     WHERE no_hold.id = d.id ), 0) "parts hold"
FROM (
  SELECT id, timestampdiff( SQL_TSI_DAY, min(d1), max(d2)) duration
  FROM #t GROUP BY id) d  

     
       
--The outer query gets the duration of the repair work. The complex subquery calculates 
--the total number of days not waiting for parts. 
--This is done by locating the start dates where the vehicle is not waiting for parts, 
--and then count the number of days until it begins to wait for parts again:  

// 1) The query for finding the starting dates when the vehicle is not waiting for parts, 
// i.e. finding all d2 that is not within any date range where the vehicle is waiting for part.
// The DISTINCT is needed to removed duplicate starting "no hold" period.

SELECT DISTINCT id, d2 
FROM #t t1
WHERE ( 
  SELECT sum(IIF(t1.d2 between t2.d1 and t2.d2, 1, 0)) 
  FROM #t t2 
  WHERE t2.id = t1.id and t2.d2 <> t1.d2) = 0 
  AND d2 <> (
    SELECT max(d2) 
    FROM #t t3 
    WHERE t3.id = t1.id) 
      
// 2) The days where it vehicle is not waiting for part is the date from the above query till the vehicle is // waiting for part again      
timestampdiff( SQL_TSI_DAY, no_hold.d2, ( SELECT min(d1) FROM t t4 WHERE t4.id = no_hold.id and t4.d1 > no_hold.d2 ) )

--Combining the two above and aggregating all such periods gives the number of days 
--that the vehicle is not waiting for parts. The final query adds an extra 
--condition to calculate result for each id from the outer query.
--This probably is not terribly efficient on very large table with many ids. 
--It should fine if the id is limited to one or just a few.