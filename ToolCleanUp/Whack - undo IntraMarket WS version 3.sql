stk# 22734C ('3912defd-32e9-46b5-9898-e2e8332a7a3b')
imws to honda on 11/24/14, they never booked it, should NOT have been wsales to honda
honda VehicleInventoryItemID : '9f7215f3-87a9-4edd-aeed-e3b90bd23a1b'

SELECT * 
FROM VehicleItemStatuses 
WHERE VehicleItemID = (
  SELECT VehicleItemID 
  FROM VehicleInventoryItems 
  WHERE VehicleInventoryItemID = '9f7215f3-87a9-4edd-aeed-e3b90bd23a1b')
  

DECLARE @hondaViiID string;
DECLARE @gmViiID string;
DECLARE @VehicleItemID string;
DECLARE @imwsDate date;
BEGIN TRANSACTION;
TRY  
@VehicleItemID = 'e5ec4583-f18e-0140-bb3b-49858ed373f0';
@hondaViiID = '9f7215f3-87a9-4edd-aeed-e3b90bd23a1b';
@gmViiID = '3912defd-32e9-46b5-9898-e2e8332a7a3b';
@imwsDate = '11/24/2014';
/*
-- VehicleItemStatuses
SELECT * 
FROM VehicleItemStatuses 
WHERE VehicleItemID = (
  SELECT VehicleItemID 
  FROM VehicleInventoryItems 
  WHERE VehicleInventoryItemID = '3912defd-32e9-46b5-9898-e2e8332a7a3b'); 
*/  
DELETE 
FROM VehicleItemStatuses
WHERE VehicleItemID = @VehicleItemID
  AND CAST(fromts AS sql_date) = @imwsDate;
   
UPDATE VehicleItemStatuses
SET thruts = CAST(NULL AS sql_timestamp)
WHERE VehicleItemID = @VehicleItemID
  AND CAST(thruts AS sql_date) =  @imwsDate;
/*   
-- honda only 1 VehicleInventoryItemStatuses, booking pending  
SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID ='3912defd-32e9-46b5-9898-e2e8332a7a3b'   
*/
DELETE 
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = @hondaViiID;
DELETE 
FROM VehicleInventoryItems 
WHERE VehicleInventoryItemID = @hondaViiID;

-- gm
/*
SELECT *
FROM vehicleSales
WHERE VehicleInventoryItemID = '3912defd-32e9-46b5-9898-e2e8332a7a3b'

SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = '3912defd-32e9-46b5-9898-e2e8332a7a3b'
  AND (CAST(fromts AS sql_date) = '11/24/2014' OR CAST(thruts AS sql_date) = '11/24/2014');
*/  
DELETE
FROM viiwtf 
WHERE VehicleInventoryItemID = @gmViiID;

DELETE
FROM vehicleSales 
WHERE VehicleInventoryItemID = @gmViiID;

DELETE 
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = @gmViiID
  AND status IN ('RawMaterials_Delivered','RawMaterials_Sold',
    'RMFlagWSB_WholesaleBuffer','RMFlagWTF_WTF','RMFlagPulled_Pulled',
    'RMFlagRB_ReconBuffer');

UPDATE VehicleInventoryItemStatuses
SET thruTS = CAST(NULL AS sql_timestamp)
WHERE VehicleInventoryItemID = @gmViiID
  AND CAST(thruTS AS sql_date) = @imwsDate;      

SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = @gmviiID
  AND (CAST(fromts AS sql_date) = '11/24/2014' OR CAST(thruts AS sql_date) = '11/24/2014');

UPDATE ReconAuthorizations
SET thruTs = CAST(NULL AS sql_timestamp)
WHERE VehicleInventoryItemID = @gmViiID
  AND cast(thruts AS sql_date) = @imwsDate;
  
UPDATE AuthorizedReconItems
SET typ = 'AuthorizedReconItem_Initial'
WHERE ReconAuthorizationID = (
  SELECT ReconAuthorizationID 
  FROM ReconAuthorizations 
  WHERE VehicleInventoryItemID = @gmViiID
    AND thruts IS NULL)
AND typ = 'AuthorizedReconItem_Removed';  

UPDATE VehicleInventoryItems
SET thruts = CAST(NULL AS sql_timestamp)
WHERE VehicleInventoryItemID = @gmViiID;
   
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;    
