SELECT * FROM MakeModelClassifications 

SELECT m.VehicleType, m.VehicleSegment, 
  CASE 
    WHEN d.amount BETWEEN 0 AND 6000 THEN '0-6k'
  	WHEN d.amount BETWEEN 6001 AND 8000 THEN '6-8k'
  	WHEN d.amount BETWEEN 8001 AND 10000 THEN '8-10k'
  	WHEN d.amount BETWEEN 10001 AND 12000 THEN '10-12k'
  	WHEN d.amount BETWEEN 12001 AND 14000 THEN '12-14k'
  	WHEN d.amount BETWEEN 14001 AND 16000 THEN '14-16k'
  	WHEN d.amount BETWEEN 16001 AND 18000 THEN '16-18k'
  	WHEN d.amount BETWEEN 18001 AND 20000 THEN '18-20k'
    WHEN d.amount BETWEEN 20001 AND 22000 THEN '20-22k'
  	WHEN d.amount BETWEEN 22001 AND 24000 THEN '22-24k'
  	WHEN d.amount BETWEEN 24001 AND 26000 THEN '24-26k'
  	WHEN d.amount BETWEEN 26001 AND 28000 THEN '26-28k'
  	WHEN d.amount BETWEEN 28001 AND 30000 THEN '28-30k'
  	WHEN d.amount BETWEEN 30001 AND 32000 THEN '30-32k'
  	WHEN d.amount BETWEEN 32001 AND 34000 THEN '32-34k'
  	WHEN d.amount BETWEEN 34001 AND 36000 THEN '34-36k'
  	WHEN d.amount BETWEEN 36001 AND 38000 THEN '36-38k'
  	WHEN d.amount BETWEEN 38001 AND 40000 THEN '38-40k'
  	WHEN d.amount BETWEEN 40001 AND 100000 THEN '40k+'
    END AS PriceBand
-- SELECT COUNT(*) -- current inventory
FROM VehicleInventoryItems v
LEFT JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
INNER JOIN MakeModelClassifications m ON i.make = m.make
  AND i.model = m.model
INNER JOIN (
  SELECT VehicleInventoryItemID, VehiclePricingID, VehiclePricingTS 
  FROM VehiclePricings x
  WHERE VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID  = x.VehicleInventoryItemID  
    GROUP BY VehicleInventoryItemID)) p ON v.VehicleInventoryItemID = p.VehicleInventoryItemID  
LEFT JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID
  AND d.typ = 'VehiclePricingDetail_BestPrice'
WHERE v.ThruTS IS NULL 
  AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
 
SELECT TRIM(t) + '-' + TRIM(s), PriceBand, COUNT(*) Owned
INTO #owned
FROM (  
  SELECT types.description AS t, segments.description AS s, 
    CASE 
      WHEN d.amount BETWEEN 0 AND 6000 THEN '0-6k'
    	WHEN d.amount BETWEEN 6001 AND 8000 THEN '6-8k'
    	WHEN d.amount BETWEEN 8001 AND 10000 THEN '8-10k'
    	WHEN d.amount BETWEEN 10001 AND 12000 THEN '10-12k'
    	WHEN d.amount BETWEEN 12001 AND 14000 THEN '12-14k'
    	WHEN d.amount BETWEEN 14001 AND 16000 THEN '14-16k'
    	WHEN d.amount BETWEEN 16001 AND 18000 THEN '16-18k'
    	WHEN d.amount BETWEEN 18001 AND 20000 THEN '18-20k'
		WHEN d.amount BETWEEN 20001 AND 22000 THEN '20-22k'
    	WHEN d.amount BETWEEN 22001 AND 24000 THEN '22-24k'
    	WHEN d.amount BETWEEN 24001 AND 26000 THEN '24-26k'
    	WHEN d.amount BETWEEN 26001 AND 28000 THEN '26-28k'
    	WHEN d.amount BETWEEN 28001 AND 30000 THEN '28-30k'
    	WHEN d.amount BETWEEN 30001 AND 32000 THEN '30-32k'
    	WHEN d.amount BETWEEN 32001 AND 34000 THEN '32-34k'
    	WHEN d.amount BETWEEN 34001 AND 36000 THEN '34-36k'
    	WHEN d.amount BETWEEN 36001 AND 38000 THEN '36-38k'
    	WHEN d.amount BETWEEN 38001 AND 40000 THEN '38-40k'
    	WHEN d.amount BETWEEN 40001 AND 100000 THEN '40k+'
      END AS PriceBand
  -- SELECT COUNT(*) -- current inventory
  FROM VehicleInventoryItems v
  LEFT JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
  INNER JOIN MakeModelClassifications m ON i.make = m.make
    AND i.model = m.model
  INNER JOIN (
    SELECT VehicleInventoryItemID, VehiclePricingID, VehiclePricingTS 
    FROM VehiclePricings x
    WHERE VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings
      WHERE VehicleInventoryItemID  = x.VehicleInventoryItemID  
      GROUP BY VehicleInventoryItemID)) p ON v.VehicleInventoryItemID = p.VehicleInventoryItemID  
  LEFT JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID
    AND d.typ = 'VehiclePricingDetail_BestPrice'
  LEFT JOIN TypDescriptions types ON m.VehicleType = types.Typ
  LEFT JOIN TypDescriptions segments ON m.VehicleSegment = segments.typ 
  WHERE v.ThruTS IS NULL 
    AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')) y  
GROUP BY TRIM(t) + '-' + TRIM(s), PriceBand
ORDER BY owned DESC 


SELECT COUNT(*) -- incoming vehicles - no price
FROM VehicleInventoryItems v
LEFT JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
INNER JOIN MakeModelClassifications m ON i.make = m.make
  AND i.model = m.model
left JOIN (
  SELECT VehicleInventoryItemID, VehiclePricingID, VehiclePricingTS 
  FROM VehiclePricings x
  WHERE VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID  = x.VehicleInventoryItemID  
    GROUP BY VehicleInventoryItemID)) p ON v.VehicleInventoryItemID = p.VehicleInventoryItemID  
WHERE v.ThruTS IS NULL 
  AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
  AND p.VehicleInventoryItemID IS NULL 


  
SELECT m.vehicletype, m.vehiclesegment, 
  CASE 
    WHEN soldamount BETWEEN 0 AND 6000 THEN '0-6k'
  	WHEN soldamount BETWEEN 6001 AND 8000 THEN '6-8k'
  	WHEN soldamount BETWEEN 8001 AND 10000 THEN '8-10k'
  	WHEN soldamount BETWEEN 10001 AND 12000 THEN '10-12k'
  	WHEN soldamount BETWEEN 12001 AND 14000 THEN '12-14k'
  	WHEN soldamount BETWEEN 14001 AND 16000 THEN '14-16k'
  	WHEN soldamount BETWEEN 16001 AND 18000 THEN '16-18k'
  	WHEN soldamount BETWEEN 18001 AND 20000 THEN '18-20k'
    WHEN soldamount BETWEEN 20001 AND 22000 THEN '20-22k'
  	WHEN soldamount BETWEEN 22001 AND 24000 THEN '22-24k'
  	WHEN soldamount BETWEEN 24001 AND 26000 THEN '24-26k'
  	WHEN soldamount BETWEEN 26001 AND 28000 THEN '26-28k'
  	WHEN soldamount BETWEEN 28001 AND 30000 THEN '28-30k'
  	WHEN soldamount BETWEEN 30001 AND 32000 THEN '30-32k'
  	WHEN soldamount BETWEEN 32001 AND 34000 THEN '32-34k'
  	WHEN soldamount BETWEEN 34001 AND 36000 THEN '34-36k'
  	WHEN soldamount BETWEEN 36001 AND 38000 THEN '36-38k'
  	WHEN soldamount BETWEEN 38001 AND 40000 THEN '38-40k'
  	WHEN soldamount BETWEEN 40001 AND 100000 THEN '40k+'
  END AS PriceBands 
-- SELECT COUNT(*) -- 172
FROM VehicleSales s
INNER JOIN VehicleInventoryItems v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID 
  AND v.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
LEFT JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
INNER JOIN MakeModelClassifications m ON i.make = m.make
  AND i.model = m.model  
WHERE typ = 'VehicleSale_Retail'
  AND status = 'VehicleSale_Sold'
  AND CAST(SoldTS AS sql_date) > curdate() - 30
  
/*------------------------------------------------------------------------------
#sold
#owned
------------------------------------------------------------------------------*/

SELECT TRIM(t) + '-' + TRIM(s) AS SizeAndShape, PriceBand AS PriceBand, COUNT(*) Owned
-- DROP TABLE #owned
INTO #owned
FROM (  
  SELECT types.description AS t, segments.description AS s, 
    CASE 
      WHEN d.amount BETWEEN 0 AND 6000 THEN '0-6k'
    	WHEN d.amount BETWEEN 6001 AND 8000 THEN '6-8k'
    	WHEN d.amount BETWEEN 8001 AND 10000 THEN '8-10k'
    	WHEN d.amount BETWEEN 10001 AND 12000 THEN '10-12k'
    	WHEN d.amount BETWEEN 12001 AND 14000 THEN '12-14k'
    	WHEN d.amount BETWEEN 14001 AND 16000 THEN '14-16k'
    	WHEN d.amount BETWEEN 16001 AND 18000 THEN '16-18k'
    	WHEN d.amount BETWEEN 18001 AND 20000 THEN '18-20k'
		WHEN d.amount BETWEEN 20001 AND 22000 THEN '20-22k'
    	WHEN d.amount BETWEEN 22001 AND 24000 THEN '22-24k'
    	WHEN d.amount BETWEEN 24001 AND 26000 THEN '24-26k'
    	WHEN d.amount BETWEEN 26001 AND 28000 THEN '26-28k'
    	WHEN d.amount BETWEEN 28001 AND 30000 THEN '28-30k'
    	WHEN d.amount BETWEEN 30001 AND 32000 THEN '30-32k'
    	WHEN d.amount BETWEEN 32001 AND 34000 THEN '32-34k'
    	WHEN d.amount BETWEEN 34001 AND 36000 THEN '34-36k'
    	WHEN d.amount BETWEEN 36001 AND 38000 THEN '36-38k'
    	WHEN d.amount BETWEEN 38001 AND 40000 THEN '38-40k'
    	WHEN d.amount BETWEEN 40001 AND 100000 THEN '40k+'
      END AS PriceBand
  -- SELECT COUNT(*) -- current inventory
  FROM VehicleInventoryItems v
  LEFT JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
  INNER JOIN MakeModelClassifications m ON i.make = m.make
    AND i.model = m.model
  INNER JOIN (
    SELECT VehicleInventoryItemID, VehiclePricingID, VehiclePricingTS 
    FROM VehiclePricings x
    WHERE VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings
      WHERE VehicleInventoryItemID  = x.VehicleInventoryItemID  
      GROUP BY VehicleInventoryItemID)) p ON v.VehicleInventoryItemID = p.VehicleInventoryItemID  
  LEFT JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID
    AND d.typ = 'VehiclePricingDetail_BestPrice'
  LEFT JOIN TypDescriptions types ON m.VehicleType = types.Typ
  LEFT JOIN TypDescriptions segments ON m.VehicleSegment = segments.typ 
  WHERE v.ThruTS IS NULL 
    AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
	AND NOT EXISTS (-- exclude WSB
	  SELECT 1
	  FROM VehicleInventoryItemStatuses
	  WHERE VehicleInventoryItemID = v.VehicleInventoryItemID
	    AND category = 'RMFlagWSB'
		AND ThruTS IS NULL)) y  
GROUP BY TRIM(t) + '-' + TRIM(s), PriceBand
ORDER BY owned DESC 
    
SELECT TRIM(t) + '-' + TRIM(s) AS SizeAndShape, PriceBand AS PriceBand, COUNT(*) AS Sales
-- DROP TABLE #sold
INTO #sold
FROM (
  SELECT types.description AS t, segments.description AS s, 
    CASE 
      WHEN soldamount BETWEEN 0 AND 6000 THEN '0-6k'
    	WHEN soldamount BETWEEN 6001 AND 8000 THEN '6-8k'
    	WHEN soldamount BETWEEN 8001 AND 10000 THEN '8-10k'
    	WHEN soldamount BETWEEN 10001 AND 12000 THEN '10-12k'
    	WHEN soldamount BETWEEN 12001 AND 14000 THEN '12-14k'
    	WHEN soldamount BETWEEN 14001 AND 16000 THEN '14-16k'
    	WHEN soldamount BETWEEN 16001 AND 18000 THEN '16-18k'
    	WHEN soldamount BETWEEN 18001 AND 20000 THEN '18-20k'
        WHEN soldamount BETWEEN 20001 AND 22000 THEN '20-22k'
    	WHEN soldamount BETWEEN 22001 AND 24000 THEN '22-24k'
    	WHEN soldamount BETWEEN 24001 AND 26000 THEN '24-26k'
    	WHEN soldamount BETWEEN 26001 AND 28000 THEN '26-28k'
    	WHEN soldamount BETWEEN 28001 AND 30000 THEN '28-30k'
    	WHEN soldamount BETWEEN 30001 AND 32000 THEN '30-32k'
    	WHEN soldamount BETWEEN 32001 AND 34000 THEN '32-34k'
    	WHEN soldamount BETWEEN 34001 AND 36000 THEN '34-36k'
    	WHEN soldamount BETWEEN 36001 AND 38000 THEN '36-38k'
    	WHEN soldamount BETWEEN 38001 AND 40000 THEN '38-40k'
    	WHEN soldamount BETWEEN 40001 AND 100000 THEN '40k+'
    END AS PriceBand 
  -- SELECT COUNT(*) -- 172
  FROM VehicleSales s
  INNER JOIN VehicleInventoryItems v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID 
    AND v.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
  LEFT JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
  INNER JOIN MakeModelClassifications m ON i.make = m.make
    AND i.model = m.model  
  LEFT JOIN TypDescriptions types ON m.VehicleType = types.Typ
  LEFT JOIN TypDescriptions segments ON m.VehicleSegment = segments.typ 	
  WHERE s.typ = 'VehicleSale_Retail'
    AND s.status = 'VehicleSale_Sold'
    AND CAST(SoldTS AS sql_date) > curdate() - 60) y  
GROUP BY TRIM(t) + '-' + TRIM(s), PriceBand


select s.SizeAndShape, s.PriceBand,
  s.Sales, o.Owned
FROM #owned o, #sold s 
WHERE o.SizeAndShape = s.SizeAndShape
  AND o.PriceBand = s.PriceBand
ORDER BY s.Sales desc


-- 3, 1 each
select *
FROM #owned o
WHERE NOT EXISTS ( 
  SELECT 1
  FROM #sold  
  WHERE SizeAndShape = o.SizeAndShape)
  
-- none  
select *
FROM #Sold s
WHERE NOT EXISTS ( 
  SELECT 1
  FROM #owned  
  WHERE SizeAndShape = s.SizeAndShape)  
  
/**********************************
exclude ws buffer FROM owned
**********************************/

  SELECT types.description AS t, segments.description AS s, 
    CASE 
      WHEN d.amount BETWEEN 0 AND 6000 THEN '0-6k'
    	WHEN d.amount BETWEEN 6001 AND 8000 THEN '6-8k'
    	WHEN d.amount BETWEEN 8001 AND 10000 THEN '8-10k'
    	WHEN d.amount BETWEEN 10001 AND 12000 THEN '10-12k'
    	WHEN d.amount BETWEEN 12001 AND 14000 THEN '12-14k'
    	WHEN d.amount BETWEEN 14001 AND 16000 THEN '14-16k'
    	WHEN d.amount BETWEEN 16001 AND 18000 THEN '16-18k'
    	WHEN d.amount BETWEEN 18001 AND 20000 THEN '18-20k'
		WHEN d.amount BETWEEN 20001 AND 22000 THEN '20-22k'
    	WHEN d.amount BETWEEN 22001 AND 24000 THEN '22-24k'
    	WHEN d.amount BETWEEN 24001 AND 26000 THEN '24-26k'
    	WHEN d.amount BETWEEN 26001 AND 28000 THEN '26-28k'
    	WHEN d.amount BETWEEN 28001 AND 30000 THEN '28-30k'
    	WHEN d.amount BETWEEN 30001 AND 32000 THEN '30-32k'
    	WHEN d.amount BETWEEN 32001 AND 34000 THEN '32-34k'
    	WHEN d.amount BETWEEN 34001 AND 36000 THEN '34-36k'
    	WHEN d.amount BETWEEN 36001 AND 38000 THEN '36-38k'
    	WHEN d.amount BETWEEN 38001 AND 40000 THEN '38-40k'
    	WHEN d.amount BETWEEN 40001 AND 100000 THEN '40k+'
      END AS PriceBand
  -- SELECT COUNT(*) -- current inventory
  FROM VehicleInventoryItems v
  LEFT JOIN VehicleItems i ON v.VehicleItemID = i.VehicleItemID 
  INNER JOIN MakeModelClassifications m ON i.make = m.make
    AND i.model = m.model
  INNER JOIN (
    SELECT VehicleInventoryItemID, VehiclePricingID, VehiclePricingTS 
    FROM VehiclePricings x
    WHERE VehiclePricingTS = (
      SELECT MAX(VehiclePricingTS)
      FROM VehiclePricings
      WHERE VehicleInventoryItemID  = x.VehicleInventoryItemID  
      GROUP BY VehicleInventoryItemID)) p ON v.VehicleInventoryItemID = p.VehicleInventoryItemID  
  LEFT JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID
    AND d.typ = 'VehiclePricingDetail_BestPrice'
  LEFT JOIN TypDescriptions types ON m.VehicleType = types.Typ
  LEFT JOIN TypDescriptions segments ON m.VehicleSegment = segments.typ 
  WHERE v.ThruTS IS NULL 
    AND owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
	AND NOT EXISTS (-- exclude WSB
	  SELECT 1
	  FROM VehicleInventoryItemStatuses
	  WHERE VehicleInventoryItemID = v.VehicleInventoryItemID
	    AND category = 'RMFlagWSB'
		AND ThruTS IS NULL)

SELECT *
FROM VehicleInventoryItemStatuses
WHERE VehicleInventoryItemID = 'bc97aed4-5dee-44a0-ade8-6abe673edf46'
  AND ThruTS IS NULL 