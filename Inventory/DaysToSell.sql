SELECT COUNT(viix.VehicleInventoryItemID) AS Total, -- 1335
  SUM(CASE WHEN av.FromTS IS NOT NULL THEN 1 ELSE 0 END) AS Available,
  SUM(CASE WHEN (av.FromTS IS NOT NULL AND timestampdiff(sql_tsi_day, av.fromts, av.thruts) BETWEEN 0 AND 21) THEN 1 ELSE 0 END) AS AvailFresh,
  SUM(CASE WHEN (av.FromTS IS NOT NULL AND timestampdiff(sql_tsi_day, av.fromts, av.thruts) > 21) THEN 1 ELSE 0 END) AS AvailOver21,
--  SUM(CASE WHEN av.FromTS IS NULL THEN 1 ELSE 0 END) AS NeverAvailable,
  SUM(CASE WHEN (av.FromTS IS NULL AND rm.FromTS IS NOT NULL AND pul.FromTS IS NULL) THEN 1 ELSE 0 END) AS FromRaw,-- raw: Never available - Raw Materials - Never pulled 
  SUM(CASE WHEN (av.FromTS IS NULL AND pul.FromTS IS NOT NULL) THEN 1 ELSE 0 END) AS FromRecon, -- this IS ok pulled but never available
  SUM(CASE WHEN (av.FromTS IS NULL AND pul.FromTS IS NULL AND rm.FromTS IS NULL AND va.typ = 'VehicleAcquisition_InAndOut') THEN 1 ELSE 0 END) AS InAndOut,
  SUM(CASE WHEN (av.FromTS IS NULL AND pul.FromTS IS NULL AND rm.FromTS IS NULL AND va.typ = 'VehicleAcquisition_IntraMarketPurchase') THEN 1 ELSE 0 END) AS IntraMarket
FROM VehicleInventoryItems viix
INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
  AND vs.typ = 'VehicleSale_Retail'
  AND vs.status = 'VehicleSale_Sold'
LEFT JOIN VehicleInventoryItemStatuses pul ON viix.VehicleInventoryItemID = pul.VehicleInventoryItemID
  AND pul.category = 'RMFlagPulled' 
  AND pul.FromTS = (-- most recent stint IN pulled
    SELECT MAX(fromts)
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID)       
LEFT JOIN VehicleInventoryItemStatuses av ON viix.VehicleInventoryItemID = av.VehicleInventoryItemID
  AND av.category = 'RMFlagAV'  
  AND av.FromTS = ( -- most recent stint IN available
    SELECT MAX(fromts)
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagAV'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID)  
LEFT JOIN VehicleInventoryItemStatuses rm ON viix.VehicleInventoryItemID = rm.VehicleInventoryItemID 
  AND rm.category = 'RMFlagRMP'  
  AND rm.FromTS = ( -- most recent stint IN Raw
    SELECT MAX(fromts)
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagRMP'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID)      
LEFT JOIN VehicleAcquisitions va ON viix.VehicleInventoryItemID = va.VehicleInventoryItemID   
WHERE viix.ThruTS IS NOT NULL 
AND CAST(viix.ThruTS AS sql_date) > curdate() - 255 -- sales within last 255 days
AND viix.OwningLocationID = ( --  owner
  SELECT partyid
  FROM Organizations
  WHERE name = 'rydells')
  
  
--------------------------------------------------------------------------------
-- what are we selling FROM raw
--------------------------------------------------------------------------------
-- BY price
-- SELECT viix.stocknumber, vs.soldamount, viix.fromts, viix.thruts, vix.yearmodel, vix.make, vix.model, vix.TRIM
SELECT 
  SUM(CASE WHEN vs.soldamount < 6001 THEN 1 ELSE 0 END) AS "<6k",
  SUM(CASE WHEN vs.soldamount BETWEEN 6001 AND 10000 THEN 1 ELSE 0 END) AS "6k - 10k",
  SUM(CASE WHEN vs.soldamount BETWEEN 10001 AND 20000 THEN 1 ELSE 0 END) AS "10k - 20k",
  SUM(CASE WHEN vs.soldamount BETWEEN 20001 AND 30000 THEN 1 ELSE 0 END) AS "20k - 30k",
  SUM(CASE WHEN vs.soldamount > 30000 THEN 1 ELSE 0 END) AS ">30k"
FROM VehicleInventoryItems viix
INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
  AND vs.typ = 'VehicleSale_Retail'
  AND vs.status = 'VehicleSale_Sold'
LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID   
LEFT JOIN VehicleInventoryItemStatuses pul ON viix.VehicleInventoryItemID = pul.VehicleInventoryItemID
  AND pul.category = 'RMFlagPulled' 
  AND pul.FromTS = (-- most recent stint IN pulled
    SELECT MAX(fromts)
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagPulled'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID)       
LEFT JOIN VehicleInventoryItemStatuses av ON viix.VehicleInventoryItemID = av.VehicleInventoryItemID
  AND av.category = 'RMFlagAV'  
  AND av.FromTS = ( -- most recent stint IN available
    SELECT MAX(fromts)
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagAV'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID)  
LEFT JOIN VehicleInventoryItemStatuses rm ON viix.VehicleInventoryItemID = rm.VehicleInventoryItemID 
  AND rm.category = 'RMFlagRMP'  
  AND rm.FromTS = ( -- most recent stint IN Raw
    SELECT MAX(fromts)
    FROM VehicleInventoryItemStatuses 
    WHERE category = 'RMFlagRMP'
    AND VehicleInventoryItemID = viix.VehicleInventoryItemID)      
LEFT JOIN VehicleAcquisitions va ON viix.VehicleInventoryItemID = va.VehicleInventoryItemID   
WHERE viix.ThruTS IS NOT NULL 
AND CAST(viix.ThruTS AS sql_date) > curdate() - 255 -- sales within last 255 days
AND viix.OwningLocationID = ( --  owner
  SELECT partyid
  FROM Organizations
  WHERE name = 'rydells')  
AND av.FromTS IS NULL
AND rm.FromTS IS NOT NULL
AND pul.FromTS IS NULL   
 
-- BY soldby
SELECT t.fullname, t.HowMany AS Total, r.HowMany AS FromRaw, ((r.HowMany)*100/(t.HowMany)*100)/100
FROM (
  SELECT p.fullname, COUNT(*) AS HowMany
  -- SELECT COUNT(DISTINCT viix.VehicleInventoryItemID ) -- aha, the problem IS repeated records
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
    AND vs.typ = 'VehicleSale_Retail'
    AND vs.status = 'VehicleSale_Sold'
  LEFT JOIN people p ON vs.soldby = p.partyid
  LEFT JOIN VehicleAcquisitions va ON viix.VehicleInventoryItemID = va.VehicleInventoryItemID   
  WHERE viix.ThruTS IS NOT NULL 
  AND CAST(viix.ThruTS AS sql_date) > curdate() - 255 -- sales within last 255 days
  AND viix.OwningLocationID = ( --  owner
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')
  GROUP BY p.fullname) as t
LEFT JOIN (
  SELECT p.fullname, COUNT(*) AS HowMany
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleSales vs ON viix.VehicleInventoryItemID = vs.VehicleInventoryItemID
    AND vs.typ = 'VehicleSale_Retail'
    AND vs.status = 'VehicleSale_Sold'
  LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID   
  LEFT JOIN people p ON vs.soldby = p.partyid
  LEFT JOIN VehicleInventoryItemStatuses pul ON viix.VehicleInventoryItemID = pul.VehicleInventoryItemID
    AND pul.category = 'RMFlagPulled' 
    AND pul.FromTS = (-- most recent stint IN pulled
      SELECT MAX(fromts)
      FROM VehicleInventoryItemStatuses 
      WHERE category = 'RMFlagPulled'
      AND VehicleInventoryItemID = viix.VehicleInventoryItemID)       
  LEFT JOIN VehicleInventoryItemStatuses av ON viix.VehicleInventoryItemID = av.VehicleInventoryItemID
    AND av.category = 'RMFlagAV'  
    AND av.FromTS = ( -- most recent stint IN available  
      SELECT MAX(fromts)
      FROM VehicleInventoryItemStatuses 
      WHERE category = 'RMFlagAV'
      AND VehicleInventoryItemID = viix.VehicleInventoryItemID)  
  LEFT JOIN VehicleInventoryItemStatuses rm ON viix.VehicleInventoryItemID = rm.VehicleInventoryItemID 
    AND rm.category = 'RMFlagRMP'  
    AND rm.FromTS = ( -- most recent stint IN Raw
      SELECT MAX(fromts)
      FROM VehicleInventoryItemStatuses 
      WHERE category = 'RMFlagRMP'
      AND VehicleInventoryItemID = viix.VehicleInventoryItemID)      
  LEFT JOIN VehicleAcquisitions va ON viix.VehicleInventoryItemID = va.VehicleInventoryItemID   
  WHERE viix.ThruTS IS NOT NULL 
  AND CAST(viix.ThruTS AS sql_date) > curdate() - 255 -- sales within last 255 days
  AND viix.OwningLocationID = ( --  owner
    SELECT partyid
    FROM Organizations
    WHERE name = 'rydells')  
  AND av.FromTS IS NULL
  AND rm.FromTS IS NOT NULL
  AND pul.FromTS IS NULL 
  GROUP BY p.fullname) AS r ON t.fullname = r.fullname
ORDER BY r.HowMany DESC 
--------------------------------------------------------------------------------
-- Tests  suppositions  AND assertions
--------------------------------------------------------------------------------
603 sold that were never available
375 FROM raw
135 FROM recon
45 IN AND out
108 intra market
which adds up to 663 oops, so WHERE IS the overlap
ok
specified InAndOut AND IntraMarket AS never pulled AND never Raw:
603 sold that were never available
375 FROM raw
135 FROM recon
44 IN AND out
47 intra market
which adds up to 601  CLOSE enough? 

-- no sold InAndOut was ever available
SELECT (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID),
  viisx.*
FROM VehicleInventoryItemStatuses viisx
INNER JOIN VehicleSales vs ON viisx.VehicleInventoryItemID = vs.VehicleInventoryItemID 
WHERE EXISTS (
  SELECT 1
  FROM VehicleAcquisitions
  WHERE typ = 'VehicleAcquisition_InAndOut'
  AND VehicleInventoryItemID = viisx.VehicleInventoryItemID)  
and viisx.category = 'RMFlagAV'

-- are ALL InAndOut acquisitions sold: Yes
SELECT *
FROM VehicleAcquisitions va
LEFT JOIN VehicleSales vs ON va.VehicleInventoryItemID = vs.VehicleInventoryItemID 
WHERE va.typ = 'VehicleAcquisition_InAndOut'
AND vs.VehicleInventoryItemID IS NULL 

-- some IntraMarket Purchases became available
SELECT (SELECT stocknumber FROM VehicleInventoryItems WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID),
  viisx.*
FROM VehicleInventoryItemStatuses viisx
INNER JOIN VehicleSales vs ON viisx.VehicleInventoryItemID = vs.VehicleInventoryItemID 
WHERE EXISTS (
  SELECT 1
  FROM VehicleAcquisitions
  WHERE typ = 'VehicleAcquisition_IntraMarketPurchase'
  AND VehicleInventoryItemID = viisx.VehicleInventoryItemID)  
and viisx.category = 'RMFlagAV'