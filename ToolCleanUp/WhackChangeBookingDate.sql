-- check assumptions
-- this fix IS being done prior to anything being done to the vehicle after booking
-- inspection, walk, notes, statuses, locations, etc done after booking should NOT be changed
-- capture the TS entered at booking to use AS an identifier for records to change

VehicleInventoryItems.FromTS
VehicleInventoryItemStatuses ? ALL FromTS's
VehicleItemStatuses.ThruTS of Category 'VehicleEvaluation', AND FromTS of at least Booked & maybe finished (since this IS mostly being done after the fact)
VehicleItemStatuses.FromTS of category 'Inventory'
VehicleCostComponents.VehicleCostComponentTS
VehicleItem Tables ThruTS:  viInsuranceInfo, viLicenseInfo, viOwnerInfo, viTitleInfo
VehicleItemPhysicalLocations.FromTS
VehicleInventoryItemNotes.NotesTS