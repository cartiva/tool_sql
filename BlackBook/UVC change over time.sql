--H4031A
SELECT vin, vinyear, make, model, series, groupnumbers, blackbookpubdate
FROM bb.blackbookadt
WHERE vin = '1HGCP268'
AND vinyear = '9'

SELECT vin, vinyear, model
INTO #xx
FROM bb.blackbookadt



--15307xxa
/*
This IS another Honda Accord that bb initially categorized only a 
single series (EX), groupnumber (4185), uvc (012)
until 4/25/11
WHEN series EAND, groupnumber 4427 AND uvc 194 are added to the above
AND THEN ON 10/31/2011
series EX,groupnumber 4185 and uvc 012 are removed altogether

*/
-- the fix
-- change groupnumber FROM 4185 to 4427, AND uvc to 194

SELECT * FROM blackbookresolver WHERE VehicleItemID = '74b0a898-c0d9-1545-966e-f5c13f22c927'

SELECT *
FROM BlackBookResolver
WHERE VehicleItemID = (
  SELECT VehicleItemID
  FROM VehicleItems 
  WHERE VIN = '1HGCM56896A012986')
  
SELECT vin, vinyear, make, model, series, groupnumbers, uvc, blackbookpubdate
FROM bb.blackbookadt
WHERE vin = '1HGCM568'
AND vinyear = '6'  
  
SELECT *
FROM bb.blackbookadt  
WHERE vin = '1HGCP368'
AND vinyear = '8'
AND blackbookpubdate = '04/25/2011'

-- so what IS the general solution
-- how to find those vehicles for which this IS true

SELECT COUNT(*) FROM blackbookresolver
SELECT * FROM blackbookresolver

SELECT COUNT(*) 
FROM (
SELECT groupnumber, vin, vinyear, uvc
FROM blackbookresolver
GROUP BY groupnumber, vin, vinyear, uvc)x 

SELECT b.vin, b.vinyear, b.make, b.model, b.series, b.groupnumbers, b.uvc, 
  min(b.blackbookpubdate), MAX(b.blackbookpubdate)
FROM bb.blackbookadt b
INNER JOIN (
  SELECT groupnumber, vin, vinyear, uvc
  FROM blackbookresolver
  GROUP BY groupnumber, vin, vinyear, uvc) r ON b.vin = r.vin
    AND b.groupnumbers = r.groupnumber
    AND b.uvc = r.uvc 
--WHERE vin = '1HGCP368'
--AND vinyear = '8'  
GROUP BY b.vin, b.vinyear, b.make, b.model, b.series, b.groupnumbers, b.uvc

select * FROM bb.blackbookadt


/**** 6/18/12 it happened again ****************/

was originally eval/sold AS 13834XX (sold 4/8/11)

SELECT *
FROM bb.blackbookadt
WHERE vin = '2g1wg5ek'
  AND blackbookpubdate = '06/04/2012'
  
  
SELECT * FROM VehicleItems WHERE vin = '2G1WG5EK9B1124412'  

SELECT *
FROM blackbookresolver
WHERE VehicleItemID = '0e3c9a62-dc34-604f-bf0b-54316bad87df'

0068 / 976

was 0067 / 735
4/11/11 added 0068 / 976: the LT
prior to this date, there was no LT for this vehicle
ON 6/20/11 the LS was removed
this vehicle had originally been categorized AS the LS


/**** 9/24/12 it happened again ****************/
originally sold AS 9682X ON 1/16/09


SELECT * FROM VehicleItems WHERE vin = '1HGCM56867A182689'  

SELECT *
FROM blackbookresolver
WHERE VehicleItemID = '47a40675-4891-7c4c-9990-bc050fafe7f7'

SELECT *
FROM bb.blackbookadt
WHERE vin = '1HGCM568'
  AND vinyear = '7'
--  AND blackbookpubdate = '09/24/2012'

ON 4/25/ 11 series changed to EX-L
uvc changes FROM 009 to 192
groupnumber changes FROM 4146 to 4392
so, change the blackbook resolver record for this vehicle

SELECT *
FROM blackbookresolver
WHERE VehicleItemID = '47a40675-4891-7c4c-9990-bc050fafe7f7'

/* 10/17/13 aggain */
2006 accord
originally sold IN 2010 AS stock H2554A
vin  '1HGCM56896A012986'
VehicleItemID '74b0a898-c0d9-1545-966e-f5c13f22c927'

on 4/25 uvc changed FROM 009 to 192, GROUP FROM 4141 to 4397

SELECT * FROM VehicleItems WHERE vin = '1HGCM56896A012986'

SELECT *
FROM bb.blackbookadt
WHERE vin = '1HGCM568'
  AND vinyear = '6'
  
SELECT *
FROM blackbookresolver
WHERE VehicleItemID = '74b0a898-c0d9-1545-966e-f5c13f22c927'
-- find them all ?
DROP TABLE #xx; 
SELECT b.vin, b.vinyear, b.make, b.model, b.series, b.groupnumbers, b.uvc
INTO #xx
FROM bb.blackbookadt b
WHERE year(blackbookpubdate) = 2011 AND month(blackbookpubdate) = 4
AND make = 'Honda'


SELECT * FROM #xx

SELECT * FROM #xx WHERE vin = '1HGCM568' AND vinyear = '6'

SELECT vin, vinyear
FROM (
  SELECT vin, vinyear, groupnumbers, uvc
  FROM #xx
  GROUP BY vin, vinyear, groupnumbers, uvc) a
GROUP BY vin, vinyear
HAVING COUNT(*) > 1  

-- yep ALL these
SELECT *
FROM #xx a
INNER JOIN (
  SELECT vin, vinyear
  FROM (
    SELECT vin, vinyear, groupnumbers, uvc
    FROM #xx
    GROUP BY vin, vinyear, groupnumbers, uvc) a
  GROUP BY vin, vinyear
  HAVING COUNT(*) > 1) b on a.vin = b.vin AND a.vinyear = b.vinyear
ORDER BY a.vin, a.vinyear  

select *
FROM (
  SELECT a.vin, a.vinyear, MIN(a.groupnumbers) AS g1, MAX(a.groupnumbers) AS g2, MIN(a.uvc) AS u1, MAX(a.uvc) AS u2
  FROM #xx a
  INNER JOIN (
    SELECT vin, vinyear
    FROM (
      SELECT vin, vinyear, groupnumbers, uvc
      FROM #xx
      GROUP BY vin, vinyear, groupnumbers, uvc) a
    GROUP BY vin, vinyear
    HAVING COUNT(*) > 1) b on a.vin = b.vin AND a.vinyear = b.vinyear
  GROUP BY a.vin, a.vinyear) a 
LEFT JOIN blackbookresolver b on a.vin = b.vin AND a.vinyear = b.vinyear  
ORDER BY a.vin, a.vinyear
  
-- 9/19/2014
happened again, but on a grand caravan 
got a call FROM tweeten, eval on this 2013 throws an error on eval black book values 
vin: 2C4RDGCG6DR549565
after 7/21/14, the uvc changed FROM 629 to 627
SELECT * 
FROM bb.blackbookadt
WHERE groupnumbers = (
  SELECT groupnumbers
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG6DR549565'))
AND vin = (
  SELECT vin
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG6DR549565'))     
AND vinyear = (
  SELECT vinyear
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG6DR549565'))     
ORDER BY blackbookpubdate desc      

nothing ELSE changed, just the uvc

-- 6/30/15 another accord

JHMCP26878C030719
sold AS H3392X on 3/3/11
back IN now

SELECT *
FROM bb.blackbookadt
WHERE vin = 'JHMCP268'
  AND vinyear = '8'
  
was GROUP: 4184 UVC: 009
now GROUP: 4429 UVC: 192  

UPDATE blackbookresolver
SET groupnumber = '4429',
    uvc = '192'
-- SELECT * FROM blackbookresolver
WHERE VehicleItemID = (
  SELECT VehicleItemID
  FROM VehicleItems
  WHERE vin = 'JHMCP26878C030719')
  
--8/3/15
2010 Honda Accord Crosstour (H8211A) 
originally IN system May 2014

SELECT *
FROM bb.blackbookadt
WHERE vin = '5J6TF2H5'
  AND vinyear = 'A'  
  
on 9/22/14, UVC changed FROM 174 to 214, GroupNumber remains the same  
UPDATE blackbookresolver
SET uvc = '214'
-- select * FROM blackbookresolver 
WHERE VehicleItemID = '29830a76-20be-bb4e-91ca-eb19bf459171'

-- 12/8/15
select *
FROM blackbookresolver
WHERE VehicleItemID = '86a83fde-0cad-4d44-be81-7960d871fb4c'

SELECT *
FROM bb.blackbookadt
WHERE vin = '2G61T5S3'
  AND vinyear = 'D' 
  
  
UPDATE blackbookresolver
SET uvc = '155'
-- select * FROM blackbookresolver 
WHERE VehicleItemID = '86a83fde-0cad-4d44-be81-7960d871fb4c'

  
  
-- 2/26/16
-- originally here IN 2013, uvc changed 8/4/14
SELECT * 
FROM bb.blackbookadt
WHERE groupnumbers = (
  SELECT groupnumbers
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG4DR524485'))
AND vin = (
  SELECT vin
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG4DR524485'))     
AND vinyear = (
  SELECT vinyear
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG4DR524485'))   
    
SELECT *
FROM blackbookresolver
WHERE VehicleItemID = 'd074f2cb-128a-ee4e-b670-3e5f00138a13'

UPDATE blackbookresolver
SET uvc = '627'
WHERE VehicleItemID = 'd074f2cb-128a-ee4e-b670-3e5f00138a13'


-- 10/5/16
-- originally here IN 2013, uvc changed 8/4/14
select *
FROM VehicleInventoryItems
WHERE VehicleItemID = (
  SELECT VehicleItemID
  FROM VehicleItems
  WHERE vin = '2C4RDGCG0CR301956')
  
  
SELECT * 
FROM bb.blackbookadt
WHERE groupnumbers = (
  SELECT groupnumbers
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG0CR301956'))
AND vin = (
  SELECT vin
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG0CR301956'))     
AND vinyear = (
  SELECT vinyear
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG0CR301956'))   

SELECT * 
FROM VehicleItems
WHERE vin = '2C4RDGCG0CR301956'
    
SELECT *
FROM blackbookresolver
WHERE VehicleItemID = '0f9d17f6-57bc-344c-96a7-151d2bfffe9c'

UPDATE blackbookresolver
SET uvc = '627',
    groupnumber = '7140'
WHERE VehicleItemID = '0f9d17f6-57bc-344c-96a7-151d2bfffe9c'


SELECT *
FROM blackbookresolver
WHERE VehicleItemID = 'd074f2cb-128a-ee4e-b670-3e5f00138a13'
UNION
SELECT *
FROM blackbookresolver
WHERE VehicleItemID = '0f9d17f6-57bc-344c-96a7-151d2bfffe9c'




SELECT * 
FROM bb.blackbookadt
WHERE groupnumbers = (
  SELECT groupnumbers
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG4DR524485'))
AND vin = (
  SELECT vin
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG4DR524485'))     
AND vinyear = (
  SELECT vinyear
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG4DR524485'))   
UNION 	
SELECT * 
FROM bb.blackbookadt
WHERE groupnumbers = (
  SELECT groupnumbers
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG0CR301956'))
AND vin = (
  SELECT vin
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG0CR301956'))     
AND vinyear = (
  SELECT vinyear
  FROM blackbookresolver
  WHERE VehicleItemID = (
    SELECT VehicleItemID
    FROM VehicleItems
    WHERE vin = '2C4RDGCG0CR301956'))   
