SELECT v.stocknumber, g.salesstatus, DaysAvail, TotalGross
--  g.stocknumber, g.totalgross  
from GrossLogTable g
INNER JOIN VehicleInventoryItems v ON g.Stocknumber = v.stocknumber
INNER JOIN (
  SELECT VehicleInventoryItemID, SUM(timestampdiff(sql_tsi_day, CAST(FromTS AS sql_date), CAST(ThruTS AS sql_date))) AS DaysAvail 
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagAV'
  GROUP BY VehicleInventoryItemID) s ON v.VehicleInventoryItemID = s.VehicleInventoryItemID 
WHERE LEFT(g.stocknumber, 1) NOT IN ('H', 'C')


SELECT COUNT(g.stocknumber) AS "Count", DaysAvail, round(SUM(TotalGross), 0) AS TotalGross, salesstatus, round(SUM(TotalGross)/COUNT(g.stocknumber), 0) AS AvgGrossPer
FROM GrossLogTable g
INNER JOIN VehicleInventoryItems v ON g.Stocknumber = v.stocknumber
INNER JOIN (
  SELECT VehicleInventoryItemID, SUM(timestampdiff(sql_tsi_day, CAST(FromTS AS sql_date), CAST(ThruTS AS sql_date))) AS DaysAvail 
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagAV'
  GROUP BY VehicleInventoryItemID) s ON v.VehicleInventoryItemID = s.VehicleInventoryItemID 
WHERE left(g.stocknumber, 1) = 'H'-- NOT IN ('H', 'C')
-- AND salesstatus = 'Retail'
GROUP BY DaysAvail, salesstatus
ORDER BY salesstatus, daysavail





-- doesn't seem right that there IS only 1 ws with 0 days available

SELECT v.stocknumber, g.salesstatus, DaysAvail, TotalGross
--  g.stocknumber, g.totalgross  
from GrossLogTable g
inner JOIN VehicleInventoryItems v ON g.Stocknumber = v.stocknumber
inner JOIN (
  SELECT VehicleInventoryItemID, SUM(timestampdiff(sql_tsi_day, CAST(FromTS AS sql_date), CAST(ThruTS AS sql_date))) AS DaysAvail 
  FROM VehicleInventoryItemStatuses
  WHERE category = 'RMFlagAV'
  GROUP BY VehicleInventoryItemID) s ON v.VehicleInventoryItemID = s.VehicleInventoryItemID 
WHERE left(g.stocknumber, 1) NOT IN ('H', 'C')
AND daysavail = 0
AND salesstatus = 'Wholesale'