
SELECT ln.stocknumber, ln.year, ln.make, ln.model, 
  ln.bodystyle AS lBody, uc.bodystyle as uBody,
  ln.glump AS lGlump, uc.glump AS uGlump, 
  ln.segment AS lSegment, uc.Segment AS uSegment,
  CASE 
    WHEN ln.bodystyle <> uc.bodystyle THEN 'BStyle' 
    WHEN ln.Segment <> uc.Segment THEN 'Segment'
    WHEN ln.Glump <> uc.Glump THEN 'Glump'
  END AS diff 
FROM StPaulAvailable ln
LEFT JOIN (
  SELECT stocknumber, year, make, model, bodystyle, glump, segment
  FROM usedcars
  WHERE market = 'SP-St. Paul') uc ON ln.stocknumber = uc.stocknumber
WHERE (
    (ln.bodystyle <> uc.bodystyle) OR 
    (ln.glump <> uc.glump) OR 
    (ln.segment <> uc.segment))

SELECT diff, COUNT(*) -- 231 glump
FROM (
  SELECT ln.stocknumber, ln.year, ln.make, ln.model, 
    ln.bodystyle AS lBody, uc.bodystyle as uBody,
    ln.glump AS lGlump, uc.glump AS uGlump, 
    ln.segment AS lSegment, uc.Segment AS uSegment,
    CASE 
      WHEN ln.bodystyle <> uc.bodystyle THEN 'BStyle' 
      WHEN ln.Segment <> uc.Segment THEN 'Segment'
      WHEN ln.Glump <> uc.Glump THEN 'Glump'
    END AS diff 
  FROM StPaulAvailable ln
  LEFT JOIN (
    SELECT stocknumber, year, make, model, bodystyle, glump, segment
    FROM usedcars
    WHERE market = 'SP-St. Paul') uc ON ln.stocknumber = uc.stocknumber
  WHERE (
      (ln.bodystyle <> uc.bodystyle) OR 
      (ln.glump <> uc.glump) OR 
      (ln.segment <> uc.segment))) x
GROUP BY diff    

-- stocknumbers are unique within the market
SELECT stocknumber
FROM usedcars
WHERE market = 'SP-St. Paul'
GROUP BY stocknumber
HAVING COUNT(*) > 1

-- fuck even more issues: status
-- 64274X IN notes available for 4 days, IN aging section of report: never ON lot

SELECT ln.stocknumber, ln.year, ln.make,  
  ln.salesstatus lStatus, uc.salesstatus AS uStatus
SELECT COUNT(*)  -- 112
FROM StPaulAvailable ln
LEFT JOIN (
  SELECT stocknumber, year, make, model, bodystyle, glump, segment, salesstatus
  FROM usedcars
  WHERE market = 'SP-St. Paul') uc ON ln.stocknumber = uc.stocknumber
WHERE ln.salesstatus <> uc.salesstatus

