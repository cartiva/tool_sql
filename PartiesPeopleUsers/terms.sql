-- 7/13/15, mike tweten
-- don't need to DO anything with the user TABLE, just remove ALL the below
-- AND we are good to go
-- SELECT * FROM people WHERE lastname = 'karaba'
SELECT *
FROM applicationusers
WHERE partyid = 'd577d86a-dfcf-944a-9f3f-0bda75c897d5'

SELECT *
FROM partyprivileges
WHERE partyid = 'd577d86a-dfcf-944a-9f3f-0bda75c897d5'

SELECT *
FROM partyrelationships
WHERE partyid2 = 'd577d86a-dfcf-944a-9f3f-0bda75c897d5'

UPDATE partyRelationships
SET thruts = now()
WHERE partyid2 = '8dde8e37-cee5-e24b-a797-bc148f142d3b'
  AND thruts IS null;
  
UPDATE partyprivileges 
SET thruts = now()
WHERE partyid = '8dde8e37-cee5-e24b-a797-bc148f142d3b'
  AND thruts IS NULL;
  
UPDATE applicationusers
SET thruts = now()
WHERE partyid = '8dde8e37-cee5-e24b-a797-bc148f142d3b'
  AND thruts IS NULL;
