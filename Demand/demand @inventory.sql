/*
FROM the old sp
@Inventory = (
  select count(*)
  from CartivaDps.UsedCars
  where (Market = @MarketID and VehicleType = @VehicleType and PriceBand = @PriceBand) 
    and (
      SalesStatus in (
        'In Transit', 'Raw Materials', 'Recon Buffer', 'Recon WIP', 'Pricing Buffer') 
    or (
      SalesStatus = 'Available' and (CurDate() - DateOnLot < 31))));
SELECT vehicletype FROM cartivadps.usedcars	  
*/	 
/*
there IS no model with multiple vehicle_type

SELECT model
FROM (
  SELECT model, vehicletype
  FROM MakeModelClassifications
  GROUP BY model, vehicletype
  HAVING COUNT(*) > 1) wtf
GROUP BY model
HAVING COUNT(*) > 1

therefor removed make FROM VehicleItems join
*/
--  
-- there IS no price ON TNA
SELECT COUNT(*)
FROM VehicleInventoryItemStatuses viisx
INNER JOIN VehicleInventoryItems viix ON viisx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
  AND viix.LocationID IN (
    SELECT PartyID2
    FROM PartyRelationships
    WHERE typ = 'PartyRelationship_MarketLocations'
    AND PartyID1 = 'A4847DFC-E00E-42E7-89EE-CD4368445A82') 
INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
--  AND vix.make IN ( -- don't need make
--    SELECT make
--    FROM MakeModelClassifications 
--    WHERE VehicleType = (
--      SELECT vehicletype 
--      FROM MakeModelClassifications 
--      WHERE make = 'Chevrolet' 
--        AND model = 'Suburban'))  
  AND vix.Model IN (
    SELECT Model
    FROM MakeModelClassifications 
    WHERE VehicleType = (
      SELECT distinct vehicletype 
      FROM MakeModelClassifications 
      WHERE  model = 'Suburban'
	  AND make = 'Chevrolet'))
INNER JOIN VehiclePricings vpx ON viix.VehicleInventoryItemID = vpx.VehicleInventoryItemID
  AND vpx.VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID = vpx.VehicleInventoryItemID 
    GROUP BY vpx.VehicleInventoryItemID)
INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
  AND vpdx.typ = 'VehiclePricingDetail_BestPrice'		  
WHERE ((
  viisx.Category = 'RMFlagAV'
    AND viisx.ThruTS IS NULL
    AND Curdate() - cast(viisx.FromTS AS sql_date) < 31)
  OR (
  viisx.Category IN (/*'RMFlagTNA',*/'RMFlagRMP','RMFlagRB','RMFlagPB')
    AND viisx.ThruTS IS NULL))
AND vpdx.amount BETWEEN (SELECT PriceFrom FROM PriceBands WHERE PriceBand = '32-34k')
      AND (SELECT PriceThru FROM PriceBands WHERE PriceBand = '32-34k')
