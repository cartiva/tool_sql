--SELECT *
--FROM LocationPhysicalVehicleLocations
-- SELECT * FROM organizations
-- rydells: 'B6183892-C79D-4489-A58C-B526DF948B06'
-- honda cartiva: '4CD8E72C-DC39-4544-B303-954C99176671'
-- 
DECLARE @LocationID string;
@LocationID = (
  SELECT partyid
  FROM organizations
  WHERE name = 'honda cartiva');

INSERT INTO LocationPhysicalVehicleLocations values(
@LocationID ,
(SELECT newidstring(d) FROM system.iota),
false,
true,
'M',
'Southwest of Building');


SELECT *
FROM LocationPhysicalVehicleLocations
ORDER BY locationshortname

SELECT *
FROM LocationPhysicalVehicleLocations
WHERE locationshortname LIKE 'Honda F%'


UPDATE LocationPhysicalVehicleLocations
SET Active = false
WHERE locationshortname LIKE 'Honda F%'

select * from LocationPhysicalVehicleLocations

UPDATE LocationPhysicalVehicleLocations
SET locationshortname = 'Pricing - East of new car'
-- SELECT * FROM LocationPhysicalVehicleLocations
WHERE locationshortname = 'Pricing - South of used cars'

UPDATE LocationPhysicalVehicleLocations
SET LocationDescription = 'in the Shop'
-- SELECT * FROM LocationPhysicalVehicleLocations
WHERE locationshortname = 'Rydell Detail'