-- sp.GetCurrentStatusForVIN
-- brought up BY H2853B, vin: 2C8GP64L35R244271 

-- IF a vin has been here multiple times, this returns multiple records
  SELECT  top 1 
    vis.status, coalesce(vii.stocknumber, ''), vis.Category, 
	  right(convert(convert(vis.FromTS, SQL_DATE), SQL_CHAR),5) + '-' + left(convert(convert(vis.FromTS, SQL_DATE), SQL_CHAR),4) as FromDate,
	  CurDate() - convert(vis.FromTS, SQL_DATE) as Age,
	  (select convert(value, SQL_Integer) from PartyAttributes where PartyID = 
	    'A4847DFC-E00E-42E7-89EE-CD4368445A82' and typ = 'PartyAttribute_DaysToDisplayIncompleteEvaluations') as Incomplete,
	  (select convert(value, SQL_Integer) from PartyAttributes where PartyID = 
	    'A4847DFC-E00E-42E7-89EE-CD4368445A82' and typ = 'PartyAttribute_DaysToDisplayFinishedEvaluations') as Finished,
	  CASE 
  	  WHEN vis.Category = 'VehicleEvaluation' then
  	    coalesce((SELECT name FROM Organizations WHERE partyid = (  // a saved DataCollection might NOT have a location
  	      SELECT locationid
  		  FROM VehicleEvaluations 
  		  WHERE VehicleEvaluationID = vis.TableKey)),'')
  	  WHEN vis.Category = 'Inventory' THEN
  	    (SELECT name FROM Organizations WHERE partyid = (
  		  SELECT locationid 
  		  FROM VehicleInventoryItems
  		  WHERE VehicleItemID = vis.VehicleItemID
  		  AND ThruTS IS NULL))
	  ELSE ''
	  END AS Location
  from VehicleItemStatuses vis
  inner join VehicleItems vi on vi.VehicleItemID = vis.VehicleItemID
  left outer join VehicleInventoryItems vii on vii.VehicleItemID = vis.VehicleItemID
  where vis.ThruTS is null
  and vi.vin = '1GKFK33059R249747'
  ORDER BY vii.fromts DESC 


SELECT stocknumber, 
  (select vin FROM VehicleItems WHERE VehicleItemID = viix.VehicleItemID), 
  fromts, thruts
FROM VehicleInventoryItems viix
WHERE VehicleItemID IN (  
SELECT VehicleItemID
FROM VehicleInventoryItems
GROUP BY VehicleItemID
HAVING COUNT(*) > 1)
ORDER BY VehicleItemID 