SELECT vii.VehicleInventoryItemID, vii.stocknumber, left(TRIM(vi.YearModel) + ' ' + TRIM(vi.Make) + ' ' + TRIM(vi.Model),25),
  cast(vii.FromTS AS SQL_DATE) AS Acquired,
  cast(vinsp.VehicleInspectionTS AS SQL_DATE) AS Inspected, 
  cast(vw.VehicleWalkTS AS SQL_DATE) AS Walked, 
    
  (SELECT cast(FromTS AS SQL_DATE)
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
    AND category = 'RMFlagPulled') AS Pulled, 
  (SELECT cast(FromTS AS SQL_DATE)
    FROM VehicleInventoryItemStatuses
    WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
    AND category = 'RMFlagAV') AS Available,      
  
  cast(vs.SoldTS AS SQL_DATE) AS Sold,
  cast(vii.ThruTS AS SQL_DATE) AS Delivered,
  
CASE 
  WHEN EXISTS(
    SELECT 1
    FROM AuthorizedReconItems ari
    INNER JOIN VehicleReconItems vri ON vri.vehiclereconItemID = ari.VehicleReconItemID 
    INNER JOIN ReconAuthorizations ra ON ra.ReconAuthorizationID = ari.ReconAuthorizationID
    WHERE vri.VehicleInventoryItemID = vii.VehicleInventoryItemID 
    AND vri.typ LIKE 'Mechanical%'
    AND ra.BasisTable <> 'VehicleInspections') THEN 'YES'
  ELSE 'NO'
 END AS "Mech Auth",
CASE 
  WHEN EXISTS(
    SELECT 1
    FROM AuthorizedReconItems ari
    INNER JOIN VehicleReconItems vri ON vri.vehiclereconItemID = ari.VehicleReconItemID 
    INNER JOIN ReconAuthorizations ra ON ra.ReconAuthorizationID = ari.ReconAuthorizationID
    WHERE vri.VehicleInventoryItemID = vii.VehicleInventoryItemID 
    AND vri.typ LIKE 'Body%'
    AND ra.BasisTable <> 'VehicleInspections') THEN 'YES'
  ELSE 'NO'
 END AS "Body Auth",
CASE 
  WHEN EXISTS(
    SELECT 1
    FROM AuthorizedReconItems ari
    INNER JOIN VehicleReconItems vri ON vri.vehiclereconItemID = ari.VehicleReconItemID 
    INNER JOIN ReconAuthorizations ra ON ra.ReconAuthorizationID = ari.ReconAuthorizationID
    WHERE vri.VehicleInventoryItemID = vii.VehicleInventoryItemID 
    AND vri.typ LIKE '%Appearance%'
    AND ra.BasisTable <> 'VehicleInspections') THEN 'YES'
  ELSE 'NO'
 END AS "App Auth"   
  
  
-- SELECT COUNT(*) -- 148 sales 
FROM VehicleInventoryItems vii
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN VehicleSales vs ON vs.VehicleInventoryItemID = vii.VehicleInventoryItemID 
  AND vs.Typ = 'VehicleSale_Retail' AND vs.Status = 'VehicleSale_Sold'
LEFT JOIN VehicleInspections vinsp ON vinsp.VehicleInventoryItemID = vii.VehicleInventoryItemID
LEFT JOIN VehicleWalks vw ON vw.VehicleInventoryItemID = vii.VehicleInventoryItemID  
WHERE month(vs.SoldTS) = 8 -- use vs.SoldTS to include SoldNotDelivered
AND year(vs.SoldTS) = 2010 
AND vii.LocationID = (
  SELECT partyid
  FROM Organizations
  WHERE name = 'Rydells')
 

  
  
/*
10274A:  sales buffer 8/16, removed 8/16; THEN again ON 8/26

SELECT * FROM AuthorizedReconItems
SELECT * FROM ReconAuthorizations
SELECT DISTINCT typ FROM VehicleReconItems
SELECT * FROM VehicleInspections WHERE VehicleInventoryItemID = '6c6ce8a2-b4cf-4d1e-b8f2-184a70800c00'

11835X walk but no inspection

11776X - Honda (H2325LA) car sold at Rydells



SELECT * 
FROM VehicleReconItems vri
WHERE vri.VehicleInventoryItemID = 'd4650a29-3953-467a-8a02-454d7c6d3930'

SELECT COUNT(*)
FROM AuthorizedReconItems ari
INNER JOIN VehicleReconItems vri ON vri.vehiclereconItemID = ari.VehicleReconItemID 
INNER JOIN ReconAuthorizations ra ON ra.ReconAuthorizationID = ari.ReconAuthorizationID
WHERE vri.VehicleInventoryItemID = 'd4650a29-3953-467a-8a02-454d7c6d3930'
AND vri.typ LIKE 'Mechanical%'
AND ra.BasisTable <> 'VehicleInspections'

SELECT DISTINCT typ FROM VehicleAcquisitions

*/