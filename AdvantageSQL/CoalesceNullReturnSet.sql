-- works
DECLARE @result integer;
@result = coalesce ((
  SELECT Sum(Coalesce(bbad.Amount, 0)) 
  FROM BlackBookAddsDeducts bbad 
  WHERE bbad.TableKey = 'x'),0);
-- @result = coalesce(@result, 0);
SELECT @result FROM system.iota;  

-- does NOT work
DECLARE @result integer;
@result = (
  SELECT Sum(Coalesce(bbad.Amount, 0)) 
  FROM BlackBookAddsDeducts bbad 
  WHERE bbad.TableKey = 'x');
-- @result = coalesce(@result, 0);
SELECT @result FROM system.iota;  



does not work:
SELECT coalesce(InternetComment, 'none') 
  FROM VehicleInventoryItemInternetComments 
  WHERE VehicleInventoryItemID = 'cfb7a9fe-bd51-cc47-940c-a62936986b5b';

works:  
SELECT coalesce((
  SELECT InternetComment
  FROM VehicleInventoryItemInternetComments 
  WHERE VehicleInventoryItemID = 'cfb7a9fe-bd51-cc47-940c-a62936986b5b'), 'none')
FROM system.iota  