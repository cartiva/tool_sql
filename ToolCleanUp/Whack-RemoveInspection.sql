/*
it IS essential that this be run against vehicles WHERE nothing has been done to them since inspection
put IN some tests for this:
  VehicleItemPhysicalLocations with ts after inspection
  walkpending flag still OPEN
  reconauthorization: only FROM this inspection

physical location
notes
certification
statuses
reconauthorizations
vehiclereconitems
authorizedreconitems
reconnotes with bt of vehicleinspections
inpsection

SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = 'e52c7afd-5ea9-44f4-af68-a0131f4cb436'



SELECT * FROM vehicleinspections WHERE VehicleInventoryItemID = 'e52c7afd-5ea9-44f4-af68-a0131f4cb436'
inspts : 11/04/2010 2:23:31 PM
SELECT * FROM vehicleitemphysicallocations WHERE VehicleInventoryItemID = 'e52c7afd-5ea9-44f4-af68-a0131f4cb436'
*/

DECLARE @VehicleInventoryItemID string;
DECLARE @InspectionTS timestamp;
DECLARE @VehicleInspectionID string;
DECLARE @ReconAuthorizationID string;

@VehicleInventoryItemID = 'e52c7afd-5ea9-44f4-af68-a0131f4cb436';
@InspectionTS = (SELECT VehicleInspectionTS FROM VehicleInspections WHERE VehicleInventoryItemID = @VehicleInventoryItemID);
@VehicleInspectionID = (SELECT VehicleInspectionID FROM VehicleInspections WHERE VehicleInventoryItemID = @VehicleInventoryItemID);
@ReconAuthorizationID = (SELECT ReconAuthorizationID FROM ReconAuthorizations WHERE VehicleInventoryItemID = @VehicleInventoryItemID);

BEGIN TRANSACTION;
TRY 
  -- VehicleItemPhysicalLocations 
  DELETE FROM VehicleItemPhysicalLocations
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND FromTS = @InspectionTS
  AND ThruTS IS NULL;
  
  UPDATE VehicleItemPhysicalLocations
  SET Thruts = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND ThruTS = @InspectionTS;
  -- VehicleInventoryItemNotes 
  DELETE 
  FROM VehicleInventoryItemNotes 
  WHERE basistable = @VehicleInspectionID;
  -- FactoryCertifications
  DELETE 
  FROM FactoryCertifications
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  --SELECT * FROM VehicleInventoryItemStatuses WHERE VehicleInventoryItemID = '7ec115ae-530a-4198-b6f9-1e737c6fc6c6'
  --
  --SELECT *
  --FROM VehicleInventoryItemStatuses viisx
  --INNER JOIN VehicleInspections vinsp ON vinsp.VehicleInventoryItemID = viisx.VehicleInventoryItemID 
  --WHERE viisx.ThruTS IS NULL 
  --AND EXISTS (
  --  SELECT 1
  --  FROM VehicleInventoryItemStatuses 
  --  WHERE VehicleInventoryItemID = viisx.VehicleInventoryItemID
  --  AND category = 'RMFlagWP'
  --  AND ThruTS IS NULL)
  --ORDER BY viisx.status
  -- VehicleInventoryItemStatuses 
  UPDATE VehicleInventoryItemStatuses 
  SET ThruTS = NULL
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND category = 'RMFlagIP';
  --SELECT * FROM reconauthorizations WHERE VehicleInventoryItemID = 'e52c7afd-5ea9-44f4-af68-a0131f4cb436'
  --SELECT * FROM VehicleReconItems WHERE VehicleInventoryItemID = 'e52c7afd-5ea9-44f4-af68-a0131f4cb436'
  -- VehicleReconItems 
  DELETE 
  FROM VehicleReconItems 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  -- AuthorizedReconItems 
  DELETE 
  FROM AuthorizedReconItems
  WHERE ReconAuthorizationID = @ReconAuthorizationID;
  -- ReconAuthorizations 
  DELETE 
  FROM ReconAuthorizations
  WHERE ReconAuthorizationID = @ReconAuthorizationID;
  -- ReconNotes
  DELETE 
  FROM ReconNotes
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  -- VehicleInspections
  DELETE 
  FROM VehicleInspections
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
  
COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END TRY;  
