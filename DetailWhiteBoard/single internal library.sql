any vehicle with OPEN appearance recon
a COLUMN for GTG
AND a COLUMN for walked today

what about vehicles with other (other, upholstery)?

-- based on other jobs    
CREATE PROCEDURE dwbGetInternalJobs
   ( 
      Misc CICHAR ( 25 ) OUTPUT,
      Customer CICHAR ( 40 ) OUTPUT,
      Vehicle CICHAR ( 80 ) OUTPUT,
      Keys CICHAR ( 26 ) OUTPUT,
      Job Memo OUTPUT,
      G2G cichar(1) output,
      WT cichar(1) output
   ) 
BEGIN 
/*
EXECUTE PROCEDURE dwbGetInternalJobs();
*/
INSERT INTO __output
  SELECT cast(a.CurrentPriority AS sql_char), a.stocknumber AS Customer, 
    CASE
      WHEN e.exteriorcolor IS NULL THEN left(TRIM(e.make) + ' ' + TRIM(e.model), 60)
      ELSE left(TRIM(e.exteriorcolor) + ' ' + TRIM(e.model), 60) 
    END AS Vehicle,  coalesce(x.KeyStatus, 'Unknown') AS Keys,
    CASE
      WHEN h.typ = 'PartyCollection_AppearanceReconItem' THEN 
        CASE
          WHEN upper(h.Description) in ('FULL DETAIL', 'CAR DETAIL','TRUCK DETAIL') THEN 'Detail'
          WHEN upper(h.Description) IN ('CAR BUFF','TRUCK BUFF') THEN 'Buff'
          WHEN (upper(h.description) LIKE 'DELUXE%' OR upper(h.description) LIKE 'AUCTION%') THEN 'Deluxe'
          WHEN upper(h.Description) = 'PRICING RINSE' THEN 'PR' 
          ELSE 'WTF' 
        END
      ELSE 'Other'
    END AS Detail,
    CASE 
      WHEN (  
          SELECT replace(status,'MechanicalReconProcess_','')
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
          AND category = 'MechanicalReconProcess'
          AND ThruTS IS NULL) = 'NoIncompleteReconItems'
        AND (  
          SELECT replace(status,'BodyReconProcess_','')
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = a.VehicleInventoryItemID
          AND category = 'BodyReconProcess'
          AND ThruTS IS NULL) = 'NoIncompleteReconItems'
        THEN 'X'
      ELSE '' END AS G2G,
    CASE 
      WHEN CAST(w.VehicleWalkTS AS sql_date) = curdate() THEN 'X'
      ELSE ''
    END AS WT                  
  FROM VehicleInventoryItems a
  INNER JOIN VehicleInventoryItemStatuses d ON a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.status = 'AppearanceReconProcess_NotStarted'
      AND d.thruts IS NULL  
  INNER JOIN VehicleItems e ON a.VehicleItemID = e.VehicleItemID 
  INNER JOIN reconauthorizations f ON a.VehicleInventoryItemID = f.VehicleInventoryItemID 
    AND f.thruts IS NULL 
  INNER JOIN AuthorizedReconItems g ON f.ReconAuthorizationID = g.ReconAuthorizationID   
    AND g.startts IS NULL 
  INNER JOIN VehicleReconItems h ON g.VehicleReconItemID = h.VehicleReconItemID   
    AND h.typ = 'PartyCollection_AppearanceReconItem'  
  LEFT JOIN KeyperKeyStatus x ON a.stocknumber = x.stocknumber   
  LEFT JOIN detailwhiteboard y on a.stocknumber = y.customer
  LEFT JOIN VehicleWalks w on a.VehicleInventoryItemID = w.VehicleInventoryItemID   
  WHERE a.thruts IS NULL;
END;    