SELECT i.stocknumber, m.FromTS, m.ThruTS, 
  timestampdiff(sql_tsi_hour, m.fromts, m.thruts) hours, 
  left(s.fullname, 25) AS Scheduler, l.locationshortname, p.fullname AS mover, status
--SELECT *  
FROM vehiclestomove m
INNER JOIN VehicleInventoryItems i ON m.VehicleInventoryItemID = i.VehicleInventoryItemID 
  AND i.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
LEFT JOIN people s ON m.schedulerid = s.partyid
LEFT JOIN people p ON m.moverid = p.partyid
LEFT JOIN locationphysicalvehiclelocations l ON m.MoveToLocation = PhysicalVehicleLocationID
WHERE CAST(m.fromts AS sql_date) > curdate() - 90


SELECT Scheduler, status, min(fromts), max(fromts), COUNT(*), avg(hours)
FROM (
  SELECT i.stocknumber, m.FromTS, m.ThruTS, 
    timestampdiff(sql_tsi_hour, m.fromts, m.thruts) hours, 
    left(s.fullname, 25) AS Scheduler, l.locationshortname, p.fullname AS mover, status
  --SELECT *  
  FROM vehiclestomove m
  INNER JOIN VehicleInventoryItems i ON m.VehicleInventoryItemID = i.VehicleInventoryItemID 
    AND i.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
  LEFT JOIN people s ON m.schedulerid = s.partyid
  LEFT JOIN people p ON m.moverid = p.partyid
  LEFT JOIN locationphysicalvehiclelocations l ON m.MoveToLocation = PhysicalVehicleLocationID
  WHERE CAST(m.fromts AS sql_date) > curdate() - 90) z
GROUP BY Scheduler, status  
ORDER BY COUNT(*) DESC 
