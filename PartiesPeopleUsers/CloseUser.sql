-- partyid: 50946F93-260A-428A-8BCE-E8E58A2C4887

-- leave people, he may no longer be an employee,but he IS still a person
-- partyid remains
-- SELECT * FROM users WHERE username LIKE 'jpu%'
DECLARE @PartyID string;
DECLARE @NowTS timestamp;

@PartyID = (SELECT PartyID FROM users WHERE username = 'jpurtle');
@NowTS = (SELECT now() FROM system.iota);

BEGIN TRANSACTION;
TRY 
  UPDATE contactmechanisms
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE users
  SET Active = False
  WHERE partyid = @PartyID;
  
  UPDATE ApplicationUsers
  SET ThruTS = @NowTS
  WHERE partyid = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE PartyRelationships
  SET ThruTS = @NowTS
  WHERE partyid2 = @PartyID
  AND ThruTS IS NULL;
  
  UPDATE PartyPrivileges
  SET ThruTS = @NowTS
  WHERE PartyID = @PartyID
  AND ThruTS IS NULL;
  
  COMMIT WORK;
CATCH ALL
  ROLLBACK WORK;
  RAISE;
END;    

