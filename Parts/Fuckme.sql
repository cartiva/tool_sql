
/*
only vri.PartsInStock
only vri.typ = Mech
only OPEN AuthorizedReconItems ( status <> complete)
for the current OPEN ReconAuthorization
only AuthorizedReconItems WHERE status <> removed

SELECT DISTINCT typ FROM AuthorizedReconItems 
*/
-- ordered AND NOT received
SELECT viix.stocknumber, vrix.description, PartsInStock AS PartsReqd,totalpartsamount, partsquantity, 
  po.OrderedTS, ReceivedTS, CancelledTS, ReturnedTS
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.status <> 'AuthorizedReconItem_Complete' -- only OPEN AuthorizedReconItem
--  AND arix.typ <> 'AuthorizedReconItem_Removed' -- only NOT removed AuthorizedReconItems 
LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
LEFT JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID  
WHERE EXISTS ( -- current ReconAuthorization only -- hmm, this also seems to exclude removed aris, 
  SELECT 1     -- which makes sense, WHEN an AuthorizedReconItem IS removed, the ReconAuthorization IS closed AND a new one IS created
  FROM ReconAuthorizations 
  WHERE ReconAuthorizationID = arix.ReconAuthorizationID
  AND thruts IS NULL)
AND vrix.typ LIKE 'M%'
AND vrix.PartsInStock = True
AND po.OrderedTS IS NOT NULL
AND po.ReceivedTS IS NULL
AND po.CancelledTS IS NULL
AND po.ReturnedTS IS NULL 
ORDER BY viix.stocknumber

-- NOT yet ordered
SELECT viix.stocknumber, vrix.description, PartsInStock AS PartsReqd, totalpartsamount, partsquantity, 
  po.OrderedTS, po.ReceivedTS, po.CancelledTS, po.ReturnedTS
FROM VehicleReconItems vrix
INNER JOIN AuthorizedReconItems arix ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.status <> 'AuthorizedReconItem_Complete' -- only OPEN AuthorizedReconItem
  AND arix.typ <> 'AuthorizedReconItem_Removed' -- only NOT removed AuthorizedReconItems 
LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
LEFT JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID 
WHERE EXISTS ( -- current ReconAuthorization only -- hmm, this also seems to exclude removed aris, 
  SELECT 1     -- which makes sense, WHEN an AuthorizedReconItem IS removed, the ReconAuthorization IS closed AND a new one IS created
  FROM ReconAuthorizations 
  WHERE ReconAuthorizationID = arix.ReconAuthorizationID
  AND thruts IS NULL)
AND vrix.typ LIKE 'M%'
AND vrix.PartsInStock = True
AND po.VehicleReconItemID IS NULL
AND viix.stocknumber IS NOT NULL 
ORDER BY viix.stocknumber

-- *** ORDER TO BE CANCELLED ***
-- Orderdered AND NOT Received
-- VehicleReconItems removed :: ari.typ = Removed
-- AuthorizedReconItem doesn't exist IN current ReconAuthorization

SELECT viix.stocknumber, po.* 
FROM PartsOrders po
INNER JOIN AuthorizedReconItems arix ON po.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.typ = 'AuthorizedReconItem_Removed'
INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
INNER JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID  
WHERE po.OrderedTS IS NOT NULL
AND po.ReceivedTS IS NULL
AND po.CancelledTS IS NULL
AND po.ReturnedTS IS NULL 

-- ORDER to be returned
-- ORDER received
-- VehicleReconItems removed 

SELECT viix.stocknumber, po.* 
FROM PartsOrders po
INNER JOIN AuthorizedReconItems arix ON po.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.typ = 'AuthorizedReconItem_Removed'
INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID
INNER JOIN VehicleInventoryItems viix ON vrix.VehicleInventoryItemID = viix.VehicleInventoryItemID   
WHERE po.OrderedTS IS NOT NULL
AND po.ReceivedTS IS NOT NULL
AND po.CancelledTS IS NULL
AND po.ReturnedTS IS NULL 