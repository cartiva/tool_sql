SELECT cast(VehicleEvaluationTS as sql_date) as EvalDate, disposition, stocknumber, yearmodel AS year, make, model, milesperyear AS "miles per year", 
  evalAppeal, evalDemand, substring(evalpackage, position('_' IN evalpackage) + 1, 8) AS package, evalACV, 
  TotalEvalRecon, 
  evalacv + totalevalrecon 
    +
    CASE
      WHEN source = 'Auction' THEN 350
      ELSE 0
    END 
    +
    CASE
      WHEN evalpackage = 'ReconPackage_Factory' THEN
      CASE make
        WHEN 'Cadillac' THEN 1200
        WHEN 'Chevrolet' THEN 399
        WHEN 'Buick' THEN 399
        WHEN 'GMC' THEN 399
        WHEN 'Honda' THEN 400
        WHEN 'Nissan' THEN 325
        ELSE 0
      END 
      ELSE 0
    END
    +
    CASE evalpackage
      WHEN 'ReconPackage_AsIs' THEN 0
      WHEN 'ReconPackage_AsIsWS' THEN 0
      WHEN 'ReconPackage_WS' THEN 0
      ELSE 500
    END 
    +
    CASE EvalAppeal
      WHEN 'W/S' THEN 0
      ELSE
        CASE
          WHEN cast(coalesce(evalappeal, evaldemand) AS sql_integer) + cast(evaldemand AS sql_integer) = 2 THEN -- 1
            CASE evalPackage
              WHEN 'ReconPackage_AsIsWS' THEN 500
              ELSE 1800
            END 
          WHEN cast(coalesce(evalappeal, evaldemand) AS sql_integer) + cast(evaldemand AS sql_integer) > 4 THEN -- 3
            CASE evalPackage
              WHEN 'ReconPackage_AsIsWS' THEN 1200
              ELSE 3800
            END         
          ELSE -- 2
            CASE evalPackage
              WHEN 'ReconPackage_AsIsWS' THEN 900
              ELSE 2700
            END         
        END 
    END AS "IN Transit Price w/formula",
    evalacv + totalevalrecon
    +
    CASE
      WHEN evalpackage = 'ReconPackage_Factory' THEN
      CASE make
        WHEN 'Cadillac' THEN 1200
        WHEN 'Chevrolet' THEN 399
        WHEN 'Buick' THEN 399
        WHEN 'GMC' THEN 399
        WHEN 'Honda' THEN 400
        WHEN 'Nissan' THEN 325
        ELSE 0
      END 
      ELSE 0
    END 
    + 3000 AS "IN Transit Price - no formula",   
  walkprice, lastprice, lastpricedate, soldamount
 
--  cast(coalesce(evalappeal, evaldemand) AS sql_integer) + cast(evaldemand AS sql_integer),
--  CASE -- cast(coalesce(evalappeal, evaldemand) AS sql_integer) + cast(evaldemand AS sql_integer)
--    WHEN cast(coalesce(evalappeal, evaldemand) AS sql_integer) + cast(evaldemand AS sql_integer) = 2 THEN 1
--    WHEN cast(coalesce(evalappeal, evaldemand) AS sql_integer) + cast(evaldemand AS sql_integer) > 4 THEN 3
--    ELSE 2
--  END
--SELECT * 
-- DROP TABLE #wtf
INTO #wtf
FROM #jon j

SELECT * FROM #wtf

SELECT COUNT(*), round(SUM("IN Transit Price - no formula" - walkprice)/ COUNT(*), 0) AS "Avg No Formula",
round(SUM("IN Transit Price w/formula" - walkprice)/COUNT(*), 0) AS "avg Formula"
FROM #wtf

