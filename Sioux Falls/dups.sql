SELECT vehicleid, stocknumber, vin, dateacquired, salesstatus
FROM usedcars
WHERE vin IN (
  SELECT vin
  FROM usedcars
  WHERE trim(market) = 'SF-Sioux Falls'
  GROUP BY vin, stocknumber
  HAVING COUNT(*) > 1)
AND stocknumber IN (
  SELECT stocknumber
  FROM usedcars
  WHERE trim(market) = 'SF-Sioux Falls'
  GROUP BY vin, stocknumber
  HAVING COUNT(*) > 1)
ORDER BY stocknumber



-- ok this limits it to exact duplicates, including salesstatus, only
SELECT COUNT(*) FROM (
SELECT t.vehicleid, t.stocknumber, t.vin, t.salesstatus
FROM usedcars t
INNER JOIN (
  SELECT stocknumber, vin, salesstatus
  FROM usedcars
  WHERE TRIM(market) = 'SF-Sioux Falls'
  GROUP BY stocknumber, vin, salesstatus
  HAVING COUNT(*) > 1) d ON t.stocknumber = d.stocknumber
    AND t.vin = d.vin
    AND t.salesstatus = d.salesstatus
WHERE TRIM(market) = 'SF-Sioux Falls') wtf  
ORDER BY t.vin    


DELETE 
-- SELECT COUNT(*) -- 575
FROM usedcars 
WHERE vehicleid in (
  SELECT max(t.vehicleid)
  FROM usedcars t
  INNER JOIN (
    SELECT stocknumber, vin, salesstatus
    FROM usedcars
    WHERE TRIM(market) = 'SF-Sioux Falls'
    GROUP BY stocknumber, vin, salesstatus
    HAVING COUNT(*) > 1) d ON t.stocknumber = d.stocknumber
      AND t.vin = d.vin
      AND t.salesstatus = d.salesstatus
  WHERE TRIM(market) = 'SF-Sioux Falls'
  GROUP BY t.stocknumber, t.vin, t.salesstatus)