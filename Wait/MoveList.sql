/*
              pulledTS                     pbTS                          Test                                                            Output 
                |----------------------------|
1.  FromTS|-------------|ThruTS                            FromTS < pulledTS AND ThruTS > pulledTS AND ThruTS <= pbTS      ThruTS - pulledTS
2.  FromTS|-------------------------------------|ThruTS    FromTS < pulledTS AND ThruTS >= pbTS                                pbTS - pulledTS           
3        FromTS|--------------|ThruTS                      FromTS >= pulledTS AND FromTS < pbTS AND ThruTS <= pbTS            ThruTS - FromTS
4.       FromTS|--------------------------------|ThruTS    FromTS >= pulledTS AND pulledTS < pbTS AND ThruTS >= pbTS           pbTS - FromTS                                                       
*/ 

SELECT j.VehicleInventoryItemID, pulledTS, pbTS, m.FromTS, m.ThruTS
FROM #jon j
LEFT JOIN vehiclestomove m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE m.ThruTS > j.pulledTS
AND m.FromTS <= j.pbTS

-- can be multiple moves IN the interval
-- so first, individual business hours
SELECT j.VehicleInventoryItemID, pulledTS, pbTS, m.FromTS, m.ThruTS,
  CASE 
    WHEN FromTS < pulledTS AND ThruTS > pulledTS AND ThruTS <= pbTS THEN      
      (select Utilities.BusinessHoursFromInterval(pulledTS, ThruTS, 'Service') FROM system.iota)
    WHEN FromTS < pulledTS AND ThruTS >= pbTS THEN 
      (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota)  
    WHEN FromTS >= pulledTS AND FromTS < pbTS AND ThruTS <= pbTS THEN  
      (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota) 
    WHEN  FromTS >= pulledTS AND pulledTS < pbTS AND ThruTS >= pbTS THEN 
      (select Utilities.BusinessHoursFromInterval(FromTS, pbTS, 'Service') FROM system.iota)      
   END AS MoveBusHours
FROM #jon j
LEFT JOIN vehiclestomove m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE m.ThruTS > j.pulledTS
AND m.FromTS <= j.pbTS

-- GROUP it BY vehicle - gives total move time within interval BY vehicle
SELECT j.VehicleInventoryItemID, 
  SUM (
    CASE 
      WHEN FromTS < pulledTS AND ThruTS > pulledTS AND ThruTS <= pbTS THEN      
        (select Utilities.BusinessHoursFromInterval(pulledTS, ThruTS, 'Service') FROM system.iota)
      WHEN FromTS < pulledTS AND ThruTS >= pbTS THEN 
        (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota)  
      WHEN FromTS >= pulledTS AND FromTS < pbTS AND ThruTS <= pbTS THEN  
        (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota) 
      WHEN  FromTS >= pulledTS AND pulledTS < pbTS AND ThruTS >= pbTS THEN 
        (select Utilities.BusinessHoursFromInterval(FromTS, pbTS, 'Service') FROM system.iota)      
     END) AS MoveBusHours
FROM #jon j
LEFT JOIN vehiclestomove m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID 
-- this IS the key to just getting the right movesw
WHERE m.ThruTS > j.pulledTS
AND m.FromTS <= j.pbTS
GROUP BY j.VehicleInventoryItemID 


-- shit but what about BY week -- don't care, that IS done IN the
-- overall query, linking to these BY vehicle

-- same AS recon, need to generate a temp TABLE that can be joined to IN the big TABLE
-- unlike recon, there will NOT be more than 1 move with the same from/thru
-- LIKE recon, there can be more than 1 move IN the interval

-- so i know that this data IS ALL relevant, every one of these records IS a move within the interval
-- so, yes it's ok to get the bus for each interval


-- now narrow it down to one record for each vehicle, with total MoveBusHours
-- IS this the time to generate the temp TABLE for joining to the base interval TABLE
-- the move version of #ReconWIP
-- let's TRY it
-- don't need pull/pb ts IN this temp TABLE, i think


-- now TRY the JOIN 
-- select * FROM #move


-- must contain a single record for each vehicle
SELECT j.VehicleInventoryItemID, 
  SUM(
    CASE 
      WHEN FromTS < pulledTS AND ThruTS > pulledTS AND ThruTS <= pbTS THEN      
        (select Utilities.BusinessHoursFromInterval(pulledTS, ThruTS, 'Service') FROM system.iota)
      WHEN FromTS < pulledTS AND ThruTS >= pbTS THEN 
        (select Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota)  
      WHEN FromTS >= pulledTS AND FromTS < pbTS AND ThruTS <= pbTS THEN  
        (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota) 
      WHEN  FromTS >= pulledTS AND pulledTS < pbTS AND ThruTS >= pbTS THEN 
        (select Utilities.BusinessHoursFromInterval(FromTS, pbTS, 'Service') FROM system.iota)      
     END) AS MoveBusHours
-- DROP TABLE #move   
-- INTO #move   
FROM #jon j
LEFT JOIN vehiclestomove m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID 
WHERE m.ThruTS > j.pulledTS
AND m.FromTS <= j.pbTS
GROUP BY j.VehicleInventoryItemID 
-- so now this gives me each vehicle that was moved within the pull-pb interval
-- AND the total amount of move bushours
-- with no dups
SELECT VehicleInventoryItemID FROM #move GROUP BY VehicleInventoryItemID HAVING COUNT(*) > 1


-- should this be avg hours move list for vehicles that were moved
-- OR avg hours move list for ALL vehicles that are IN pull-pb interval
-- need count/per week
-- total move time per week



SELECT TheWeek,
  MIN(TheDate) AS "Week First Day", MAX(TheDate) AS "Week Last Day",
  COUNT(j.VehicleInventoryItemID) AS "to PB (Count)",
  SUM(timestampdiff(sql_tsi_day, cast(pulledTS AS sql_date), cast(pbTS AS sql_date)))/COUNT(j.VehicleInventoryItemID) AS "Avg Days Pull->PB per Vehicle",
  SUM((SELECT Utilities.BusinessHoursFromInterval(pulledTS, pbTS, 'Service') FROM system.iota))/COUNT(j.VehicleInventoryItemID) AS "Avg Bus Hours Pull->PB per Vehicle",
  SUM(MechCount) AS MechCount,
  CASE 
    WHEN SUM(MechCount) <> 0 THEN SUM(MechBusHours)/SUM(MechCount) 
    ELSE 0
  END  AS "Avg Mech WIP BusHours per Vehicle",
  SUM(BodyCount) AS BodyCount,
  CASE
    WHEN SUM(BodyCount) <> 0 THEN SUM(BodyBusHours)/SUM(BodyCount) 
    ELSE 0
  END AS "Avg Body WIP BusHours per Vehicle",
  SUM(AppCount) AS AppCount,
  CASE
    WHEN SUM(AppCount) <> 0 THEN SUM(AppBusHours)/SUM(AppCount) 
    ELSE 0
  END AS "Avg App WIP BusHours per Vehicle",
  COUNT(m.VehicleInventoryItemID) AS VehiclesMoved, -- number of cars moved
  SUM(MoveBusHours)/COUNT(m.VehicleInventoryItemID) AS "Avg MoveBusHours per Vehicle Moved",
  SUM(MoveBusHours)/COUNT(j.VehicleInventoryItemID) AS "Avg MoveBusHours per Vehicle"  
FROM #jon j
LEFT JOIN #ReconWip r ON j.VehicleInventoryItemID = r.VehicleInventoryItemID 
LEFT JOIN #move m ON j.VehicleInventoryItemID = m.VehicleInventoryItemID 
GROUP BY TheWeek  


-- well AND good, but what i really need IS to break up the move hours to the various depts
-- DROP TABLE #move
SELECT x.VehicleInventoryItemID, 
  SUM(
    CASE 
      WHEN LEFT(seq, 1) = 'm' THEN 
        CASE
          WHEN FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
     WHEN substring(seq, 2, 1) = 'm' THEN
        CASE   
          WHEN LEFT(seq, 1) = 'b' THEN 
            CASE
              WHEN FromTS >= BodyEnd AND FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
          WHEN LEFT(seq, 1) = 'a' THEN 
            CASE
              WHEN FromTS >= AppEnd AND FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
        END
    WHEN seq = 'bam' THEN
      CASE 
        WHEN FromTS >= AppEnd AND FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END  
    WHEN seq = 'abm' THEN 
      CASE
        WHEN FromTS >= BodyEnd AND FromTS < MechStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END 
    END) AS "Mech",
  SUM(
    CASE
      WHEN LEFT(seq, 1) = 'b' THEN 
        CASE
          WHEN FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
     WHEN substring(seq, 2, 1) = 'b' THEN
        CASE   
          WHEN LEFT(seq, 1) = 'm' THEN 
            CASE
              WHEN FromTS >= MechEnd AND FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
          WHEN LEFT(seq, 1) = 'a' THEN 
            CASE
              WHEN FromTS >= AppEnd AND FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
        END
    WHEN seq = 'mab' THEN
      CASE 
        WHEN FromTS >= AppEnd AND FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END  
    WHEN seq = 'amb' THEN 
      CASE
        WHEN FromTS >= MechEnd AND FromTS < BodyStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END     
    END) AS "Body",
  SUM(
    CASE
      WHEN LEFT(seq, 1) = 'a' THEN 
        CASE
          WHEN FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
        END 
     WHEN substring(seq, 2, 1) = 'a' THEN
        CASE   
          WHEN LEFT(seq, 1) = 'm' THEN 
            CASE
              WHEN FromTS >= MechEnd AND FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
          WHEN LEFT(seq, 1) = 'b' THEN 
            CASE
              WHEN FromTS >= BodyEnd AND FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
            END 
        END
    WHEN seq = 'mba' THEN
      CASE 
        WHEN FromTS >= BodyEnd AND FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END  
    WHEN seq = 'bma' THEN 
      CASE
        WHEN FromTS >= MechEnd AND FromTS < AppStart THEN (select Utilities.BusinessHoursFromInterval(FromTS, ThruTS, 'Service') FROM system.iota)
      END     
    END) AS "App"
-- SELECT * 
INTO #move
FROM (
  SELECT w.*, m.FromTS, m.ThruTS,
    CASE
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'none'
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NULL THEN 'm'
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NULL THEN 'b'
      WHEN MechStart IS NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN 'a'
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart is NULL THEN
        CASE
          WHEN MechStart < BodyStart THEN 'mb'
          ELSE 'bm'
        END
      WHEN MechStart IS NOT NULL AND BodyStart IS NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN MechStart < AppStart THEN 'ma'
          ELSE 'am'
        END
      WHEN MechStart IS NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN
        CASE
          WHEN BodyStart < AppStart THEN 'ba'
          ELSE 'ab'
        END 
      WHEN MechStart IS NOT NULL AND BodyStart IS NOT NULL AND AppStart IS NOT NULL THEN 
        CASE 
          WHEN MechStart < BodyStart AND MechStart < AppStart AND BodyStart < AppStart THEN 'mba'
          WHEN MechStart < BodyStart AND MechStart < AppStart AND AppStart < BodyStart THEN 'mab'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND MechStart < AppStart THEN 'bma'
          WHEN BodyStart < MechStart AND BodyStart < AppStart AND AppStart < MechStart THEN 'bam'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND BodyStart < MechStart THEN 'abm'
          WHEN AppStart < BodyStart AND AppStart < MechStart AND MechStart < BodyStart THEN 'amb'
        END 
    END AS seq   
  FROM #WipStartTimes w
  LEFT JOIN vehiclestomove m ON w.VehicleInventoryItemID = m.VehicleInventoryItemID 
  WHERE m.ThruTS > w.pulledTS
  AND m.FromTS <= w.pbTS) x  
WHERE LEFT(seq, 1) = 'm' -- AND VehicleInventoryItemID = '0c550b55-5eb4-42e5-965e-0a83786152d3'
GROUP BY VehicleInventoryItemID  