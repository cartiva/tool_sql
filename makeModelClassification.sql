     
-- usually the issue IS non classified vehicles    
SELECT b.make, b.model, i.stocknumber, i.VehicleInventoryItemID, b.VehicleItemID, b.vin, CAST(i.fromts AS sql_date)
FROM VehicleInventoryItems i
LEFT JOIN VehicleItems b on i.VehicleItemID = b.VehicleItemID 
WHERE ThruTs IS NULL
AND NOT EXISTS (
  SELECT 1
  FROM MakeModelClassifications
  WHERE model = (
    SELECT model
    FROM VehicleItems
    WHERE VehicleItemID = i.VehicleItemID))    
    
SELECT *
FROM MakeModelClassifications -- WHERE model = 'fusion'  
WHERE make = 'ford'
ORDER BY model

SELECT * FROM VehicleItems WHERE vin = 'SPL31118788'

select * FROM MakeModelClassifications 
SELECT DISTINCT vehiclesegment, vehicletype FROM MakeModelClassifications 
SELECT * FROM MakeModelClassifications WHERE vehicletype = 'VehicleType_Crossover'
-- Black book now separates RAM out FROM dodge AS a make
INSERT INTO MakeModelClassifications 
values('Ram', '1500', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
INSERT INTO MakeModelClassifications 
values('Ram', '3500', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- chevy sonic
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Sonic', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- 1952 chevy deluxe
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Deluxe', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Domestic')
-- Volks CC
INSERT INTO MakeModelClassifications 
values('Volkswagen', 'CC', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '06/01/2012 08:00:00', NULL, 'MfgOriginTyp_Import')
-- buick encore
INSERT INTO MakeModelClassifications 
values('Buick', 'Encore', 'VehicleSegment_Compact','VehicleType_Crossover',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- toyota Prius V
INSERT INTO MakeModelClassifications 
values('Toyota', 'Prius V', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Subaru BRZ
INSERT INTO MakeModelClassifications 
values('Subaru', 'BRZ', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- KIA Forte
INSERT INTO MakeModelClassifications 
values('KIA', 'Forte', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- impala limited
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Impala Limited LTZ', 'VehicleSegment_Large','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')


SELECT * FROM MakeModelClassifications WHERE model like 'Impala%'

SELECT * FROM MakeModelClassifications WHERE model = 'Impala Limited LTZ'

SELECT *
FROM VehicleItems
WHERE vin = 'NM0LS7DN7AT010012'

SELECT *
FROM blackbookresolver
WHERE vin = '1C6RD7LT'

-- ford transit connect van
INSERT INTO MakeModelClassifications 
values('Ford', 'Transit Connect', 'VehicleSegment_Midsize','VehicleType_Van',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- missing FROM MakeModelClassifications 
SELECT a.stocknumber, b.vin, b.yearmodel, b.make, b.model, CAST(a.fromts AS sql_date)
FROM VehicleInventoryItems a
INNER JOIN VehicleItems b on a.VehicleItemID = b.VehicleItemID
WHERE a.thruTS IS NULL
AND NOT EXISTS (
  SELECT 1
  FROM MakeModelClassifications
  WHERE make = b.make
    AND model = b.model)

-- suzuki kizashi   
INSERT INTO MakeModelClassifications 
values('Suzuki', 'Kizashi', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- subaru xv crosstrek
SELECT * FROM MakeModelClassifications WHERE model = 'juke'

INSERT INTO MakeModelClassifications 
values('Subaru', 'XV Crosstrek', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Meredecs-Benz GLK Class

SELECT DISTINCT make, model
FROM MakeModelClassifications
WHERE make LIKE 'merced%'

SELECT DISTINCT make, model
FROM MakeModelClassifications
WHERE vehiclesegment = 'VehicleSegment_Compact'
  AND vehicleType = 'VehicleType_SUV'
  
SELECT * FROM MakeModelClassifications WHERE model = 'touareg'

INSERT INTO MakeModelClassifications 
values('Mercedes-Benz', 'GLK Class', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- volks tiguan
SELECT * FROM MakeModelClassifications WHERE make = 'volkswagen'
SELECT * FROM MakeModelClassifications WHERE make = 'mitsubishi' AND model = 'outlander'

INSERT INTO MakeModelClassifications 
values('Volkswagen', 'Tiguan', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- hyundai veloster
INSERT INTO dps.MakeModelClassifications 
values('Hyundai', 'Veloster', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- MV-1 van
INSERT INTO MakeModelClassifications 
values('MV-1', 'Deluxe', 'VehicleSegment_Midsize','VehicleType_Van',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Chevrolet Citation
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Citation', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Hyundai Santa Fe Sport (same AS buck enclave)
INSERT INTO MakeModelClassifications 
SELECT 'Hyundai','Santa Fe Sport',vehiclesegment,vehicletype,luxury,commercial,
  sport, '01/01/2014 08:00:00', ThruTS, 'MfgOriginTyp_Import' 
FROM MakeModelClassifications 
WHERE model = 'enclave';

-- isuzu flat bed
INSERT INTO MakeModelClassifications 
SELECT make,'Flat Bed',vehiclesegment,vehicletype,luxury,commercial,sport,fromts,
  thruts,mfgOriginTyp
FROM MakeModelClassifications
WHERE make = 'isuzu'
AND model = 'i-370'

-- Chev city express
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'City Express', 'VehicleSegment_Midsize','VehicleType_Van',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- prius C
INSERT INTO MakeModelClassifications 
values('Toyota', 'Prius C', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- subaru wrx
INSERT INTO MakeModelClassifications 
values('Subaru', 'WRX', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Chev Trax
INSERT INTO MakeModelClassifications 
values('Chevrolet', 'Trax', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Audi Q5
INSERT INTO MakeModelClassifications 
values('Audi', 'Q5', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Fiat 500
INSERT INTO MakeModelClassifications 
values('Fiat', '500', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '01/01/2014 08:00:00', NULL, 'MfgOriginTyp_Import')

-- ford fiesta
INSERT INTO MakeModelClassifications 
values('Ford', 'Fiesta', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- ram dakota
INSERT INTO MakeModelClassifications 
values('Ram', 'Dakota', 'VehicleSegment_Midsize','VehicleType_Pickup',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Mitsubishi outlander sport
INSERT INTO MakeModelClassifications 
values('Mitsubishi', 'Outlander Sport', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Nissan Versa Note
INSERT INTO MakeModelClassifications 
values('Nissan', 'Versa Note', 'VehicleSegment_Compact','VehicleType_Car',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Cadillac ELR
INSERT INTO MakeModelClassifications 
values('Cadillac', 'ELR', 'VehicleSegment_Large','VehicleType_Car',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Jeep Renegade
INSERT INTO MakeModelClassifications 
values('Jeep', 'Renegade', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- cruze limited
INSERT INTO MakeModelClassifications 
select make, 'Cruze Limited', vehiclesegment,vehicletype,luxury,commercial,
  sport,now(),cast(NULL AS sql_timestamp), mfgorigintyp
-- select *  
FROM MakeModelClassifications 
WHERE model = 'cruze';

-- malibu limited
INSERT INTO MakeModelClassifications 
select make, 'Malibu Limited', vehiclesegment,vehicletype,luxury,commercial,
  sport,now(),cast(NULL AS sql_timestamp), mfgorigintyp
-- select *  
FROM MakeModelClassifications 
WHERE model = 'malibu';

-- Infinity JX
INSERT INTO MakeModelClassifications 
values('Infiniti', 'JX', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '11/01/2015 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Datsun 280Z
INSERT INTO MakeModelClassifications 
values('Datsun', '280Z', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Buick Cascada
INSERT INTO MakeModelClassifications 
values('Buick', 'Cascada', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- GMC Acadia Limited
INSERT INTO MakeModelClassifications
values('GMC', 'Acadia Limited', 'VehicleSegment_Midsize','VehicleType_Crossover',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Cadillac XT5
INSERT INTO MakeModelClassifications
values('Cadillac', 'XT5', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Ford C-Max
INSERT INTO MakeModelClassifications
values('Ford', 'C-Max', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Kia Cadenza
INSERT INTO MakeModelClassifications
values('Kia', 'Cadenza', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Buick Envision
INSERT INTO MakeModelClassifications
values('Buick', 'Envision', 'VehicleSegment_Midsize','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Chevrolet Biscayne
INSERT INTO MakeModelClassifications
values('Chevrolet', 'Biscayne', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Honda Crosstour
INSERT INTO MakeModelClassifications
values('Honda', 'Crosstour', 'VehicleSegment_Compact','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- subaru crosstrek
INSERT INTO MakeModelClassifications
values('Subaru', 'Crosstrek', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Honda HR-V
INSERT INTO MakeModelClassifications
values('Honda', 'HR-V', 'VehicleSegment_Small','VehicleType_SUV',false, false, false, '12/01/2016 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Ford F550SD
INSERT INTO MakeModelClassifications 
values('Ford', 'F550SD', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Ford F650SD
INSERT INTO MakeModelClassifications 
values('Ford', 'F650', 'VehicleSegment_Large','VehicleType_Pickup',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

-- Datsun Fairlady
INSERT INTO MakeModelClassifications 
values('Datsun', 'Fairlady', 'VehicleSegment_Small','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- BMW 4-Series
INSERT INTO MakeModelClassifications 
values('BMW', '4-Series', 'VehicleSegment_Midsize','VehicleType_Car',false, false, false, '09/01/2013 08:00:00', NULL, 'MfgOriginTyp_Import')

-- Ford T250
INSERT INTO MakeModelClassifications 
values('Ford', 'T250 Vans', 'VehicleSegment_Large','VehicleType_Van',false, false, false, '01/01/2015 08:00:00', NULL, 'MfgOriginTyp_Domestic')

