SELECT *
FROM 
(EXECUTE PROCEDURE GetFreshInventory('A4847DFC-E00E-42E7-89EE-CD4368445A82', '', 14)) wtf


-- GetLocatorVehicles
-- PictureAvailable
SELECT vii.stocknumber, 
  (SELECT 
    CASE 
    WHEN (
      SELECT viip.Picture 
      FROM VIIPictures viip  
      INNER JOIN VIIPictureSequences viips 
      ON viips.SequenceNo = viip.SequenceNo 
      AND viips.RepresentativePicture = True 
  	  AND viips.SetType = (
        SELECT viipu.SetType 
        FROM VIIPictureUploads viipu 
        WHERE viipu.VehicleInventoryItemID = vii.VehicleInventoryItemID)  
      WHERE viip.VehicleInventoryItemID = vii.VehicleInventoryItemID) is NULL  
      THEN False  
      ELSE True  
    END FROM system.iota) AS picav,
  (SELECT viip.SequenceNo
    FROM VIIPictures viip
    INNER JOIN VIIPictureSequences viips
    ON viips.SequenceNo = viip.SequenceNo
    AND viips.RepresentativePicture = True
    AND viips.SetType = (
      SELECT viipu.SetType
      FROM VIIPictureUploads viipu
      WHERE viipu.VehicleInventoryItemID = vii.VehicleInventoryItemID)
      WHERE viip.VehicleInventoryItemID = vii.VehicleInventoryItemID) AS PictureID    
FROM (EXECUTE PROCEDURE GetFreshInventory('A4847DFC-E00E-42E7-89EE-CD4368445A82', '', 14)) vii
ORDER BY stocknumber


SELECT vii.stocknumber, 
  (SELECT viip.SequenceNo
    FROM VIIPictures viip
    INNER JOIN VIIPictureSequences viips
    ON viips.SequenceNo = viip.SequenceNo
    AND viips.RepresentativePicture = True
    AND viips.SetType = (
      SELECT viipu.SetType
      FROM VIIPictureUploads viipu
      WHERE viipu.VehicleInventoryItemID = vii.VehicleInventoryItemID)
      WHERE viip.VehicleInventoryItemID = vii.VehicleInventoryItemID) AS PictureID
FROM (EXECUTE PROCEDURE GetFreshInventory('A4847DFC-E00E-42E7-89EE-CD4368445A82', '', 14)) vii      