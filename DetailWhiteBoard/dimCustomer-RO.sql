SELECT * FROM factroline

SELECT * FROM factro

SELECT * FROM stgArkonaSDPLOPC


SELECT * FROM stgArkonaSDPLOPC WHERE sohmchg + socamt + sopamt <> 0

SELECT COUNT(*) FROM (
SELECT bnco#, bnkey
FROM stgArkonaBOPNAME
GROUP BY bnco#, bnkey
HAVING COUNT(*) > 1
) x 


SELECT *
FROM stgarkonabopname
WHERE bnkey IN (
  SELECT bnkey
  FROM stgArkonaBOPNAME
  GROUP BY bnco#, bnkey
  HAVING COUNT(*) > 1)

-- CAVEAT EMPTOR --------------------------------------------------------------
very small (21) subset with dups, so exclude these  
start out with old CustomerKeyFromDates = 7/31/2009 (bncrtts = 1/1/0001)

-- 7/17 for ROs will be retrieving  cust info based ON combination of bnkey AND name, 
-- so that needs to be the criteria FROM no dups IN dimCustomer

SELECT bnkey FROM (
select bnco#, bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
from stgArkonaBOPNAME
group by bnco#, bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail  
) x GROUP BY bnco#, bnkey, bnsnam HAVING COUNT(*) > 1

SELECT * FROM stgArkonaBOPNAME WHERE bnkey = 1036816
-- hmm WHERE bntype = '' either the name IS blank OR it IS old data 11/22/2o11
SELECT * FROM stgArkonaBOPNAME WHERE bntype = '' ORDER BY bnupdts DESC
SELECT customertype, COUNT(*) FROM dimcustomer GROUP BY customertype
SELECT * FROM dimcustomer WHERE customertype = 'unknown'
SELECT * FROM dimcustomer WHERE fullname = ''
DELETE FROM keymapDimCustomer WHERE CustomerKey IN (select customerkey FROM dimcustomer WHERE customertype = 'unknown' OR (fullname = '' AND lastname = ''))
DELETE FROM dimcustomer WHERE customertype = 'unknown' OR (fullname = '' AND lastname = '')

SELECT bnkey,fullname FROM dimCustomer GROUP BY bnkey,fullname HAVING COUNT(*) > 1

SELECT bnkey FROM (
select bnco#, bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
from stgArkonaBOPNAME
group by bnco#, bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail  
) x GROUP BY bnco#, bnkey HAVING COUNT(*) > 1

SELECT bnkey, bnsnam FROM (
select bnco#, bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail
from stgArkonaBOPNAME
group by bnco#, bnkey, bnsnam, bnphon, bnbphn, bncphon, bnemail 
) x GROUP BY bnkey, bnsnam HAVING COUNT(*) > 1

SELECT * FROM stgArkonaBOPNAME WHERE bnkey IN (1000123,1000324,1036816)

SELECT COUNT(*) FROM (
SELECT bnco#, bnkey
FROM stgArkonaBOPNAME
GROUP BY bnco#, bnkey HAVING COUNT(*) > 1
) x

SELECT bnsalu, COUNT(*) FROM stgArkonaBOPNAME GROUP BY bnsalu
-- fuck county, collection IS NOT reliable, best bet may be zipcode &/OR city
SELECT bncnty, COUNT(*) FROM stgArkonaBOPNAME GROUP BY bncnty ORDER BY COUNT(*) DESC
-- actually they are ALL pretty spotty, so leave them ALL IN 
SELECT bnzip, COUNT(*) FROM stgArkonaBOPNAME GROUP BY bnzip ORDER BY COUNT(*) DESC
SELECT bncity, COUNT(*) FROM stgArkonaBOPNAME GROUP BY bncity ORDER BY COUNT(*) desc

initial cut of dimCustomer will be a small subset of information that seems
relevant to ROs, pulled FROM BOPNAME

SELECT bnco#,bnkey,bntype,bnsnam,bnlnam, bnfnam,bnmidi,bnphon,bnbphn, 
  bncphon,bnemail,bncity,bncnty,bnstcd,bnzip,bncrtts, bnupdts
-- SELECT COUNT(*)   
FROM stgArkonaBOPNAME a
WHERE bnco# IN ('RY1','RY2','RY3')
  AND NOT EXISTS (
    SELECT 1
    FROM (
      SELECT bnco#, bnkey
      FROM stgArkonaBOPNAME
      GROUP BY bnco#, bnkey HAVING COUNT(*) > 1) z  
    WHERE z.bnco# = a.bnco#
      AND z.bnkey = a.bnkey)      
ORDER BY bnupdts DESC     


SELECT bnco#,bnkey,bntype,bnsnam,bnlnam, bnfnam,bnmidi,bnphon,bnbphn, 
  bncphon,bnemail,bncity,bncnty,bnstcd,bnzip,bncrtts, bnupdts
-- SELECT COUNT(*)   
FROM stgArkonaBOPNAME a
WHERE bnco# IN ('RY1','RY2','RY3')
  AND NOT EXISTS (
    SELECT 1
    FROM (
      SELECT bnco#, bnkey
      FROM stgArkonaBOPNAME
      GROUP BY bnco#, bnkey HAVING COUNT(*) > 1) z  
    WHERE z.bnco# = a.bnco#
      AND z.bnkey = a.bnkey)   
      
      
SELECT bncrtts, bnupdts,
  CAST(LEFT(bncrtts,10) AS sql_date)
FROM stgarkonaBOPNAME
WHERE bnkey = 1070278 
  

-- NK
SELECT bnco#,bnkey,bntype,bnsnam,bnlnam, bnfnam,bnmidi,bnphon,bnbphn, 
  bncphon,bnemail,bncity,bncnty,bnstcd,bnzip,bncrtts, bnupdts
-- SELECT COUNT(*)   
INTO #wtfr
FROM stgArkonaBOPNAME a
WHERE bnco# IN ('RY1','RY2','RY3')
  AND NOT EXISTS (
    SELECT 1
    FROM (
      SELECT bnco#, bnkey
      FROM stgArkonaBOPNAME
      GROUP BY bnco#, bnkey HAVING COUNT(*) > 1) z  
    WHERE z.bnco# = a.bnco#
      AND z.bnkey = a.bnkey)     
      
SELECT COUNT(*) FROM #wtfr  -- 182447
-- bingo, here IS the NK 
SELECT bnco#, bnkey
FROM #wtfr
GROUP BY bnco#, bnkey
HAVING COUNT(*) > 1  

DROP TABLE dimCustomer;
CREATE TABLE dimCustomer (
  CustomerKey autoinc,
  StoreCode cichar(3),
  BNKEY integer,
  CustomerTypeCode cichar(1),
  CustomerType cichar(12),
  FullName cichar(50),
  LastName cichar(50),
  FirstName cichar(15),
  MiddleName cichar(25),
  HomePhone cichar(12),
  BusinessPhone cichar(20),
  CellPhone cichar(12),
  Email cichar(60),
  City cichar(20),
  County cichar(20),
  State cichar(2),
  Zip cichar(9),
  CurrentRow logical,
  RowChangeDate date,
  RowChangeDateKey integer,
  RowFromTS timestamp,
  RowThruTS timestamp,
  RowChangeReason cichar(100),
  CustomerKeyFromDate date,
  CustomerKeyFromDateKey integer,
  CustomerKeyThruDate date,
  CustomerKeyThruDateKey integer) IN database;
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'dimCustomer','dimCustomer.adi','PK',
   'CustomerKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('dimCustomer','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'dimCustomerObjectsfail');   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','StoreCode', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','BNKEY', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKey', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKeyFromDate', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );  
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKeyFromDateKey', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKeyThruDate', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );   
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'dimCustomer','CustomerKeyThruDateKey', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );     


DELETE FROM dimCustomer;    
INSERT INTO dimCustomer (StoreCode,BNKEY,CustomerTypeCode,CustomerType,
  FullName,LastName,FirstName,MiddleName,HomePhone,BusinessPhone,CellPhone,
  Email,City,County,State,Zip, CurrentRow, CustomerKeyFromDate, CustomerKeyFromDateKey,
  CustomerKeyThruDate, CustomerKeyThruDateKey)
SELECT bnco#,bnkey,bntype,
  CASE bntype
    WHEN 'C' THEN 'Company'
    WHEN 'I' THEN 'Person'
    ELSE 'Unknown'
  END,
  bnsnam,bnlnam, bnfnam,bnmidi,bnphon,bnbphn, 
  bncphon,bnemail,bncity,bncnty,bnstcd,bnzip, true,
  CAST(LEFT(bncrtts,10) AS sql_date),
  (SELECT datekey FROM day WHERE thedate = CAST(LEFT(bncrtts,10) AS sql_date)),
  (SELECT thedate FROM day WHERE datetype = 'NA'),
  (SELECT datekey FROM day WHERE datetype = 'NA')
-- SELECT COUNT(*)   
FROM stgArkonaBOPNAME a
WHERE bnco# IN ('RY1','RY2','RY3')
  AND NOT EXISTS (
    SELECT 1
    FROM (
      SELECT bnco#, bnkey
      FROM stgArkonaBOPNAME
      GROUP BY bnco#, bnkey HAVING COUNT(*) > 1) z  
    WHERE z.bnco# = a.bnco#
      AND z.bnkey = a.bnkey);   
/*      
-- no datekey for 1/1/0001
SELECT DISTINCT customerKeyfromdate from dimCustomer WHERE customerKeyFromDateKey IS NULL 
UPDATE dimCustomer
SET CustomerKeyFromDate = '07/31/2009',
    CustomerKeyFromDateKey = (SELECT datekey FROM day WHERE thedate = '07/31/2009')
WHERE CustomerKeyFromDate = '01/01/0001' 

*/
DROP TABLE keyMapDimCustomer;     
CREATE TABLE keyMapDimCustomer (
  CustomerKey integer,
  StoreCode cichar(3),
  BNKEY integer) IN database;         
-- index: PK  
EXECUTE PROCEDURE sp_CreateIndex90( 
   'keyMapDimCustomer','keyMapDimCustomer','PK',
   'CustomerKey','',2051,512,'' ); 
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('keyMapDimCustomer','Table_Primary_Key',
  'PK', 'APPEND_FAIL', 'keyMapDimCustomerfail');   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCustomer','StoreCode', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCustomer','BNKEY', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL ); 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'keyMapDimCustomer','CustomerKey', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );

DELETE FROM keyMapDimCustomer;      
INSERT INTO keyMapDimCustomer (CustomerKey,StoreCode,BNKEY)
SELECT CustomerKey,StoreCode,BNKEY
FROM dimCustomer a
WHERE NOT EXISTS (
  SELECT 1
  FROM keyMapDimCustomer
  WHERE CustomerKey = a.CustomerKey);   

--RI  
--EXECUTE PROCEDURE sp_DropReferentialIntegrity('dimCustomer-KeyMap');
EXECUTE PROCEDURE sp_CreateReferentialIntegrity ( 
     'dimCustomer-KeyMap','dimCustomer','keyMapDimCustomer','PK',2,2,NULL,'','');  

DROP TABLE xfmDimCustomer; 
CREATE TABLE xfmDimCustomer(
  StoreCode cichar(3),
  BNKEY integer,
  CustomerTypeCode cichar(1),
  CustomerType cichar(12),
  FullName cichar(50),
  LastName cichar(50),
  FirstName cichar(15),
  MiddleName cichar(25),
  HomePhone cichar(12),
  BusinessPhone cichar(20),
  CellPhone cichar(12),
  Email cichar(60),
  City cichar(20),
  County cichar(20),
  State cichar(2),
  Zip cichar(9)) IN database;
-- NK, the dim TABLE can't have a unique NK comprised of StoreCode/BNKEY because 
-- of type 2 changes, but this one can & should
-- cleanse the data before putting it IN dimCustomer
EXECUTE PROCEDURE sp_CreateIndex90( 
  'xfmDimCustomer','xfmDimCustomer.adi','NK','StoreCode;BNKEY','',2051,512,'' );   
-- TABLE: PK
EXECUTE PROCEDURE sp_ModifyTableProperty('xfmDimCustomer','Table_Primary_Key',
  'NK', 'APPEND_FAIL', 'dimCustomerObjectsfail');   
-- non nullable 
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimCustomer','StoreCode', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL );    
EXECUTE PROCEDURE sp_ModifyFieldProperty ( 'xfmDimCustomer','BNKEY', 
  'Field_Can_Be_Null', 'False', 'APPEND_FAIL', NULL ); 
   
  
ALTER PROCEDURE xfmCustDim ()
BEGIN
/*
EXECUTE PROCEDURE xfmCustDim();
*/
TRY
  DELETE FROM xfmDimCustomer;
  BEGIN TRANSACTION; 
  TRY 
    INSERT INTO xfmDimCustomer
    SELECT bnco#,bnkey,bntype,
      CASE bntype
        WHEN 'C' THEN 'Company'
        WHEN 'I' THEN 'Person'
        ELSE 'Unknown'
      END,    
      bnsnam,bnlnam, bnfnam,bnmidi,bnphon,bnbphn, 
      bncphon,bnemail,bncity,bncnty,bnstcd,bnzip
    FROM tmpBOPNAME a
    WHERE bnco# IN ('RY1','RY2','RY3')
      AND NOT EXISTS (
        SELECT 1
        FROM (
          SELECT bnco#, bnkey
          FROM tmpBOPNAME
          GROUP BY bnco#, bnkey HAVING COUNT(*) > 1) z  
        WHERE z.bnco# = a.bnco#
          AND z.bnkey = a.bnkey);
  COMMIT WORK;  	  
  CATCH ALL
    ROLLBACK;
    RAISE SPxfmEmpDIM(999, __errtext);   
  END TRY; // transaction
CATCH ALL
--  RAISE SPxfmEmpDIM(666, __errtext);
  RAISE;
END TRY;  
END;        

SELECT * FROM tmpBOPNAME WHERE bnkey = 1074886
-- any changes?
SELECT *
FROM xfmDimCustomer a
INNER JOIN dimCustomer b ON a.storecode = b.storecode AND a.bnkey = b.bnkey
  AND b.currentrow = true
  AND (
    a.fullname <> b.fullname OR
    a.lastname <> b.lastname OR
    a.firstname <> b.firstname OR
    a.middlename <> b.middlename OR
    a.BusinessPhone <> b.BusinessPhone OR
    a.HomePhone <> b.HomePhone OR
    a.CellPhone <> b.CellPhone OR
    a.email <> b.email OR
    a.city <> b.city OR
    a.county <> b.county OR
    a.zip <> b.zip OR
    a.state <> b.state)
    
SELECT fullname, homephone,businessphone,cellphone FROM xfmdimcustomer WHERE bnkey = 307209  
UNION 
SELECT fullname, homephone,businessphone,cellphone FROM dimcustomer WHERE bnkey = 307209   

SELECT a.storecode, a.bnkey, a.fullname, b.fullname,
  CASE WHEN a.fullname <> b.fullname THEN 'X' END AS fullname,
  CASE WHEN a.lastname <> b.lastname THEN 'X' END AS lastname,
  CASE WHEN a.firstname <> b.firstname THEN 'X' END AS firstname,
  CASE WHEN a.middlename <> b.middlename THEN 'X' END AS middlename,
  CASE WHEN a.BusinessPhone <> b.BusinessPhone THEN 'X' END AS BusinessPhone,
  CASE WHEN a.HomePhone <> b.HomePhone THEN 'X' END AS HomePhone,
  CASE WHEN a.CellPhone <> b.CellPhone THEN 'X' END AS CellPhone,
  CASE WHEN a.email <> b.email THEN 'X' END AS email,
  CASE WHEN a.city <> b.city THEN 'X' END AS city,
  CASE WHEN a.county <> b.county THEN 'X' END AS county,
  CASE WHEN a.zip <> b.zip THEN 'X' END AS zip,
  CASE WHEN a.state <> b.state THEN 'X' END AS state  
FROM xfmDimCustomer a
INNER JOIN dimCustomer b ON a.storecode = b.storecode AND a.bnkey = b.bnkey
  AND b.currentrow = true
  AND (
    a.fullname <> b.fullname OR
    a.lastname <> b.lastname OR
    a.firstname <> b.firstname OR
    a.middlename <> b.middlename OR
    a.BusinessPhone <> b.BusinessPhone OR
    a.HomePhone <> b.HomePhone OR
    a.CellPhone <> b.CellPhone OR
    a.email <> b.email OR
    a.city <> b.city OR
    a.county <> b.county OR
    a.zip <> b.zip OR
    a.state <> b.state)    
    
SELECT *
FROM xfmDimCustomer a
WHERE NOT EXISTS (
  SELECT 1
  FROM dimCustomer
  WHERE storecode = a.storecode
    AND bnkey = a.bnkey)    
  
SELECT bnco#,bnkey,bntype,bnsnam,bnlnam, bnfnam,bnmidi,bnphon,bnbphn, 
  bncphon,bnemail,bncity,bncnty,bnstcd,bnzip,bncrtts, bnupdts
-- SELECT COUNT(*)   
FROM tmpBOPNAME a
WHERE bnco# IN ('RY1','RY2','RY3')
  AND NOT EXISTS (
    SELECT 1
    FROM (
      SELECT bnco#, bnkey
      FROM tmpBOPNAME
      GROUP BY bnco#, bnkey HAVING COUNT(*) > 1) z  
    WHERE z.bnco# = a.bnco#
      AND z.bnkey = a.bnkey)  
      
SELECT * FROM dimcustomer WHERE bnkey = 1074991      