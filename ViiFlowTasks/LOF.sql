SELECT *
FROM contactmechanisms
WHERE partyid IN (
  SELECT partyid
  FROM people 
  WHERE lastname IN ('Olderbak', 'Shirek')) 
  
jolderbak@gfhonda.com  

nshirek@gfhonda.com

SELECT *
FROM contactmechanisms

INSERT INTO contactmechanisms values(
  'ContactMechanism_WorkEmail',
  'B2B31AC2-B6A8-4F46-A9DE-B606EF33B433',
  'jolderbak@gfhonda.com', 
  now(), 
  NULL)
  
  
SELECT *
FROM viiFlowTasks  

-- LOF yes OR no
SELECT v.VehicleInventoryItemID, max(v.VehicleInspectionTS) AS Inspected,
  MAX(
  CASE
    WHEN f.FlowTaskTS IS NULL THEN 'NO'
    ELSE 'YES'
  END) AS LOF
FROM VehicleInspections v
LEFT JOIN ViiFlowTasks f ON v.VehicleInventoryItemID = f.VehicleInventoryItemID 
  AND f.typ = 'ViiFlowTask_LOF'
WHERE v.VehicleINspectionTS > timestampadd(sql_tsi_month,-8, now())
GROUP BY v.VehicleInventoryItemID 
ORDER BY Inspected DESC 

SELECT da.TheWeek, MIN(da.TheDate) AS Beginning, MAX(da.TheDate) AS Ending,
  COUNT(v.VehicleInventoryItemID) AS Inspections,
  SUM(CASE WHEN f.FlowTaskTS IS NULL THEN 0 ELSE 1 END) AS LOF
FROM ( -- da 
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17      
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 118
  AND TheDate <= CurDate()) da    
LEFT JOIN VehicleInspections v ON da.TheDate = CAST(v.VehicleInspectionTS AS sql_date) 
  AND v.VehicleINspectionTS > timestampadd(sql_tsi_day,-118, now()) 
LEFT JOIN ViiFlowTasks f ON v.VehicleInventoryItemID = f.VehicleInventoryItemID 
  AND f.typ = 'ViiFlowTask_LOF' 
AND v.VehicleInventoryItemID IN (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems
  WHERE owninglocationid = (SELECT partyid FROM organizations WHERE name = 'Rydells'))
GROUP BY da.TheWeek

SELECT x.*, round((LOF*1.0/Inspections)*100,0) AS Percentage
FROM (
  SELECT da.TheWeek, MIN(da.TheDate) AS Beginning, MAX(da.TheDate) AS Ending,
    COUNT(v.VehicleInventoryItemID) AS Inspections,
    SUM(CASE WHEN f.FlowTaskTS IS NULL THEN 0 ELSE 1 END) AS LOF
  FROM (
    SELECT d.*, 
      CASE 
        WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
        WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
        WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
        WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
        WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
        WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
        WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
        WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
        WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
        WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
        WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
        WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
        WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
        WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
        WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
        WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
        WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17      
      END AS TheWeek
    FROM dds.day d
    WHERE TheDate >= CurDate() - 118
    AND TheDate <= CurDate()) da    
  LEFT JOIN VehicleInspections v ON da.TheDate = CAST(v.VehicleInspectionTS AS sql_date) 
    AND v.VehicleINspectionTS > timestampadd(sql_tsi_day,-118, now()) 
  LEFT JOIN ViiFlowTasks f ON v.VehicleInventoryItemID = f.VehicleInventoryItemID 
    AND f.typ = 'ViiFlowTask_LOF' 
  AND v.VehicleInventoryItemID IN (
    SELECT VehicleInventoryItemID 
    FROM VehicleInventoryItems
    WHERE owninglocationid = (SELECT partyid FROM organizations WHERE name = 'Rydells'))
  GROUP BY da.TheWeek) x