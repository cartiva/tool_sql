SELECT stocknumber, price, year, left(make,15), left(model,15), salesstatus, vehicletype, segment
FROM usedcars
WHERE market = 'MT-Minot'
AND salesstatus NOT IN ('Sold','Wholesaled')
ORDER BY salesstatus, vehicletype, segment


SELECT *
FROM usedcars
WHERE stocknumber = '36447A'

SELECT salesstatus, COUNT(*)
FROM usedcars
WHERE market = 'MT-Minot'
AND salesstatus NOT IN ('Sold','Wholesaled')
GROUP BY salesstatus

SELECT stocknumber, price, year, left(make,15), left(model,15), salesstatus, vehicletype, segment
FROM usedcars
WHERE market = 'MT-Minot'
AND salesstatus = 'In Transit'
--AND stocknumber = '32355A'
ORDER BY salesstatus, vehicletype, segment

SELECT salesstatus, COUNT(*)
FROM usedcars
WHERE market = 'MT-Minot'
--AND salesstatus NOT IN ('Sold','Wholesaled')
GROUP BY salesstatus

select*
FROM usedcars where stocknumber = '38429X'


-- usedcars without a matching stocknumber IN MinotAvailable
SELECT u.stocknumber, vin, u.price, u.year, left(u.make,15), left(u.model,15), u.salesstatus, u.dateacquired
FROM usedcars u
WHERE market = 'MT-Minot'
AND salesstatus NOT IN ('Sold','Wholesaled')
AND NOT EXISTS (
  SELECT 1
  FROM minotavailable
  WHERE stocknumber = u.stocknumber)
ORDER BY u.stocknumber  
  
SELECT *
FROM minotavailable
WHERE stocknumber = '34336X'  

SELECT stocknumber, salesstatus, dateacquired, dateonlot, datesold
FROM usedcars u
WHERE market = 'MT-Minot'
AND stocknumber = '37187A'

SELECT *
FROM usedcars u
WHERE market = 'MT-Minot'
AND stocknumber like '35263%'

SELECT *
FROM usedcars u
WHERE market = 'MT-Minot'
AND vin = '1GNDT13S132223370'

DELETE 
FROM usedcars
WHERE market = 'MT-Minot'
AND stocknumber IN ('29653SA','38568A','38611XA','38830A') 

