-- IF it's NOT booked there IS no VEhicleInventoryItemID AND no stocknumber
-- so how to determine what records we are talking about

-- doesn't WORK WHERE selected = suggested
DECLARE @Stocknumber string;
DECLARE @VehicleInventoryItemID string;
DECLARE @Adjusted integer;
DECLARE @Selected integer;
DECLARE @Suggested integer;
DECLARE @ChangeAmount integer;

@Stocknumber = 'h1115xa'; --sug: 980 sel/adj: 2000
@ChangeAmount = 850;

@VehicleInventoryItemID = (
  SELECT VehicleInventoryItemID
  FROM VehicleInventoryItems
  WHERE stocknumber = @Stocknumber);
@Adjusted = (
  SELECT AdjustedACV 
  FROM ACVs  -- WHERE VehicleInventoryITemID = (SELECT VehicleInventoryItemID FROM VEhicleInventoryITems WHERE stocknumber = '93862XXA')
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND BasisTable = 'VehicleEvaluations');
@Selected = (
  SELECT SelectedACV 
  FROM ACVs 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND BasisTable = 'VehicleEvaluations');
@Suggested = (
  SELECT SuggestedACV 
  FROM ACVs 
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID
  AND BasisTable = 'VehicleEvaluations');
  
IF @Adjusted = @Selected THEN
  UPDATE ACVs
  SET 
    AdjustedACV = AdjustedACV + @ChangeAmount,
    SelectedACV = SelectedACV + @ChangeAmount
  WHERE VehicleInventoryItemID = @VehicleInventoryItemID;
END IF;

UPDATE VehiclePriceComponents
SET Amount = Amount + @ChangeAmount
WHERE VehicleInventoryItemID = @VehicleInventoryItemID;


/*

SELECT 2600-1750 FROM system.iota

SELECT * 
FROM VehicleInventoryItems
WHERE stocknumber = '13154a'

-- viiid: 'cc9c8c06-9fe2-4d44-bd6f-4c215b8eb3ff'

SELECT vii.VehicleInventoryItemID 
FROM VehicleInventoryItems vii
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
WHERE vi.vin = '1G6DM57N830142544'

SELECT * FROM vehiclepricings WHERE VehicleInventoryItemID = 'cc9c8c06-9fe2-4d44-bd6f-4c215b8eb3ff'

SELECT * FROM vehiclepricingdetails WHERE vehiclepricingid = '7261cdd1-7dbf-4b61-a776-752f62e1a737'

SELECT *  --547 1750 1750
FROM ACVs
WHERE VehicleInventoryITemID = 'bad93b67-b1cb-4f6b-9a12-b1a44d298438'

SELECT *  -- 1750 - 2478
FROM VehiclePriceComponents
WHERE VehicleInventoryITemID = 'bad93b67-b1cb-4f6b-9a12-b1a44d298438'

SELECT *
FROM VehicleEvaluations
WHERE VehicleInventoryITemID = 'bad93b67-b1cb-4f6b-9a12-b1a44d298438'


SELECT stocknumber
FROM VehicleInventoryItems
WHERE VehicleInventoryItemID = '4ed02d6d-5f47-4bc0-9040-aa13e7489575'

SELECT *
FROM acvs 
WHERE (adjustedacv <> selectedacv) AND (suggestedACV <> SelectedACV)

SELECT *
FROM acvs
WHERE vehicleInventoryItemID = '5c2bdf8d-6e01-475b-a928-61eaec3ae10c'

SELECT *
FROM VehicleEvaluations
WHERE vehicleInventoryItemID = 'd07e0830-bd1b-43f8-a13e-9a77403426a4'


*/
-- non booked
SELECT *
FROM VehicleEvaluations
WHERE VehicleitemID = --'cf4f2ac4-bb88-704b-8416-90ce72080946'
  (
  select VehicleItemID
  FROM VehicleItems
  WHERE vin like '%b041799')
  
SELECT * FROM users WHERE username = 'bjacobson'  -- 60092eb4-4a65-2248-a9d6-4318d2356c81
  
-- veID  '68ee4867-cbeb-4f41-b860-f5f2aa6aefaf'
-- viID  'e594b350-e0c5-e14e-b9bc-b9126f099ca9'

SELECT * FROM VEhicleItemStatuses WHERE VehicleItemID = 'e594b350-e0c5-e14e-b9bc-b9126f099ca9' ORDER BY fromts
SELECT * FROM VEhicleEvaluations WHERE VehicleEvaluationID = '84af7e6a-b39e-4c0d-88a9-67651e8944cf'
SELECT * FROM acvs WHERE tablekey = 'e277b381-4aad-4ce6-bff6-acbc543d4cb2'
SELECT * FROM VehiclePriceComponents WHERE tablekey = 'e277b381-4aad-4ce6-bff6-acbc543d4cb2'


SELECT *
FROM vehicleEvaluations
WHERE 
SELECT *
FROM VehicleEvaluations
WHERE VehicleInventoryItemID = (
  SELECT VehicleInventoryItemID 
  FROM VehicleInventoryItems 
  WHERE stocknumber = '10025a')
  
SELECT * FROM VehicleItems WHERE vin = '2GCEK13Z761292582'  
    

SELECT * FROM VehicleEvaluations WHERE VehicleInventoryItemID = (
SELECT VehicleInventoryItemID FROM VehicleInventoryItems WHERE stocknumber = '14135a')

select *
from VehicleEvaluations
where VehicleEvaluationID = '7326c412-2f3c-4adb-89e8-705752f5d3be'

  					
SELECT *
FROM acvs 
WHERE tablekey = '7326c412-2f3c-4adb-89e8-705752f5d3be'


SELECT *
FROM VehiclePriceComponents
WHERE tablekey = '7326c412-2f3c-4adb-89e8-705752f5d3be'


UPDATE VehiclePricecomponents
SET notes = 'ACV $13000, Showed $13500.  Minot Chrysler offerred $13500'
WHERE TableKey = 'f1a3137f-d6e9-4ff9-9bfa-d6cea1c5f723'
AND typ = 'VehiclePriceComponent_ACV'

SELECT * 
FROM VehicleItems
WHERE yearmodel = '2009'
AND model LIKE '%silverado%'
AND vehicleitemid IN (
  SELECT vehicleitemidu
  FROM vehicleitemownerinfo
  WHERE lastname LIKE '%emer%')
  
SELECT *
FROM VehicleEvaluations
WHERE VehicleItemID IN (  
SELECT VehicleItemID  
FROM VehicleItems
WHERE yearmodel = '2009'
AND model LIKE '%silverado%'
AND vehicleitemid IN (
  SELECT vehicleitemid
  FROM vehicleitemownerinfo
  WHERE lastname LIKE '%emer%'))