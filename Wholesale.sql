SELECT vii.stocknumber, LEFT(TRIM(vi.YearModel) + ' ' + TRIM(vi.Make) + ' ' + TRIM(vi.Model),25) AS Vehicle,
  cast(vii.FromTS AS SQL_DATE) AS Acquired,
  cast(vs.SoldTS AS SQL_DATE) AS Sold,
  vs.SoldAmount,
  coalesce((
    SELECT TOP 1 vpd.Amount
    FROM VehiclePricings vp
    INNER JOIN VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID 
    WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID 
    AND vpd.Typ = 'VehiclePricingDetail_BestPrice'
    ORDER BY VehiclePricingTS DESC) ,0) AS BestPrice, 
  coalesce ((
    SELECT FullName 
    FROM Organizations
    WHERE PartyID = vs.SoldTo collate ads_default_ci), SoldTo) AS SoldTo
-- SELECT COUNT(*) -- 53 
FROM VehicleInventoryItems vii
INNER JOIN VehicleItems vi ON vi.VehicleItemID = vii.VehicleItemID
INNER JOIN VehicleSales vs ON vs.VehicleInventoryItemID = vii.VehicleInventoryItemID 
WHERE month(vs.SoldTS) = 8 -- use vs.SoldTS to include SoldNotDelivered
AND year(vs.SoldTS) = 2010 
AND vs.Typ = 'VehicleSale_Wholesale'
AND vs.Status = 'VehicleSale_Sold'
AND vii.LocationID = (
  SELECT partyid
  FROM Organizations
  WHERE name = 'Rydells')

/*
SELECT * FROM vehiclesales

SELECT * FROM VehiclePriceComponents
SELECT DISTINCT typ FROM VehiclePriceComponents

SELECT * FROM vehiclepricings

SELECT * FROM vehiclepricingDetails

SELECT * FROM system.storedprocedures WHERE name LIKE '%ric%' ORDER BY name
*/