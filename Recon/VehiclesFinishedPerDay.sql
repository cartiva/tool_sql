-- for appearance, at least, need to separate out destination of finished WORK 
-- eg lot, dcu, sales buffer, etc

SELECT 
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/13/2011') AS "M Monday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/14/2011') AS "M Tuesday",      
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/15/2011') AS "M Wednesday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/17/2011') AS "M Thursday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/18/2011')AS "M Friday",     
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Mech%')
        AND vrix.Description <> 'Inspection Fee'
      WHERE cast(CompleteTS AS sql_date) = '01/19/2011')AS "M Saturday"                
FROM system.iota  

--SELECT 
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Body%')
      WHERE cast(CompleteTS AS sql_date) = '11/22/2010') AS "B Monday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Body%')
      WHERE cast(CompleteTS AS sql_date) = '11/23/2010') AS "B Tuesday",      
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Body%')
      WHERE cast(CompleteTS AS sql_date) = '11/17/2010') AS "B Wednesday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Body%')
      WHERE cast(CompleteTS AS sql_date) = '11/18/2010') AS "B Thursday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Body%')
      WHERE cast(CompleteTS AS sql_date) = '11/19/2010') AS "B Friday",  
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('Body%')
      WHERE cast(CompleteTS AS sql_date) = '11/20/2010') AS "B Saturday",                      
--FROM system.iota    

-- SELECT 
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('%Appear%%')
      WHERE cast(CompleteTS AS sql_date) = '11/22/2010') AS "A Monday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('%Appear%%')
      WHERE cast(CompleteTS AS sql_date) = '11/23/2010') AS "A Tuesday",      
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('%Appear%%')
      WHERE cast(CompleteTS AS sql_date) = '11/17/2010') AS "A Wednesday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('%Appear%%')
      WHERE cast(CompleteTS AS sql_date) = '11/18/2010') AS "A Thursday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('%Appear%%')
      WHERE cast(CompleteTS AS sql_date) = '11/19/2010') AS "A Friday",
  (SELECT COUNT(DISTINCT vrix.VehicleInventoryItemID) 
      FROM AuthorizedReconItems arix
      INNER JOIN VehicleReconItems vrix 
        ON vrix.VehicleReconItemID = arix.VehicleReconItemID 
        AND vrix.typ LIKE ('%Appear%%')
      WHERE cast(CompleteTS AS sql_date) = '11/20/2010') AS "A Saturday"                      
FROM system.iota  


