TRY - TRANSACTION nesting 

-- current usage:
BEGIN TRANSACTION;
TRY 
  ...
  COMMIT WORK;
CATCH ALL 
  ROLLBACK;
  RAISE;
END TRY;  
-- current usage offends my sense of symmetry
-- 1.
TRY 
  BEGIN TRANSACTION;
    ...
  COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

-- 2.
BEGIN TRANSACTION;
  TRY
    ...
  CATCH ALL
    ROLLBACK;
    RAISE;
  END TRY;        
COMMIT WORK;

-- i'm liking 1. the most
