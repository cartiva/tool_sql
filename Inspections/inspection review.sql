-- DO 2 queries, master - detail : vehicle - recon

-- need miles

-- ALL IN one
SELECT a.VehicleInspectionTS, b.stocknumber, c.fullname, d.description, 
  d.totalpartsamount, d.laboramount, f.yearmodel, f.make, f.model, f.bodystyle, 
  f.TRIM, f.engine, f.exteriorcolor, e.category, ee.description AS Area,
  CASE 
    WHEN g.VehicleWalkID IS NULL THEN 'No'
    ELSE 'Yes'
  END AS walked
FROM vehicleinspections a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations bb on b.owninglocationid = bb.partyid
  AND bb.name = 'Rydells'
INNER JOIN people c on a.technicianid = c.partyid
INNER JOIN VehicleReconItems d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.typ <> 'MechanicalReconItem_Inspection'
INNER JOIN typcategories e on d.typ = e.typ
  AND e.category = 'MechanicalReconItem'
INNER JOIN typdescriptions ee on e.typ = ee.typ  
INNER JOIN VehicleItems f on a.VehicleItemID = f.VehicleItemID   
LEFT JOIN VehicleWalks g on a.VehicleInventoryItemID = g.VehicleInventoryItemID 
--WHERE CAST(vehicleinspectionts AS sql_date) > curdate() - 3
WHERE CAST(vehicleinspectionts AS sql_date) = curdate()
--  AND NOT EXISTS (
--    SELECT 1
--    FROM vehiclewalks
--    WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)

-- master
-- this includes inspections for which there are no mechanical items
SELECT a.VehicleInspectionTS, b.stocknumber, c.fullname, 
  f.yearmodel, f.make, f.model, f.bodystyle, 
  f.TRIM, f.engine, f.exteriorcolor,
  CASE 
    WHEN g.VehicleWalkID IS NULL THEN 'No'
    ELSE 'Yes'
  END AS walked, h.value AS miles 
FROM vehicleinspections a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations bb on b.owninglocationid = bb.partyid
  AND bb.name = 'Rydells'
INNER JOIN people c on a.technicianid = c.partyid
INNER JOIN VehicleItems f on a.VehicleItemID = f.VehicleItemID   
LEFT JOIN VehicleWalks g on a.VehicleInventoryItemID = g.VehicleInventoryItemID 
LEFT JOIN VehicleItemMileages h on a.VehicleItemID = h.VehicleItemID 
  AND VehicleItemMileageTS = (
    SELECT MAX(VehicleItemMileageTS)
    FROM VehicleItemMileages
    WHERE VehicleItemID = h.VehicleItemID)
WHERE CAST(vehicleinspectionts AS sql_date) = curdate()

-- GROUP relevant fields together, ~ each COLUMN = 1 line IN email
SELECT b.stocknumber, 
  trim(stocknumber) + ':  Inspected by ' + trim(c.fullname) 
    + ' at ' 
    + iif(hour(a.VehicleInspectionTS) < 12, trim(CAST(hour(a.VehicleInspectionTS) AS sql_char)), 
		    trim(CAST(hour(a.VehicleInspectionTS) - 12 AS sql_char))) 
		+ ':' 
		+ iif(minute(a.VehicleInspectionTS) < 10, '0' + trim(CAST(minute(a.VehicleInspectionTS) AS sql_char)), 
		    trim(CAST(minute(a.VehicleInspectionTS) AS sql_char)))
		+ iif(hour(a.VehicleInspectionTS) < 12, ' AM', ' PM'),
  trim(f.yearmodel) + ' ' + trim(f.make) + ' ' + trim(f.model) + ' ' 
	  + coalesce(trim(f.TRIM), '') + ' ' + TRIM(coalesce(f.bodystyle))
    + '  Engine: ' + trim(coalesce(f.engine)),
	'Miles: ' +  cast(coalesce(h.value, 0) AS sql_char)
FROM vehicleinspections a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations bb on b.owninglocationid = bb.partyid
  AND bb.name = 'Rydells'
INNER JOIN people c on a.technicianid = c.partyid
INNER JOIN VehicleItems f on a.VehicleItemID = f.VehicleItemID   
LEFT JOIN VehicleItemMileages h on a.VehicleItemID = h.VehicleItemID 
  AND VehicleItemMileageTS = (
    SELECT MAX(VehicleItemMileageTS)
    FROM VehicleItemMileages
    WHERE VehicleItemID = h.VehicleItemID)
WHERE CAST(vehicleinspectionts AS sql_date) = curdate()





SELECT b.stocknumber, d.description, 
  d.totalpartsamount, d.laboramount, ee.description AS Area
FROM vehicleinspections a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN organizations bb on b.owninglocationid = bb.partyid
  AND bb.name = 'Rydells'
INNER JOIN people c on a.technicianid = c.partyid
INNER JOIN VehicleReconItems d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND d.typ <> 'MechanicalReconItem_Inspection'
INNER JOIN typcategories e on d.typ = e.typ
  AND e.category = 'MechanicalReconItem'
INNER JOIN typdescriptions ee on e.typ = ee.typ  
WHERE b.stocknumber = '25421b'
