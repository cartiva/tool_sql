SELECT * FROM vUsedCarsOpenRecon



   SELECT 
/*
TRY to refactor to 3 separate views     
started with make it just mechanical AND it still took fucking 6 seconds
*/
  viixx.OwningLocationID, viixx.LocationID, 
  CASE 
    WHEN rp.VehicleInventoryItemID IS NULL THEN ''
    ELSE
      CASE -- rp.VehicleInventoryItemID IS NOT NULL 
        WHEN rs.MechStatus IS NULL THEN ''
        ELSE 
          CASE -- has OPEN recon 
            WHEN rs.MechStatus = 'MechanicalReconProcess_InProcess' THEN ''
            ELSE
              CASE -- NOT IN WIP
                WHEN MechSched IS NULL THEN 'NS:'
                ELSE
                  CASE -- scheduled
                    WHEN TimeStampDiff(sql_tsi_second, MechSched, now()) > 0  THEN 'OD:'
                    ELSE trim(cast(month(MechSched) AS sql_char)) + '/' + trim(cast(dayofmonth(MechSched) AS sql_char)) + ':'
                  END 
              END 
          END 
      END 
  END AS mReconPlan,
  viixx.VehicleInventoryItemID, viixx.stocknumber, ps.partsstatus, 
  CASE -- Tires
    WHEN arix.status IS NOT NULL THEN 'D'  
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND ps.partsstatus = 'Rcvd' then 'R'
	  WHEN tvr.VehicleInventoryItemID  IS NOT NULL AND ps.partsstatus = 'Ord' THEN 'O'
    WHEN tvr.VehicleInventoryItemID  IS NOT NULL THEN 'X'
	  ELSE ''
  END AS Tires, 
  viixx.CurrentPriority,
  rs.MechStatus,
  CASE
    WHEN rs.MechStatus = 'MechanicalReconProcess_NotStarted' THEN 'Not Started'
    WHEN rs.MechStatus = 'MechanicalReconProcess_InProcess' THEN 'WIP'
    ELSE 'No Open Items'
  END AS MechStatusDisplay,
  vix.yearmodel, vix.make, vix.model, vix.exteriorcolor,
  CASE -- Move List'
    WHEN vtm.VehicleInventoryItemID IS NOT NULL THEN 'X'
	ELSE ''
  END AS OnMoveList,
  CASE
    WHEN mechstatus IS NOT NULL THEN ''
    WHEN bodystatus IS NOT NULL THEN ''
    WHEN rs.appstatus = 'AppearanceReconProcess_InProcess' THEN ''
    ELSE 'X'
  END AS G2G,
  coalesce(a.keystatus, 'Unknown') AS KeyStatus,
  Utilities.CurrentViiLocationWithoutStore(viixx.VehicleInventoryItemID) AS PhysLocation
  
-- SELECT *  
FROM VehicleInventoryItems viixx -- OPEN VehicleInventoryItems only 
INNER JOIN ( -- limit to only VehicleInventoryItems with OPEN recon 
  SELECT viix.stocknumber, mrs.MechStatus, brs.BodyStatus, ars.AppStatus, viix.VehicleInventoryItemID 
  FROM VehicleInventoryItems viix
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS MechStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'MechanicalReconProcess'
    AND status <> 'MechanicalReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) mrs ON viix.VehicleInventoryItemID = mrs.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS BodyStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'BodyReconProcess'
    AND status <> 'BodyReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) brs ON viix.VehicleInventoryItemID = brs.VehicleInventoryItemID 
  LEFT JOIN (
    SELECT VehicleInventoryItemID, status AS AppStatus 
    FROM VehicleInventoryItemStatuses
    WHERE category = 'AppearanceReconProcess'
    AND status <> 'AppearanceReconProcess_NoIncompleteReconItems'
    AND ThruTS IS NULL) ars ON viix.VehicleInventoryItemID = ars.VehicleInventoryItemID     
  WHERE viix.ThruTS IS NULL  
    AND mrs.MechStatus IS NOT NULL) rs ON viixx.VehicleInventoryItemID = rs.VehicleInventoryItemID
LEFT JOIN ( -- parts status
  SELECT rax.VehicleInventoryItemID, 
    CASE
      WHEN SUM(
        CASE 
          WHEN po.VehicleReconItemID IS NOT NULL THEN 1 ELSE 0 END) = 0 THEN '' 
          ELSE
            CASE 
              WHEN SUM(
                CASE 
                  WHEN po.VehicleReconItemID IS NOT NULL THEN 1 ELSE 0 END) = SUM(CASE WHEN po.ReceivedTS IS NOT NULL THEN 1 ELSE 0 END) 
                    THEN 'Rcvd'
                  ELSE
                    CASE 
                      WHEN SUM(
                        CASE 
                          WHEN po.VehicleReconItemID IS NOT NULL AND po.OrderedTS IS NULL THEN 1 ELSE 0 END) > 0 THEN 'Not Ord'
              ELSE 'Ord'
            END 
       END 
    END AS PartsStatus
  FROM ReconAuthorizations rax -- only OPEN ReconAuthorizations 
  INNER JOIN AuthorizedReconItems arix ON rax.ReconAuthorizationID = arix.ReconAuthorizationID 
    AND arix.Status <> 'AuthorizedReconItem_Complete' 
  INNER JOIN VehicleReconItems vrix ON arix.VehicleReconItemID = vrix.VehicleReconItemID 
    AND vrix.typ LIKE 'M%'
  LEFT JOIN PartsOrders po ON vrix.VehicleReconItemID = po.VehicleReconItemID 
  WHERE rax.ThruTS IS NULL 
  GROUP BY rax.VehicleInventoryItemID) ps ON viixx.VehicleInventoryItemID = ps.VehicleInventoryItemID 
LEFT JOIN -- Move List
  VehiclesToMove vtm ON viixx.VehicleInventoryItemID = vtm.VehicleInventoryItemID 
  AND vtm.ThruTS IS NULL 
/**/  
LEFT JOIN (-- yr make model color,
  SELECT yearmodel, make, model, exteriorcolor, VehicleItemID  
  FROM vehicleitems) vix ON viixx.VehicleItemID = vix.VehicleItemID 
/**/  
LEFT JOIN ( -- tires
  SELECT distinct vr.VehicleInventoryItemID, vr.VehicleReconItemID -- 8/7/11: without DISTINCT generated multiple records, don't know why
  FROM VehicleReconItems vr
  INNER JOIN AuthorizedReconItems arix ON vr.VehicleReconItemID = arix.VehicleReconItemID
  WHERE ((vr.typ = 'MechanicalReconItem_Tires')
    OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%'))
--  AND vr.partsinstock = true
  AND EXISTS (
    SELECT 1
  	FROM ReconAuthorizations raix
  	INNER JOIN AuthorizedReconItems arix ON raix.ReconAuthorizationID = arix.ReconAuthorizationID
  	  AND arix.Status <> 'AuthorizedReconItem_Complete' 
  	WHERE raix.VehicleInventoryItemID = vr.VehicleInventoryItemID 
  	AND ThruTS IS NULL)) tvr ON viixx.VehicleInventoryItemID = tvr.VehicleInventoryItemID 	
LEFT JOIN -- for tire status of D
  AuthorizedReconItems arix ON tvr.VehicleReconItemID = arix.VehicleReconItemID 
  AND arix.Status = 'AuthorizedReconItem_Complete'   
/*  
LEFT JOIN ( -- ReconPlans
  SELECT v.VehicleInventoryItemID, 
    (SELECT ScheduleTS 
      FROM reconplans 
      WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
        AND typ = 'ReconPlan_Mechanical' 
        AND ThruTS IS NULL) AS MechSched,
    (SELECT PromiseTS 
      FROM reconplans 
      WHERE VehicleInventoryItemID = v.VehicleInventoryItemID 
        AND typ = 'ReconPlan_Mechanical' 
        AND ThruTS IS NULL) AS MechProm
  FROM (
    SELECT DISTINCT VehicleInventoryItemID
    FROM reconplans
    WHERE ThruTS IS NULL) v) rp ON viixx.VehicleInventoryItemID = rp.VehicleInventoryItemID   
*/    
LEFT JOIN reconplans rp on viixx.VehicleInventoryItemID = rp.VehicleInventoryItemID 
  AND rp.typ = 'ReconPlan_Mechanical'
  AND rp.thruTS IS NULL    
LEFT JOIN keyperkeystatus a ON viixx.stocknumber = a.stocknumber    
WHERE viixx.ThruTS IS NULL;

