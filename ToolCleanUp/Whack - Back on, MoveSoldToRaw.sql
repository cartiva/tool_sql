/*        UsedCarsDM.MoveVehicleFromSoldToRawMaterials(VehicleInventoryItemID,
          PhysVehLocAndNoteControl1.VehicleLocationValue, PhysVehLocAndNoteControl1.VehicleLocationDetail,
          UserSession.UserID, NowTS, PhysVehLocAndNoteControl1.Notes);
        UsedCarsDM.UpdateVehicleInventoryItemStatuses(VehicleInventoryItemID, UserSession.UserID, NowTS);
        UsedCarsDM.UpdateVehicleInventoryItemPriority(VehicleInventoryItemID);
*/
DECLARE @VehicleInventoryItemID string;
DECLARE @PhysicalVehicleLocationID string;
DECLARE @LocationDetail string;
DECLARE @PartyID string;
DECLARE @NowTS Timestamp;
DECLARE @Notes string;

@VehicleInventoryItemID = '1dfce686-a972-4b4b-b494-c605edde0212';
@PhysicalVehicleLocationID = '4f87889a-4bca-724d-8b82-12c851c15c7e';
@LocationDetail = NULL;
@PartyID = (SELECT partyid FROM users WHERE username = 'jon');
@NowTS = '04/20/2011 14:14:14';
@Notes = 'manually backed on';
        
BEGIN TRANSACTION;
TRY
  EXECUTE PROCEDURE MoveVehicleFromSoldToRawMaterials (
    @VehicleInventoryItemID,
    @PhysicalVehicleLocationID,
    @LocationDetail,
    @PartyID,
    @NowTS,
    @Notes);
  EXECUTE PROCEDURE UpdateVehicleInventoryItemStatuses(
    @VehicleInventoryItemID,
    @PartyID, 
    @NowTS);  
  EXECUTE PROCEDURE UpdateVehicleInventoryItemPriority(@VehicleInventoryItemID); 
  COMMIT WORK; 
CATCH ALL
  ROLLBACK WORK;  
  RAISE; //This re-raises the exception
END; //TRY             