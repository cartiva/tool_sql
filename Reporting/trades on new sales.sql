
SELECT Period, Sales, Trades, round(trades*100.0/sales, 2) AS Percent
FROM ( 
  SELECT 'Year To Date' AS Period,
    COUNT(*) AS Sales,
    SUM(CASE WHEN trade = 'yes' THEN 1 ELSE 0 END) AS Trades
  FROM ( 
    SELECT bmvin, 
      case when bmtacv <> 0 then 'yes' else 'no' end as Trade, 
      bmdtaprv
    FROM dds.stgArkonaBOPMAST
    WHERE bmdtaprv > '12/31/2011'
      AND bmvtyp = 'N'
      AND bmwhsl IN ('R','L')
      AND bmco# = 'RY1'
      AND bmstat = 'U') a   
  UNION
  SELECT 'Last 30',
    COUNT(*) AS Sales,
    SUM(CASE WHEN trade = 'yes' THEN 1 ELSE 0 END) AS Trades
  FROM ( 
    SELECT bmvin, 
      case when bmtacv <> 0 then 'yes' else 'no' end as Trade, 
      bmdtaprv
    FROM dds.stgArkonaBOPMAST
    WHERE bmdtaprv BETWEEN curdate() - 30 AND curdate()
      AND bmvtyp = 'N'
      AND bmwhsl IN ('R','L')
      AND bmco# = 'RY1'
      AND bmstat = 'U') a     
  UNION
  SELECT 'Last 60',
    COUNT(*) AS Sales,
    SUM(CASE WHEN trade = 'yes' THEN 1 ELSE 0 END) AS Trades
  FROM ( 
    SELECT bmvin, 
      case when bmtacv <> 0 then 'yes' else 'no' end as Trade, 
      bmdtaprv
    FROM dds.stgArkonaBOPMAST
    WHERE bmdtaprv BETWEEN curdate() - 60 AND curdate()
      AND bmvtyp = 'N'
      AND bmwhsl IN ('R','L')
      AND bmco# = 'RY1'
      AND bmstat = 'U') a    
  UNION
  SELECT 'Last 90',
    COUNT(*) AS Sales,
    SUM(CASE WHEN trade = 'yes' THEN 1 ELSE 0 END) AS Trades
  FROM ( 
    SELECT bmvin, 
      case when bmtacv <> 0 then 'yes' else 'no' end as Trade, 
      bmdtaprv
    FROM dds.stgArkonaBOPMAST
    WHERE bmdtaprv BETWEEN curdate() - 90 AND curdate()
      AND bmvtyp = 'N'
      AND bmwhsl IN ('R','L')
      AND bmco# = 'RY1'
      AND bmstat = 'U') a) x       