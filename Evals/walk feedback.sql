SELECT now(), now() - 1 FROM system.iota

select *
FROM vehiclewalks a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
WHERE b.stocknumber = '23928C'

select * FROM VehicleEvaluations 

select * FROM people

SELECT * FROM VehicleInventoryItemNotes a 
WHERE subcategory = 'VehicleWalk_EvaluatorFeedback'

SELECT cast(d.vehiclewalkts as sql_date) as walkDate, e.fullname as walker,
  g.fullname AS evaluator, b.stocknumber, c.vin,  c.yearmodel, c.make, c.model,
  cast(a.notes AS sql_longvarchar), 
  CASE
    WHEN h.VehicleInventoryItemID IS NULL THEN 'Inventory'
    WHEN h.typ = 'VehicleSale_Retail' THEN 'Retail'
    WHEN h.typ = 'VehicleSale_Wholesale' THEN 'Wholesale'
  END AS disposition
FROM VehicleInventoryItemNotes a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN vehiclewalks d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND 
INNER JOIN people e on d.vehiclewalkerid = e.partyid
INNER JOIN VehicleEvaluations f on a.VehicleInventoryItemID = f.VehicleInventoryItemID 
INNER JOIN people g on f.vehicleevaluatorid = g.partyid
LEFT JOIN vehiclesales h on a.VehicleInventoryItemID = h.VehicleInventoryItemID 
WHERE subcategory = 'VehicleWalk_EvaluatorFeedback'
  AND CAST(notesTS AS sql_date) > '01/01/2013'
ORDER BY cast(d.vehiclewalkts as sql_date) DESC   


-- export to pg
SELECT cast(d.vehiclewalkts as sql_date) as walk_date, e.fullname as walker,
  g.fullname AS evaluator, b.stocknumber, 
  cast(a.notes AS sql_longvarchar) AS walk_notes
FROM VehicleInventoryItemNotes a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN vehiclewalks d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
INNER JOIN people e on d.vehiclewalkerid = e.partyid
INNER JOIN VehicleEvaluations f on a.VehicleInventoryItemID = f.VehicleInventoryItemID 
INNER JOIN people g on f.vehicleevaluatorid = g.partyid
LEFT JOIN vehiclesales h on a.VehicleInventoryItemID = h.VehicleInventoryItemID 
WHERE subcategory = 'VehicleWalk_EvaluatorFeedback'
  AND CAST(notesTS AS sql_date) > '06/01/2017'
  

-- 11/28/16 cahalan wants 90 days of walks that have feedback BY evaluator 
-- wants any appeal diffs
-- changes IN acv
SELECT cast(d.vehiclewalkts as sql_date) as walkDate, e.fullname as walker,
  g.fullname AS evaluator, b.stocknumber, c.vin,  c.yearmodel, c.make, c.model,
  cast(a.notes AS sql_longvarchar),
  b.VehicleInventoryItemID,
  d.vehiclewalkid,
  f.vehicleevaluationid
INTO #wtf_1  
FROM VehicleInventoryItemNotes a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID 
INNER JOIN vehiclewalks d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
  AND CAST(d.vehiclewalkts AS sql_date) > curdate() - 90
INNER JOIN people e on d.vehiclewalkerid = e.partyid
INNER JOIN VehicleEvaluations f on a.VehicleInventoryItemID = f.VehicleInventoryItemID 
INNER JOIN people g on f.vehicleevaluatorid = g.partyid
WHERE subcategory = 'VehicleWalk_EvaluatorFeedback'
--  AND CAST(notesTS AS sql_date) > '01/01/2013'
 

-- appeal score card diff 
-- DROP TABLE #appeal;
SELECT a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid, 
  CASE WHEN b.paintbody <> c.paintbody THEN 'Paint/Body' + '|' ELSE '' END
  +
  CASE WHEN b.rust <> c.rust THEN 'Rust' + '|'  ELSE '' END
  + 
  CASE WHEN b.upholsteryCarpet <> c.upholsteryCarpet THEN 'Upholstery/Carpet' + '|'  ELSE '' END  
  + 
  CASE WHEN b.odor <> c.odor THEN 'Odor' + '|'  ELSE '' END  
  + 
  CASE WHEN b.tires <> c.tires THEN 'Tires' + '|'  ELSE '' END 
  + 
  CASE WHEN b.IntExtColor <> c.IntExtColor THEN 'Int/Ext Color' + '|'  ELSE '' END   
  + 
  CASE WHEN b.WheelLook <> c.WheelLook THEN 'Wheel Look' + '|'  ELSE '' END 
  + 
  CASE WHEN b.FrameUnibody <> c.FrameUnibody THEN 'Frame' + '|'  ELSE '' END
  + 
  CASE WHEN b.GlassLightsLenses <> c.GlassLightsLenses THEN 'Glass/Lights/Lenses' + '|'  ELSE '' END
  + 
  CASE WHEN b.EngineTransmission <> c.EngineTransmission THEN 'Engine/Transmission' + '|'  ELSE '' END
  + 
  CASE WHEN b.SteeringSuspension <> c.SteeringSuspension THEN 'Steering/Suspension' + '|'  ELSE '' END
  + 
  CASE WHEN b.ACHeater <> c.ACHeater THEN 'AC/Heater' + '|'  ELSE '' END
  + 
  CASE WHEN b.Brakes <> c.Brakes THEN 'Brakes' + '|'  ELSE '' END  AS appeal_diff  
INTO #appeal           
FROM #wtf_1 a
LEFT JOIN appealscorecards b on a.vehicleevaluationid = b.tablekey
LEFT JOIN appealscorecards c on a.vehiclewalkid = c.tablekey
WHERE (
  b.paintbody <> c.paintbody OR
  b.rust <> c.rust OR
  b.tires <> c.tires OR 
  b.upholsteryCarpet <> c.upholsteryCarpet OR 
  b.odor <> c.odor OR
  b.IntExtColor <> c.IntExtColor OR
  b.wheellook <> c.wheellook OR
  b.FrameUnibody <> c.FrameUnibody OR
  b.GlassLightsLenses <> c.GlassLightsLenses OR
  b.EngineTransmission <> c.EngineTransmission OR
  b.SteeringSuspension <> c.SteeringSuspension OR
  b.ACHeater <> c.ACHeater OR
  b.Brakes <> c.Brakes)
  

-- bb adds deducts
-- only 6 rows, don't bother
select *
FROM ( 
  SELECT a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid, COUNT(*) AS eval_count, SUM(amount) AS eval_amount
  FROM #wtf_1 a
  inner JOIN BlackBookAddsDeducts b on a.vehicleevaluationid = b.tablekey
  GROUP BY a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid) k
LEFT JOIN (
  SELECT a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid, COUNT(*) AS walk_count, SUM(amount) AS walk_amount
  FROM #wtf_1 a
  inner JOIN BlackBookAddsDeducts b on a.vehiclewalkid = b.tablekey
  GROUP BY a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid) l on k.VehicleInventoryItemID = l.VehicleInventoryItemID 
where eval_count <> walk_count
    OR eval_amount <> walk_amount
	
-- acv
SELECT m.VehicleInventoryItemID, walk_acv - eval_acv AS acv_diff
INTO #acv
FROM(
  SELECT a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid, coalesce(b.selectedacv, 0) AS eval_acv
  FROM #wtf_1 a
  LEFT JOIN acvs b on a.vehicleevaluationid = b.tablekey) m
LEFT JOIN (
  SELECT a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid, coalesce(b.selectedacv, 0) AS walk_acv
  FROM #wtf_1 a
  LEFT JOIN acvs b on a.vehiclewalkid = b.tablekey) n on m.VehicleInventoryItemID = n.VehicleInventoryItemID 

-- ben spreadsheet v1
SELECT a.*, coalesce(b.appeal_diff, 'None') AS appeal_diff, c.acv_diff
FROM #wtf_1 a
LEFT JOIN #appeal b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN #acv c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 

-- ben wants recon
DROP TABLE #recon; 
SELECT a.walkdate, a.VehicleEvaluationID, a.VehicleInventoryItemID,
a.expr, b.reconappearanceamount, b.reconbodyamount, b.reconmechanicalamount, 
  b.reconglassamount, b.recontireamount, c.appearance, c.body, c.mechanical, c.glass, c.tires,
  c.appearance - b.reconappearanceamount AS appear_diff,
  c.body - b.reconbodyamount AS body_diff,
  c.mechanical - b.reconmechanicalamount AS mech_diff,
  c.glass - b.reconglassamount AS glass_diff, 
  c.tires - b.recontireamount AS tire_diff,
  (c.appearance+c.body+c.mechanical+c.glass+c.tires) - (b.reconappearanceamount+b.reconbodyamount+b.reconmechanicalamount+b.reconglassamount+b.recontireamount) AS total_recon_diff
INTO #recon  
FROM #wtf_1 a
LEFT JOIN VehicleEvaluations b on a.VehicleEvaluationID = b.VehicleEvaluationID 
LEFT JOIN vehicleevaluationfeedbacks c on a.VehicleEvaluationID = c.VehicleEvaluationID;

-- spreadsheet v2
SELECT a.*, coalesce(b.appeal_diff, 'None') AS appeal_diff, c.acv_diff,
  appear_diff, body_diff, mech_diff, glass_diff, tire_diff, total_recon_diff
FROM #wtf_1 a
LEFT JOIN #appeal b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN #acv c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN #recon d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 

-- he also wants some statistical summaries for the evaluators

SELECT evaluator, COUNT(*), MIN(walkdate), max(walkdate)
FROM #wtf_1
GROUP BY evaluator
ORDER BY COUNT(*) DESC 

---------------------------------------------------------------------------------------------------
-- new spec, NOT just evals with feedback, but feedback OR acv diff OR recon diff -----------------
---------------------------------------------------------------------------------------------------

-- look to book
-- this will serve AS the basis for evals
SELECT d.fullname, COUNT(*) AS looked,
  SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
  100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc,
  SUM(CASE WHEN a.VehicleInventoryItemID IS NOT NULL THEN 1 ELSE 0 END)
FROM VehicleEvaluations a
INNER JOIN organizations b on a.locationid  = b.partyid
  AND b.name = 'Rydells'
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked','Finished')
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) > curdate() - 90
GROUP BY d.fullname  
ORDER BY COUNT(*) DESC 

-- FROM look to book, just the booked
DROP TABLE #eval;
SELECT d.fullname AS evaluator, cast(VehicleEvaluationTS AS sql_date) AS eval_date, 
  f.fullname as walker, 
  e.stocknumber, g.vin, g.yearmodel, g.make, g.model,
  a.VehicleEvaluationID, 
  a.VehicleInventoryItemID, aa.vehicleWalkID
INTO #eval
FROM VehicleEvaluations a
-- only those that have been walked
INNER JOIN vehicleWalks aa on a.VehicleInventoryItemID = aa.VehicleInventoryItemID 
--INNER JOIN organizations b on a.locationid  = b.partyid
--  AND b.name = 'Rydells' -- RY1 only
INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
  AND c.description IN ('Booked') 
INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
INNER JOIN people f on aa.vehiclewalkerid = f.partyid
INNER JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
INNER JOIN VehicleItems g on e.VehicleItemID = g.VehicleItemID 
WHERE a.typ = 'VehicleEvaluation_Trade'
  AND cast(a.VehicleEvaluationTS AS sql_date) > curdate() - 90

SELECT * FROM vehiclewalks
  
-- has feedback
DROP TABLE #feedback;
SELECT a.VehicleInventoryItemID, cast(b.notes AS sql_longvarchar) AS walk_feedback
INTO #feedback
FROM #eval a
INNER JOIN vehiclewalks aa on a.VehicleInventoryItemID = aa.VehicleInventoryItemID 
INNER JOIN people e on aa.vehiclewalkerid = e.partyid
INNER JOIN VehicleInventoryItemNotes b on a.VehicleInventoryItemID = b.VehicleInventoryItemID  
  AND b.subcategory = 'VehicleWalk_EvaluatorFeedback'
  
-- acv
DROP TABLE #acv;
SELECT m.VehicleInventoryItemID, walk_acv - eval_acv AS acv_diff
INTO #acv
FROM(
  SELECT a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid, coalesce(b.selectedacv, 0) AS eval_acv
  FROM #eval a
  LEFT JOIN acvs b on a.vehicleevaluationid = b.tablekey) m
LEFT JOIN (
  SELECT a.VehicleInventoryItemID, a.vehiclewalkid, a.vehicleevaluationid, coalesce(b.selectedacv, 0) AS walk_acv
  FROM #eval a
  LEFT JOIN acvs b on a.vehiclewalkid = b.tablekey) n on m.VehicleInventoryItemID = n.VehicleInventoryItemID
WHERE walk_acv - eval_acv <> 0;

-- recon
DROP TABLE #recon; 
SELECT a.VehicleInventoryItemID, 
--  b.reconappearanceamount, b.reconbodyamount, b.reconmechanicalamount, 
--  b.reconglassamount, b.recontireamount, c.appearance, c.body, c.mechanical, c.glass, c.tires,
  c.appearance - b.reconappearanceamount AS appear_diff,
  c.body - b.reconbodyamount AS body_diff,
  c.mechanical - b.reconmechanicalamount AS mech_diff,
  c.glass - b.reconglassamount AS glass_diff, 
  c.tires - b.recontireamount AS tire_diff,
  (c.appearance+c.body+c.mechanical+c.glass+c.tires) - (b.reconappearanceamount+b.reconbodyamount+b.reconmechanicalamount+b.reconglassamount+b.recontireamount) AS total_recon_diff
INTO #recon  
FROM #eval a
LEFT JOIN VehicleEvaluations b on a.VehicleEvaluationID = b.VehicleEvaluationID 
LEFT JOIN vehicleevaluationfeedbacks c on a.VehicleEvaluationID = c.VehicleEvaluationID
WHERE (c.appearance+c.body+c.mechanical+c.glass+c.tires) - (b.reconappearanceamount+b.reconbodyamount+b.reconmechanicalamount+b.reconglassamount+b.recontireamount) <> 0
 
 
SELECT COUNT(*) FROM #eval -- 690
SELECT COUNT(*) FROM #feedback --297
SELECT COUNT(*) FROM #acv -- 49
SELECT COUNT(*) FROM #recon -- 101

SELECT COUNT(*) from (
select VehicleEvaluationID, vehiclewalkid, VehicleInventoryItemID 
FROM #feedback
UNION
select VehicleEvaluationID, vehiclewalkid, VehicleInventoryItemID 
FROM #acv
union
select VehicleEvaluationID, vehiclewalkid, VehicleInventoryItemID 
FROM #recon
) x


select *
INTO #evals_1
FROM #eval a
WHERE EXISTS (
  SELECT 1
  FROM #feedback
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)
OR EXISTS (
  SELECT 1
  FROM #acv
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)
OR EXISTS (
  SELECT 1
  FROM #recon
  WHERE VehicleInventoryItemID = a.VehicleInventoryItemID)    

  
SELECT a.*, b.walk_feedback, c.acv_diff, d.appear_diff, d.body_diff, d.mech_diff,
  d.glass_diff, d.tire_diff, d.total_recon_diff
FROM #evals_1 a
LEFT JOIN #feedback b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
LEFT JOIN #acv c on a.VehicleInventoryItemID = c.VehicleInventoryItemID 
LEFT JOIN #recon d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 




-- look to book
SELECT x.*, y.the_count
FROM (
  SELECT d.fullname, COUNT(*) AS looked,
    SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END) AS booked,
    100 * SUM(CASE WHEN c.description = 'Booked' THEN 1 ELSE 0 END)/COUNT(*) AS perc
  FROM VehicleEvaluations a
  INNER JOIN organizations b on a.locationid  = b.partyid
-- AND b.name = 'Rydells'
  INNER JOIN StatusDescriptions c on a.currentstatus = c.status  
    AND c.description IN ('Booked','Finished')
  INNER JOIN people d on a.VehicleEvaluatorID = d.partyid  
  LEFT JOIN VehicleInventoryItems e on a.VehicleInventoryItemID = e.VehicleInventoryItemID 
  WHERE a.typ = 'VehicleEvaluation_Trade'
    AND cast(a.VehicleEvaluationTS AS sql_date) > curdate() - 90
  GROUP BY d.fullname) x 
LEFT JOIN (
  SELECT evaluator, COUNT(*) AS the_count
  FROM #evals_1
  GROUP BY evaluator) y on x.fullname = y.evaluator  
WHERE y.evaluator IS NOT NULL 
ORDER BY looked DESC   


ORDER BY COUNT(*) DESC 