/*
Syntax

sp_EnableQueryLogging(

TableName,CHARACTER, 255,

TruncateExistingData, Boolean

LogOnlyUnoptimizedQueries, Boolean

MinimmTimeBeforeLogging, Integer

EncryptionPassword, CHARACTER, 20 )

Parameters

TableName (I)
 Name of the table to log queries in.
 
TruncateExistingData (I)
 True if existing data should be removed from the table.
 
LogOnlyUnoptimizedQueries (I)
 When True, only queries that are un-optimized are logged.
 
MinimmTimeBeforeLogging (I)
 The minimum number of seconds a query must be processed by before it will be logged. A value of NULL or 0 means log all queries.
 
EncryptionPassword (I)
 On free connections, the password to use if the log table is encrypted.
 
*/
/*
1-15-11 thru 1-17-11: 
		ran with no MinimumTimeBefore Logging
		over 1M records
fucking horseshit: can't DROP TABLE even though logging has been disabled
	
1-18-11 14:37
	   restart logging with MIN time of 1 sec	
	   1-22-11 shut it down	
4/16/11 turned it back ON with TABLE name = 'zQueryLog20110416'
4/19/11 turned it off   
8/11/11 turned it back ON with TABLE name = 'zQueryLog20110811'
  hoping to see OPEN recon views take much less time 
8/14/11 turned it off 
10/30/12 early morning (especially) intermettent slowness, i notice particularly
  IN the opening of manage vehiclles
  let's see IF this will tell us anything 
  SET filter to 3 seconds
11/18/12 shut it off, now need to find some time to analyze, what i would LIKE
to see IS IF early morning IS detectably slower  

9/18/13
enable it, identify, any OPEN track calls, they will NOT WORK on the new 64bit server
10/5 disable

2/4/14
enable, tool IS slow, don't know why, SET filter to 2 sec
*/		
EXECUTE PROCEDURE sp_enableQueryLogging(
'zQueryLog20140204',
false,
false,
2,'');

/*
11/01/14
enable, tool IS slow, don't know why, SET filter to 2 sec
disabled 11/17/14
*/		
EXECUTE PROCEDURE sp_enableQueryLogging(
'zQueryLog20141101',
false,
false,
2,'');

EXECUTE PROCEDURE sp_DisableQueryLogging();

execute procedure sp_PackTable( 'zQueryLog20121030' )

DROP TABLE zQueryLog;


delete FROM zQueryLog20121030;

SELECT [start time], COUNT(*) FROM zQueryLog GROUP BY [start time] 

SELECT * FROM zQueryLog ORDER BY [run time] DESC -- views are the big hitters  specifically OpenRecon

SELECT [start time],[end time],
  case
    when timestampdiff(sql_tsi_second, [start time], [END time]) <> 0 THEN timestampdiff(sql_tsi_second, [start time], [END time])
	ELSE frac_second([END time]) - frac_second([start time]) 
  END, 
	
	[run time],optimized, query
FROM zQueryLog
WHERE query LIKE '%FROM vUsed%'
ORDER BY [run time] DESC 

SELECT COUNT(*) FROM zQueryLog
SELECT COUNT(*) FROM zQueryLog WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
SELECT COUNT(*) FROM zQueryLog WHERE UPPER(query) NOT LIKE '%EXECUTE PROCEDURE%'
SELECT COUNT(*) FROM zQueryLog WHERE query LIKE '%FROM vUsed%'

SELECT [start time],[end time],[run time],optimized, query
FROM zQueryLog
ORDER BY [start time] DESC 

-- 6/16-2017 --------------------------------------------------------------------------------------
-- bev has had it, loading OPEN recon often times out
-- SET filter to 5 sec

-- select * from zQueryLog20141101

select * from zQueryLog20170616

-- had to ADD the description field to the table
EXECUTE PROCEDURE sp_enableQueryLogging(
'zQueryLog20170616',
false,
false,
5,'');

EXECUTE PROCEDURE sp_DisableQueryLogging();

delete from zQueryLog20170616;

select * FROM vusedcarsopenrecon


-- 6/16-2017 --------------------------------------------------------------------------------------




select 
  query, optimized, [run time],
  position('Execute' IN query) AS "Execute", 
  position('Procedure ' IN query) AS "PROCEDURE plus space",
  position(' ' IN query) AS "Space",
  position('(' IN query) AS "LEFT Paren",
  substring(query, 19, position('(' IN query)),
  LEFT(query, position('(' IN query))
-- SELECT COUNT(*)  
from zQueryLog
WHERE position('Execute' IN query) = 1


SELECT [start time],[end time],[run time],optimized, query
FROM zQueryLog
WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
and position('Execute' IN query) <> 1

SELECT [start time],[end time],[run time],optimized, query
FROM zQueryLog
WHERE UPPER(query) NOT LIKE '%EXECUTE PROCEDURE%'


SELECT [start time],[end time],[run time],optimized, query
-- SELECT COUNT(*) -- missing something, only 69
FROM zQueryLog
WHERE UPPER(query) LIKE '%SELECT * FROM V%'


select 
  query, optimized, [run time],
  position('Execute' IN query) AS "Execute", 
  position('Procedure ' IN query) AS "PROCEDURE plus space",
  position(' ' IN query) AS "Space",
  position('(' IN query) AS "LEFT Paren",
  substring(query, 19, position('(' IN query)),
  LEFT(query, position('(' IN query))
-- SELECT COUNT(*)  
from zQueryLog20121030
WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
ORDER BY [run time] DESC 



SUBSTRING( str, pos, len )  Returns a portion of str starting at pos, up to length len.
 
select 
  [run time] AS RunTime,
  case
    when LEFT(substring(query, 19, position('(' IN query)),1) <> '' THEN 
	CASE 
	  WHEN position('(' IN substring(query, 19, position('(' IN query))) <> 0 then
	    substring(substring(query, 19, position('(' IN query)),1, position('(' IN substring(query, 19, position('(' IN query)))-1) 
    END 
  END AS StoredProc
from zQueryLog20121030 z
WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'

-- stored procs
SELECT left(cast(StoredProc AS sql_char),50) AS SP, MAX(RunTime) AS "MAX", 
  MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", COUNT(*) AS "COUNT" 
FROM (
  select 
    [run time] AS RunTime,
    case
      when LEFT(substring(query, 19, position('(' IN query)),1) <> '' THEN 
  	CASE 
  	  WHEN position('(' IN substring(query, 19, position('(' IN query))) <> 0 then
  	    substring(substring(query, 19, position('(' IN query)),1, position('(' IN substring(query, 19, position('(' IN query)))-1) 
      END 
    END AS StoredProc
  from zQueryLog20140204 z
  WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
  AND [run time] > 1000) wtf
WHERE StoredProc IS NOT NULL   
GROUP BY SP
ORDER BY COUNT(*) DESC 
--ORDER BY cast(AVG(RunTime) as sql_integer) DESC 

-- views
SELECT left(cast(query AS sql_char),100) AS q, MAX(RunTime) AS "MAX", 
  MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", 
  COUNT(*) AS "CNT"
FROM (
  select 
    [run time] AS RunTime, query
  from zQueryLog20140204 z
  WHERE query LIKE '%FROM vU%') wtf
WHERE query IS NOT NULL   
GROUP BY q
ORDER BY COUNT(*)  DESC 

SELECT * FROM zQueryLog20140204 

-- 04/16/2011
-- stored procs
SELECT left(cast(StoredProc AS sql_char),50) AS SP, MAX(RunTime) AS "MAX", 
  MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", 
  COUNT(*) AS "COUNT", MIN(starttime), MAX(starttime)  
FROM (
  select [Start Time] AS StartTime,
    [run time] AS RunTime,
    case
      when LEFT(substring(query, 19, position('(' IN query)),1) <> '' THEN 
  	CASE 
  	  WHEN position('(' IN substring(query, 19, position('(' IN query))) <> 0 then
  	    substring(substring(query, 19, position('(' IN query)),1, position('(' IN substring(query, 19, position('(' IN query)))-1) 
      END 
    END AS StoredProc
  from zQueryLog20141101 z
  WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
  AND [run time] > 1000) wtf
WHERE StoredProc IS NOT NULL   
GROUP BY SP
ORDER BY cast(AVG(RunTime) as sql_integer) DESC 

-- views
SELECT left(cast(query AS sql_char),100) AS q, MAX(RunTime) AS "MAX", MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", COUNT(*) 
FROM (
  select 
    [run time] AS RunTime, query
  from zQueryLog20141101 z
  WHERE query LIKE '%FROM vU%') wtf
WHERE query IS NOT NULL   
GROUP BY q
ORDER BY COUNT(*)  DESC 

SELECT left(cast(query AS sql_char),250), MAX([Run Time]) AS "MAX", MIN([Run Time]) AS "MIN", cast(AVG([Run Time]) as sql_integer) AS "AVG", COUNT(*) 
FROM zQueryLog20130918
GROUP BY left(cast(query AS sql_char),250)
ORDER BY COUNT(*) DESC

SELECT MIN([start time]), MAX([start time]) FROM zQueryLog20121030

SELECT * 
FROM zquerylog20140204
WHERE lower(cast(query as sql_longvarchar)) LIKE '%opentrack%'

-- stored procs
SELECT left(cast(StoredProc AS sql_char),50) AS SP, MAX(RunTime) AS "MAX", 
  MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", 
  COUNT(*) AS "COUNT", MIN(starttime), MAX(starttime)  
FROM (
  select [Start Time] AS StartTime,
    [run time] AS RunTime,
    case
      when LEFT(substring(query, 19, position('(' IN query)),1) <> '' THEN 
  	CASE 
  	  WHEN position('(' IN substring(query, 19, position('(' IN query))) <> 0 then
  	    substring(substring(query, 19, position('(' IN query)),1, position('(' IN substring(query, 19, position('(' IN query)))-1) 
      END 
    END AS StoredProc
  from zQueryLog20141101 z
  WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
  AND [run time] > 1000) wtf
WHERE StoredProc IS NOT NULL   
GROUP BY SP
ORDER BY cast(AVG(RunTime) as sql_integer) DESC 

-- 2/5/14
-- query log filter IS SET to 2 seconds, no instances of GetUsedCarsMainMenuSummaryByMarket
--       or GetUsedCarsMainMenuSummaryByLocation since rebooting this morning, ie, taking less than 2 sec
-- go ahead AND disable query logging
-- stored procs: look at some individual ones
SELECT left(cast(StoredProc AS sql_char),50) AS SP, RunTime, StartTime 
FROM (
  select [Start Time] AS StartTime,
    [run time] AS RunTime,
    case
      when LEFT(substring(query, 19, position('(' IN query)),1) <> '' THEN 
  	CASE 
  	  WHEN position('(' IN substring(query, 19, position('(' IN query))) <> 0 then
  	    substring(substring(query, 19, position('(' IN query)),1, position('(' IN substring(query, 19, position('(' IN query)))-1) 
      END 
    END AS StoredProc
  from zQueryLog20141101 z
  WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
  AND [run time] > 1000) wtf
--WHERE left(cast(StoredProc AS sql_char),50) LIKE 'GetUsedCarsMainMenuSummaryByMarket%'
WHERE left(cast(StoredProc AS sql_char),50) LIKE 'GetUsedCarsMainMenuSummaryByLocation%'


-- compare stored procs FROM 20140204 with stored procs FROM 20141101
SELECT *
FROM ( -- 20140204
  SELECT left(cast(StoredProc AS sql_char),50) AS SP, MAX(RunTime) AS "MAX", 
    MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", 
    COUNT(*) AS "COUNT"
  FROM (
    select [Start Time] AS StartTime,
      [run time] AS RunTime,
      case
        when LEFT(substring(query, 19, position('(' IN query)),1) <> '' THEN 
    	CASE 
    	  WHEN position('(' IN substring(query, 19, position('(' IN query))) <> 0 then
    	    substring(substring(query, 19, position('(' IN query)),1, position('(' IN substring(query, 19, position('(' IN query)))-1) 
        END 
      END AS StoredProc
    from zQueryLog20140204 z
    WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
    AND [run time] > 1000) wtf
  WHERE StoredProc IS NOT NULL   
  GROUP BY SP) a
LEFT JOIN (  
  SELECT left(cast(StoredProc AS sql_char),50) AS SP, MAX(RunTime) AS "MAX", 
    MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", 
    COUNT(*) AS "COUNT"
  FROM ( -- 20141101
    select [Start Time] AS StartTime,
      [run time] AS RunTime,
      case
        when LEFT(substring(query, 19, position('(' IN query)),1) <> '' THEN 
    	CASE 
    	  WHEN position('(' IN substring(query, 19, position('(' IN query))) <> 0 then
    	    substring(substring(query, 19, position('(' IN query)),1, position('(' IN substring(query, 19, position('(' IN query)))-1) 
        END 
      END AS StoredProc
    from zQueryLog20141101 z
    WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
    AND [run time] > 1000) wtf
  WHERE StoredProc IS NOT NULL   
  GROUP BY SP) b on a.sp = b.sp;

  
-- compare views FROM 20140204 with stored procs FROM 20141101
SELECT *
FROM (
  SELECT left(cast(query AS sql_char),100) AS q, MAX(RunTime) AS "MAX", 
    MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", 
    COUNT(*) AS "CNT"
  FROM (
    select 
      [run time] AS RunTime, query
    from zQueryLog20140204 z
    WHERE query LIKE '%FROM vU%') wtf
  WHERE query IS NOT NULL   
  GROUP BY q) a
LEFT JOIN ( 
  SELECT left(cast(query AS sql_char),100) AS q, MAX(RunTime) AS "MAX", 
    MIN(RunTime) AS "MIN", cast(AVG(RunTime) as sql_integer) AS "AVG", 
    COUNT(*) AS "CNT"
  FROM (
    select 
      [run time] AS RunTime, query
    from zQueryLog20141101 z
    WHERE query LIKE '%FROM vU%') wtf
  WHERE query IS NOT NULL   
  GROUP BY q) b on a.q = b.q; 
  
--11/17/14
here IS a particular bad actor
FROM comparing stored procs
sp GetUsedCarsMainMenuSummaryByLocation
ran 302 times
avg up FROM 2596 to 6217  

  SELECT left(cast(StoredProc AS sql_char),50) AS SP, RunTime, starttime
  FROM ( -- 20141101
    select [Start Time] AS StartTime,
      [run time] AS RunTime,
      case
        when LEFT(substring(query, 19, position('(' IN query)),1) <> '' THEN 
    	CASE 
    	  WHEN position('(' IN substring(query, 19, position('(' IN query))) <> 0 then
    	    substring(substring(query, 19, position('(' IN query)),1, position('(' IN substring(query, 19, position('(' IN query)))-1) 
        END 
      END AS StoredProc
    from zQueryLog20141101 z
    WHERE UPPER(query) LIKE '%EXECUTE PROCEDURE%'
    AND [run time] > 1000) wtf
  WHERE StoredProc = 'GetUsedCarsMainMenuSummaryByLocation'
ORDER by