/*
11/4/13
  Arne Holmen, ry1 sales consultant, moves to ry2
  what already EXISTS here (stremick, ry2 -> ry1) looks ok, but does NOT seem comprehensive
  so, look at adding a new user AND make sure ALL relevant tables are being addressed
    tables populated with a new user:
      Parties
      People
      ContactMechanisms
      Users
      ApplicationUsers
      PartyRelationships
      PartyPrivileges
   the one that conserned me was users defaultMarket, but it IS NOT a concern,
     duh, it IS market NOT location
   so it appears that the only place store level location actually comes INTO play
     IS IN partyPrivileges   
   therefore, the following IS good enuf
SELECT * FROM organizations   
  
*/
DECLARE @PartyID string;
DECLARE @Username string;
DECLARE @Privilege string;
DECLARE @LocationID string; 
DECLARE @NowTS timestamp;
DECLARE @Location string;

@Username = 'aholmen';
@PartyID = (SELECT PartyId FROM users WHERE username = @username);
@NowTS = Now();
@Privilege = 'Privilege_SC';
@LocationID = (SELECT PartyID FROM Organizations WHERE name = 'Honda Cartiva');

BEGIN TRANSACTION;
TRY 
  UPDATE PartyPrivileges -- removes current privileges
  SET ThruTS = now()
  WHERE partyid = @PartyID; 
 

  IF (SELECT COUNT(*) FROM Users WHERE username = @username) = 0 THEN
    RAISE UserException( 101, 'Bad Username');
  END IF;
  
  IF (SELECT COUNT(*) FROM Privileges WHERE privilege = @privilege) = 0 THEN
    RAISE UserException( 102, 'Bad Privilege');
  END IF; 
  IF @LocationID IS NULL OR @LocationID = '' THEN
    RAISE UserException( 103, 'Missing LocationID');
  END IF;   
  IF @PartyID IS NULL OR @PartyID = '' THEN
    RAISE UserException( 104, 'No PartyID');
  END IF;
  INSERT INTO PartyPrivileges (PartyID, LocationID, Privilege, FromTS)
    VALUES (@PartyID, @LocationID, @Privilege, @NowTS);  
   
  COMMIT WORK;
CATCH ALL 
  ROLLBACK WORK;
  RAISE;
END;  

