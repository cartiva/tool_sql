SELECT c.thedate, c.dayname, 
  count(CASE WHEN b.fullname = 'Jacob Olson' THEN thedate END) AS "Jacob Olson",
  count(CASE WHEN b.fullname = 'Brock Hillebrand' THEN thedate END) AS "Brock Hillebrand",
  count(CASE WHEN b.fullname = 'Jama Jama' THEN thedate END) AS "Jama Jama",
  count(CASE WHEN b.fullname = 'Gulled Welli' THEN thedate END) AS "Gulled Welli",
  count(CASE WHEN b.fullname = 'Troy Parlato' THEN thedate END) AS "Troy Parlato",
  count(CASE WHEN b.fullname = 'Kevin Hanson' THEN thedate END) AS "Kevin Hanson",
  count(CASE WHEN b.fullname = 'Aubrey Crane' THEN thedate END) AS "Aubrey Crane",
  count(CASE WHEN b.fullname NOT IN ('Jama Jama','Brock Hillebrand','Jacob Olson',
    'Gulled Welli','Troy Parlato','Kevin Hanson','Aubrey Crane') THEN thedate END) AS Other,
  COUNT(*) AS total
FROM vehiclestomove a
LEFT JOIN people b ON a.moverid = b.partyid
LEFT JOIN dds.day c ON CAST(a.thruts AS sql_date) = c.thedate
WHERE a.status = 'VehicleMove_Completed'
  AND c.thedate BETWEEN curdate() - 30 AND curdate()
GROUP BY c.thedate, c.dayname

union

SELECT curdate() + 1, 'TOTALS', SUM("Jacob Olson"), SUM("Brock Hillebrand"),SUM("Jama Jama"),
  SUM("Gulled Welli"),SUM("Troy Parlato"),SUM("Kevin Hanson"),SUM("Aubrey Crane"),SUM(Other),
  SUM(total)
FROM (
SELECT c.thedate, c.dayname, 
  count(CASE WHEN b.fullname = 'Jacob Olson' THEN thedate END) AS "Jacob Olson",
  count(CASE WHEN b.fullname = 'Brock Hillebrand' THEN thedate END) AS "Brock Hillebrand",
  count(CASE WHEN b.fullname = 'Jama Jama' THEN thedate END) AS "Jama Jama",
  count(CASE WHEN b.fullname = 'Gulled Welli' THEN thedate END) AS "Gulled Welli",
  count(CASE WHEN b.fullname = 'Troy Parlato' THEN thedate END) AS "Troy Parlato",
  count(CASE WHEN b.fullname = 'Kevin Hanson' THEN thedate END) AS "Kevin Hanson",
  count(CASE WHEN b.fullname = 'Aubrey Crane' THEN thedate END) AS "Aubrey Crane",
  count(CASE WHEN b.fullname NOT IN ('Jama Jama','Brock Hillebrand','Jacob Olson',
    'Gulled Welli','Troy Parlato','Kevin Hanson','Aubrey Crane') THEN thedate END) AS Other,
  COUNT(*) AS total
FROM vehiclestomove a
LEFT JOIN people b ON a.moverid = b.partyid
LEFT JOIN dds.day c ON CAST(a.thruts AS sql_date) = c.thedate
WHERE a.status = 'VehicleMove_Completed'
  AND c.thedate BETWEEN curdate() - 30 AND curdate()
GROUP BY c.thedate, c.dayname) e

SELECT b.fullname, COUNT(*)
FROM vehiclestomove a
LEFT JOIN people b ON a.moverid = b.partyid
LEFT JOIN dds.day c ON CAST(a.thruts AS sql_date) = c.thedate
WHERE a.status = 'VehicleMove_Completed'
  AND c.thedate BETWEEN curdate() - 28 AND curdate()
GROUP BY fullname  
ORDER BY COUNT(*) desc


SELECT *
FROM vehiclestomove a
LEFT JOIN people b ON a.moverid = b.partyid
LEFT JOIN dds.day c ON CAST(a.thruts AS sql_date) = c.thedate
WHERE a.status = 'VehicleMove_Completed'
  AND c.thedate BETWEEN curdate() - 28 AND curdate()
  AND b.fullname IS NULL   
  
SELECT * FROM system.storedprocedures WHERE name LIKE '%move%'