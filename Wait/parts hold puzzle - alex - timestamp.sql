CREATE TABLE #t (
  id integer, 
  d1 timestamp,
  d2 timestamp);
-- DROP TABLE #t
-- DELETE FROM #t  
INSERT INTO #t values (1, CAST('08/01/2011 08:00:00' AS sql_timestamp), CAST('08/08/2011 10:00:00' AS sql_timestamp));
INSERT INTO #t values (1, CAST('08/02/2011 13:00:00' AS sql_timestamp), CAST('08/06/2011 09:00:00' AS sql_timestamp));
INSERT INTO #t values (1, CAST('08/12/2011 16:00:00' AS sql_timestamp), CAST('08/14/2011 12:00:00' AS sql_timestamp));
INSERT INTO #t values (1, CAST('08/03/2011 08:00:00' AS sql_timestamp), CAST('08/10/2011 10:00:00' AS sql_timestamp));
-- pattern 1 
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date)); 
-- pattern 2
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/10/2011' AS sql_date), CAST('08/12/2011' AS sql_date));
-- pattern 3  
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/08/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/02/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/12/2011' AS sql_date), CAST('08/14/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/03/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 4  
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/04/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 5  
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/06/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
-- pattern 6  
INSERT INTO #t values (1, CAST('08/01/2011' AS sql_date), CAST('08/06/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/06/2011' AS sql_date), CAST('08/10/2011' AS sql_date));
INSERT INTO #t values (1, CAST('08/12/2011' AS sql_date), CAST('08/14/2011' AS sql_date));

  
--This SQL statement seems to get what you want (t is the table name of the 
--sampe table):

SELECT d.id,
   d.duration, 
   d.duration - 
      IFNULL((
        SELECT SUM(timestampdiff(SQL_TSI_hour, t1.d2, (SELECT MIN(d1) FROM #t t4 WHERE t4.id = t1.id AND t4.d1 > t1.d2))) 
        FROM #t t1 
        WHERE t1.id = d.id 
        AND (
          SELECT SUM(IIF(t1.d2 BETWEEN t2.d1 AND t2.d2, 1, 0)) 
          FROM #t t2 
          WHERE t2.id = t1.id 
          AND t2.d2 <> t1.d2 ) = 0 
          AND  d2 <> (
            SELECT MAX(d2) 
            FROM #t t3 WHERE t3.id = t1.id)), 0) "parts hold"
FROM (
  SELECT id, timestampdiff(SQL_TSI_hour, MIN(d1), MAX(d2)) duration
  FROM #t GROUP BY id) d;

SELECT MIN(d1), MAX(d2) FROM #t GROUP BY id 

  
/*
The outer query gets the duration of the repair work. The complex subquery 
calculates the total number of days not waiting for parts. This is done by 
locating the start dates where the vehicle is not waiting for parts, and 
then count the number of days until it begins to wait for parts again:

1) The query for finding the starting dates when the vehicle is not waiting for parts, 
   i.e. finding all d2 that is not within any date range where the vehicle is waiting for part.

select d2 from t t1
where (select sum(IIF(t1.d2 between t2.d1 and t2.d2, 1, 0)) from t t2 
where t2.id = t1.id and t2.d2 <> t1.d2 ) = 0 AND 
           d2 <> (select max(d2) from t t3 where t3.id = t1.id))

2) The days where it vehicle is not waiting for part is the date from the above query till the vehicle is 
   waiting for part again

timestampdiff( SQL_TSI_DAY, t1.d2, ( SELECT min(d1) from t t4 where t4.id 
= t1.id and t4.d1 > t1.d2 ) )


Combining the two above and aggregating all such periods gives the number 
of days that the vehicle is not waiting for parts. The final query adds an 
extra condition to calculate result for each id from the outer query.

--
Alex
*/
