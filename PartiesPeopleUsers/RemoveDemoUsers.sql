SELECT p.partyid, pe.fullname, cm.description, u.username, au.appname, pp.privilege, pr.typ
FROM parties p
LEFT JOIN people pe ON pe.partyid = p.partyid
LEFT JOIN ContactMechanisms cm ON cm.partyid = p.partyid
LEFT JOIN users u ON u.partyid = p.partyid
LEFT JOIN applicationusers au ON au.partyid = p.partyid
LEFT JOIN partyprivileges pp ON pp.partyid = p.partyid
LEFT JOIN partyrelationships pr ON pr.partyid2 = p.partyid
WHERE u.username = 'demosc'

DECLARE @Cur CURSOR AS
  SELECT partyid
  FROM users 
  WHERE username LIKE '%jhob%';
OPEN @cur;
TRY
  WHILE FETCH @Cur DO
    DELETE FROM partyrelationships WHERE partyid2 = @Cur.partyid;
    DELETE FROM partyprivileges WHERE partyid = @Cur.partyid;
    DELETE FROM applicationusers WHERE partyid = @Cur.partyid;
    DELETE FROM users WHERE partyid = @Cur.partyid;
    DELETE FROM ContactMechanisms WHERE partyid = @Cur.partyid;
    DELETE FROM people WHERE partyid = @Cur.partyid;
    DELETE FROM parties WHERE partyid = @Cur.partyid;
  END WHILE;
FINALLY
  CLOSE @Cur;
END TRY;    