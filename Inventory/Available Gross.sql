-- available gross --
-- grouped by available age BY week 
-- current cost FROM arkona
-- raw materials ?? --
SELECT TheWeek, stocknumber
-- SELECT * 
FROM (
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      WHEN TheDate >= CurDate() - 62 AND TheDate <= CurDate() - 56 THEN 9
      WHEN TheDate >= CurDate() - 69 AND TheDate <= CurDate() - 63 THEN 10
      WHEN TheDate >= CurDate() - 76 AND TheDate <= CurDate() - 70 THEN 11
      WHEN TheDate >= CurDate() - 83 AND TheDate <= CurDate() - 77 THEN 12
      WHEN TheDate >= CurDate() - 90 AND TheDate <= CurDate() - 84 THEN 13
      WHEN TheDate >= CurDate() - 97 AND TheDate <= CurDate() - 91 THEN 14
      WHEN TheDate >= CurDate() - 104 AND TheDate <= CurDate() - 98 THEN 15
      WHEN TheDate >= CurDate() - 111 AND TheDate <= CurDate() - 105 THEN 16
      WHEN TheDate >= CurDate() - 118 AND TheDate <= CurDate() - 112 THEN 17 
      WHEN TheDate >= CurDate() - 125 AND TheDate <= CurDate() - 119 THEN 18 
      WHEN TheDate >= CurDate() - 132 AND TheDate <= CurDate() - 120 THEN 19 
      WHEN TheDate >= CurDate() - 139 AND TheDate <= CurDate() - 127 THEN 20 
      WHEN TheDate >= CurDate() - 146 AND TheDate <= CurDate() - 134 THEN 21 
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate >= CurDate() - 55
  AND TheDate <= CurDate()) da  
LEFT JOIN (
  SELECT *
  FROM VehicleInventoryItemStatuses 
  WHERE category = 'RMFlagAV'
  AND ThruTS IS NULL) s ON da.TheDate = CAST(s.FromTS AS sql_date)
INNER JOIN VehicleInventoryItems v ON s.VehicleInventoryItemID = v.VehicleInventoryItemID 
  AND LEFT(v.stocknumber, 1) NOT IN ('H','C')  


SELECT count(*)VehicleInventoryItemID, TheWeek 
-- SELECT theweek, s.VehicleInventoryItemID 
FROM VehicleInventoryItemStatuses s
LEFT JOIN (
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      ELSE 999
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate <= CurDate()) da ON da.TheDate = CAST(s.FromTS AS sql_date)
LEFT JOIN VehiclePricings p ON s.VehicleInventoryItemID =  p.VehicleInventoryItemID   
WHERE s.category = 'RMFlagAV'
AND ThruTS IS NULL 
GROUP BY TheWeek



-- price
-- need cost
SELECT count(*)VehicleInventoryItemID, TheWeek, SUM(Amount) 
-- SELECT theweek, s.VehicleInventoryItemID, vp.amount
FROM VehicleInventoryItemStatuses s
LEFT JOIN (
  SELECT d.*, 
    CASE 
      WHEN TheDate >= CurDate() - 6 AND TheDate <= CurDate() - 0 THEN 1
      WHEN TheDate >= CurDate() - 13 AND TheDate <= CurDate() - 7 THEN 2
      WHEN TheDate >= CurDate() - 20 AND TheDate <= CurDate() - 14 THEN 3
      WHEN TheDate >= CurDate() - 27 AND TheDate <= CurDate() - 21 THEN 4
      WHEN TheDate >= CurDate() - 34 AND TheDate <= CurDate() - 28 THEN 5
      WHEN TheDate >= CurDate() - 41 AND TheDate <= CurDate() - 35 THEN 6
      WHEN TheDate >= CurDate() - 48 AND TheDate <= CurDate() - 42 THEN 7
      WHEN TheDate >= CurDate() - 55 AND TheDate <= CurDate() - 49 THEN 8
      ELSE 999
    END AS TheWeek
  FROM dds.day d
  WHERE TheDate <= CurDate()) da ON da.TheDate = CAST(s.FromTS AS sql_date)
LEFT JOIN (
  SELECT p.VehicleInventoryItemID, d.Amount 
  FROM VehiclePricings p
  LEFT JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
  WHERE d.typ = 'VehiclePricingDetail_BestPrice'
  AND p.VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID = p.VehicleInventoryItemID)) vp ON s.VehicleInventoryItemID = vp.VehicleInventoryItemID 
WHERE s.category = 'RMFlagAV'
AND ThruTS IS NULL 
GROUP BY TheWeek


SELECT p.VehicleInventoryItemID, d.Amount 
FROM VehiclePricings p
LEFT JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
WHERE d.typ = 'VehiclePricingDetail_BestPrice'
AND p.VehiclePricingTS = (
  SELECT MAX(VehiclePricingTS)
  FROM VehiclePricings
  WHERE VehicleInventoryItemID = p.VehicleInventoryItemID)
  
  
SELECT VehicleInventoryItemID FROM (
SELECT p.VehicleInventoryItemID, d.Amount 
-- INTO #xx
FROM VehiclePricings p
LEFT JOIN VehiclePricingDetails d ON p.VehiclePricingID = d.VehiclePricingID 
WHERE d.typ = 'VehiclePricingDetail_BestPrice'
AND p.VehiclePricingTS = (
  SELECT MAX(VehiclePricingTS)
  FROM VehiclePricings
  WHERE VehicleInventoryItemID = p.VehicleInventoryItemID)) a GROUP BY VehicleInventoryItemID HAVING COUNT(*) > 1
    
