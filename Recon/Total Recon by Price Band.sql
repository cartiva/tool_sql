


SELECT viix.StockNumber, vix.yearmodel, vix.make, vix.model, sales.SoldDate, sales.SoldAmount, 
  coalesce(m.total, 0) AS MechTotal,
  coalesce(b.total, 0) AS BodyTotal,
  coalesce(a.total, 0) AS AppTotal,
  coalesce(m.total, 0) + coalesce(b.total, 0) + coalesce(a.total, 0) AS Total 
FROM (
  SELECT VehicleInventoryItemID, cast(SoldTS AS sql_Date) AS SoldDate, SoldAmount
  FROM vehicleSales
  WHERE SoldTS > '08/31/2010 00:00:00'
  AND typ = 'VehicleSale_Retail'
  AND status <> 'VehicleSale_SaleCanceled') sales
LEFT JOIN (
  SELECT VehicleInventoryItemID, SUM(TotalPartsAmount + LaborAmount) AS total  
  FROM VehicleReconItems vrib
  INNER JOIN AuthorizedReconItems arib ON vrib.VehicleReconItemID = arib.VehicleReconItemID 
  WHERE vrib.typ LIKE 'B%'
  AND arib.status = 'AuthorizedReconItem_Complete'
  GROUP BY VehicleInventoryItemID) b ON sales.VehicleInventoryItemID = b.VehicleInventoryItemID 
LEFT JOIN (
  SELECT VehicleInventoryItemID, SUM(TotalPartsAmount + LaborAmount) AS total  
  FROM VehicleReconItems vria
  INNER JOIN AuthorizedReconItems aria ON vria.VehicleReconItemID = aria.VehicleReconItemID 
  WHERE vria.typ LIKE 'A%'
  AND aria.status = 'AuthorizedReconItem_Complete'
  GROUP BY VehicleInventoryItemID) a ON sales.VehicleInventoryItemID = a.VehicleInventoryItemID   
LEFT JOIN (
  SELECT VehicleInventoryItemID, SUM(TotalPartsAmount + LaborAmount) AS total  
  FROM VehicleReconItems vrim
  INNER JOIN AuthorizedReconItems arim ON vrim.VehicleReconItemID = arim.VehicleReconItemID 
  WHERE vrim.typ LIKE 'M%'
  AND arim.status = 'AuthorizedReconItem_Complete'
  GROUP BY VehicleInventoryItemID) m ON sales.VehicleInventoryItemID = m.VehicleInventoryItemID    
LEFT JOIN VehicleInventoryItems viix ON sales.VehicleInventoryItemID = viix.VehicleInventoryItemID 
LEFT JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID   
WHERE sales.soldAmount < 8001  
ORDER BY sales.SoldDate DESC

