SELECT * FROM (
  SELECT 
 
  CASE 
    WHEN po.ReceivedTS IS NOT NULL then 'R'
	WHEN po.OrderedTS IS NOT NULL THEN 'O'
    ELSE 'X'
  END AS Tires,
 
  ps.partsstatus, viix.VehicleInventoryItemID, 
  viix.LocationID, viix.OwningLocationID, viix.StockNumber, vi.VIN, vi.YearModel,
  vi.Make, vi.Model, viix.CurrentPriority,  
    CASE ms.status
      WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
      WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
  	  WHEN 'MechanicalReconProcess_NotStarted' THEN
        CASE 
          WHEN (
            SELECT COUNT(VehicleInventoryItemID)
            FROM VehicleInventoryItemStatuses
            WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
            AND category = 'MechanicalReconDispatched'
            AND ThruTS IS NULL) = 0 THEN 'Not Started'
          ELSE
            'Dispatched'
          END
    END AS MechanicalStatus,
    CASE bs.status
      WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
      WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
  	  WHEN 'BodyReconProcess_NotStarted' THEN
        CASE 
          WHEN (
            SELECT COUNT(VehicleInventoryItemID)
            FROM VehicleInventoryItemStatuses
            WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
            AND category = 'BodyReconDispatched'
            AND ThruTS IS NULL) = 0 THEN 'Not Started'
          ELSE
            'Dispatched'
          END
    END AS BodyStatus,
    CASE ast.status
      WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
      WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'No Open Items'
  	  WHEN 'AppearanceReconProcess_NotStarted' THEN
        CASE 
          WHEN (
            SELECT COUNT(VehicleInventoryItemID)
            FROM VehicleInventoryItemStatuses
            WHERE VehicleInventoryItemID = viix.VehicleInventoryItemID
            AND category = 'AppearanceReconDispatched'
            AND ThruTS IS NULL) = 0 THEN 'Not Started'
          ELSE
            'Dispatched'
          END
    END AS AppearanceStatus,
  vi.ExteriorColor AS Color,
  CASE 
    WHEN vtm.VehicleInventoryItemID IS NOT NULL THEN 'X'
	ELSE ''
  END AS OnMoveList 	
  FROM VehicleInventoryItems viix
  INNER JOIN VehicleItems vi on vi.VehicleItemID = viix.VehicleItemID
  INNER JOIN MakeModelClassifications mmc ON mmc.Make = vi.Make
    AND mmc.Model = vi.Model
    AND mmc.ThruTS IS NULL  
  LEFT JOIN  VehicleInventoryItemStatuses bs ON viix.VehicleInventoryItemID = bs.VehicleInventoryItemID
    AND bs.category = 'BodyReconProcess'
    AND bs.ThruTS IS NULL 
  LEFT JOIN  VehicleInventoryItemStatuses ms ON viix.VehicleInventoryItemID = ms.VehicleInventoryItemID
    AND ms.category = 'MechanicalReconProcess'
    AND ms.ThruTS IS NULL 
  LEFT JOIN  VehicleInventoryItemStatuses ast ON viix.VehicleInventoryItemID = ast.VehicleInventoryItemID
    AND ast.category = 'AppearanceReconProcess'
    AND ast.ThruTS IS NULL 
  LEFT JOIN (
    SELECT * 
	FROM (
	  EXECUTE PROCEDURE GetViiPartsStatus()) wtf ) ps ON viix.VehicleInventoryItemID = ps.VehicleInventoryItemID	
  LEFT JOIN VehiclesToMove vtm ON viix.VehicleInventoryItemID = vtm.VehicleInventoryItemID 
    AND vtm.ThruTS IS NULL 
  LEFT JOIN (
    SELECT vr.VehicleInventoryItemID, vr.VehicleReconItemID 
    FROM VehicleReconItems vr
    WHERE vr.typ = 'MechanicalReconItem_Tires'
      OR (vr.typ = 'MechanicalReconItem_Other' AND vr.Description LIKE '%tire%')
    AND vr.partsinstock = true) tvr ON viix.VehicleInventoryItemID = tvr.VehicleInventoryItemID 	
  LEFT JOIN PartsOrders po ON tvr.VehicleReconItemID = po.VehicleReconItemID 
  WHERE viix.ThruTS IS NULL 
  AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')) wtf1
WHERE BodyStatus <> 'No Open Items' OR MechanicalStatus <> 'No Open Items' OR AppearanceStatus <> 'No Open Items'
-- ORDER BY stocknumber