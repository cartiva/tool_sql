fuck it, i DO NOT want to go through ALL the stored procs WHERE this change
will result IN subquery returns ....
short cut, just UPDATE the existing row to Pre Auction

done on 7/6/14


SELECT left(name, 50), sql_script
FROM system.storedprocedures
WHERE contains (lower(CAST(sql_Script AS sql_longvarchar)), 'typdescriptions')
  AND name NOT LIKE 'z%'
ORDER BY left(name, 50)

SELECT left(name, 50), sql_script
FROM system.storedprocedures
WHERE lower(CAST(sql_Script AS sql_longvarchar)) LIKE '%typdescriptions%'
AND name NOT LIKE 'z%'

SELECT left(name, 50), sql_script
FROM system.storedprocedures
WHERE lower(CAST(sql_Script AS sql_longvarchar)) LIKE '%typdescriptions%'
  AND lower(CAST(sql_Script AS sql_longvarchar)) LIKE '%reconpac%'
  AND name NOT LIKE 'z%'


SELECT *
FROM typDescriptions
ORDER BY typ

/* nope, NOT today
DECLARE @now timestamp;
@now = (SELECT now() FROM system.iota);
BEGIN TRANSACTION;
TRY 
UPDATE typDescriptions
  SET thruTs = @now
WHERE typ = 'ReconPackage_AsIsWS'
  AND thruTs IS NULL;
INSERT INTO typDescriptions (typ, fromTS, thruTS, Description, sequence)
SELECT typ, @now, CAST(NULL AS sql_timestamp), 'Pre Auction', Sequence
FROM typDescriptions 
WHERE typ = 'ReconPackage_AsIsWS'; 
COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   
*/   

-- 9/16/14 some sales stats

** Note ** 
the only thing i changed was typDescriptions.Description
FROM AS IS WS to PRe Auction

the thing to keep IN mind IS that, the description changed IN July
AS IS ws to pre auction
but, probably more importantly, how a consultant IS paid also changed
FROM 1/2 a point, to a whole point, i believe

SELECT month(saledate), max(f.monthName), COUNT(*)
FROM (
  SELECT b.stocknumber, c.vin, CAST(a.soldts AS sql_date) AS saleDate, d.*, dd.*
  FROM vehicleSales a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID 
  INNER JOIN VehicleItems c on b.VehicleItemID = c.VehicleItemID  
  LEFT JOIN selectedReconPAckages d on a.VehicleInventoryItemID = d.VehicleInventoryItemID 
    AND d.selectedReconPackageTS = (
      SELECT MAX(selectedReconPackageTS)
      FROM selectedReconPAckages
      WHERE VehicleInventoryItemID = d.VehicleInventoryItemID)
  INNER JOIN typDescriptions dd on d.typ = dd.typ      
  WHERE year(a.soldts) = 2014
    AND a.typ = 'VehicleSale_Retail'
    AND a.status = 'VehicleSale_Sold') e   
LEFT JOIN dds.day f on e.saledate = f.thedate      
WHERE e.description = 'Pre Auction' 
GROUP BY month(saledate) 