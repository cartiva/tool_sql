-- display vehicle promise time ON SB AND DCU grids
-- date/No Sched
/*
so what IS a veh promise time?
MAX promisetime(mProm, bProm, aProm)
get back ON track, i'll be adding join/s to the sb & dcu views
IF the vehicle IS IN sb/dcu AND has OPEN recon THEN there needs to be a promts
*/
/*
SELECT * 
FROM ReconPlans

DROP TABLE #jon
SELECT *
INTO #jon
FROM vUsedCarsOpenRecon

SELECT * FROM #jon

SELECT stocknumber
FROM #jon
--- hmmm colaesce ?
  SELECT v.VehicleInventoryItemID, 
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS MechSched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS BodySched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS AppSched,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS MechProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS BodyProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS AppProm    
  FROM (
    SELECT DISTINCT VehicleInventoryItemID
    FROM reconplans
    WHERE ThruTS IS NULL
    AND VehicleInventoryItemID IN (
      SELECT VehicleInventoryItemID
      FROM VehicleInventoryItems
      WHERE ThruTS IS NULL)) v
      
*/      
      

--   SELECT 
   
/*
11-2-2008 changed Price to pull FROM PricingDetails
01-23-2009 added ExpectedDeliveryDate

DROP view vUsedCarsSalesBuffer
SELECT * FROM vUsedCarsSalesBuffer
 */
/**/
  select 
  stocknumber,
--  CASE
--    WHEN rs.category = 'MechanicalReconProcess' THEN 
--      CASE rs.status
--        WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
--      END
--  END AS mStatus,
  mSched, mProm,
  bSched, bProm,
  aSched, aProm, 
--  rp.VehicleInventoryItemID,
--
/*   
--  rp.MechStatus, 
      CASE (
          SELECT status
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
          AND category = 'MechanicalReconProcess'
          AND ThruTS IS NULL)
        WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
        WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
        WHEN 'MechanicalReconProcess_NotStarted' THEN 'Open'
      END AS MechStatus,

--  rp.BodyStatus, 
      CASE (
          SELECT status
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
          AND category = 'BodyReconProcess'
          AND ThruTS IS NULL)
        WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
        WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'None'
        WHEN 'BodyReconProcess_NotStarted' THEN 'Open'
      END AS BodyStatus,

--  rp.AppStatus 
     
      CASE (
          SELECT status
          FROM VehicleInventoryItemStatuses 
          WHERE VehicleInventoryItemID = vii.VehicleInventoryItemID
          AND category = 'AppearanceReconProcess'
          AND ThruTS IS NULL)
        WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
        WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'None'
        WHEN 'AppearanceReconProcess_NotStarted' THEN 'Open'
      END AS AppStatus,
*/
  
--  Utilities.CurrentViiReconStatus(vii.VehicleInventoryItemID) AS ReconStatus
  
  vii.VehicleInventoryItemID, 
  vii.LocationID,
  vii.OwningLocationID,
  vii.StockNumber,
  vi.VIN,
  Coalesce(Current_Date() - Convert(viis.FromTS, SQL_DATE), 0) AS DaysInStatus,
  Coalesce(Trim(vi.YearModel), '') + ' ' + Coalesce(Trim(vi.Make), '') + ' ' + 
    Coalesce(Trim(vi.Model), '') AS VehicleDescription,        
  viisp.ExpectedDeliveryDate,
  
  (SELECT TOP 1 vpd.Amount
    FROM VehiclePricings vp
    inner join VehiclePricingDetails vpd on vpd.VehiclePricingID = vp.VehiclePricingID and vpd.Typ = 'VehiclePricingDetail_BestPrice' 
    WHERE vp.VehicleInventoryItemID = vii.VehicleInventoryItemID
    ORDER BY VehiclePricingTS DESC) as Price,
    
  coalesce((
    SELECT p.LastName
    FROM People p 
    WHERE p.PartyID = viisp.SalesConsultantPartyID), viisp.SalesConsultantPartyID collate ads_default_ci) AS SalesConsultantName,
 (SELECT p.LastName
    FROM People p 
    WHERE p.PartyID = viisp.ManagerPartyID) AS TeamLeaderName,
  vi.ExteriorColor AS Color,
  
--  Utilities.CurrentViiReconStatus(vii.VehicleInventoryItemID) AS ReconStatus,
  
  CASE Utilities.IsViiWtf(vii.VehicleInventoryItemID)
    WHEN true THEN 'X'
    ELSE ''
  END AS WTF 
  
 
--SELECT vii.stocknumber
FROM VehicleInventoryItemStatuses viis
INNER join VehicleInventoryItems vii
  on vii.VehicleInventoryItemID = viis.VehicleInventoryItemID
-- what IF there are 2 VehicleInventoryItems for a VehicleItem,
-- apparently nothing, did a test AND ok   
INNER JOIN VehicleItems vi
  ON vi.VehicleItemID = vii.VehicleItemID 
INNER JOIN MakeModelClassifications mmc
          on mmc.Make = vi.Make
          AND mmc.Model = vi.Model
          AND mmc.ThruTS IS NULL
INNER JOIN VehicleInventoryItemSalePending viisp ON viisp.VehicleInventoryItemID = viis.VehicleInventoryItemID
  AND viisp.ThruTS IS NULL
--LEFT JOIN ReconPlans rp ON vii.VehicleInventoryItemID = rp.VehicleInventoryItemID 
--  AND rp.ThruTS IS NULL 

-- ok this IS going to be a recurring pattern
-- because reconplans uses types, there will be multiple records  
      

LEFT JOIN ( -- ReconPlans
  SELECT v.VehicleInventoryItemID, 
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS mSched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS bSched,
    (SELECT ScheduleTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS aSched,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Mechanical' AND ThruTS IS NULL) AS mProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Body' AND ThruTS IS NULL) AS bProm,
    (SELECT PromiseTS FROM reconplans WHERE VehicleInventoryItemID = v.VehicleInventoryItemID AND typ = 'ReconPlan_Appearance' AND ThruTS IS NULL) AS aProm
  FROM (
    SELECT DISTINCT VehicleInventoryItemID
    FROM reconplans
    WHERE ThruTS IS NULL) v) rp ON vii.VehicleInventoryItemID = rp.VehicleInventoryItemID   

/*    
LEFT JOIN ( -- Recon Statuses
  SELECT VehicleInventoryItemID,
    (select
      CASE 
        WHEN category = 'MechanicalReconProcess' THEN
        CASE status
          WHEN 'MechanicalReconProcess_InProcess' THEN 'WIP'
          WHEN 'MechanicalReconProcess_NoIncompleteReconItems' THEN 'None'
          WHEN 'MechanicalReconProcess_NotStarted' THEN 'Open'
        END
      END 
      FROM system.iota) AS MechStatus
    
      CASE 
        WHEN  category = 'BodyReconProcess' THEN
        CASE status
          WHEN 'BodyReconProcess_InProcess' THEN 'WIP'
          WHEN 'BodyReconProcess_NoIncompleteReconItems' THEN 'None'
          WHEN 'BodyReconProcess_NotStarted' THEN 'Open'
        END 
      END AS BodyStatus,
     
      CASE 
        WHEN category = 'AppearanceReconProcess' THEN
        CASE status
          WHEN 'AppearanceReconProcess_InProcess' THEN 'WIP'
          WHEN 'AppearanceReconProcess_NoIncompleteReconItems' THEN 'None'
          WHEN 'AppearanceReconProcess_NotStarted' THEN 'Open'
        END 
    END AS AppStatus  
       
  FROM VehicleInventoryItemStatuses vs
  WHERE category IN ('MechanicalReconProcess', 'BodyReconProcess', 'AppearanceReconProcess')
  and ThruTS is null) rs ON viis.VehicleInventoryItemID = rs.VehicleInventoryItemID 
*/  
/*
LEFT JOIN (
  SELECT VehicleInventoryItemID, category, status
  FROM VehicleInventoryItemStatuses 
  WHERE ThruTS IS NULL
  AND category IN ('MechanicalReconProcess', 'BodyReconProcess','AppearanceReconProcess')) rs ON viis.VehicleInventoryItemID = rs.VehicleInventoryItemID 
*/ 
    
  
WHERE viis.Status = 'RMFlagSB_SalesBuffer'
--WHERE viis.Status = 'RawMaterials_Sold'
AND viis.ThruTS IS NULL
ORDER BY stocknumber

--
      
      
      