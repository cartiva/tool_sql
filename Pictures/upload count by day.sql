select *
FROM viipictures

SELECT CAST(uploadts AS sql_date) AS [Date], e.dayname, c.fullname AS [Uploaded By], 
  COUNT(*) AS Vehicles
FROM viipictureuploads a
--INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
--  AND b.owninglocationid = (
--    SELECT partyid
--	FROM organizations
--	WHERE fullname = 'GF-Rydells')
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) BETWEEN '01/02/2015' AND curdate() -1
GROUP BY CAST(uploadts AS sql_date), c.fullname, e.dayname
ORDER BY CAST(uploadts AS sql_date) DESC

-- modify for a daily run
SELECT CAST(uploadts AS sql_date) AS [Date], e.dayname, c.fullname AS [Uploaded By], 
  b.stocknumber, a.uploadts
FROM viipictureuploads a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) = curdate() - 1
UNION 
SELECT CAST(uploadts AS sql_date) AS [Date], e.dayname, ' - Total -', CAST(COUNT(*) AS sql_char),
  CAST(NULL AS sql_timestamp)
FROM viipictureuploads a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) = curdate() - 1
GROUP BY CAST(uploadts AS sql_date), e.dayname
ORDER BY uploadts

-- modify for a daily run
-- ADD a picture count
SELECT CAST(uploadts AS sql_date) AS [Date], c.fullname AS [Uploaded By], 
  b.stocknumber AS [Stock #], a.uploadts AS [Upload Time], COUNT(*) AS Pics
FROM viipictureuploads a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
INNER JOIN viipictures f on b.VehicleInventoryItemID = f.VehicleInventoryItemID 
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) = curdate() - 1
GROUP BY CAST(uploadts AS sql_date), e.dayname, c.fullname, 
  b.stocknumber, a.uploadts
UNION 
SELECT CAST(uploadts AS sql_date) AS [Date], 'Total for ' + TRIM(e.dayname), CAST(COUNT(*) AS sql_char),
  CAST(NULL AS sql_timestamp), CAST(NULL AS sql_integer)
FROM viipictureuploads a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) = curdate() - 1
GROUP BY CAST(uploadts AS sql_date), e.dayname
ORDER BY uploadts



SELECT * 
FROM viipictureuploads a 
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
WHERE b.stocknumber = '24011xx'

-- modify for automating the daily email
-- for the week of 7/6 - 7/12
SELECT date, [uploaded by],[stock #], CAST([upload time] AS sql_time) AS [upload time], pics
FROM (
SELECT CAST(uploadts AS sql_date) AS [Date], c.fullname AS [Uploaded By], 
  b.stocknumber AS [Stock #], a.uploadts AS [Upload Time], COUNT(*) AS Pics
FROM viipictureuploads a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
INNER JOIN viipictures f on b.VehicleInventoryItemID = f.VehicleInventoryItemID 
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) between '07/06/2015' and '07/12/2015' --curdate() - 1
GROUP BY CAST(uploadts AS sql_date), e.dayname, c.fullname, 
  b.stocknumber, a.uploadts
UNION 
SELECT CAST(uploadts AS sql_date) AS [Date], 'Total for ' + TRIM(e.dayname), CAST(COUNT(*) AS sql_char),
  CAST(NULL AS sql_timestamp), CAST(NULL AS sql_integer)
FROM viipictureuploads a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) between '07/06/2015' and '07/12/2015' --curdate() - 1
GROUP BY CAST(uploadts AS sql_date), e.dayname
) x
ORDER BY date, CAST([upload time] AS sql_time)


SELECT date, [uploaded by],[stock #], CAST([upload time] AS sql_time) AS [upload time], pics
FROM (
SELECT CAST(uploadts AS sql_date) AS [Date], c.fullname AS [Uploaded By], 
  b.stocknumber AS [Stock #], a.uploadts AS [Upload Time], COUNT(*) AS Pics
FROM viipictureuploads a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
INNER JOIN viipictures f on b.VehicleInventoryItemID = f.VehicleInventoryItemID 
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) = curdate()
GROUP BY CAST(uploadts AS sql_date), e.dayname, c.fullname, 
  b.stocknumber, a.uploadts
UNION 
SELECT CAST(uploadts AS sql_date) AS [Date], 'Total for ' + TRIM(e.dayname), CAST(COUNT(*) AS sql_char),
  CAST(NULL AS sql_timestamp), CAST(NULL AS sql_integer)
FROM viipictureuploads a
INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
LEFT JOIN people c on a.uploadedby = c.partyid
WHERE CAST(uploadts AS sql_date) = curdate() 
GROUP BY CAST(uploadts AS sql_date), e.dayname
) x
ORDER BY date, CAST([upload time] AS sql_time)




/*
what i want IS IF there are no pictures uploaded ON a day, change the subject
line and body to read " No pictures uploaded ON Sunday, 7/12/2015 "
the assumption IS that this will run at ~4AM each day, AND derive picture/date
data for the previous day
*/


	SELECT COUNT(*)
	FROM viipictureuploads a
  WHERE CAST(a.uploadts AS sql_date) = curdate() - 1


SELECT date, LEFT([uploaded by], 20) AS [upload by],[stock #], 
  CAST([upload time] AS sql_time) AS [upload time], 
	pics
FROM (
  SELECT CAST(uploadts AS sql_date) AS [Date], c.fullname AS [Uploaded By], 
    b.stocknumber AS [Stock #], a.uploadts AS [Upload Time], COUNT(*) AS Pics
  FROM viipictureuploads a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
  INNER JOIN viipictures f on b.VehicleInventoryItemID = f.VehicleInventoryItemID 
  LEFT JOIN people c on a.uploadedby = c.partyid
  WHERE CAST(uploadts AS sql_date) = curdate() - 1
  GROUP BY CAST(uploadts AS sql_date), e.dayname, c.fullname, 
    b.stocknumber, a.uploadts
  UNION 
  SELECT CAST(uploadts AS sql_date) AS [Date], 'Total for ' + TRIM(e.dayname), CAST(COUNT(*) AS sql_char),
    CAST(NULL AS sql_timestamp), CAST(NULL AS sql_integer)
  FROM viipictureuploads a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
  LEFT JOIN people c on a.uploadedby = c.partyid
  WHERE CAST(uploadts AS sql_date) = curdate() - 1
  GROUP BY CAST(uploadts AS sql_date), e.dayname) x
ORDER BY date, CAST([upload time] AS sql_time)


SELECT date, LEFT([uploaded by], 20) AS [upload by],[stock #], 
  CAST([upload time] AS sql_time) AS [upload time], 
	pics
FROM (
  SELECT CAST(uploadts AS sql_date) AS [Date], c.fullname AS [Uploaded By], 
    b.stocknumber AS [Stock #], a.uploadts AS [Upload Time], COUNT(*) AS Pics
  FROM viipictureuploads a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
  INNER JOIN viipictures f on b.VehicleInventoryItemID = f.VehicleInventoryItemID 
  LEFT JOIN people c on a.uploadedby = c.partyid
  WHERE CAST(uploadts AS sql_date) = curdate() - 1
  GROUP BY CAST(uploadts AS sql_date), e.dayname, c.fullname, 
    b.stocknumber, a.uploadts) x
ORDER BY date, CAST([upload time] AS sql_time)	


-- actual sql FROM email python script
SELECT LEFT([uploaded by], 20) AS [upload by], [stock #],
  CAST([upload time] AS sql_time) AS [upload time], pics
FROM (
  SELECT c.fullname AS [Uploaded By],
    b.stocknumber AS [Stock #], a.uploadts AS [Upload Time], COUNT(*) AS Pics
  FROM viipictureuploads a
  INNER JOIN VehicleInventoryItems b on a.VehicleInventoryItemID = b.VehicleInventoryItemID
  INNER JOIN dds.day e on CAST(uploadts AS sql_date) = e.thedate
  INNER JOIN viipictures f on b.VehicleInventoryItemID = f.VehicleInventoryItemID
  LEFT JOIN people c on a.uploadedby = c.partyid
  WHERE CAST(uploadts AS sql_date) = curdate() - 1
  GROUP BY CAST(uploadts AS sql_date), e.dayname, c.fullname,
    b.stocknumber, a.uploadts) x
ORDER BY CAST([upload time] AS sql_time)