SELECT * FROM VehiclePricingDetails

SELECT viix.stocknumber, vp.VehiclePricingTS, vpd.amount
FROM vehiclepricings vp
INNER JOIN vehiclepricingdetails vpd ON vp.VehiclePricingID = vpd.VehiclePricingID
INNER JOIN VehicleInventoryItems viix ON vp.VehicleInventoryItemID = viix.VehicleInventoryItemID
WHERE vpd.typ = 'VehiclePricingDetail_BestPrice'
AND viix.ThruTS IS NULL 
ORDER BY viix.stocknumber,  vp.VehiclePricingTS


-- multiple pricings since AS of date
SELECT vp.VehicleInventoryItemID
FROM vehiclepricings vp
INNER JOIN vehiclepricingdetails vpd ON vp.VehiclePricingID = vpd.VehiclePricingID
INNER JOIN VehicleInventoryItems viix ON vp.VehicleInventoryItemID = viix.VehicleInventoryItemID
WHERE vpd.typ = 'VehiclePricingDetail_BestPrice'
AND viix.ThruTS IS NULL 
AND CAST(vp.VehiclePricingTS AS sql_date) >= curdate() - 30
GROUP BY vp.VehicleInventoryItemID 
HAVING COUNT(*) > 1

-- every pricing generates a VehiclePricingDetails of typ VehiclePricingDetail_BestPrice
SELECT *
FROM vehiclepricings vp
WHERE NOT EXISTS (
  SELECT 1
  FROM VehiclePricingDetails
  WHERE VehiclePricingID = vp.VehiclePricingID 
  AND typ = 'VehiclePricingDetail_BestPrice')



-- 140 ms (IN is faster than JOIN OR EXISTS)
-- NOT too bad
-- 10726c shows here, NOT cary
-- 11040b priced 3/21/2011, but same AS previous price
SELECT max(viix1.stocknumber), vp1.VehicleInventoryItemID, vpd1.amount, MAX(vp1.VehiclePricingTS) AS theDate
FROM VehiclePricings vp1
INNER JOIN VehiclePricingDetails vpd1 ON vp1.VehiclePricingID = vpd1.VehiclePricingID
LEFT JOIN VehicleInventoryItems viix1 ON vp1.VehicleInventoryItemID = viix1.VehicleInventoryItemID 
WHERE vp1.VehicleInventoryItemID IN (
  SELECT vp.VehicleInventoryItemID -- multiple pricings since AS of date
  FROM vehiclepricings vp
  INNER JOIN vehiclepricingdetails vpd ON vp.VehiclePricingID = vpd.VehiclePricingID
  INNER JOIN VehicleInventoryItems viix ON vp.VehicleInventoryItemID = viix.VehicleInventoryItemID -- limits to current inventory
  WHERE vpd.typ = 'VehiclePricingDetail_BestPrice'
  AND viix.ThruTS IS NULL 
  AND CAST(vp.VehiclePricingTS AS sql_date) >= curdate() - 24
  GROUP BY vp.VehicleInventoryItemID 
  HAVING COUNT(*) > 1)
AND vpd1.typ = 'VehiclePricingDetail_BestPrice'
GROUP BY vp1.VehicleInventoryItemID, vpd1.amount 
ORDER BY max(viix1.stocknumber), theDate DESC 

-- maybe a CASE based ON COUNT of records, SELECT top 1, ???


-- start with every pricing within x days, ON current inventory
-- ah ah ahaaa? eliminate walk pricings - they are always the first one, so
-- a walk pricing performed within x days IS irrelevant
SELECT vpx.VehicleInventoryItemID 
FROM VehiclePricings vpx 
INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL -- current inventory only
WHERE CAST(vpx.VehiclePricingTS AS sql_date) >= curdate() - 7
AND vpx.BasisTable <> 'VehicleWalks'
-- now i want to know for which of these vehicles IS this 
-- a repricing, ie, 
-- at any time IN the last x days, did this vehicle have a price different
-- FROM the above price
-- shit, it doesn't matter WHEN the previous pricing was
-- what we want IS any vehicle for which the price has changed IN the last x days
-- so, does this pricing CREATE a best price that IS different FROM the
-- previous best price

-- DO i care IF it has been repriced many times IN the last x days
-- OR, once it IS established that the most recent price IN the last x days IS
-- a different price, ALL i care about IS the last 2 prices



SELECT viix1.stocknumber, vpdx1.amount, COUNT(*)
FROM VehiclePricings vpx1
INNER JOIN VehiclePricingDetails vpdx1 ON vpx1.VehiclePricingID = vpdx1.VehiclePricingID 
INNER JOIN VehicleInventoryItems viix1 ON vpx1.VehicleInventoryItemID = viix1.VehicleInventoryItemID
  AND viix1.ThruTS IS NULL -- current inventory only
WHERE vpx1.VehicleInventoryItemID IN (
  SELECT vpx.VehicleInventoryItemID 
  FROM VehiclePricings vpx 
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL -- current inventory only
  WHERE CAST(vpx.VehiclePricingTS AS sql_date) >= curdate() - 7
  AND vpx.BasisTable <> 'VehicleWalks')
AND vpdx1.typ = 'VehiclePricingDetail_BestPrice' 
GROUP BY viix1.stocknumber, vpdx1.amount
HAVING COUNT(*) > 1
ORDER BY viix1.stocknumber 


-- GROUP BY VehicleInventoryItemID, amount
-- JOIN AND return differences

-- most current price (SET within last x days)
-- which IS different than the previous price
-- but IF it IS repriced, say 4 times, IN the last x days
-- 

-- hanging up ON :  look at pricing (VehiclePricings), look at price (VehiclePricingDetails)
-- don't care about pricing IF price doesn't change


-- current best price ON current inventory
SELECT viix.StockNumber, vpx.VehiclePricingTS, vpdx.Amount 
FROM VehiclePricings vpx 
INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
  AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL -- current inventory only
WHERE vpx.VehiclePricingTS = (
  SELECT MAX(VehiclePricingTS)
  FROM VehiclePricings
  WHERE VehicleInventoryItemID = vpx.VehicleInventoryItemID 
  GROUP BY vpx.VehicleInventoryItemID) 

-- ok, this IS CLOSE
-- p TABLE now needs to just return the most recent price 
-- 
SELECT c.VehicleInventoryItemID, c.stocknumber, c.camount, c.cdate, p.pamount, p.pdate
FROM ( --c current best price ON current inventory
  SELECT vpx.VehicleInventoryItemID, viix.StockNumber, vpdx.Amount AS cAmount, vpx.VehiclePricingTS AS cDate
  FROM VehiclePricings vpx 
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL -- current inventory only
  WHERE vpx.VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID = vpx.VehicleInventoryItemID 
    GROUP BY vpx.VehicleInventoryItemID)) c
INNER JOIN ( --p priced within x days 
  SELECT vpx1.VehicleInventoryItemID, vpdx1.Amount AS pAmount, vpx1.VehiclePricingTS AS pDate 
  FROM VehiclePricings vpx1 
  INNER JOIN VehicleInventoryItems viix1 ON vpx1.VehicleInventoryItemID = viix1.VehicleInventoryItemID
    AND viix1.ThruTS IS NULL -- current inventory only 
  INNER JOIN VehiclePricingDetails vpdx1 ON vpx1.VehiclePricingID = vpdx1.VehiclePricingID 
    AND vpdx1.typ = 'VehiclePricingDetail_BestPrice'
  WHERE CAST(vpx1.VehiclePricingTS AS sql_date) >= curdate() - 30) p 
ON c.cAmount <> p.pAmount AND c.VehicleInventoryItemID = p.VehicleInventoryItemID -- price <> current price
ORDER BY stocknumber

/* done ON 4-2 trying something different ON 4-3
-- shit, this was CLOSE
-- excludes 12579X:  3/23-20999, 3/16-20999, 3/15-20499
SELECT c.VehicleInventoryItemID, c.stocknumber, c.camount, p.pamount, c.camount-p.pamount, p.pdate
-- SELECT COUNT(*)
FROM ( --c current best price ON current inventory
  SELECT vpx.VehicleInventoryItemID, viix.StockNumber, vpdx.Amount AS cAmount
  FROM VehiclePricings vpx 
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL -- current inventory only
  WHERE vpx.VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID = vpx.VehicleInventoryItemID 
    GROUP BY vpx.VehicleInventoryItemID)) c
INNER JOIN ( --p priced within x days 
  SELECT vpx1.VehicleInventoryItemID, vpdx1.Amount AS pAmount, vpx1.VehiclePricingTS AS pDate 
  FROM VehiclePricings vpx1 
  INNER JOIN VehicleInventoryItems viix1 ON vpx1.VehicleInventoryItemID = viix1.VehicleInventoryItemID
    AND viix1.ThruTS IS NULL -- current inventory only 
  INNER JOIN VehiclePricingDetails vpdx1 ON vpx1.VehiclePricingID = vpdx1.VehiclePricingID 
    AND vpdx1.typ = 'VehiclePricingDetail_BestPrice'
  WHERE CAST(vpx1.VehiclePricingTS AS sql_date) >= curdate() - 30) p 
      ON c.cAmount <> p.pAmount AND c.VehicleInventoryItemID = p.VehicleInventoryItemID -- price <> current price
	  
WHERE pdate IN (
  SELECT top 2 VehiclePricingTS
  FROM VehiclePricings vpx1 
  INNER JOIN VehicleInventoryItems viix1 ON vpx1.VehicleInventoryItemID = viix1.VehicleInventoryItemID
    AND viix1.ThruTS IS NULL -- current inventory only 
  INNER JOIN VehiclePricingDetails vpdx1 ON vpx1.VehiclePricingID = vpdx1.VehiclePricingID 
    AND vpdx1.typ = 'VehiclePricingDetail_BestPrice'
  WHERE CAST(vpx1.VehiclePricingTS AS sql_date) >= curdate() - 30
  AND vpx1.VehicleInventoryItemID = p.VehicleInventoryItemID 
  ORDER BY VehiclePricingTS DESC)  
ORDER BY stocknumber
*/

-- 4/3 this seems to WORK
-- initial SELECT worked except, IF repriced multiple times within last x days
-- it returned multiple recordsc
-- SELECT Utilities.DropTablesIfExist('#jon') FROM system.iota;
DROP TABLE #jon;
SELECT c.VehicleInventoryItemID, c.stocknumber, c.camount, c.cdate, p.pamount, p.pdate
INTO #jon
FROM ( --c current best price ON current inventory
  SELECT vpx.VehicleInventoryItemID, viix.StockNumber, vpdx.Amount AS cAmount, vpx.VehiclePricingTS AS cDate
  FROM VehiclePricings vpx 
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL -- current inventory only
  WHERE vpx.VehiclePricingTS = (
    SELECT MAX(VehiclePricingTS)
    FROM VehiclePricings
    WHERE VehicleInventoryItemID = vpx.VehicleInventoryItemID 
    GROUP BY vpx.VehicleInventoryItemID)) c
INNER JOIN ( --p priced within x days 
  SELECT vpx1.VehicleInventoryItemID, vpdx1.Amount AS pAmount, vpx1.VehiclePricingTS AS pDate 
  FROM VehiclePricings vpx1 
  INNER JOIN VehicleInventoryItems viix1 ON vpx1.VehicleInventoryItemID = viix1.VehicleInventoryItemID
    AND viix1.ThruTS IS NULL -- current inventory only 
  INNER JOIN VehiclePricingDetails vpdx1 ON vpx1.VehiclePricingID = vpdx1.VehiclePricingID 
    AND vpdx1.typ = 'VehiclePricingDetail_BestPrice'
  WHERE CAST(vpx1.VehiclePricingTS AS sql_date) >= curdate() - 30) p 
ON c.cAmount <> p.pAmount AND c.VehicleInventoryItemID = p.VehicleInventoryItemID; -- price <> current price

SELECT *
-- SELECT COUNT(*) 
FROM #jon j1
WHERE pdate = ( -- this takes care of the multiple record problem
  SELECT MAX(pdate)
  FROM #jon
  WHERE VehicleInventoryItemID = j1.VehicleInventoryItemID
  GROUP BY VehicleInventoryItemID)
ORDER BY stocknumber;


-- uh oh
-- repriced today, does NOT get listed BY cary OR me
-- repriced, yes, but, the repricing amount = current price

SELECT * FROM VehiclePricings WHERE VehicleInventoryItemID = '27190202-c822-449a-8d19-87d386e9f0de'

-- ALL pricings ON current inventory:
  SELECT stocknumber, vpx1.VehicleInventoryItemID, vpdx1.Amount AS pAmount, vpx1.VehiclePricingTS AS pDate 
  FROM VehiclePricings vpx1 
  INNER JOIN VehicleInventoryItems viix1 ON vpx1.VehicleInventoryItemID = viix1.VehicleInventoryItemID
    AND viix1.ThruTS IS NULL -- current inventory only 
  INNER JOIN VehiclePricingDetails vpdx1 ON vpx1.VehiclePricingID = vpdx1.VehiclePricingID 
    AND vpdx1.typ = 'VehiclePricingDetail_BestPrice'
  ORDER BY stocknumber asc,  VehiclePricingTS desc 
-- repricing implies more than one pricing  
  SELECT stocknumber, vpx1.VehicleInventoryItemID, vpdx1.Amount AS pAmount, vpx1.VehiclePricingTS AS pDate 
  FROM VehiclePricings vpx1 
  INNER JOIN VehicleInventoryItems viix1 ON vpx1.VehicleInventoryItemID = viix1.VehicleInventoryItemID
    AND viix1.ThruTS IS NULL -- current inventory only 
  INNER JOIN VehiclePricingDetails vpdx1 ON vpx1.VehiclePricingID = vpdx1.VehiclePricingID 
    AND vpdx1.typ = 'VehiclePricingDetail_BestPrice'
  WHERE vpx1.VehicleInventoryItemID IN (
    SELECT vpx.VehicleInventoryItemID
	FROM VehiclePricings vpx
	INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
	WHERE viix.ThruTS IS NULL
	GROUP BY vpx.VehicleInventoryItemID 
	HAVING COUNT(*) > 1)
  ORDER BY stocknumber asc,  VehiclePricingTS desc 
  
-- repricing implies more than one pricing  
-- interested in NOT more than one pricing, more than one price
-- wahoo, this looks LIKE it works
  DROP TABLE #jon;
  DROP TABLE #jon1;
  SELECT stocknumber, vpx.VehicleInventoryItemID, vpdx.Amount, max(vpx.VehiclePricingTS) AS pDate 
  INTO #jon -- more than one pricing
  FROM VehiclePricings vpx 
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL -- current inventory only 
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  GROUP BY stocknumber, vpx.VehicleInventoryItemID, vpdx.Amount;  
  
  SELECT *
  INTO #jon1 -- more than one price AND priced within last x days
  FROM #jon j
  WHERE stocknumber IN (
    SELECT stocknumber 
    FROM #jon
    GROUP BY stocknumber
    HAVING COUNT(*) > 1)
  AND EXISTS (
    SELECT 1
    FROM #jon
    WHERE stocknumber = j.stocknumber
    AND CAST(pdate AS sql_date) >= curdate() - 14);
-- SELECT * FROM #jon
-- SELECT * FROM #jon1  

  SELECT distinct stocknumber, VehicleInventoryItemID, 
    (SELECT top 1 amount FROM #jon1 WHERE stocknumber = j1.stocknumber ORDER BY pdate DESC) AS CurPrice,
    (SELECT top 1 start at 2 amount FROM #jon1 WHERE stocknumber = j1.stocknumber ORDER BY pdate DESC)  AS PrevPrice,
    (SELECT top 1 pdate FROM #jon1 WHERE stocknumber = j1.stocknumber ORDER BY pdate DESC) AS PriceDate
  FROM #jon1 j1;
 
  
-- repricing implies more than one pricing  
-- interested in NOT more than one pricing, more than one price
-- wahoo, this looks LIKE it works
-- 4/5 12312xx:  4/4: 16499, 3/16: 16499, 1/12: 15999 - shows ON list, should NOT
-- also 12448xx, 12579xx, 11948a, 12156a (ALL increase)
-- AND (decrease) 12405xx
-- hmm, so more than one price IN #jon within x days
-- so i need to know IF there IS more than a price generated IN the last x days
-- which IS different that the previous price

-- 4/6 ok, a new tack
-- ALL pricings within last x days
-- ok, so, this IS every pricing within last x days
SELECT vpx.VehicleInventoryItemID, vpx.VehiclePricingTS, vpdx.amount
FROM VehiclePricings vpx
INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
  AND viix.ThruTS IS NULL  
INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
  AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
WHERE CAST(vpx.VehiclePricingTS AS sql_date) >= curdate() - 14 


SELECT VehicleInventoryItemID -- more than one price per VehicleInventoryItem
FROM ( -- unique viiid/price
  SELECT vpx.VehicleInventoryItemID
  FROM VehiclePricings vpx 
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL -- current inventory only 
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  GROUP BY vpx.VehicleInventoryItemID, vpdx.amount) wtf
GROUP BY VehicleInventoryItemID 
HAVING COUNT(*) > 1  

-- JOIN them all to serve AS a filter
-- so this gives ALL current inventory with mult prices that have been priced within last x days
-- q0406-1: 14 days (saved output AS excel)
SELECT distinct viix.stocknumber, vpx.vehiclepricingts, vpdx.amount
FROM VehiclePricings vpx -- pricings ON current inventory
INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL 
INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
  AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
INNER JOIN ( -- vehicle was priced IN last x days
  SELECT viix.stocknumber, vpx.VehicleInventoryItemID, vpx.VehiclePricingTS, vpdx.amount
  FROM VehiclePricings vpx
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND viix.ThruTS IS NULL  
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  WHERE CAST(vpx.VehiclePricingTS AS sql_date) >= curdate() - 14) dp ON vpx.VehicleInventoryItemID = dp.VehicleInventoryItemID 
INNER JOIN ( -- more than one price per VehicleInventoryItem
  SELECT VehicleInventoryItemID 
  FROM ( -- unique viiid/price
    SELECT vpx.VehicleInventoryItemID
    FROM VehiclePricings vpx 
    INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
      AND viix.ThruTS IS NULL -- current inventory only 
    INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
      AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
    GROUP BY vpx.VehicleInventoryItemID, vpdx.amount) wtf
  GROUP BY VehicleInventoryItemID 
  HAVING COUNT(*) > 1)  mp ON dp.VehicleInventoryItemID = mp.VehicleInventoryItemID
ORDER BY viix.stocknumber, vpx.VehiclePricingTS DESC  

-- yep, this IS it
DROP TABLE #jon;
SELECT distinct viix.stocknumber, vpx.vehiclepricingts, vpdx.amount
INTO #jon
FROM VehiclePricings vpx -- pricings ON current inventory
INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL 
INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
  AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
INNER JOIN ( -- vehicle was priced IN last x days
  SELECT viix.stocknumber, vpx.VehicleInventoryItemID, vpx.VehiclePricingTS, vpdx.amount
  FROM VehiclePricings vpx
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND viix.ThruTS IS NULL  
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  WHERE CAST(vpx.VehiclePricingTS AS sql_date) >= curdate() - 28) dp ON vpx.VehicleInventoryItemID = dp.VehicleInventoryItemID 
INNER JOIN ( -- more than one price per VehicleInventoryItem
  SELECT VehicleInventoryItemID 
  FROM ( -- unique viiid/price
    SELECT vpx.VehicleInventoryItemID
    FROM VehiclePricings vpx 
    INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
      AND viix.ThruTS IS NULL -- current inventory only 
    INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
      AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
    GROUP BY vpx.VehicleInventoryItemID, vpdx.amount) wtf
  GROUP BY VehicleInventoryItemID 
  HAVING COUNT(*) > 1)  mp ON dp.VehicleInventoryItemID = mp.VehicleInventoryItemID;

SELECT *
FROM #jon j
WHERE -- IF the current price IS different than the previous price, we want it
    (SELECT top 1 amount FROM #jon WHERE stocknumber = j.stocknumber ORDER BY stocknumber, VehiclePricingTS DESC) -- current price
      <>
    (SELECT top 1 start at 2 amount FROM #jon WHERE stocknumber = j.stocknumber ORDER BY stocknumber, VehiclePricingTS DESC) -- next previous price
UNION
SELECT *
FROM #jon j
WHERE -- this IS the rest, current price = previous price
    (SELECT top 1 amount FROM #jon WHERE stocknumber = j.stocknumber ORDER BY stocknumber, VehiclePricingTS DESC)
      =
    (SELECT top 1 start at 2 amount FROM #jon WHERE stocknumber = j.stocknumber ORDER BY stocknumber, VehiclePricingTS DESC)
AND EXISTS ( -- but IF there IS a price that IS different than the current price AND was generated within the last x days, we want it
  SELECT 1
  FROM #jon
  WHERE stocknumber = j.stocknumber
  AND CAST(VehiclePricingTS AS sql_date) >= curdate() - 28
  AND amount <> (SELECT top 1 amount FROM #jon WHERE stocknumber = j.stocknumber ORDER BY stocknumber, VehiclePricingTS DESC))
ORDER BY stocknumber asc, vehiclepricingts DESC;

-- refactored to use WHERE/OR rather than a UNION
-- VehicleInventoryItemID instead of stocknumber
DROP TABLE #jon;

SELECT DISTINCT viix.VehicleInventoryItemID, vpx.vehiclepricingts, vpdx.amount
INTO #jon
FROM VehiclePricings vpx -- pricings ON current inventory
INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
  AND viix.ThruTS IS NULL 
INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
  AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
INNER JOIN ( -- vehicle was priced IN last x days
  SELECT viix.VehicleInventoryItemID, vpx.VehicleInventoryItemID, vpx.VehiclePricingTS, vpdx.amount
  FROM VehiclePricings vpx
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND viix.ThruTS IS NULL  
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  WHERE CAST(vpx.VehiclePricingTS AS sql_date) >= curdate() - 1) dp ON vpx.VehicleInventoryItemID = dp.VehicleInventoryItemID 
INNER JOIN ( -- more than one price per VehicleInventoryItem
  SELECT VehicleInventoryItemID 
  FROM ( -- unique viiid/price
    SELECT vpx.VehicleInventoryItemID
    FROM VehiclePricings vpx 
    INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
      AND viix.ThruTS IS NULL -- current inventory only 
    INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
      AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
    GROUP BY vpx.VehicleInventoryItemID, vpdx.amount) wtf
  GROUP BY VehicleInventoryItemID 
  HAVING COUNT(*) > 1)  mp ON dp.VehicleInventoryItemID = mp.VehicleInventoryItemID;

SELECT *
FROM #jon j
WHERE ((-- IF the current price IS different than the previous price, we want it
  (SELECT top 1 amount FROM #jon WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC) -- current price
    <>
  (SELECT top 1 start at 2 amount FROM #jon WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC)) -- next previous price
OR ((-- this IS the rest, current price = previous price, AND EXISTS a price generated within last x days
  (SELECT top 1 amount FROM #jon WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC)
    =
  (SELECT top 1 start at 2 amount FROM #jon WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC))
    AND ((CAST(VehiclePricingTS AS sql_date) >= curdate() - 1) 
      AND (amount <> (SELECT top 1 amount FROM #jon WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC)))))
ORDER BY VehicleInventoryItemID asc, vehiclepricingts DESC;



  
-- 4/6 this IS it
-- restructure it for the stored proc
  DECLARE @MarketID string;
  DECLARE @LocationID string;
  DECLARE @DaysBack integer;
  @MarketID = 'A4847DFC-E00E-42E7-89EE-CD4368445A82';
  @LocationID = 'B6183892-C79D-4489-A58C-B526DF948B06';
  @DaysBack = 7;  
  DROP TABLE #vptt;
  DROP TABLE #vptt1;
  SELECT DISTINCT viix.VehicleInventoryItemID, vpx.vehiclepricingts, vpdx.amount
  INTO #vptt
  FROM VehiclePricings vpx -- pricings ON current inventory
  INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
    AND viix.ThruTS IS NULL 
	AND -- selected market/Location
      CASE 
        WHEN @LocationID <> '' THEN viix.LocationID = @LocationID
        ELSE viix.LocationID IN (
          SELECT PartyID2
          FROM PartyRelationships
          WHERE PartyID1 = @MarketID
          AND typ = 'PartyRelationship_MarketLocations'
		  AND ThruTS IS NULL)
      END	    
  INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
    AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
  INNER JOIN ( -- vehicle was priced IN last x days
    SELECT viix.VehicleInventoryItemID, vpx.VehicleInventoryItemID, vpx.VehiclePricingTS, vpdx.amount
    FROM VehiclePricings vpx
    INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID 
      AND viix.ThruTS IS NULL  
    INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID
      AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
    WHERE CAST(vpx.VehiclePricingTS AS sql_date) >= curdate() - @DaysBack) dp ON vpx.VehicleInventoryItemID = dp.VehicleInventoryItemID 
  INNER JOIN ( -- more than one price per VehicleInventoryItem
    SELECT VehicleInventoryItemID 
    FROM ( -- unique viiid/price
      SELECT vpx.VehicleInventoryItemID
      FROM VehiclePricings vpx 
      INNER JOIN VehicleInventoryItems viix ON vpx.VehicleInventoryItemID = viix.VehicleInventoryItemID
        AND viix.ThruTS IS NULL -- current inventory only 
      INNER JOIN VehiclePricingDetails vpdx ON vpx.VehiclePricingID = vpdx.VehiclePricingID 
        AND vpdx.typ = 'VehiclePricingDetail_BestPrice'
      GROUP BY vpx.VehicleInventoryItemID, vpdx.amount) wtf
    GROUP BY VehicleInventoryItemID 
    HAVING COUNT(*) > 1)  mp ON dp.VehicleInventoryItemID = mp.VehicleInventoryItemID;

  SELECT *
  INTO #vptt1
  FROM #vptt j
  WHERE ((-- IF the current price IS different than the previous price, we want it
    (SELECT top 1 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC) -- current price
      <>
    (SELECT top 1 start at 2 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC)) -- next previous price
  OR ((-- this IS the rest, current price = previous price, AND EXISTS a price generated within last x days
    (SELECT top 1 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC)
      =
    (SELECT top 1 start at 2 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC))
      AND ((CAST(VehiclePricingTS AS sql_date) >= curdate() - 7//*@DaysBack*/) 
        AND (amount <> (SELECT top 1 amount FROM #vptt WHERE VehicleInventoryItemID = j.VehicleInventoryItemID ORDER BY VehicleInventoryItemID, VehiclePricingTS DESC)))));


        
  SELECT viix.stocknumber,  wtf.CurrentPrice, wtf.PreviousPrice, wtf.PriceDate
  FROM (
    SELECT distinct VehicleInventoryItemID, 
      (SELECT top 1 amount FROM #vptt1 WHERE VehicleInventoryItemID  = j1.VehicleInventoryItemID  ORDER BY VehiclePricingTS DESC) AS CurrentPrice,
      (SELECT top 1 start at 2 amount FROM #vptt1 WHERE VehicleInventoryItemID = j1.VehicleInventoryItemID ORDER BY VehiclePricingTS DESC)  AS PreviousPrice,
      (SELECT top 1 VehiclePricingTS FROM #vptt1 WHERE VehicleInventoryItemID = j1.VehicleInventoryItemID ORDER BY VehiclePricingTS DESC) AS PriceDate
    FROM #vptt1 j1) wtf
  INNER JOIN VehicleInventoryItems viix ON wtf.VehicleInventoryItemID = viix.VehicleInventoryItemID 
    AND viix.ThruTS IS NULL 
  INNER JOIN VehicleItems vix ON viix.VehicleItemID = vix.VehicleItemID 
  INNER JOIN SelectedReconPackages srp ON viix.VehicleInventoryItemID = srp.VehicleInventoryItemID  
    AND srp.SelectedReconPackageTS = (
      SELECT MAX(SelectedReconPackageTS)
      FROM SelectedReconPackages
  	  WHERE VehicleInventoryItemID = srp.VehicleInventoryItemID 
  	  GROUP BY VehicleInventoryItemID); 

-- 12045A SHIT
  

