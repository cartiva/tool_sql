-- this IS part of sp.GetGlumpExtract_New, which IS used BY reportprocessor2 to generate
-- the crayon report
-- the problem IS IN generating the MileageCategory
-- eg, 9/4/2012, 2013 modelyear results IN the divisor being = 0
-- boom
-- the short term fix, edited the month/day that gets concantenated FROM -09-01 to 08-01, 
-- which results IN the divisor being = 12
SELECT *
FROM (
select a.VehicleItemID, a.vin, a.yearmodel,
TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(a.yearmodel, SQL_INTEGER) - 1, SQL_CHAR)) + '-08-01', SQL_DATE), CurDate()) * 12 AS divisor
FROM VehicleItems a) a
--WHERE divisor = 0
WHERE VehicleItemID IN ('24b78e2a-520a-f644-b913-0c02bdf85fbd','8fb18093-1a3c-e547-b4fb-a26e0e24c8cc','03095f6d-922c-ec4e-9ad8-33718b984857','72d42c78-3c7f-874b-92a2-d8a0d2028d72')

SELECT * FROM VehicleItems

SELECT VehicleItemID, value 
FROM VehicleItemMileages b
WHERE VehicleItemMileageTS = (
  SELECT MAX(VehicleItemMileageTS)
  FROM VehicleItemMileages
  WHERE VehicleItemID = b.VehicleItemID
  GROUP BY VehicleItemID)
  
SELECT value  

SELECT vii.*,
  case
    when (c.Mileage = 0) or (Length(Trim(vii.YearModel)) <> 4) or (Locate('.', vii.YearModel) <> 0) or (Locate('C', vii.Yearmodel) <> 0) or (Locate('/', vii.Yearmodel) <> 0) or ((vii.Yearmodel < '1901') OR (vii.Yearmodel > '2050')) then Null
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 4000 then 'Freaky Low'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 8000 then 'Very Low'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 12000 then 'Low'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 15000 then 'Average'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 18000 then 'High'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 22000 then 'Very High'
    when c.Mileage / TimeStampDiff(SQL_TSI_MONTH, Convert(Trim(Convert(Convert(vii.YearModel, SQL_INTEGER) - 1, SQL_CHAR)) + '-09-01', SQL_DATE), CurDate()) * 12 <= 999999 then 'Freaky High'
    else
      'Unknown'
  end as "MileageCategory"
FROM VehicleItems vii  
LEFT JOIN (
  SELECT VehicleItemID, value AS mileage 
  FROM VehicleItemMileages b
  WHERE VehicleItemMileageTS = (
    SELECT MAX(VehicleItemMileageTS)
    FROM VehicleItemMileages
    WHERE VehicleItemID = b.VehicleItemID
    GROUP BY VehicleItemID)) c ON vii.VehicleItemID = c.VehicleItemID 
WHERE vii.VehicleItemID IN (
  SELECT VehicleItemID
  FROM VehicleInventoryItems
  WHERE thruts IS NULL)    