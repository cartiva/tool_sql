SELECT p.fullname, COUNT(*), MAX(CAST(viix.FromTS AS sql_date)), 
  MIN(CAST(viix.FromTS AS sql_Date)), Month(viix.FromTS), year(viix.FromTS)
FROM VehicleInventoryItems viix
LEFT JOIN people p ON viix.BookerID = p.partyid
WHERE cast(viix.Fromts AS sql_date) > curdate() - 200
AND viix.owninglocationid = (SELECT partyid FROM organizations WHERE name = 'rydells')
GROUP BY p.fullname, Month(viix.FromTS), year(viix.FromTS)
ORDER BY p.fullname, year(viix.FromTS) DESC, month(viix.FromTS) desc 


