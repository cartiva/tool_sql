-- fucking with PartyCollections to change appearance recon items for ry1
/* locations
ry1: 'B6183892-C79D-4489-A58C-B526DF948B06'
ry2: '4CD8E72C-DC39-4544-B303-954C99176671'
ry3: '1B6768C6-03B0-4195-BA3B-767C0CAC40A6' 
*/
-- the first take, a total hack
UPDATE PartyCollections
SET PArtyID = 'xxxxxxxx-C79D-4489-A58C-B526DF948B06'
WHERE partyid = 'B6183892-C79D-4489-A58C-B526DF948B06';

INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Pricing Rinse', NULL, 6, 1);
INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Auction Wash Supreme', NULL, 25, 2);
INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Full Detail', NULL, 195, 3);
INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Car Buff', NULL, 245, 4);
INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Truck Buff', NULL, 300, 5);   
 
-- a more "reasoned" approach  
-- ADD fromts/thruts to PartyCollections    

ALTER TABLE PartyCollections
ADD COLUMN FromTS Timestamp
ADD COLUMN ThruTS Timestamp

UPDATE PartyCollections
SET fromts = '01/01/2001 00:00:01'

UPDATE PartyCollections
SET ThruTS = now()
WHERE partyid LIKE 'xxxx%'

UPDATE PartyCollections
SET PartyID = 'B6183892-C79D-4489-A58C-B526DF948B06'
WHERE partyid LIKE 'xxxx%'

UPDATE PartyCollections
SET ThruTS = now()
WHERE PartyID = 'B6183892-C79D-4489-A58C-B526DF948B06'
  AND ThruTS IS NULL 
  
-- ok, the TABLE IS now IN the state it needs to be  



-- it appears that only 1 sp references PartyCollections
-- GetReconAppearanceItemsForReconAuthorization
-- code, no match ON PartyCollections (except DogConst)
-- sp called FROM AdjustRecon, WalkVehicle, WalkVehicleAlt

SELECT name FROM system.storedprocedures 
WHERE contains (SQL_SCRIPT, 'partycollections');

SELECT name FROM system.storedprocedures
WHERE upper(cast(sql_script AS sql_char)) LIKE '%PARTYCOLLECTIONS%'

-- so, let's edit the sp to respect NULL ThruTS

-- production FB 69
DECLARE @Now timestamp;
@Now = (SELECT now() FROM system.iota);
BEGIN TRANSACTION;
TRY 
  ALTER TABLE PartyCollections
  ADD COLUMN FromTS Timestamp
  ADD COLUMN ThruTS Timestamp;
  
  DROP index PartyCollections.PK_PARTYCOLLECTIONS;
   
  EXECUTE PROCEDURE sp_CreateIndex90( 
    'PartyCollections',
    'PartyCollections.adi',
    'PK_PARTYCOLLECTIONS',
    'PartyID;Typ;Sequence;ThruTS',
    '',
    2051,
    512,
    '' ); 
    
  UPDATE PartyCollections
  SET fromts = '01/01/2001 00:00:01';
  
  UPDATE PartyCollections
  SET ThruTS = @Now
  WHERE PartyID = 'B6183892-C79D-4489-A58C-B526DF948B06';
  
  INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
    'Pricing Rinse', NULL, 6, 1);
  INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
    'Auction Wash Supreme', NULL, 25, 2);
  INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
    'Full Detail', NULL, 195, 3);
  INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
    'Car Buff', NULL, 245, 4);
  INSERT INTO PartyCollections values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
    'Truck Buff', NULL, 300, 5);   
  COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

/* 3/26/12 ADD pricing rinse to honda appearance menu  */
SELECT *
FROM partycollections
WHERE typ = 'PartyCollection_AppearanceReconItem'
  AND partyid = '4CD8E72C-DC39-4544-B303-954C99176671'
ORDER BY sequence  

UPDATE PartyCollections
SET sequence = sequence - 100
WHERE typ = 'PartyCollection_AppearanceReconItem'
  AND partyid = '4CD8E72C-DC39-4544-B303-954C99176671'
  
INSERT INTO PartyCollections values(
  '4CD8E72C-DC39-4544-B303-954C99176671',
  'PartyCollection_AppearanceReconItem',
  'Pricing Rinse', 
  NULL,
  6,
  101,
  now(),
  NULL)   

/* 5/8/12 ADD change honda to be the same AS rydell  */  

DECLARE @Now timestamp;
@Now = (SELECT now() FROM system.iota);
BEGIN TRANSACTION;
TRY 

  UPDATE PartyCollections
  SET ThruTS = @Now
  WHERE PartyID = '4CD8E72C-DC39-4544-B303-954C99176671'
  AND sequence <> 1; -- pricing rinse IS already seq 1
  
  INSERT INTO PartyCollections values('4CD8E72C-DC39-4544-B303-954C99176671', 'PartyCollection_AppearanceReconItem',
    'Auction Wash Supreme', NULL, 25, 2, now(), null);
  INSERT INTO PartyCollections values('4CD8E72C-DC39-4544-B303-954C99176671', 'PartyCollection_AppearanceReconItem',
    'Full Detail', NULL, 195, 3, now(), null);
  INSERT INTO PartyCollections values('4CD8E72C-DC39-4544-B303-954C99176671', 'PartyCollection_AppearanceReconItem',
    'Car Buff', NULL, 245, 4, now(), null);
  INSERT INTO PartyCollections values('4CD8E72C-DC39-4544-B303-954C99176671', 'PartyCollection_AppearanceReconItem',
    'Truck Buff', NULL, 300, 5, now(), null);   
  COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;  

-- 11/6/12
-- change value of Auction Wash Supreme to Deluxe wash
SELECT *
FROM partycollections
UPDATE partycollections
  SET value = 'Deluxe Wash'
WHERE value = 'Auction Wash Supreme';

-- 11/7/13
now that detail has been ordered to DO their WORK first, we are running INTO 
problems WHERE cars that are detailed first CREATE chemical problems IN body shop
so, ben/trent have decided that the fix IS to separate out interior AND exterior WORK
IN detail appearance items, which are storerd IN PartyCollections

-- current appearance recon items
SELECT b.name, a.value, a.amount, a.sequence
FROM partycollections a
LEFT JOIN organizations b on a.partyid = b.partyid
WHERE a.thruts IS NULL 
  AND a.typ = 'PartyCollection_AppearanceReconItem'
  
DO NOT know about honda yet, but at least for ry1 FROM:
name	      value	           amount	sequence	
Rydells	    Pricing Rinse	   6	    1
Rydells	    Deluxe Wash	     25	    2
Rydells	    Full Detail	     195	  3
Rydells	    Car Buff	       245	  4
Rydells	    Truck Buff	     300	  5

to: 
name	      value	           amount	sequence	
Rydells	    Pricing Rinse	   6	    1
Rydells	    Deluxe Wash	     25	    2
Rydells	    Interior         145	  3
Rydells     Exterior         50     4
Rydells	    Car Buff	       100	  5
Rydells	    Truck Buff	     155	  6

WORRIES AND CONCERNS
1. There are short hand descriptions of detail WORK (Detail, PR, etc) used IN 
   a bunch of different places both IN the tool AND IN Detail Whiteboard
   find AND change ALL those ...
2. most vehicles requiring detail will now have to have at least 2 items selected
   Interior AND an exterior selection
   i anticipate a shit load of multiple listings for a vehicle because of that,
   shit i am already shaky on a vehicle showing up twice IF it has a 
   detail AND upholstery
   Possibly, queries will now have to GROUP AND THEN come up with short descriptions
     that include multiple aspects of the job
   
does car buff imply an included Exterior?
      


-- CLOSE out crookston PartyCollections rows
UPDATE PartyCollections
SET ThruTS = now()
WHERE partyid = (
  SELECT partyid
  FROM organizations
  WHERE name = 'Crookston');
  
  
-- 7/7/15
full detail FROM 195 to 220
car buff FROM 245 to 275
truck buff FROM 300 to 330
new category:
  Program Vehicle 195
  
DECLARE @Now timestamp;
@Now = (SELECT now() FROM system.iota);
BEGIN TRANSACTION;
TRY 
-- CLOSE the old row and INSERT the new row 
-- ry1 full detail
UPDATE partycollections 
SET thruts = @now
WHERE partyid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  AND value = 'Full Detail'
  AND ThruTS IS NULL;
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts,thruts)
values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Full Detail', 220, 3, @now, CAST(NULL AS sql_timestamp)); 
-- ry2 full detail
UPDATE partycollections 
SET thruts = @now
WHERE partyid = '4CD8E72C-DC39-4544-B303-954C99176671'
  AND value = 'Full Detail'
  AND ThruTS IS NULL;
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts,thruts)
values('4CD8E72C-DC39-4544-B303-954C99176671', 'PartyCollection_AppearanceReconItem',
  'Full Detail', 220, 3, @now, CAST(NULL AS sql_timestamp)); 
-- ry1 car buff
UPDATE partycollections 
SET thruts = @now
WHERE partyid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  AND value = 'Car Buff'
  AND ThruTS IS NULL;
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts,thruts)
values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Car Buff', 275, 4, @now, CAST(NULL AS sql_timestamp)); 
-- ry2 car buff
UPDATE partycollections 
SET thruts = @now
WHERE partyid = '4CD8E72C-DC39-4544-B303-954C99176671'
  AND value = 'Car Buff'
  AND ThruTS IS NULL;
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts,thruts)
values('4CD8E72C-DC39-4544-B303-954C99176671', 'PartyCollection_AppearanceReconItem',
  'Car Buff', 275, 4, @now, CAST(NULL AS sql_timestamp)); 
-- ry1 truck buff
UPDATE partycollections 
SET thruts = @now
WHERE partyid = 'B6183892-C79D-4489-A58C-B526DF948B06'
  AND value = 'Truck Buff'
  AND ThruTS IS NULL;
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts,thruts)
values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Truck Buff', 330, 5, @now, CAST(NULL AS sql_timestamp)); 
-- ry2 truck buff
UPDATE partycollections 
SET thruts = @now
WHERE partyid = '4CD8E72C-DC39-4544-B303-954C99176671'
  AND value = 'Truck Buff'
  AND ThruTS IS NULL;
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts,thruts)
values('4CD8E72C-DC39-4544-B303-954C99176671', 'PartyCollection_AppearanceReconItem',
  'Truck Buff', 330, 5, @now, CAST(NULL AS sql_timestamp)); 
-- ry1 program vehicle  
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts,thruts)
values('B6183892-C79D-4489-A58C-B526DF948B06', 'PartyCollection_AppearanceReconItem',
  'Program Vehicle', 195, 6, @now, CAST(NULL AS sql_timestamp));   
-- ry2 program vehicle  
INSERT INTO partycollections(partyid,typ,value,amount,sequence,fromts,thruts)
values('4CD8E72C-DC39-4544-B303-954C99176671', 'PartyCollection_AppearanceReconItem',
  'Program Vehicle', 195, 6, @now, CAST(NULL AS sql_timestamp));   
  COMMIT WORK;
CATCH ALL
  ROLLBACK;
  RAISE;
END TRY;   
